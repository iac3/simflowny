/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.simflownyUtilsPlugin;

import com.sun.xml.xsom.parser.AnnotationContext;
import com.sun.xml.xsom.parser.AnnotationParser;
import com.sun.xml.xsom.parser.AnnotationParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 * Annotation factory to read appinfo annotations from the XSD.
 * @author bminano
 *
 */
public class XSOMAnnotationParser implements AnnotationParserFactory {
    /**
     * Parser.
     * @author bminano
     *
     */
    private class XsdAnnotationParser extends AnnotationParser {
        private StringBuilder documentation = new StringBuilder();
        @Override
        public ContentHandler getContentHandler(AnnotationContext context,
                String parentElementName, ErrorHandler handler, EntityResolver resolver) {
            return new ContentHandler() {
                private boolean parsingDocumentation = false;
                public void characters(char[] ch, int start, int length) throws SAXException {
                    if (parsingDocumentation) {
                        documentation.append(ch, start, length);
                    }
                }
                public void endElement(String uri, String localName, String name) throws SAXException {
                    if (localName.equals("appinfo")) {
                        documentation.append("&&");
                        parsingDocumentation = false;
                    }
                }
                public void startElement(String uri, String localName, String name, Attributes atts) throws SAXException {
                    if (localName.equals("appinfo")) {
                        parsingDocumentation = true;
                    }
                }
                public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
                    
                }
                public void skippedEntity(java.lang.String name) throws SAXException { }
                public void processingInstruction(java.lang.String target, java.lang.String data) throws SAXException { }
                public void endPrefixMapping(java.lang.String prefix) throws SAXException { }
                public void startPrefixMapping(java.lang.String prefix, java.lang.String uri) throws SAXException { }
                public void endDocument() throws SAXException { }
                public void startDocument() throws SAXException { }
                public void setDocumentLocator(Locator locator) { }
            };
        }
        @Override
        public Object getResult(Object existing) {
            return documentation.toString().trim().split("&&");
        }
    }
    /**
     * Creates the parser.
     * @return The parser
     */
    public AnnotationParser create() {
        return new XsdAnnotationParser();
    }
}
