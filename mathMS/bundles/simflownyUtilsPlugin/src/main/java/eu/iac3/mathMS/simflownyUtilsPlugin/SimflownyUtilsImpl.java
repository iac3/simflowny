/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.simflownyUtilsPlugin;

import com.sun.xml.xsom.XSFacet;
import com.sun.xml.xsom.XSParticle;
import com.sun.xml.xsom.XSRestrictionSimpleType;
import com.sun.xml.xsom.XSSimpleType;
import com.sun.xml.xsom.XSTerm;

import uk.ac.ed.ph.snuggletex.upconversion.MathMLUpConverter;
import uk.ac.ed.ph.snuggletex.upconversion.UpConversionOptionDefinitions;
import uk.ac.ed.ph.snuggletex.upconversion.UpConversionOptions;
import uk.ac.ed.ph.snuggletex.utilities.MathMLUtilities;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This auxiliary class collects some common XSOM procedures.
 * @author bminano
 *
 */
@Component //
(//
    immediate = true, //
    name = "SimflownyUtils", //
    property = //
    { //
      "service.exported.interfaces=*", //
      "service.exported.configs=org.apache.cxf.ws", //
      "org.apache.cxf.ws.address=/SimflownyUtils" //
    } //
)
public final class SimflownyUtilsImpl implements SimflownyUtils {
    static final int IND = 4;
    public static final String MMSURI = "urn:mathms";
    public static final String SMLURI = "urn:simml";
    /**
     * Constructor.
     * @throws SUException 
     */
    
    public SimflownyUtilsImpl() throws SUException {

    }
    
    /**
     * XSD simple type restriction class. 
     * @author bminano
     *
     */
    public static class SimpleTypeRestriction {
        public String[] enumeration = null;
        public String   maxValue    = null;
        public String   minValue    = null;
        public String   length      = null;
        public String   maxLength   = null;
        public String   minLength   = null;
        public String[] pattern     = null;
        public String   totalDigits = null;

        /**
         * Returns the restriction.
         * @return  The restriction.
         */
        public String toString() {
            String enumValues = "";
            if (enumeration != null) {
                for (String val : enumeration) {
                    enumValues += val + ", ";
                }
                enumValues = enumValues.substring(0, enumValues.lastIndexOf(','));
            }
            
            String patternValues = "";
            if (pattern != null) {
                for (String val : pattern) {
                    patternValues += "(" + val + ")|";
                }
                patternValues = patternValues.substring(0, patternValues.lastIndexOf('|'));
            }
            String retval = "";
            //retval += maxValue    == null ? "" : "[MaxValue  = "   + maxValue      + "]";
            //retval += minValue    == null ? "" : "[MinValue  = "   + minValue      + "]";
            //retval += maxLength   == null ? "" : "[MaxLength = "   + maxLength     + "]";
            //retval += minLength   == null ? "" : "[MinLength = "   + minLength     + "]";
            //retval += pattern     == null ? "" : "[Pattern(s) = "  + patternValues + "]";
            //retval += totalDigits == null ? "" : "[TotalDigits = " + totalDigits   + "]";
            //retval += length      == null ? "" : "[Length = "      + length        + "]";
            
            if (enumeration != null) {
                retval += "[Values = " + enumValues + "]";
            }

            return retval;
        }
    }
    
    /**
     * Gets the simple restrictions for a XSD simple type.
     * @param xsSimpleType          The type
     * @param simpleTypeRestriction The restriction
     */
    public static void initRestrictions(XSSimpleType xsSimpleType, SimpleTypeRestriction simpleTypeRestriction)
    {
        XSRestrictionSimpleType restriction = xsSimpleType.asRestriction();
        if (restriction != null) {
            Vector<String> enumeration = new Vector<String>();
            Vector<String> pattern     = new Vector<String>();

            for (XSFacet facet : restriction.getDeclaredFacets()) {
                if (facet.getName().equals(XSFacet.FACET_ENUMERATION)) {
                    enumeration.add(facet.getValue().value);
                }
                if (facet.getName().equals(XSFacet.FACET_MAXINCLUSIVE)) {
                    simpleTypeRestriction.maxValue = facet.getValue().value;
                }
                if (facet.getName().equals(XSFacet.FACET_MININCLUSIVE)) {
                    simpleTypeRestriction.minValue = facet.getValue().value;
                }
                if (facet.getName().equals(XSFacet.FACET_MAXEXCLUSIVE)) {
                    simpleTypeRestriction.maxValue = String.valueOf(Integer.parseInt(facet.getValue().value) - 1);
                }
                if (facet.getName().equals(XSFacet.FACET_MINEXCLUSIVE)) {
                    simpleTypeRestriction.minValue = String.valueOf(Integer.parseInt(facet.getValue().value) + 1);
                }
                if (facet.getName().equals(XSFacet.FACET_LENGTH)) {
                    simpleTypeRestriction.length = facet.getValue().value;
                }
                if (facet.getName().equals(XSFacet.FACET_MAXLENGTH)) {
                    simpleTypeRestriction.maxLength = facet.getValue().value;
                }
                if (facet.getName().equals(XSFacet.FACET_MINLENGTH)) {
                    simpleTypeRestriction.minLength = facet.getValue().value;
                }
                if (facet.getName().equals(XSFacet.FACET_PATTERN)) {
                    pattern.add(facet.getValue().value);
                }
                if (facet.getName().equals(XSFacet.FACET_TOTALDIGITS)) {
                    simpleTypeRestriction.totalDigits = facet.getValue().value;
                }
            }
            if (enumeration.size() > 0) {
                simpleTypeRestriction.enumeration = enumeration.toArray(new String[] {});
            }
            if (pattern.size() > 0) {
                simpleTypeRestriction.pattern = pattern.toArray(new String[] {});
            }
        }
    }
    
    /**
     * Checks when a particle is ordered.
     * @param particle  The particle
     * @return          True not ordered
     */
    public static boolean isNotOrdered(XSParticle particle) {
        if (particle != null) {
            XSTerm term = particle.getTerm();
            if (term.isModelGroup()) {
                if (term.asModelGroup().getCompositor().toString().equals("choice")) {
                    return true;
                }
                XSParticle[] list = term.asModelGroup().getChildren();
                XSParticle particleN = list[0];
                return isNotOrdered(particleN); 
            }
            else {
                if (term.isModelGroupDecl()) {  
                    XSParticle[] list = term.asModelGroupDecl().getModelGroup().getChildren();
                    if (term.asModelGroupDecl().getModelGroup().getCompositor().toString().equals("choice")) {
                        return true;
                    }
                    XSParticle particleN = list[0];
                    return isNotOrdered(particleN); 
                }
            }
        }
        return false;
    }
    
    /**
     * Transform a schematron xpath assert to jsonPath format.
     * Or operator is not available in xpath, so it is implemented as different jsonPaths
     * @param xpath     The xpath
     * @return          The jsonPaths
     */
    public static String xpathToJsonpath(String xpath) {
        String jsonPath = "";
        String patternString = "((\\/\\/)?(\\/+|\\.\\/[\\w-]*::)\\w*:\\w*([\\[].*\\])?\\|?)+[\\s]*=[\\s]*\\.|'[A-Za-z0-9_]*'[\\s]*=[\\s]*\\.";
        String ancestorPatternString = "\\.\\/.*?(?=\\/\\/)|\\.\\/.*?(?=\\])";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(xpath);
        while (matcher.find()) {
            String xpathInd = matcher.group(0).trim();
            //Or operator not implemented, create different jsonPath
            String[] paths = xpathInd.substring(0, xpathInd.lastIndexOf(" =")).split("\\|");
            for (int i = 0; i < paths.length; i++) {
                if (paths[i].matches("'[A-Za-z0-9_]*'")) {
                    jsonPath = jsonPath + "^" + paths[i];
                }
                else {
                    //Take ancestor expressions from the string
                    Pattern pattern2 = Pattern.compile(ancestorPatternString);
                    Matcher matcher2 = pattern2.matcher(paths[i]);
                    StringBuffer sb = new StringBuffer();
                    int counter = 0;
                    ArrayList<String> replacements = new ArrayList<String>();
                    while (matcher2.find()) {
                        matcher2.appendReplacement(sb, "*" + counter);
                        replacements.add(matcher2.group(0));
                        counter++;
                    }
                    matcher2.appendTail(sb);
                    String tmpPath = sb.toString();
                    //Convert the xpath string into JSPath
                    String replacedXpath = tmpPath.replaceAll("/", ".").replaceAll(" =", ".input ==").replaceAll("\\[", "{.")
                            .replaceAll("\\]", "}").replaceAll("mms:([a-zA-Z]*)", "children{.tag == \\\\\"$1\\\\\"}");
                    jsonPath = jsonPath + "^" + replacedXpath + ".input";
                    //Set again the ancestor expressions in their former place
                    String selector = "\\*[0-9]*";
                    pattern2 = Pattern.compile(selector);
                    matcher2 = pattern2.matcher(jsonPath);
                    sb = new StringBuffer();
                    counter = 0;
                    while (matcher2.find()) {
                        matcher2.appendReplacement(sb, replacements.get(counter));
                        counter++;
                    }
                    matcher2.appendTail(sb);
                    jsonPath = sb.toString();
                }
            }
        }
        //Concatenation operation. Check issue #54
        patternString = "((\\/+|\\.\\/[\\w-]*::)\\w*:\\w*([\\[].*\\])?\\|?)+\\/concat\\(text\\(\\), [^)]*\\)[\\s]*=[\\s]*\\.";
        pattern = Pattern.compile(patternString);
        matcher = pattern.matcher(xpath);
        while (matcher.find()) {
            String xpathInd = matcher.group(0).trim();
            //Or operator not implemented, create different jsonPath
            String[] paths = xpathInd.substring(0, xpathInd.lastIndexOf(" =")).split("\\|");
            for (int i = 0; i < paths.length; i++) {
                //Take ancestor expressions from the string
                Pattern pattern2 = Pattern.compile(ancestorPatternString);
                Matcher matcher2 = pattern2.matcher(paths[i]);
                StringBuffer sb = new StringBuffer();
                int counter = 0;
                ArrayList<String> replacements = new ArrayList<String>();
                while (matcher2.find()) {
                    matcher2.appendReplacement(sb, "*" + counter);
                    replacements.add(matcher2.group(0));
                    counter++;
                }
                matcher2.appendTail(sb);
                String tmpPath = sb.toString();
                
                //Split the operation 
                String[] parts = tmpPath.split("/concat\\(text\\(\\)");
                String firstOp = parts[0];
                String lastOps = parts[1].substring(parts[1].indexOf(',') + 1, parts[1].lastIndexOf(')'));

                //Convert the xpath string into JSPath
                firstOp = firstOp.replaceAll("/", ".").replaceAll(" =", ".input ==").replaceAll("\\[", "{.")
                        .replaceAll("\\]", "}").replaceAll("mms:([a-zA-Z]*)", "children{.tag == \\\\\"$1\\\\\"}") + ".input";
                
                if (lastOps.matches("((\\/+|\\.\\/[\\w-]*::)\\w*:\\w*([\\[].*\\])?\\|?)+[\\s]*=[\\s]*\\.")) {
                    lastOps = lastOps.replaceAll("/", ".").replaceAll(" =", ".input ==").replaceAll("\\[", "{.")
                            .replaceAll("\\]", "}").replaceAll("mms:([a-zA-Z]*)", "children{.tag == \\\\\"$1\\\\\"}") + ".input";
                }

                jsonPath = jsonPath + "^concat(" + firstOp + ", " + lastOps + ")";
                //Set again the ancestor expressions in their former place
                String selector = "\\*[0-9]*";
                pattern2 = Pattern.compile(selector);
                matcher2 = pattern2.matcher(jsonPath);
                sb = new StringBuffer();
                counter = 0;
                while (matcher2.find()) {
                    matcher2.appendReplacement(sb, replacements.get(counter));
                    counter++;
                }
                matcher2.appendTail(sb);
                jsonPath = sb.toString();
            }
        }

        
        return jsonPath;
    }
    
    /**
     * Parses a simple type element.
     * @param simpleType    The simple type object
     * @return              The type
     */
    public static String printSimpleType(XSSimpleType simpleType) {
        SimpleTypeRestriction restriction = new SimpleTypeRestriction();
        SimflownyUtilsImpl.initRestrictions(simpleType, restriction);
        return restriction.toString();
    }
    
    /**
     * Gets the schematron content restriction of an element.
     * @param schematron        The schematron content
     * @param absPath           The path of current element
     * @return                  The restriction
     * @throws SUException      SU008 Schematron not valid
     *                           SU00X External error
     */
    public static String schematronRestriction(Document schematron, String absPath) throws SUException {
        if (schematron != null) {
            String tmpPath = absPath.replaceAll("\\[[0-9]*\\]", "");
            NodeList rules = Utils.find(schematron, "//scm:rule[matches('" + tmpPath + "', concat('^',replace(@context, '//', '([/a-z:A-Z])*/'), '$'))]");
            for (int i = 0; i < rules.getLength(); i++) {
                NodeList asserts = Utils.find(rules.item(i), "./scm:assert");
                if (asserts.getLength() == 0) {
                    throw new SUException(SUException.SU002, "One assert by rule is mandatory");
                }
                String restriction = asserts.item(0).getAttributes().getNamedItem("test").getTextContent();
                String content = xpathToJsonpath(restriction);
                if (!content.equals("")) {
                    return "[Content = '" + content + "']";
                }
            }
        }
        return "";
    }
    
    
    /**
     * Converts a document manager tree into json.
     * @param tree  The tree
     * @return      Json object from the tree
     */
    public static String docManagerTreeToJSON(Node tree) {
        JSONObject jsonTree = new JSONObject(); 
        
        jsonTree.put("text", "/");
        NodeList children = tree.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            jsonTree.accumulate("children", getJSONCollection(children.item(i)));
        }
        
        return jsonTree.toString();
    }
    
    /**
     * Recursive generation of json object from a collection xml element.
     * @param collection    The collection element
     * @return              The json object
     */
    private static JSONObject getJSONCollection(Node collection) {
        JSONObject child = new JSONObject(); 
        child.put("text", collection.getFirstChild().getTextContent());
        child.put("expanded", true);
        if (collection.getChildNodes().getLength() > 1) {
            NodeList children = collection.getLastChild().getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
                child.accumulate("children", getJSONCollection(children.item(i)));
            }
        }
        return child;
    }
    
    /**
     * Converts a document manager list into json.
     * @param list  The list
     * @return      Json object from the tree
     */
    public static String docManagerListToJSON(Node list) {
        JSONObject jsonTree = new JSONObject(); 
        
        jsonTree.put("success", true);
        NodeList children = list.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Element childElem = (Element) children.item(i);
            JSONObject child = new JSONObject(); 
            child.put("dataindex", false);
            child.put("type", childElem.getElementsByTagNameNS(MMSURI, "documentType").item(0).getTextContent());
            child.put("name", childElem.getElementsByTagNameNS(MMSURI, "name").item(0).getTextContent());
            child.put("author", childElem.getElementsByTagNameNS(MMSURI, "author").item(0).getTextContent());
            child.put("id", childElem.getElementsByTagNameNS(MMSURI, "id").item(0).getTextContent());
            child.put("version", childElem.getElementsByTagNameNS(MMSURI, "version").item(0).getTextContent());
            child.put("date", childElem.getElementsByTagNameNS(MMSURI, "date").item(0).getTextContent());
            child.put("Description", childElem.getElementsByTagNameNS(MMSURI, "description").item(0).getTextContent());
            jsonTree.accumulate("results", child);
        }
        
        return jsonTree.toString();
    }
    
    public static String infixToMathML(Document asciiMathMLDocument) throws SUException {
        try {
            //Create the converter with MathML content option
            MathMLUpConverter upConverter = new MathMLUpConverter();
            UpConversionOptions upConversionOptions = new UpConversionOptions();
            upConversionOptions.setSpecifiedOption(UpConversionOptionDefinitions.DO_CONTENT_MATHML_NAME, "true");
            upConversionOptions.setSpecifiedOption(UpConversionOptionDefinitions.DO_MAXIMA_NAME, "false");
            Document upConvertedDocument = upConverter.upConvertASCIIMathML(asciiMathMLDocument, upConversionOptions);
            //Isolate the MathML content annotation from the rest
            Element mathElement = upConvertedDocument.getDocumentElement();
            Document cMathMLDocument = MathMLUtilities.isolateAnnotationXML(mathElement, MathMLUpConverter.CONTENT_MATHML_ANNOTATION_NAME);
            //Replace inline SimML
            ByteArrayOutputStream output = Utils.replaceSimml(cMathMLDocument);
            return output.toString("UTF-8");
        } 
        catch (Exception e) {
            e.printStackTrace(System.out);
            throw new SUException(SUException.SU006, e.getMessage());
        }
    }
}
