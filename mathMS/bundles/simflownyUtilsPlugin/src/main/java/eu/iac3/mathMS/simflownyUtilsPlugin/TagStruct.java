/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.simflownyUtilsPlugin;

import java.util.LinkedHashSet;

/**
 * Class representing the struct of an element.
 * @author bminano
 *
 */
public class TagStruct {
    String tag;
    String jsonPath;
    String type;
    String pureType;
    boolean ordered;
    int order;
    String recursive;
    LinkedHashSet<TagChild> children;
    
    /**
     * Constructor.
     * @param name      name
     * @param jsonPath  jsonPath
     * @param type      type
     * @param pureType  pureType
     * @param ordered   ordered
     * @param order         The order
     */
    public TagStruct(String name, String jsonPath, String type, String pureType, boolean ordered, int order) {
        this.tag = name;
        this.jsonPath = jsonPath;
        this.type = type;
        this.pureType = pureType;
        this.ordered = ordered;
        this.children = new LinkedHashSet<TagChild>();
        this.order = order;
        recursive = null;
    }
    
    /**
     * Adds a child to the children list.
     * @param child The child
     */
    public void addChild(TagChild child) {
        children.add(child);
    }
    
    /**
     * Adds some children to the children list.
     * @param children  The children to add
     */
    public void addChildren(LinkedHashSet<TagChild> children) {
        this.children.addAll(children);
    }
    
    
    public void setRecursive(String recursive) {
        this.recursive = recursive;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof TagStruct) {
            TagStruct comp = (TagStruct) object;
            if (this.type.equals("instructionSet")) {
                return this.tag.equals(comp.tag);
            }
            return this.tag.equals(comp.tag) && this.jsonPath.equals(comp.jsonPath);
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return tag.hashCode();
    }
}
