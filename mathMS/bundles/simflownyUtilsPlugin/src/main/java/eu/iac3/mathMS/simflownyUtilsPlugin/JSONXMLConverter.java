/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.simflownyUtilsPlugin;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.text.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * This class transform the JSON into XML.
 * @author bminano
 *
 */
class JSONXMLConverter {
    static final String MTURI = "http://www.w3.org/1998/Math/MathML";
    static final String MMSURI = "urn:mathms";
    static final String SMLURI = "urn:simml";
    private JSONObject json;
    
    /**
     * Constructor. Parse the json content.
     * @param content       The json content.
     * @throws SUException  MM002   Not a valid Document
     */
    JSONXMLConverter(String content) throws SUException {
        try {
            json = new JSONObject(content);
        }
        catch (JSONException e) {
            throw new SUException(SUException.SU001, e.toString());
        }
    }

    /**
     * Parse the children from the parent.
     * @param parent            The DOM parent 
     * @param jsonChild         The JSON children
     * @param simml             True if simml element
     * @return                  True if no children
     * @throws SUException      MM002 - Not a valid XML Document
     *                          MM00X - External error
     */
    public static boolean getArray(Element parent, Object jsonChild, boolean simml) throws SUException {
        boolean emptyArray = true;
        JSONArray jsonArr = (JSONArray) jsonChild;

        for (int k = 0; k < jsonArr.length(); k++) {
            if (jsonArr.get(k) instanceof JSONObject) {
                JSONObject child = (JSONObject) jsonArr.get(k);
                boolean emptyChild = false;
                //If the simml has a mathML child its content must be appended directly 
                if (child.getString("tag").equals("math")) {
                    if (child.isNull("input") || child.getString("input").equals("")) {
                        Element newTag = Utils.createElement(parent.getOwnerDocument(), MMSURI, "math");
                        parent.appendChild(newTag);
                    }
                    else {
                        String input = child.getString("input");
                        parent.appendChild(parent.getOwnerDocument().importNode(
                                Utils.stringToDom(StringEscapeUtils.unescapeHtml4(input)
                                        //.replaceAll("'", "\"")
                                        .replaceFirst("(<mt:math)", "$1 xmlns:mt=\"http://www.w3.org/1998/Math/MathML\" xmlns:sml=\"urn:simml\""))
                                        .getFirstChild(), true));
                    }
                }
                else {
                    if (child.has("attribute") && child.getBoolean("attribute")) {
                        parseAttributeJson(parent, child);
                    }
                    else {
                        emptyChild = parseJson(parent, child, simml);
                    }
                }
                emptyArray = emptyArray && emptyChild;
            }
        }
        return emptyArray;
    }
    
    /**
     * Sets the attribute of an element.
     * @param parent        The element
     * @param jsonObject    The json attribute
     */
    public static void parseAttributeJson(Element parent, JSONObject jsonObject) {
        parent.setAttribute(jsonObject.getString("tag"), jsonObject.getString("input"));
    }

    /**
     * Parse a json element into a XML element.
     * @param parent        The DOM parent of the element
     * @param jsonObject    The JSON element
     * @param simml         True if simml element
     * @return              True if no element
     * @throws SUException  MM002 - Not a valid XML Document
     *                       MM00X - External error
     */
    public static boolean parseJson(Element parent, JSONObject jsonObject, boolean simml) throws SUException {
        boolean emptyTag = false;
        String uri = MMSURI;
        //The mathML elements inside simml tags must be ignored
        if (jsonObject.getString("tag").equals("math")) {
            JSONArray jsonArr = (JSONArray) jsonObject.get("children");
            parseJson(parent, (JSONObject) jsonArr.get(0), simml);
            return false;
        }
        
        //Check when the element is simml
        if (jsonObject.getString("tag").equals("simml") || jsonObject.getString("tag").equals("algorithm")) {
            simml = true;
        }
        //Could be inherited from a parent simml element
        if (simml) {
            uri = SMLURI; 
        }
        Element newTag = Utils.createElement(parent.getOwnerDocument(), uri, jsonObject.getString("tag"));
        //Set input value
        if (!jsonObject.isNull("input")) {
            String input = jsonObject.getString("input");
            //Special treatment to instructionSet and mathML
            if (jsonObject.has("type") && (jsonObject.getString("type").equals("mathML"))) {
                newTag.appendChild(newTag.getOwnerDocument().importNode(Utils.stringToDom(input
                        //.replaceAll("'", "\"")
                        .replaceFirst("(<mt:math)", "$1 xmlns:mt=\"http://www.w3.org/1998/Math/MathML\" xmlns:sml=\"urn:simml\""))
                        .getFirstChild(), true));
            }
            else {
                newTag.setTextContent(input.replaceAll("\\\\\\\\", "\\\\"));
            }
        }
        boolean emptyChildren = false;
        if (jsonObject.has("children")) {
            emptyChildren = getArray(newTag, jsonObject.get("children"), simml);
        }
        if (!emptyChildren) {
            parent.appendChild(newTag);
        }
        else {
            emptyTag = emptyChildren;
        }
        
        return emptyTag;
    }
    
    /**
     * Converts the JSON object to XML.
     * @return              The DOM document
     * @throws SUException  MM002 - Not a valid XML Document
     *                       MM00X - External error
     */
    public Document toXML() throws SUException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = dbf.newDocumentBuilder();
            Document result = builder.newDocument();
            
            //Get root element
            JSONObject root = (JSONObject) json.getJSONArray("children").get(0);
            Element rootElement = Utils.createElement(result, MMSURI, root.getString("tag"));
            result.appendChild(rootElement);
            //Get children of root element
            JSONArray jsonArr = root.getJSONArray("children");
            for (int k = 0; k < jsonArr.length(); k++) {
                parseJson(rootElement, (JSONObject) jsonArr.get(k), false);
            }
            
            return result;
        } 
        catch (ParserConfigurationException e) {
            throw new SUException(SUException.SU00X, e.toString());
        }
    }
}
