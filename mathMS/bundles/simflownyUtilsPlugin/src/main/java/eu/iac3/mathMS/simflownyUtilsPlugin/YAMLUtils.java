package eu.iac3.mathMS.simflownyUtilsPlugin;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.DumperOptions.FlowStyle;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import uk.ac.ed.ph.asciimath.parser.AsciiMathParser;

import com.github.underscore.lodash.Xml;

public class YAMLUtils {
    static final String XSL_AsciiMath = "common" + File.separator + "XSLTmathmlToasciiML" + File.separator + "mmlascii.xsl";
    static final String XSL_preYaml = "common" + File.separator + "xsl" + File.separator + "preYaml.xsl";
    static final String XSL_postYaml = "common" + File.separator + "xsl" + File.separator + "postYaml.xsl";

    
    /**
     * Parses a xml with a xlst into YAML.
     * @param xml			The xml to parse.
     * @return				YAML encoding
     * @throws SUException	SU00X External error
     */
	static public String fromXML(String xml) throws SUException {
		try {
			//Preprocess xml to transform mathML to Asciimath
			xml = prepareToYaml(xml);
			
			Object result = Xml.fromXml(xml);

			//Parse with Snake yaml
			DumperOptions options = new DumperOptions();
			options.setDefaultFlowStyle(DumperOptions.FlowStyle.AUTO);

			Yaml snake = new Yaml(new Representer() {
				//Override to avoid dictionary compact representation
			    @Override
			    protected  Node representMapping(Tag tag, Map<?, ?> mapping,  FlowStyle flowStyle) {
			        return super.representMapping(tag, mapping, DumperOptions.FlowStyle.BLOCK);
			    }
			    @Override
			    protected  Node representSequence(Tag tag, Iterable<?> iterable,  FlowStyle flowStyle) {
			    	DumperOptions.FlowStyle newFlow = flowStyle;
			    	if (iterable.toString().contains("=")) {
			    		newFlow = DumperOptions.FlowStyle.BLOCK;
			    	}
			        return super.representSequence(tag, iterable, newFlow);
			    }
			},options);
			
			return snake.dump(result);
		} 
		catch (Exception e) {
			e.printStackTrace(System.out);
            throw new SUException(SUException.SU00X, e.getMessage());
		}
	}
	
	/**
	 * Conversor from XML to YAML based on:
	 * Class Converter is responsible for full YAML->JSON convertion
	 * @author  Mikhaleva Maria
	 * @version dated 27 Dec 2018
	 * {@link https://github.com/mariarheon/yaml-to-xml/blob/master/src/main/java/ru/test/Converter.java}
	 * @throws SUException 
	 */
	static public String toXML(String yaml) throws SUException {
        String res = "";
        try{
            ObjectMapper yamlReader = new ObjectMapper(new YAMLFactory());
            Object obj = yamlReader.readValue(yaml, Object.class);

            ObjectMapper jsonWriter = new ObjectMapper();
            res = jsonWriter.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        }
        catch (Exception e) {
        	e.printStackTrace();
        	throw new SUException(SUException.SU00X, e.getMessage());
        }
        
        JSONObject jsonObject = new JSONObject(res);
        
        String xml =  XML.toString(jsonObject);
        
        xml = prepareToXML(xml);
        
        return xml;
	}
	
	static String prepareToXML(String xml) throws SUException {
		//Add namespaces
		String namespaces = " xmlns:mms=\"urn:mathms\" xmlns:mt=\"http://www.w3.org/1998/Math/MathML\" xmlns:sml=\"urn:simml\"";
		xml = xml.substring(0, xml.indexOf(">")) + namespaces + xml.substring(xml.indexOf(">"));
		//#items
		xml = xml.replaceAll("<[A-Za-z0-9]*><#item>", "").replaceAll("<\\/#item><\\/[A-Za-z0-9]*>", "");
		xml = xml.replaceAll("<-self-closing>true</-self-closing>", "");
		

		Document xmlDoc = Utils.stringToDom(xml);
		
		addPrefixes(xmlDoc, xmlDoc.getDocumentElement(), false);
		
		try {
			final AsciiMathParser parser = new AsciiMathParser();

	    	NodeList mathml = Utils.find(xmlDoc, "//mms:mathML|//sml:mathML");
	    	for (int i = 0; i < mathml.getLength(); i++) {
	    		Element e = (Element) mathml.item(i);
	    		String asciimath = e.getTextContent().replaceAll("&lt;", "<").replaceAll("&gt;", ">");
	    		//Remove scientific notation
	    		Pattern patt = Pattern.compile("(-?[\\d.]+(?:E-?\\d+))");
	    		Matcher m = patt.matcher(asciimath);
	    		StringBuffer sb = new StringBuffer(asciimath.length());
	    		while (m.find()) {
	    			String text = m.group(0);
	    			text = new BigDecimal(text).toPlainString();
	    			m.appendReplacement(sb, Matcher.quoteReplacement(text));
	    		}
	    		m.appendTail(sb);
	    		asciimath = sb.toString();
	    		final Document result = parser.parseAsciiMath(asciimath);
		        String mathmlTmp = SimflownyUtilsImpl.infixToMathML(result);
	    		Element newElem = Utils.stringToDom(mathmlTmp).getDocumentElement();
	    	     
	            e.getParentNode().replaceChild(xmlDoc.importNode(newElem, true), e);
	    	}
			//Specific conversions
			TransformerFactory tFactory = new net.sf.saxon.TransformerFactoryImpl();
			Transformer transformer;
			transformer = tFactory.newTransformer(new StreamSource(XSL_postYaml));
	        ByteArrayInputStream input = new ByteArrayInputStream(Utils.domToString(xmlDoc).getBytes("UTF-8"));
	        ByteArrayOutputStream output = new ByteArrayOutputStream();
	        transformer.transform(new StreamSource(input), new StreamResult(output));
	        xmlDoc = Utils.stringToDom(output.toString("UTF-8"));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new SUException(SUException.SU00X, e.getMessage());
		}
		return Utils.domToString(xmlDoc);
	}
	
	/**
	 * Prepare xml document to transform to YAML conversion.
	 * @param xml			
	 * @return
	 * @throws SUException
	 */
	static String prepareToYaml(String xml) throws SUException {
    	//Convert xml string to Dom
    	Document xmlDoc = Utils.stringToDom(xml);
    	
    	try {	        
	    	
			TransformerFactory tFactory = new net.sf.saxon.TransformerFactoryImpl();
			Transformer transformer;
	    	//Specific conversions
			transformer = tFactory.newTransformer(new StreamSource(XSL_preYaml));
	        ByteArrayInputStream input = new ByteArrayInputStream(Utils.domToString(xmlDoc).getBytes("UTF-8"));
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            transformer.transform(new StreamSource(input), new StreamResult(output));
            xmlDoc = Utils.stringToDom(output.toString("UTF-8"));
			
			//Convert mathML to Ascii
			transformer = tFactory.newTransformer(new StreamSource(XSL_AsciiMath));
	    	NodeList mathml = Utils.find(xmlDoc, "//mt:math");
	    	for (int i = 0; i < mathml.getLength(); i++) {
	    		Element e = (Element) mathml.item(i);
	    		Element newElem = Utils.createElement(xmlDoc, SimflownyUtilsImpl.MMSURI, "mathML");
	            input = new ByteArrayInputStream(Utils.domToString(e).getBytes("UTF-8"));
	            output = new ByteArrayOutputStream();
	                       
	            transformer.transform(new StreamSource(input), new StreamResult(output));
	            String asciimath = output.toString("UTF-8");
	            asciimath = asciimath.substring(asciimath.indexOf("\">") + 2);
	            asciimath = asciimath.substring(0, asciimath.indexOf("</"));
	            asciimath = asciimath.replaceAll("&gt;", ">").replaceAll("&lt;", "<");
	            newElem.setTextContent(asciimath);
	            e.getParentNode().replaceChild(newElem, e);
	    	}
		} catch (Exception e) {
			e.printStackTrace();
			throw new SUException(SUException.SU00X, e.getMessage());
		}
    	//Convert dom to xml string
    	return Utils.removeXmlPrefixes(Utils.removeAllXmlNamespace(Utils.domToString(xmlDoc)));
    }
	
	/**
	 * Adding xml prefixes recursively.
	 * @param doc		Parent document
	 * @param node		Current node
	 * @param simml		check if inside a simml node
	 */
	static void addPrefixes(Document doc, org.w3c.dom.Node node, boolean simml) {
		boolean simmlNode = false;
		String prefix = "mms";
		String uri = SimflownyUtilsImpl.MMSURI;
		if (node.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
			if (simml || node.getLocalName().contentEquals("simml")) {
				prefix = "sml";
				uri = SimflownyUtilsImpl.SMLURI;
				simmlNode = true;
			}
			doc.renameNode(node, uri, node.getNodeName());
			node.setPrefix(prefix);
		}
		NodeList children = node.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			addPrefixes(doc, children.item(i), simmlNode);
		}
	}
}
