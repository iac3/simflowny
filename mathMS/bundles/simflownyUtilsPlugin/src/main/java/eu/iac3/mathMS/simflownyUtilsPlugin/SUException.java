/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.simflownyUtilsPlugin;

/** 
 * DPMException is the class Exception launched by the 
 * {@link ModelManager PhysicalModels} interface.
 * The exceptions that this class return are:
 * SU001	Not a valid Document
 * SU002    Schematron problem
 * SU003    Struct creation error
 * SU004    XSD schema does not exist
 * SU005    Setup error in the struct generator
 * SU006    Incorrect asciiMath expression
 * SU00X    External error
 * @author      ----
 * @version     ----
 */
@SuppressWarnings("serial")
public class SUException extends Exception {

    public static final String SU001 = "Not a valid document";
    public static final String SU002 = "Schematron problem";
    public static final String SU003 = "Struct creation error";
    public static final String SU004 = "XSD schema does not exist";
    public static final String SU005 = "Setup error in the struct generator";
    public static final String SU006 = "Incorrect asciiMath expression";
    public static final String SU00X = "External error";
	
    //External error message
    private String extError;
	  /** 
	     * Constructor for known messages.
	     *
	     * @param msg  the message	     
	     */
    SUException(String msg) {
    	super(msg);
    }
	  
	  /** 
	     * Constructor.
	     *
	     * @param intMsg  the internal error message	    
	     * @param extMsg  the external error message
	     */
    public SUException(String intMsg, String extMsg) {
		super(intMsg);
        extError = extMsg;
    }
	  /** 
     * Returns the error code.
     *
     * @return the error code	     
     */
    public String getErrorCode() {
    	return super.getMessage();
    }
	  /** 
	     * Returns the error message as a string.
	     *
	     * @return the error message	     
	     */
    public String getMessage() {
    	if (extError == null) {
            return "SUException: " + super.getMessage();
    	}
    	else {
            return "SUException: " + super.getMessage() + " (" + extError + ")";
    	}
    }  
}
