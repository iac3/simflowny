package eu.iac3.mathMS.simflownyUtilsPlugin;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 * Utility class.
 * @author bminano
 *
 */
final class Utils {
    static XPath xpath;
    static XPathFactory factory = XPathFactory.newInstance();
    static TransformerFactory tFactory;
    static Transformer transformer = null;
    static final String XSL_INLINE = "common" + File.separator + "XSLTinlineSimML" + File.separator + "inlineSimML.xsl";
    
    static {
        factory = new net.sf.saxon.xpath.XPathFactoryImpl();
        xpath = factory.newXPath();
        try {
            tFactory = new net.sf.saxon.TransformerFactoryImpl();
            transformer = tFactory.newTransformer(new StreamSource(XSL_INLINE));
        } 
        catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
        
        NamespaceContext ctx = new NamespaceContext() {
            public String getNamespaceURI(String prefix) {
                if (prefix.equals("sml")) {
                    return "urn:simml";
                }
                else {
                    if (prefix.equals("mms")) {
                        return "urn:mathms";  
                    }    
                    else {
                        if (prefix.equals("mt")) {
                            return "http://www.w3.org/1998/Math/MathML";
                        }
                        else {
                            if (prefix.equals("scm")) {
                                return "http://purl.oclc.org/dsdl/schematron";
                            }
                            else {
                                return null;
                            }
                        }
                    }
                }
            }
            // Dummy implementation - not used!
            public Iterator<String> getPrefixes(String val) {
                return null;
            }
            // Dummy implemenation - not used!
            public String getPrefix(String uri) {
                return null;
            }
        };
    
        xpath.setNamespaceContext(ctx);
    }
    
    /**
     * Private constructor.
     */
    private Utils() {
        
    }
    
    /**
     * Executes an xpath query in the document.
     * @param node          The document to find in
     * @param xpathQuery    The query
     * @return              A nodelist with the results
     * @throws SUException  SU00X External error
     */
    public static NodeList find(Node node, String xpathQuery) throws SUException {
        try {
            XPathExpression expr = xpath.compile(xpathQuery);

            return (NodeList) expr.evaluate(node, XPathConstants.NODESET);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            throw new SUException(SUException.SU00X, e.toString());
        }
    }
    
    /**
     * Converts a string to Dom.
     * 
     * @param str               The string
     * @return                  The Dom document
     * @throws SUException      SU002 - Not a valid XML Document
     *                           SU00X - External error
     */
    public static Document stringToDom(String str) throws SUException {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            docFactory.setNamespaceAware(true);
            docFactory.setIgnoringElementContentWhitespace(true);
            docFactory.setIgnoringComments(true);
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            InputStream is = new ByteArrayInputStream(str.getBytes("UTF-8"));
            Document result = docBuilder.parse(is);
            removeWhitespaceNodes(result.getDocumentElement());
            return result;
        }
        catch (SAXException e) {
            throw new SUException(SUException.SU001, e.toString());
        }
        catch (Exception e) {
            throw new SUException(SUException.SU00X, e.toString());
        }
    }
    
    /**
     * Remove the white spaced nodes from a Dom element.
     * 
     * @param e The element to delete the whitespaces from
     */
    public static void removeWhitespaceNodes(Element e) {
        NodeList children = e.getChildNodes();
        for (int i = children.getLength() - 1; i >= 0; i--) {
            Node child = children.item(i);
            if (child instanceof Text && ((Text) child).getData().trim().length() == 0) {
                e.removeChild(child);
            }
            else if (child instanceof Element) {
                removeWhitespaceNodes((Element) child);
            }
        }
    }
    
    /**
     * Create an element in the Uri namespace.
     * @param doc           The document where to create the element
     * @param uri           The namespace uri
     * @param tagName       The tag name
     * @return              The element
     */
    public static Element createElement(Document doc, String uri, String tagName) {
        Element element;
        if (uri != null) {
            element = doc.createElementNS(uri, tagName);
            if (uri.equals("http://www.w3.org/1998/Math/MathML")) {
                element.setPrefix("mt");
            }
            if (uri.equals("urn:mathms")) {
                element.setPrefix("mms");
            }
            if (uri.equals("urn:simml")) {
                element.setPrefix("sml");
            }
        }
        else {
            element = doc.createElement(tagName); 
        }
        return element;
    }
    
    /**
     * Reads a file.
     * @param path          The path of the file
     * @return              The string
     * @throws SUException  SU00X - External error
     */
    public static String readFile(String path) throws SUException {
        try {
            FileInputStream file = new FileInputStream(path);
            byte[] b = new byte[file.available()];
            file.read(b);
            file.close();
            return new String(b);
        }
        catch (Exception e) {
            throw new SUException(SUException.SU00X, e.toString());
        }
    }
    
    /**
     * Transforms the Simml infix inside the result of snuggle tex transformations into simml tags.
     * @param input                     The snuggle tex output
     * @return                          The transformed document
     * @throws TransformerException     Exception
     */
    public static ByteArrayOutputStream replaceSimml(Document input) throws TransformerException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        DOMSource domSource = new DOMSource(input);
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.transform(domSource, new StreamResult(output));
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        return output;
    }
    
    /**
     * Converts a Dom node to string.
     * 
     * @param node              The dom node
     * @return                  The String
     * @throws SUException      SU00X - External error
     */
    public static String domToString(Node node) throws SUException {
        try {
            Transformer trans = TransformerFactory.newInstance().newTransformer();
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            StreamResult result = new StreamResult(new ByteArrayOutputStream());
            DOMSource source = new DOMSource(node);
            trans.transform(source, result);
            return new String(((ByteArrayOutputStream) result.getOutputStream()).toByteArray(), "UTF-8");
        }
        catch (Exception e) {
            throw new SUException(SUException.SU00X, e.toString());
        }
    }

    /**
     * Removes the namespaces from an XML string.
     * @param xmlData   The XML string
     * @return          The XML string without namespaces
     */
    public static String removeAllXmlNamespace(String xmlData) {
        return xmlData.replaceAll("\\sxmlns[^\"]+\"[^\"]+\"", "");
    }
  
    /**
     * Removes the namespaces from an XML string.
     * @param xmlData   The XML string
     * @return          The XML string without namespaces
     */
    public static String removeXmlPrefixes(String xmlData) {
        return xmlData.replaceAll("(<)(\\w+:)", "$1") /* remove opening tag prefix */
        	    .replaceAll("(</)(\\w+:)(.*?>)", "$1$3"); /* remove closing tag prefix */
    }
}
