<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron">
    <ns uri="urn:mathms" prefix="mms"/>
    <pattern>
        <rule context="//mms:gather/mms:field">
        </rule>
        <rule context="//mms:update/mms:field">
            <assert test="//mms:fields/mms:field = .">
                Update fields must be model fields.
            </assert>
        </rule>
        <rule context="//mms:executionOrder/mms:element">
            <assert test="//mms:updates/mms:update/mms:name|//mms:gathers/mms:gather/mms:name = .">
                The elements in the execution order must be gather or update elements.
            </assert>
        </rule>
    </pattern>
</schema>