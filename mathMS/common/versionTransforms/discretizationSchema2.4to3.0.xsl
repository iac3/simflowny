<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:mt="http://www.w3.org/1998/Math/MathML" xmlns:mms="urn:mathms"  xmlns:sml="urn:simml">
    
    <xsl:template match="mms:instructionSet">
        <mms:instructionSet>
            <sml:simml>
                <xsl:apply-templates select="./*"/>
            </sml:simml>
        </mms:instructionSet>
    </xsl:template>
    
    <xsl:template match="mms:schemaParameter">
        <mms:schemaParameter>
            <xsl:copy-of select="./mms:name"/>
            <xsl:copy-of select="./mms:paramName"/>
            <xsl:if test="./mms:value">
                <mms:values>
                    <xsl:for-each select="./mms:value">
                        <mms:value><xsl:value-of select="./mms:value/mms:valueName"/></mms:value>
                    </xsl:for-each>
                </mms:values>
            </xsl:if>
        </mms:schemaParameter>
    </xsl:template>
    
    <xsl:template match="sumOverInfluenceRadius">
        <sml:iterateOverInteractions>
            <xsl:apply-templates select="./*"/>
        </sml:iterateOverInteractions>
    </xsl:template>
    
    <xsl:template match="import">
        <sml:import>
            <sml:rule><xsl:value-of select="./rule"/></sml:rule>
            <sml:name><xsl:value-of select="./name/text()"/></sml:name>
            <xsl:if test="./name/*[not ( text() )]">
                <sml:sufix>
                    <xsl:apply-templates select="./name/*[not ( text() )]"/>
                </sml:sufix>
            </xsl:if>
            <xsl:if test="./param">
                <sml:importParams>
                    <xsl:for-each select="./param">
                        <sml:importParam><xsl:value-of select="."/></sml:importParam>
                    </xsl:for-each>
                </sml:importParams>
            </xsl:if>
        </sml:import>
    </xsl:template>
    
    <xsl:template match="timeStep">
        <xsl:if test="./@append">
            <sml:iterateOverCells appendAtt="{@append}" stencilAtt="{@stencil}">
                <xsl:apply-templates select="./*"/>
            </sml:iterateOverCells>      
        </xsl:if>
        <xsl:if test="not(./@append)"></xsl:if>
        <sml:iterateOverCells stencilAtt="{@stencil}">
            <xsl:apply-templates select="./*"/>
        </sml:iterateOverCells>
    </xsl:template>
            
    <xsl:template match="boundary[@stencil]">
        <sml:boundary stencilAtt="{@stencil}">
            <xsl:apply-templates select="./*"/>
        </sml:boundary>
    </xsl:template>
    
    <xsl:template match="mt:apply[@type]">
        <mt:apply sml:type="{@type}">
            <xsl:apply-templates select="./*"/>
        </mt:apply>
    </xsl:template>
    
    <xsl:template match="mt:cn[@type]">
        <mt:cn sml:type="{@type}">
            <xsl:apply-templates select="./*"/>
        </mt:cn>
    </xsl:template>
    
    <xsl:template match="mt:ci[@type]">
        <mt:ci sml:type="{@type}">
            <xsl:apply-templates select="node()|@*"/>
        </mt:ci>
    </xsl:template>
    
    <xsl:template match="variable">
        <sml:variable>
            <mt:math>
                <xsl:apply-templates select="./*"/>
            </mt:math>
        </sml:variable>
    </xsl:template>
    
    
    <xsl:template match="timeSlice">
        <sml:timeSlice>
            <mt:math>
                <xsl:apply-templates select="./*"/>
            </mt:math>
        </sml:timeSlice>
    </xsl:template>
    
    <xsl:template match="previousVariable">
        <sml:previousVariable>
            <mt:math>
                <xsl:apply-templates select="./*"/>
            </mt:math>
        </sml:previousVariable>
    </xsl:template>
    
    <xsl:template match="auxiliaryEquations">
        <sml:auxiliaryEquations>
            <mt:math>
                <xsl:apply-templates select="./*"/>
            </mt:math>
        </sml:auxiliaryEquations>
    </xsl:template>
    
    <xsl:template match="constraints">
        <sml:constraints>
            <mt:math>
                <xsl:apply-templates select="./*"/>
            </mt:math>
        </sml:constraints>
    </xsl:template>
    
    <xsl:template match="previousPosition">
        <sml:previousPosition>
            <mt:math>
                <xsl:apply-templates select="./*"/>
            </mt:math>
        </sml:previousPosition>
    </xsl:template>
    
    <xsl:template match="newPosition">
        <sml:newPosition>
            <mt:math>
                <xsl:apply-templates select="./*"/>
            </mt:math>
        </sml:newPosition>
    </xsl:template>
    
    <xsl:template match="mms:discretizationSchema">
        <mms:discretizationSchema xmlns:mms="urn:mathms" xmlns:mt="http://www.w3.org/1998/Math/MathML"
            xmlns:sml="urn:simml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="urn:mathms file:/home/bminano/bitbucket/simflowny/mathMS/common/XSD/discretizationSchema.xsd">
            <xsl:apply-templates select="./*"/>
        </mms:discretizationSchema>
    </xsl:template>
    
    <xsl:template match="mt:semantics">
            <xsl:apply-templates select="./*"/>
    </xsl:template>
    
        <xsl:template match="mms:collection">
    </xsl:template>
   
    <xsl:template match="mms:date">
        <mms:date><xsl:value-of select="string-join(reverse(tokenize(., '/')), '-')"/></mms:date>
    </xsl:template>
   
    <xsl:template match="deployCoordinates">
        <sml:currentCell/>
    </xsl:template>
    
    <xsl:template match="sameCoordinates">
        <sml:sameCoordinate>
            <xsl:apply-templates select="./*"/>
        </sml:sameCoordinate>
    </xsl:template>
   
    <xsl:template match="mms:withXQuery">
    </xsl:template>
    
    <xsl:template match="influenceRadius">
        <sml:maxInteractionRange>
            <xsl:apply-templates select="./*"/>
        </sml:maxInteractionRange>
    </xsl:template>
    
    <xsl:template match="maxCharSpeed|omnidirectionalContext|positionAverageCalculation|iterationNumber|doubleDerivative|genericFieldContext|specialFieldContext|secondContSpatialCoordinate|differentCoordinates|distanceVariable|preCalculation|particleVolume|moveParticles|specialField|neighbourParticle|particleDistance|smoothingLength|segmentSurfaceId|segmentInteriorId|interactionSegmentId|fieldExtrapolation|auxiliaryFieldExtrapolation|currentTime|minSegmentValue|contTimeCoordinate|stencil|boundary|interiorDomainContext|segmentInteraction|name|rule|parabolicTermsSumVariables|sources|param|when|then|if|else|while|loop|incrementCoordinate2|decrementCoordinate2|return|contSpatialCoordinate|decrementCoordinate|incrementCoordinate|choose|spatialCoordinate|field|timeCoordinate|maxCharSpeedCoordinate|parabolicTermsVariable|parabolicTermsContext|parabolicTermsQ|parabolicTermsP|secondSpatialCoordinate|decrementCoordinate1DecrementCoordinate2|incrementCoordinate1DecrementCoordinate2|decrementCoordinate1IncrementCoordinate2|incrementCoordinate1IncrementCoordinate2">
        <xsl:element name="sml:{name()}"
            namespace="urn:simml">
            <xsl:apply-templates select="node()|@*"/>
        </xsl:element>
    </xsl:template>
        
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
   
</xsl:stylesheet>
