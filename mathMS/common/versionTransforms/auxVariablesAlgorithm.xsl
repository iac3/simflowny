<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"  xmlns:mms="urn:mathms"  xmlns:sml="urn:simml" xmlns:mt="http://www.w3.org/1998/Math/MathML">
    
   <xsl:template match="mms:operator[ancestor::mms:auxiliaryVariableEquation]">
      <mms:definition>
         <mms:algebraic>
                <xsl:copy-of select="."/>
         </mms:algebraic>
      </mms:definition>
   </xsl:template>
   
   <xsl:template match="@*|node()">
      <xsl:copy>
         <xsl:apply-templates select="@*|node()"/>
      </xsl:copy>
   </xsl:template>
</xsl:stylesheet>
