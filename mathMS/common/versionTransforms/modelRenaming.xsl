<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:mt="http://www.w3.org/1998/Math/MathML" xmlns:mms="urn:mathms"  xmlns:sml="urn:simml">
        
    <xsl:template match="mms:physicalModel">
        <mms:balanceLawPDEModel>
            <xsl:apply-templates select="@*|node()" />
        </mms:balanceLawPDEModel>
    </xsl:template>
    
    <xsl:template match="mms:equationSet">
        <xsl:apply-templates select="@*|node()" />
    </xsl:template>
    
    <xsl:template match="mms:canonicalPDE">
        <xsl:apply-templates select="@*|node()" />
    </xsl:template>
    
    <xsl:template match="mms:equationName">
        <mms:name>
            <xsl:apply-templates select="@*|node()" />
        </mms:name>
    </xsl:template>
    
    <xsl:template match="mms:flux">
        <mms:flux>
            <xsl:apply-templates select="./*[2]" />
            <xsl:apply-templates select="./*[1]" />
        </mms:flux>
    </xsl:template>
    
    <xsl:template match="mms:mathML">
        <xsl:apply-templates select="@*|node()" />
    </xsl:template>
    
    <xsl:template match="mms:parabolicTerm">
        <mms:parabolicTerm>
            <mms:firstDerivative>
                <mms:coordinate><xsl:value-of select="./mms:firstDerivative"/></mms:coordinate>
                <mms:argument>
                    <xsl:apply-templates select="./mms:Qterm/mms:mathML/mt:math"></xsl:apply-templates>
                    <mms:secondDerivative>
                        <mms:coordinate><xsl:value-of select="./mms:secondDerivative"/></mms:coordinate>
                        <mms:argument>
                            <xsl:apply-templates select="./mms:Pterm/mms:mathML/mt:math"></xsl:apply-templates>
                        </mms:argument>
                    </mms:secondDerivative>
                </mms:argument>
            </mms:firstDerivative>
        </mms:parabolicTerm>
    </xsl:template>
    
    <xsl:template match="mms:auxiliaryEquationData">
        <xsl:apply-templates select="@*|node()" />
    </xsl:template>
    
    <xsl:template match="mms:instructionSet">
        <mms:definition>
            <xsl:apply-templates select=".//mt:math" />
        </mms:definition>
    </xsl:template>
    
    <xsl:template match="mms:mathematicalConstraint">
        <mms:constraint>
            <xsl:apply-templates select="@*|node()" />
        </mms:constraint>
    </xsl:template>
    
    <xsl:template match="mms:charDecomposition">
        <mms:characteristicDecomposition>
            <xsl:apply-templates select="@*|node()" />
        </mms:characteristicDecomposition>
    </xsl:template>
    
    <xsl:template match="mms:generalCharDecomp">
        <mms:generalCharacteristicDecomposition>
            <xsl:apply-templates select="@*|node()" />
        </mms:generalCharacteristicDecomposition>
    </xsl:template>
    
    <xsl:template match="mms:coordCharDecomps">
        <mms:coordinateCharacteristicDecompositions>
            <xsl:apply-templates select="@*|node()" />
        </mms:coordinateCharacteristicDecompositions>
    </xsl:template>
    
    <xsl:template match="mms:coordCharDecomp">
        <mms:coordinateCharacteristicDecomposition>
            <xsl:apply-templates select="@*|node()" />
        </mms:coordinateCharacteristicDecomposition>
    </xsl:template>
        
    <xsl:template match="mms:vectorName">
        <mms:name>
            <xsl:apply-templates select="@*|node()" />
        </mms:name>
    </xsl:template>
    
    <xsl:template match="mms:auxiliaryVar">
        <mms:auxiliaryVariable>
            <xsl:apply-templates select="@*|node()" />
        </mms:auxiliaryVariable>
    </xsl:template>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>
