<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:mt="http://www.w3.org/1998/Math/MathML" xmlns:mms="urn:mathms"  xmlns:sml="urn:simml">
        
    <xsl:template match="mms:simulationProblem">
        <mms:balanceLawPDEProblem>
            <xsl:apply-templates select="@*|node()" />
        </mms:balanceLawPDEProblem>
    </xsl:template>
    
    <xsl:template match="mms:coordinateSystem">
        <mms:coordinates>
            <xsl:apply-templates select="@*|node()" />
        </mms:coordinates>        
    </xsl:template>
    
    <xsl:template match="mms:modelImports">
        <mms:models>
            <xsl:apply-templates select="@*|node()" />
        </mms:models>
    </xsl:template>
    
    <xsl:template match="mms:modelImport">
        <mms:model>
            <xsl:apply-templates select="@*|node()" />
            <xsl:variable name="modelId" select="./mms:modelId"></xsl:variable>
            <xsl:if test="//mms:coordinateEquality[mms:modelId = $modelId]">
                <mms:coordinateEquivalence>
                    <xsl:for-each select="//mms:coordinateEquality[mms:modelId = $modelId]//mms:equality">
                        <mms:equivalence>
                            <mms:problemElement><xsl:value-of select="./mms:problemData"/></mms:problemElement>
                            <mms:modelElement><xsl:value-of select="./mms:modelData"/></mms:modelElement>
                        </mms:equivalence>
                    </xsl:for-each>
                </mms:coordinateEquivalence>
            </xsl:if>
            <xsl:if test="//mms:fieldEquality[mms:modelId = $modelId]">
                <mms:fieldEquivalence>
                    <xsl:for-each select="//mms:fieldEquality[mms:modelId = $modelId]//mms:equality">
                        <mms:equivalence>
                            <mms:problemElement><xsl:value-of select="./mms:problemData"/></mms:problemElement>
                            <mms:modelElement><xsl:value-of select="./mms:modelData"/></mms:modelElement>
                        </mms:equivalence>
                    </xsl:for-each>
                </mms:fieldEquivalence>
            </xsl:if>
            <xsl:if test="//mms:tensorEquality[mms:modelId = $modelId]">
                <mms:tensorEquivalence>
                    <xsl:for-each select="//mms:tensorEquality[mms:modelId = $modelId]//mms:equality">
                        <mms:equivalence>
                            <mms:problemElement><xsl:value-of select="./mms:problemData"/></mms:problemElement>
                            <mms:modelElement><xsl:value-of select="./mms:modelData"/></mms:modelElement>
                        </mms:equivalence>
                    </xsl:for-each>
                </mms:tensorEquivalence>
            </xsl:if>
            <xsl:if test="//mms:eigenVectorEquality[mms:modelId = $modelId]">
                <mms:eigenVectorEquivalence>
                    <xsl:for-each select="//mms:eigenVectorEquality[mms:modelId = $modelId]//mms:equality">
                        <mms:equivalence>
                            <mms:problemElement><xsl:value-of select="./mms:problemData"/></mms:problemElement>
                            <mms:modelElement><xsl:value-of select="./mms:modelData"/></mms:modelElement>
                        </mms:equivalence>
                    </xsl:for-each>
                </mms:eigenVectorEquivalence>
            </xsl:if>
            <xsl:if test="//mms:parameterEquality[mms:modelId = $modelId]">
                <mms:parameterEquivalence>
                    <xsl:for-each select="//mms:parameterEquality[mms:modelId = $modelId]//mms:equality">
                        <mms:equivalence>
                            <mms:problemElement><xsl:value-of select="./mms:problemData"/></mms:problemElement>
                            <mms:modelElement><xsl:value-of select="./mms:modelData"/></mms:modelElement>
                        </mms:equivalence>
                    </xsl:for-each>
                </mms:parameterEquivalence>
            </xsl:if>
        </mms:model>
    </xsl:template>
    
    <xsl:template match="mms:modelId">
        <mms:id>
            <xsl:apply-templates select="@*|node()" />
        </mms:id>
    </xsl:template>
    
    <xsl:template match="mms:modelName">
        <mms:name>
            <xsl:apply-templates select="@*|node()" />
        </mms:name>
    </xsl:template>
    
    <xsl:template match="mms:coordinateEqualities|mms:fieldEqualities|mms:tensorEqualities|mms:eigenVectorEqualities|mms:parameterEqualities">
    </xsl:template>
    
    <xsl:template match="mms:mathML">
        <xsl:apply-templates select="@*|node()" />
    </xsl:template>
    
    <xsl:template match="mms:segmentGroups">
        <mms:region>
            <mms:name><xsl:value-of select="./mms:segmentGroup[descendant::mms:problemDomain]/mms:segmentGroupName"/></mms:name>
            <mms:interiorModels>
                <xsl:for-each select="./mms:segmentGroup[descendant::mms:problemDomain]//mms:interior/mms:modelId">
                    <xsl:variable name="modelId" select="./text()"></xsl:variable>
                    <mms:interiorModel><xsl:value-of select="//mms:modelImport[mms:modelId/text() = $modelId]/mms:modelName"/></mms:interiorModel>
                </xsl:for-each>
            </mms:interiorModels>
            <mms:spatialDomain>
                <xsl:for-each select="//mms:problemDomain/mms:coordinateLimit">
                    <xsl:apply-templates select="."></xsl:apply-templates>
                </xsl:for-each>
            </mms:spatialDomain>
            <xsl:apply-templates select="./mms:segmentGroup[descendant::mms:problemDomain]/mms:initialConditions"/>
            <xsl:apply-templates select="./mms:segmentGroup[descendant::mms:problemDomain]/mms:fieldGroups"/>
            <xsl:apply-templates select="./mms:segmentGroup[descendant::mms:problemDomain]/mms:segmentInteractions"/>
        </mms:region>
        <xsl:if test="./mms:segmentGroup[not(descendant::mms:problemDomain)]">
            <mms:subregions>
               <xsl:for-each select="./mms:segmentGroup[not(descendant::mms:problemDomain)]">
                   <mms:subregion>
                       <mms:name><xsl:value-of select="mms:segmentGroupName"/></mms:name>
                       <xsl:if test=".//mms:interior/mms:modelId">
                           <mms:interiorModels>
                               <xsl:for-each select=".//mms:interior/mms:modelId">
                                   <xsl:variable name="modelId" select="./text()"></xsl:variable>
                                   <mms:interiorModel><xsl:value-of select="//mms:modelImport[mms:modelId = $modelId]/mms:modelName"/></mms:interiorModel>
                               </xsl:for-each>
                           </mms:interiorModels>
                       </xsl:if>
                       <xsl:if test=".//mms:surface/mms:modelId">
                           <mms:surfaceModels>
                               <xsl:for-each select=".//mms:surface/mms:modelId">
                                   <xsl:variable name="modelId" select="./text()"></xsl:variable>
                                   <mms:surfaceModel><xsl:value-of select="//mms:modelImport[mms:modelId = $modelId]/mms:modelName"/></mms:surfaceModel>
                               </xsl:for-each>
                           </mms:surfaceModels>
                       </xsl:if>
                       <mms:location>
                           <xsl:for-each select=".//mms:coordinateLimits">
                               <mms:spatialDomain>
                                   <xsl:for-each select="./mms:coordinateLimit">
                                     <xsl:apply-templates select="."></xsl:apply-templates>
                                   </xsl:for-each>
                               </mms:spatialDomain>
                           </xsl:for-each>
                           <xsl:for-each select=".//mms:x3dSegmentId">
                               <mms:x3d>
                                <mms:name>X3D region</mms:name>
                                <mms:id><xsl:value-of select="."/></mms:id>
                               </mms:x3d>
                           </xsl:for-each>
                       </mms:location>
                       <xsl:apply-templates select="./mms:initialConditions"/>
                       <xsl:apply-templates select="./mms:fieldGroups"/>
                       <xsl:apply-templates select="./mms:segmentInteractions"/>
                   </mms:subregion>
               </xsl:for-each>
            </mms:subregions>
        </xsl:if> 
    </xsl:template>
    
    <xsl:template match="mms:segmentInteractions">
        <mms:interactions>
            <xsl:for-each select="./mms:segmentInteraction[mms:interaction/mms:type = 'Fixed']">
                <mms:fieldInteraction>
                    <mms:targetRegions>
                        <xsl:for-each select=".//mms:targetSegment">
                            <mms:targetRegion><xsl:value-of select="."/></mms:targetRegion>
                        </xsl:for-each>
                    </mms:targetRegions>
                    <mms:projections>
                        <xsl:for-each select=".//mms:fixedValue">
                            <mms:projection>
                                <mms:variable><xsl:value-of select="./mms:fixedVariable"/></mms:variable>
                                <mms:terms>
                                    <xsl:for-each select=".//mms:diagonalizationCoefficient">
                                        <mms:term>
                                            <xsl:apply-templates select="./mms:field"></xsl:apply-templates>
                                            <xsl:apply-templates select=".//mt:math"></xsl:apply-templates>
                                        </mms:term>
                                    </xsl:for-each>
                                </mms:terms>
                            </mms:projection>
                        </xsl:for-each>
                    </mms:projections>
                </mms:fieldInteraction>
            </xsl:for-each>
            <xsl:for-each select="./mms:segmentInteraction[mms:interaction/mms:type = 'Characteristic']">
                <mms:eigenVectorInteraction>
                    <mms:targetRegions>
                        <xsl:for-each select=".//mms:targetSegment">
                            <mms:targetRegion><xsl:value-of select="."/></mms:targetRegion>
                        </xsl:for-each>
                    </mms:targetRegions>
                    <mms:projections>
                        <xsl:for-each select=".//mms:fixedValue">
                            <mms:projection>
                                <mms:variable><xsl:value-of select="./mms:fixedVariable"/></mms:variable>
                                <xsl:apply-templates select="./mms:diagonalizationCoefficients"></xsl:apply-templates>
                            </mms:projection>
                        </xsl:for-each>
                    </mms:projections>
                </mms:eigenVectorInteraction>
            </xsl:for-each>
        </mms:interactions>
    </xsl:template>
    
    <xsl:template match="mms:equations">
        <mms:mathExpressions>
            <xsl:apply-templates select=".//mt:math"></xsl:apply-templates>
        </mms:mathExpressions>
    </xsl:template>
    
    <xsl:template match="mms:coordinateLimit">
        <mms:coordinateLimits>
            <xsl:apply-templates select="@*|node()" />
        </mms:coordinateLimits>
    </xsl:template>
    
    <xsl:template match="mms:segmentsPrecedence">
        <mms:subregionPrecedence>
            <xsl:apply-templates select="@*|node()" />
        </mms:subregionPrecedence>
    </xsl:template>
    
    <xsl:template match="mms:segmentPrecedence">
        <mms:precedence>
            <xsl:apply-templates select="@*|node()" />
        </mms:precedence>
    </xsl:template>
    
    <xsl:template match="mms:prevalentSegment">
        <mms:prevalentRegion>
            <xsl:apply-templates select="@*|node()" />
        </mms:prevalentRegion>
    </xsl:template>
    
    <xsl:template match="mms:surrogateSegment">
        <mms:surrogateRegion>
            <xsl:apply-templates select="@*|node()" />
        </mms:surrogateRegion>
    </xsl:template>
        
    <xsl:template match="mms:boundaries">
        <mms:boundaryConditions>
            <xsl:apply-templates select="@*|node()" />
        </mms:boundaryConditions>
    </xsl:template>
    
    <xsl:template match="mms:boundaryDefinition">
        <mms:boundaryPolicy>
            <xsl:apply-templates select="@*|node()" />
        </mms:boundaryPolicy>
    </xsl:template>
    
    <xsl:template match="mms:boundarySegmentGroups">
        <mms:boundaryRegions>
            <xsl:apply-templates select="@*|node()" />
        </mms:boundaryRegions>
    </xsl:template>
    
    <xsl:template match="mms:segmentGroupName">
        <mms:regionName>
            <xsl:apply-templates select="@*|node()" />
        </mms:regionName>
    </xsl:template>
    
    <xsl:template match="mms:boundariesPrecedence">
        <mms:boundaryPrecedence>
            <xsl:apply-templates select="@*|node()" />
        </mms:boundaryPrecedence>
    </xsl:template>
    
    <xsl:template match="mms:problemDomain">
    </xsl:template>
        
    <xsl:template match="mms:boundaryId">
        <mms:boundary>
            <xsl:apply-templates select="@*|node()" />
        </mms:boundary>
    </xsl:template>
    
    <xsl:template match="mms:boundaryCondition">
        <mms:boundaryCondition>
            <mms:type>
                <xsl:choose>
                    <xsl:when test="mms:type = 'Periodical'">
                        <mms:periodical/>
                    </xsl:when>
                    <xsl:when test="mms:type = 'Flat'">
                        <mms:flat/>
                    </xsl:when>
                    <xsl:when test="mms:type = 'None'">
                        <mms:extrapolation/>
                    </xsl:when>
                    <xsl:when test="mms:type = 'Static'">
                        <mms:static>
                            <xsl:apply-templates select=".//mms:initialConditions"></xsl:apply-templates>
                        </mms:static>
                    </xsl:when>
                    <xsl:when test="mms:type = 'Maximal dissipation'">
                        <mms:maximalDissipation>
                            <xsl:apply-templates select=".//mms:initialConditions"></xsl:apply-templates>
                        </mms:maximalDissipation>
                    </xsl:when>
                    <xsl:when test="mms:type = 'Fixed'">
                        <mms:algebraic>
                                <xsl:for-each select=".//mms:fixedValue">
                                    <mms:expression>
                                        <mms:field><xsl:value-of select=".//fixedField"/></mms:field>
                                        <mt:math>
                                            <xsl:apply-templates select=".//mt:math" />
                                        </mt:math>
                                    </mms:expression>
                                </xsl:for-each>
                        </mms:algebraic>
                    </xsl:when>
                    <xsl:when test="mms:type = 'Reflection'">
                        <mms:reflection>
                            <xsl:for-each select=".//mms:sign">
                                <xsl:apply-templates select="."></xsl:apply-templates>
                            </xsl:for-each>
                        </mms:reflection>
                    </xsl:when>
                </xsl:choose>
            </mms:type>
            <xsl:apply-templates select="./mms:axis"/>
            <xsl:apply-templates select="./mms:side"/>
            <xsl:apply-templates select="./mms:fields"/>
            <xsl:apply-templates select="./mms:condition"/>
        </mms:boundaryCondition>
    </xsl:template>
    
    <xsl:template match="mms:condition">
        <mms:applyIf>
            <xsl:apply-templates select=".//mt:math" />
        </mms:applyIf>
    </xsl:template>  
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>
