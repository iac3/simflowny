from subprocess import call
import sys, getopt
import os
from os.path import isdir, join, exists, isfile
import re
import lxml.etree as ET

def listOfFiles(directory):
    return [ f for f in listdir(directory) if isfile(join(directory,f)) ]


def transformar(transform, filename):
    dom = ET.parse(filename)
    newdom = transform(dom)
    with open(filename,'w') as f:
        f.write(ET.tostring(newdom, pretty_print=True))

def main():


    xslt = ET.parse("toTransformationRule.xsl")
    transform = ET.XSLT(xslt)


    origin = "/home/bminano/bitbucket/scripts/DBUploader/db"
    for root, dirs, files in os.walk(origin):
        path = root.split('/')
        for file in files:
            if re.match('[A-Za-z0-9_\-]*.xml', file):
                transformar(transform,  join(root,file))

if __name__ == "__main__":
	main()
