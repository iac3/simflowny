<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"  xmlns:mms="urn:mathms"  xmlns:sml="urn:simml">
    
    <xsl:template match="mms:instructionSet">
        <mms:instructionSet>
            <sml:simml>
                <xsl:copy-of select="./*"/>
            </sml:simml>
        </mms:instructionSet>
    </xsl:template>
    
    <xsl:template match="mms:coordinateSystem[parent::mms:segmentGroup]"></xsl:template>
    
    <xsl:template match="mms:*[ ends-with(local-name(), 'Equality') ]">
        <xsl:copy>
            <xsl:copy-of select="./mms:modelId"/>
            <mms:equalities>
                <xsl:copy-of select="./mms:equality"/>
            </mms:equalities>
        </xsl:copy>
    </xsl:template>
   
    <xsl:template match="mms:fixedValue[ancestor::mms:segmentInteraction]">
        <xsl:copy>
            <xsl:copy-of select="./mms:fixedVariable"/>
            <mms:diagonalizationCoefficients>
                <xsl:for-each select="./mms:diagCoefficient">
                    <mms:diagonalizationCoefficient>
                        <xsl:copy-of select="./mms:indexNumber"/>
                        <xsl:copy-of select="./mms:field"/>
                        <mms:mathML>
                            <xsl:apply-templates select="./mms:instructionSet/*"/>
                        </mms:mathML>
                    </mms:diagonalizationCoefficient>
                </xsl:for-each>
            </mms:diagonalizationCoefficients>
        </xsl:copy>
    </xsl:template>
   
    <xsl:template match="mms:initialCondition">
        <xsl:copy>
            <xsl:if test="./mms:condition">
                <xsl:apply-templates select="./mms:condition"></xsl:apply-templates>
            </xsl:if>
            <mms:equations>
                <xsl:apply-templates select="./mms:equation"></xsl:apply-templates>
            </mms:equations>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="mms:finalizationCondition">
        <xsl:copy>
            <xsl:if test="./mms:condition">
                <xsl:apply-templates select="./mms:condition"></xsl:apply-templates>
            </xsl:if>
            <mms:equations>
                <xsl:apply-templates select="./mms:equation"></xsl:apply-templates>
            </mms:equations>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="/mms:simulationProblem">
        <mms:simulationProblem xmlns:mms="urn:mathms"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="urn:mathms file:/home/bminano/bitbucket/simflowny/mathMS/common/XSD/simulationProblem.xsd" xmlns:mt="http://www.w3.org/1998/Math/MathML">
            <mms:head>
                <mms:name><xsl:value-of select="//mms:head/mms:name"/></mms:name>
                <mms:id><xsl:value-of select="//mms:head/mms:id"/></mms:id>
                <mms:author><xsl:value-of select="//mms:head/mms:author"/></mms:author>
                <mms:version><xsl:value-of select="//mms:head/mms:version"/></mms:version>
                <mms:date><xsl:value-of select="string-join(reverse(tokenize(//mms:head/mms:date, '/')), '-')"/></mms:date>
                <mms:description><xsl:value-of select="//mms:head/mms:description"/></mms:description>
            </mms:head>
            <mms:coordinateSystem>
                <mms:spatialCoordinates>
                    <xsl:for-each select="//mms:baseSpatialCoordinateSystem/mms:spatialCoordinate">
                        <mms:spatialCoordinate><xsl:value-of select="."/></mms:spatialCoordinate>
                    </xsl:for-each>
                </mms:spatialCoordinates>
                <mms:timeCoordinate><xsl:value-of select="//mms:timeCoordinate"/></mms:timeCoordinate>
            </mms:coordinateSystem>
            <mms:fields>
                <xsl:for-each select="/*/mms:field">
                    <mms:field><xsl:value-of select="."/></mms:field>
                </xsl:for-each>
            </mms:fields>
            <mms:auxiliaryFields>
                <xsl:for-each select="/*/mms:auxiliarField">
                    <mms:auxiliaryField><xsl:value-of select="."/></mms:auxiliaryField>
                </xsl:for-each>
            </mms:auxiliaryFields>
            <xsl:if test="/*/mms:tensor">
                <mms:tensors>
                    <xsl:for-each select="/*/mms:tensor">
                        <mms:tensor>
                            <mms:name><xsl:value-of select="./mms:name"/></mms:name>
                            <mms:order><xsl:value-of select="./mms:order"/></mms:order>
                            <mms:fields>
                                <xsl:for-each select="./mms:field">
                                    <xsl:copy-of select="."/>
                                </xsl:for-each>
                            </mms:fields>
                        </mms:tensor>
                    </xsl:for-each>
                </mms:tensors>
            </xsl:if>
            <mms:parameters>
                <xsl:for-each select="/*/mms:parameter">
                    <xsl:copy-of select="."/>
                </xsl:for-each>
            </mms:parameters>
            <mms:modelImports>
                <xsl:for-each select="/*/mms:physicalModelImport">
                    <mms:modelImport>
                        <xsl:copy-of select="./*"/>
                    </mms:modelImport>
                </xsl:for-each>
            </mms:modelImports>
            <xsl:apply-templates select=".//mms:coordinateEqualities"></xsl:apply-templates>
            <xsl:apply-templates select=".//mms:fieldEqualities"></xsl:apply-templates>
            <xsl:if test=".//mms:auxiliarFieldEqualities">
                 <mms:auxiliaryFieldEqualities>
                     <xsl:for-each select=".//mms:auxiliarFieldEqualities/*">
                         <xsl:copy-of select="./mms:modelId"/>
                         <mms:equalities>
                             <xsl:copy-of select="./mms:equality"/>
                         </mms:equalities>
                     </xsl:for-each>
                 </mms:auxiliaryFieldEqualities>
            </xsl:if>
            <xsl:apply-templates select=".//mms:tensorEqualities"></xsl:apply-templates>
            <xsl:apply-templates select=".//mms:eigenVectorEqualities"></xsl:apply-templates>
            <xsl:apply-templates select=".//mms:parameterEqualities"></xsl:apply-templates>
            <xsl:apply-templates select="/*/mms:problemDomain"/>
            <xsl:apply-templates select="/*/mms:segmentGroups"/>
            <xsl:apply-templates select="/*/mms:segmentsPrecedence"/>
            <xsl:apply-templates select="/*/mms:boundaries"/>
            <xsl:apply-templates select="/*/mms:boundariesPrecedence"/>
            <xsl:apply-templates select="/*/mms:finalizationConditions"/>
        </mms:simulationProblem>
    </xsl:template>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>
