<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mt="http://www.w3.org/1998/Math/MathML" xmlns:mms="urn:mathms" version="1.0">

    <xsl:template match="mms:discretizationSchema[descendant::mms:transformationRule]">
        <mms:transformationRule>
            <xsl:apply-templates select="./*"/>
        </mms:transformationRule>
    </xsl:template>


    <xsl:template match="mms:transformationRule[ancestor::mms:discretizationSchema]">
        <mms:rule>
            <xsl:apply-templates select="./*"/>
        </mms:rule>
    </xsl:template>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
