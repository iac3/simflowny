<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:mt="http://www.w3.org/1998/Math/MathML" xmlns:mms="urn:mathms"  xmlns:sml="urn:simml">
        
    <xsl:template match="mms:field">
        <mms:agentProperty>
            <xsl:apply-templates select="@*|node()" />
        </mms:agentProperty>
    </xsl:template>
    
    <xsl:template match="mms:fields">
        <mms:agentProperties>
            <xsl:apply-templates select="@*|node()" />
        </mms:agentProperties>
    </xsl:template>
    
    <xsl:template match="mms:fieldEqualities">
        <mms:agentPropertyEquivalences>
            <xsl:apply-templates select="@*|node()" />
        </mms:agentPropertyEquivalences>
    </xsl:template>
    
    <xsl:template match="mms:fieldEquality">
        <mms:agentPropertyEquivalence>
            <xsl:apply-templates select="@*|node()" />
        </mms:agentPropertyEquivalence>
    </xsl:template>
    
    <xsl:template match="mms:parameterEqualities">
        <mms:parameterEquivalences>
            <xsl:apply-templates select="@*|node()" />
        </mms:parameterEquivalences>
    </xsl:template>
    
    <xsl:template match="mms:parameterEquality">
        <mms:parameterEquivalence>
            <xsl:apply-templates select="@*|node()" />
        </mms:parameterEquivalence>
    </xsl:template>
    
    <xsl:template match="mms:mathML">
            <xsl:apply-templates select="@*|node()" />
    </xsl:template>
    
    <xsl:template match="mms:coordinateEqualities">
        <mms:coordinateEquivalences>
            <xsl:apply-templates select="@*|node()" />
        </mms:coordinateEquivalences>
    </xsl:template>
    
    <xsl:template match="mms:coordinateEquality">
        <mms:coordinateEquivalence>
            <xsl:apply-templates select="@*|node()" />
        </mms:coordinateEquivalence>
    </xsl:template>
    
    <xsl:template match="mms:equalities">
        <mms:equivalences>
            <xsl:apply-templates select="@*|node()" />
        </mms:equivalences>
    </xsl:template>
    
    <xsl:template match="mms:equality">
        <mms:equivalence>
            <xsl:apply-templates select="@*|node()" />
        </mms:equivalence>
    </xsl:template>
    
    <xsl:template match="mms:problemDomain">
        <mms:mesh>
            <xsl:apply-templates select="@*|node()" />
        </mms:mesh>
    </xsl:template>
    
    <xsl:template match="mms:dimensions">
        <mms:spatialDomain>
            <xsl:apply-templates select="@*|node()" />
        </mms:spatialDomain>
    </xsl:template>
    
    <xsl:template match="mms:coordinateLimit">
        <mms:coordinateLimits>
            <xsl:apply-templates select="@*|node()" />
        </mms:coordinateLimits>
    </xsl:template>
    
    <xsl:template match="mms:movement">
        <mms:motion>
            <xsl:apply-templates select="@*|node()" />
        </mms:motion>
    </xsl:template>
    
    <xsl:template match="mms:speedField">
        <mms:agentSpeed>
            <xsl:apply-templates select="@*|node()" />
        </mms:agentSpeed>
    </xsl:template>
    
    <xsl:template match="mms:synchronization[text() = 'Full']">
        <mms:evolutionStep>AllVertices</mms:evolutionStep>
    </xsl:template>
    
    <xsl:template match="mms:synchronization[text() = 'Single']">
        <mms:evolutionStep>SingleVertex</mms:evolutionStep>
    </xsl:template>
    
    <xsl:template match="mms:condition">
        <mms:applyIf>
            <mms:mathExpression>
                <xsl:apply-templates select=".//mt:math" />
            </mms:mathExpression>
        </mms:applyIf>
    </xsl:template>
    
    <xsl:template match="mms:equations">
        <mms:mathExpressions>
            <xsl:for-each select=".//mt:math">
              <mms:mathExpression>
                  <xsl:apply-templates select="." />
              </mms:mathExpression>
            </xsl:for-each>
        </mms:mathExpressions>
    </xsl:template>
    
    <xsl:template match="mms:definitions">
        <mms:rules>
            <xsl:apply-templates select="@*|node()" />
        </mms:rules>
    </xsl:template>
    
    <xsl:template match="mms:gathers">
        <mms:gatherRules>
            <xsl:apply-templates select="@*|node()" />
        </mms:gatherRules>
    </xsl:template>
    
    <xsl:template match="mms:updates">
        <mms:updateRules>
            <xsl:apply-templates select="@*|node()" />
        </mms:updateRules>
    </xsl:template>
    
    <xsl:template match="mms:gather">
        <mms:gatherRule>
            <xsl:apply-templates select="@*|node()" />
        </mms:gatherRule>
    </xsl:template>
    
    <xsl:template match="mms:update">
        <mms:updateRule>
            <xsl:apply-templates select="@*|node()" />
        </mms:updateRule>
    </xsl:template>
    
    <xsl:template match="mms:instructionSet[sml:simml]">
        <xsl:apply-templates select="./sml:simml" />
    </xsl:template>
    
    <xsl:template match="sml:simml">
        <sml:algorithm>
            <xsl:apply-templates select="@*|node()" />
        </sml:algorithm>
    </xsl:template>
    
    <xsl:template match="mms:executionOrder">
        <mms:ruleExecutionOrder>
            <xsl:apply-templates select="@*|node()" />
        </mms:ruleExecutionOrder>
    </xsl:template>
    
    <xsl:template match="mms:element">
        <mms:rule>
            <xsl:apply-templates select="@*|node()" />
        </mms:rule>
    </xsl:template>
    
    <xsl:template match="mms:boundariesPrecedence">
        <mms:boundaryPrecedence>
            <xsl:apply-templates select="@*|node()" />
        </mms:boundaryPrecedence>
    </xsl:template>
    
    <xsl:template match="mms:boundaryId">
        <mms:boundary>
            <xsl:apply-templates select="@*|node()" />
        </mms:boundary>
    </xsl:template>
    
    <xsl:template match="mms:boundaries">
        <mms:boundaryConditions>
            <xsl:apply-templates select="@*|node()" />
        </mms:boundaryConditions>
    </xsl:template>
    
    <xsl:template match="sml:smoothingLength">
        <sml:influenceRadius>
            <xsl:apply-templates select="@*|node()" />
        </sml:influenceRadius>
    </xsl:template>
    
    <xsl:template match="sml:numberOfAgents">
        <sml:globalNumberOfAgents>
            <xsl:apply-templates select="@*|node()" />
        </sml:globalNumberOfAgents>
    </xsl:template>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>
