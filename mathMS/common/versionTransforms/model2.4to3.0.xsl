<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"  xmlns:mms="urn:mathms"  xmlns:sml="urn:simml" xmlns:mt="http://www.w3.org/1998/Math/MathML">
    
    <xsl:template match="mms:instructionSet">
        <mms:instructionSet>
            <sml:simml>
                <xsl:copy-of select="./*"/>
            </sml:simml>
        </mms:instructionSet>
    </xsl:template>
        
    <xsl:template match="/">
        <mms:physicalModel xmlns:mms="urn:mathms"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="urn:mathms file:/home/bminano/bitbucket/simflowny/mathMS/common/XSD/physicalModel.xsd" xmlns:mt="http://www.w3.org/1998/Math/MathML">
            <mms:head>
                <mms:name><xsl:value-of select="//mms:head/mms:name"/></mms:name>
                <mms:id><xsl:value-of select="//mms:head/mms:id"/></mms:id>
                <mms:author><xsl:value-of select="//mms:head/mms:author"/></mms:author>
                <mms:version><xsl:value-of select="//mms:head/mms:version"/></mms:version>
                <mms:date><xsl:value-of select="string-join(reverse(tokenize(//mms:head/mms:date, '/')), '-')"/></mms:date>
                <mms:description><xsl:value-of select="//mms:head/mms:description"/></mms:description>
            </mms:head>
            <mms:coordinates>
                <mms:spatialCoordinates>
                    <xsl:for-each select="//mms:coordinates/mms:spatialCoordinate">
                        <mms:spatialCoordinate><xsl:value-of select="."/></mms:spatialCoordinate>
                    </xsl:for-each>
                </mms:spatialCoordinates>
                <mms:timeCoordinate><xsl:value-of select="//mms:coordinates/mms:timeCoordinate"/></mms:timeCoordinate>
            </mms:coordinates>
            <mms:fields>
                <xsl:for-each select="/*/mms:field">
                    <mms:field><xsl:value-of select="."/></mms:field>
                </xsl:for-each>
            </mms:fields>
            <mms:auxiliaryFields>
                <xsl:for-each select="/*/mms:auxiliarField">
                    <mms:auxiliaryField><xsl:value-of select="."/></mms:auxiliaryField>
                </xsl:for-each>
            </mms:auxiliaryFields>
            <xsl:if test="/*/mms:tensor">
                <mms:tensors>
                    <xsl:for-each select="/*/mms:tensor">
                        <mms:tensor>
                            <mms:name><xsl:value-of select="./mms:name"/></mms:name>
                            <mms:order><xsl:value-of select="./mms:order"/></mms:order>
                            <mms:fields>
                                <xsl:for-each select="./mms:field">
                                    <xsl:copy-of select="."/>
                                </xsl:for-each>
                            </mms:fields>
                        </mms:tensor>
                    </xsl:for-each>
                </mms:tensors>
            </xsl:if>
            <mms:parameters>
                <xsl:for-each select="/*/mms:parameter">
                    <xsl:copy-of select="."/>
                </xsl:for-each>
            </mms:parameters>
            <mms:equationSet>
                <xsl:if test="//mms:evolutionEquation">
                    <mms:evolutionEquations>
                        <xsl:for-each select="//mms:evolutionEquation">
                            <mms:evolutionEquation>
                                <xsl:copy-of select="./mms:equationName"/>
                                <mms:canonicalPDE>
                                    <xsl:copy-of select="./mms:canonicalPDE/mms:field"/>
                                    <xsl:if test=".//mms:CFFD/mms:flux">
                                        <mms:fluxes>
                                            <xsl:for-each select=".//mms:CFFD/mms:flux">
                                                <xsl:copy-of select="."/>
                                            </xsl:for-each>
                                        </mms:fluxes>
                                    </xsl:if>
                                    <xsl:copy-of select="./mms:canonicalPDE/mms:source"/>
                                    <xsl:if test="./mms:canonicalPDE/mms:parabolicTerms">
                                        <xsl:copy-of select="./mms:canonicalPDE/mms:parabolicTerms"/>
                                    </xsl:if>
                                </mms:canonicalPDE>
                            </mms:evolutionEquation>
                        </xsl:for-each>
                    </mms:evolutionEquations>
                </xsl:if>
                <xsl:if test="//mms:constraint//mms:mathematicalConstraint">
                    <mms:constraints>
                        <xsl:for-each select="//mms:mathematicalConstraint">
                            <mms:mathematicalConstraint>
                                <xsl:copy-of select="./mms:equationName"/>
                                <mms:canonicalPDE>
                                    <xsl:if test=".//mms:CFFD/mms:flux">
                                        <mms:fluxes>
                                            <xsl:for-each select=".//mms:CFFD/mms:flux">
                                                <xsl:copy-of select="."/>
                                            </xsl:for-each>
                                        </mms:fluxes>
                                    </xsl:if>
                                    <xsl:copy-of select="./mms:canonicalPDE/mms:source"/>
                                </mms:canonicalPDE>
                            </mms:mathematicalConstraint>
                        </xsl:for-each>
                    </mms:constraints>
                </xsl:if>
                <xsl:if test="//mms:auxiliarEquation">
                    <mms:auxiliaryEquations>
                        <xsl:for-each select="//mms:auxiliarEquation">
                            <mms:auxiliaryEquation>
                                <xsl:copy-of select="./mms:equationName"/>
                                <mms:auxiliaryEquationData>
                                    <mms:auxiliaryField><xsl:value-of select=".//mms:auxiliarField"/></mms:auxiliaryField>
                                    <xsl:apply-templates select=".//mms:instructionSet"></xsl:apply-templates>
                                </mms:auxiliaryEquationData>
                            </mms:auxiliaryEquation>
                        </xsl:for-each>
                    </mms:auxiliaryEquations>
                </xsl:if>
                <xsl:if test="//mms:charDecomposition">
                    <mms:charDecomposition>
                        <xsl:if test="//mms:generalCharDecomp">
                            <mms:generalCharDecomp>
                                <mms:eigenSpaces>
                                    <xsl:for-each select="//mms:generalCharDecomp//mms:eigenSpace">
                                        <mms:eigenSpace>
                                            <xsl:copy-of select="./mms:eigenName"/>
                                            <xsl:if test="./mms:order">
                                                <xsl:copy-of select="./mms:order"/>
                                            </xsl:if>
                                            <xsl:copy-of select="./mms:eigenValue"/>
                                            <xsl:if test="./mms:eigenVector">
                                                <mms:eigenVectors>
                                                  <xsl:for-each select="./mms:eigenVector">
                                                      <mms:eigenVector>
                                                          <xsl:copy-of select="./mms:vectorName"/>
                                                          <mms:diagonalizationCoefficients>
                                                              <xsl:for-each select="./mms:diagCoefficient">
                                                                  <mms:diagonalizationCoefficient>
                                                                      <xsl:if test="./mms:indexNumber">
                                                                          <xsl:copy-of select="./mms:indexNumber"/>
                                                                      </xsl:if>
                                                                      <xsl:if test="./mms:field">
                                                                          <xsl:copy-of select="./mms:field"/>
                                                                      </xsl:if>
                                                                      <mms:mathML>
                                                                          <xsl:copy-of select=".//mt:math"/>
                                                                      </mms:mathML>
                                                                  </mms:diagonalizationCoefficient>
                                                              </xsl:for-each>
                                                          </mms:diagonalizationCoefficients>
                                                      </mms:eigenVector>
                                                  </xsl:for-each>
                                                </mms:eigenVectors>
                                            </xsl:if>
                                        </mms:eigenSpace>
                                    </xsl:for-each>
                                </mms:eigenSpaces>
                                <xsl:if test="//mms:generalCharDecomp//mms:undiagonalization">
                                    <mms:undiagonalizations>
                                       <xsl:for-each select="//mms:generalCharDecomp//mms:undiagonalization">
                                          <mms:undiagonalization>
                                              <xsl:copy-of select="./mms:field"/>
                                              <mms:undiagonalizationCoefficients>
                                                  <xsl:for-each select="./mms:undiagCoefficient">
                                                      <mms:undiagonalizationCoefficient>
                                                          <xsl:if test="./mms:indexNumber">
                                                              <xsl:copy-of select="./mms:indexNumber"/>
                                                          </xsl:if>
                                                          <xsl:copy-of select="./mms:eigenVector"/>
                                                          <xsl:copy-of select="./mms:mathML"/>
                                                      </mms:undiagonalizationCoefficient>
                                                  </xsl:for-each>
                                              </mms:undiagonalizationCoefficients>
                                          </mms:undiagonalization>
                                       </xsl:for-each>
                                    </mms:undiagonalizations>
                                </xsl:if>
                            </mms:generalCharDecomp>
                        </xsl:if>
                        <xsl:if test="//mms:coordCharDecomp">
                            <mms:coordCharDecomps>
                                <xsl:for-each select="//mms:coordCharDecomp">
                                    <mms:coordCharDecomp>
                                        <mms:eigenSpaces>
                                            <xsl:for-each select=".//mms:eigenSpace">
                                                <mms:eigenSpace>
                                                    <xsl:copy-of select="./mms:eigenName"/>
                                                    <xsl:if test="./mms:order">
                                                        <xsl:copy-of select="./mms:order"/>
                                                    </xsl:if>
                                                    <xsl:copy-of select="./mms:eigenValue"/>
                                                    <xsl:if test="./mms:eigenVector">
                                                        <mms:eigenVectors>
                                                            <xsl:for-each select="./mms:eigenVector">
                                                                <mms:eigenVector>
                                                                    <xsl:copy-of select="./mms:vectorName"/>
                                                                    <mms:diagonalizationCoefficients>
                                                                        <xsl:for-each select="./mms:diagCoefficient">
                                                                            <mms:diagonalizationCoefficient>
                                                                                <xsl:if test="./mms:indexNumber">
                                                                                    <xsl:copy-of select="./mms:indexNumber"/>
                                                                                </xsl:if>
                                                                                <xsl:if test="./mms:field">
                                                                                    <xsl:copy-of select="./mms:field"/>
                                                                                </xsl:if>
                                                                                <mms:mathML>
                                                                                    <xsl:copy-of select=".//mt:math"/>
                                                                                </mms:mathML>
                                                                            </mms:diagonalizationCoefficient>
                                                                        </xsl:for-each>
                                                                    </mms:diagonalizationCoefficients>
                                                                </mms:eigenVector>
                                                            </xsl:for-each>
                                                        </mms:eigenVectors>
                                                    </xsl:if>
                                                </mms:eigenSpace>
                                            </xsl:for-each>
                                        </mms:eigenSpaces>
                                        <xsl:if test=".//mms:undiagonalization">
                                            <mms:undiagonalizations>
                                                <xsl:for-each select=".//mms:undiagonalization">
                                                    <mms:undiagonalization>
                                                        <xsl:copy-of select="./mms:field"/>
                                                        <mms:undiagonalizationCoefficients>
                                                            <xsl:for-each select="./mms:undiagCoefficient">
                                                                <mms:undiagonalizationCoefficient>
                                                                    <xsl:if test="./mms:indexNumber">
                                                                        <xsl:copy-of select="./mms:indexNumber"/>
                                                                    </xsl:if>
                                                                    <xsl:copy-of select="./mms:eigenVector"/>
                                                                    <xsl:copy-of select="./mms:mathML"/>
                                                                </mms:undiagonalizationCoefficient>
                                                            </xsl:for-each>
                                                        </mms:undiagonalizationCoefficients>
                                                    </mms:undiagonalization>
                                                </xsl:for-each>
                                            </mms:undiagonalizations>
                                        </xsl:if>
                                        <xsl:copy-of select="./mms:eigenCoordinate"/>
                                    </mms:coordCharDecomp>
                                </xsl:for-each>
                            </mms:coordCharDecomps>
                        </xsl:if>
                        <xsl:if test="//mms:maxCharacteristicSpeed">
                            <xsl:copy-of select="//mms:maxCharacteristicSpeed"/>
                        </xsl:if>
                        <xsl:if test="//mms:auxiliarDefinition">
                            <mms:auxiliaryDefinitions>
                                <xsl:for-each select="//mms:auxiliarDefinition">
                                    <mms:auxiliaryDefinition>
                                        <mms:auxiliaryVar><xsl:value-of select="./mms:auxiliarVar"/></mms:auxiliaryVar>
                                        <mms:mathML>
                                            <xsl:copy-of select=".//mt:math"/>
                                        </mms:mathML>
                                    </mms:auxiliaryDefinition>
                                </xsl:for-each>
                            </mms:auxiliaryDefinitions>
                        </xsl:if>
                    </mms:charDecomposition>
                </xsl:if>
            </mms:equationSet>
        </mms:physicalModel>
    </xsl:template>
</xsl:stylesheet>
