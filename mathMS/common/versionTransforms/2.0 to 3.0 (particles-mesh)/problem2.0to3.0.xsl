<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"  xmlns:mms="urn:mathms"  xmlns:sml="urn:simml" xmlns:mt="http://www.w3.org/1998/Math/MathML">
    
        
   <xsl:template match="mms:boundaryCondition[not(mms:fields)]">
      <mms:boundaryCondition>
         <xsl:copy-of select="./mms:type"></xsl:copy-of>
         <xsl:copy-of select="./mms:axis"></xsl:copy-of>
         <xsl:copy-of select="./mms:side"></xsl:copy-of>
         <mms:fields>
            <xsl:for-each select="./ancestor-or-self::mms:PDEProblem/mms:fields/mms:field">
               <mms:field><xsl:value-of select="."></xsl:value-of></mms:field>
            </xsl:for-each>
            <xsl:for-each select="./ancestor-or-self::mms:PDEProblem/mms:auxiliaryFields/mms:auxiliaryField">
               <mms:field><xsl:value-of select="."></xsl:value-of></mms:field>
            </xsl:for-each>
         </mms:fields>
         <xsl:copy-of select="./mms:applyIf"></xsl:copy-of>
         
      </mms:boundaryCondition>
   </xsl:template>
   
   <xsl:template match="@*|node()">
      <xsl:copy>
         <xsl:apply-templates select="@*|node()"/>
      </xsl:copy>
   </xsl:template>
</xsl:stylesheet>
