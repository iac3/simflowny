from subprocess import call
import sys, getopt
import os
from os.path import isdir, join, exists, isfile
import re
import lxml.etree as ET
import fnmatch

def listOfFiles(directory):
    return [ f for f in listdir(directory) if isfile(join(directory,f)) ]


def transformar(transform, filename):
    dom = ET.parse(filename)
    newdom = transform(dom)
    with open(filename,'w') as f:
        f.write(ET.tostring(newdom, pretty_print=True,encoding='UTF-8'))

def save(filename, content):
    with open(filename,'w') as f:
        print "Guardando"
        f.write(ET.tostring(content, pretty_print=True,encoding='UTF-8'))

def changePolicy(filename, problemPath):
    ns = {'mms': 'urn:mathms'}
    tree = ET.parse(filename)
    if  tree.getroot().tag == '{urn:mathms}PDEDiscretizationPolicy':
        
        problemId = tree.findall('/mms:problem/mms:id', ns)
        print filename, problemId[0].text
        matches = []
        for root, dirnames, filenames in os.walk(problemPath):
            for f in fnmatch.filter(filenames, problemId[0].text + '.xml'):
                matches.append(os.path.join(root, f))
        if len(problemId) > 0 and len(matches) > 0:
            problemId = problemId[0].text
            problem = ET.parse(matches[0])
            fields = problem.findall('/mms:fields/mms:field', ns)
            
            meshVariables = ET.Element('{urn:mathms}meshVariables')
            for field in fields:
                meshVariable = ET.SubElement(meshVariables, '{urn:mathms}meshVariable')
                meshVariable.text = field.text
            fields = problem.findall('/mms:auxiliaryFields/mms:auxiliaryField', ns)
            for field in fields:
                meshVariable = ET.SubElement(meshVariables, '{urn:mathms}meshVariable')
                meshVariable.text = field.text
            tree.getroot().insert(2, meshVariables)
        
        analysisDiscretization = tree.findall('/mms:analysisDiscretization', ns)
        if len(analysisDiscretization) > 0:
            timeVar = ET.fromstring('<mms:inputVariables xmlns:mms="urn:mathms"><mms:inputVariable><mt:math xmlns:mt="http://www.w3.org/1998/Math/MathML" xmlns:sml="urn:simml"><mt:ci><mt:msup><mt:ci><sml:field/></mt:ci><mt:apply><mt:plus/><mt:ci><sml:timeCoordinate/></mt:ci><mt:cn>1</mt:cn></mt:apply></mt:msup></mt:ci></mt:math></mms:inputVariable><mms:inputVariable><mt:math xmlns:mt="http://www.w3.org/1998/Math/MathML" xmlns:sml="urn:simml"><sml:currentCell/></mt:math></mms:inputVariable></mms:inputVariables>')
            analysisDiscretization[0].insert(1, timeVar)
        save(filename, tree)

def main():

    
    xslt = ET.parse("model2.0to3.0.xsl")
    transform = ET.XSLT(xslt)
    
    origin = "/home/bminano/Escritorio/db_newVersion/Daniele/db_2019.06.12/db_downloaded/docManager/models"
    for root, dirs, files in os.walk(origin):
        path = root.split('/')
        for file in files:
            if re.match('[A-Za-z0-9_\-]*.xml', file):
                transformar(transform,  join(root,file))

    xslt = ET.parse("problem2.0to3.0.xsl")
    transform = ET.XSLT(xslt)

    origin = "/home/bminano/Escritorio/db_newVersion/Daniele/db_2019.06.12/db_downloaded/docManager/problems"
    for root, dirs, files in os.walk(origin):
        path = root.split('/')
        for file in files:
            if re.match('[A-Za-z0-9_\-]*.xml', file):
                transformar(transform,  join(root,file))
    
    
    origin = "/home/bminano/Escritorio/db_newVersion/Daniele/db_2019.06.12/db_downloaded/docManager"
    problemPath = '/home/bminano/Escritorio/db_newVersion/Daniele/db_2019.06.12/db_downloaded/docManager/problems/'
    for root, dirs, files in os.walk(origin):
        path = root.split('/')
        for file in files:
            if re.match('[A-Za-z0-9_\-]*.xml', file):
                changePolicy(join(root,file), problemPath)




if __name__ == "__main__":
	main()
