<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"  xmlns:mms="urn:mathms"  xmlns:sml="urn:simml" xmlns:mt="http://www.w3.org/1998/Math/MathML">
    
        
   <xsl:template match="mms:evolutionEquation">
      <mms:evolutionEquation>
         <xsl:copy-of select="./mms:name"></xsl:copy-of>
         <xsl:copy-of select="./mms:field"></xsl:copy-of>
         <mms:referenceFrame>Eulerian</mms:referenceFrame>
         <xsl:copy-of select="./mms:fluxes"></xsl:copy-of>
         <xsl:copy-of select="./mms:source"></xsl:copy-of>
         <xsl:for-each select="./mms:operator">
            <xsl:copy-of select="."/>
         </xsl:for-each>
         
      </mms:evolutionEquation>
   </xsl:template>
   
   <xsl:template match="@*|node()">
      <xsl:copy>
         <xsl:apply-templates select="@*|node()"/>
      </xsl:copy>
   </xsl:template>
</xsl:stylesheet>
