<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:mt="http://www.w3.org/1998/Math/MathML" xmlns:sml="urn:simml">
    
    
    <xsl:template match="mt:apply[@sml:type='function' or type='function']">
        <sml:functionCall>
            <xsl:apply-templates select="*"/>
        </sml:functionCall>
    </xsl:template>
    
    <xsl:template match="mt:apply[mt:ci[@sml:type='shared' or type='shared']]">
        <sml:sharedVariable>
            <mt:apply>
                <xsl:for-each select="*[(@sml:type='shared' or type='shared')]">
                    <mt:ci>
                        <xsl:apply-templates select="node()"/>
                    </mt:ci>
                </xsl:for-each>
                <xsl:for-each select="*[not(@sml:type='shared' or type='shared')]">
                    <xsl:apply-templates select="."/>
                </xsl:for-each>
            </mt:apply>
        </sml:sharedVariable>
    </xsl:template>
    
    <xsl:template match="mt:apply[mt:ci[@sml:type='flux' or type='flux']]">
        <sml:flux>
            <xsl:for-each select="*[(@sml:type='flux' or type='flux')]">
                <mt:ci>
                    <xsl:apply-templates select="node()"/>
                </mt:ci>
            </xsl:for-each>
            <xsl:for-each select="*[not(@sml:type='flux' or type='flux')]">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </sml:flux>
    </xsl:template>
    
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>
