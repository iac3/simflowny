<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:mt="http://www.w3.org/1998/Math/MathML" xmlns:mms="urn:mathms"  xmlns:sml="urn:simml">
        
    <xsl:template match="lower">
        <sml:side sideAtt="lower">
            <xsl:copy-of select="./*"/>
        </sml:side>
    </xsl:template>    
    <xsl:template match="upper">
        <sml:side sideAtt="upper">
            <xsl:copy-of select="./*"/>
        </sml:side>
    </xsl:template>  
    
    <xsl:template match="mms:fixedValue[ancestor::mms:segmentInteraction]">
        <xsl:copy>
            <xsl:copy-of select="./mms:fixedVariable"/>
            <mms:diagonalizationCoefficients>
                <xsl:for-each select="./mms:diagCoefficient">
                    <mms:diagonalizationCoefficient>
                        <xsl:copy-of select="./mms:indexNumber"/>
                        <xsl:copy-of select="./mms:field"/>
                        <mms:mathML>
                            <xsl:copy-of select="./mms:instructionSet/*"/>
                        </mms:mathML>
                    </mms:diagonalizationCoefficient>
                </xsl:for-each>
            </mms:diagonalizationCoefficients>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="//*[ local-name() = //mms:baseSpatialCoordinateSystem/mms:spatialCoordinate]">
        <sml:axis axisAtt="{local-name(.)}">
            <xsl:apply-templates select="./*"/>
        </sml:axis>
    </xsl:template>
    
    <xsl:template match="mms:coordinateSystem[parent::mms:segmentGroup]"></xsl:template>
        
    <xsl:template match="mms:initialCondition">
        <xsl:copy>
            <xsl:if test="./mms:condition">
                <xsl:apply-templates select="./mms:condition"></xsl:apply-templates>
            </xsl:if>
            <mms:equations>
                <xsl:apply-templates select="./mms:equation"></xsl:apply-templates>
            </mms:equations>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="mms:finalizationCondition">
        <xsl:copy>
            <xsl:if test="./mms:condition">
                <xsl:apply-templates select="./mms:condition"></xsl:apply-templates>
            </xsl:if>
            <mms:equations>
                <xsl:apply-templates select="./mms:equation"></xsl:apply-templates>
            </mms:equations>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="mms:charDecomposition">
        <mms:charDecomposition>
            <xsl:if test=".//mms:generalCharDecomp">
                <mms:generalCharDecomp>
                    <mms:eigenSpaces>
                        <xsl:for-each select=".//mms:generalCharDecomp//mms:eigenSpace">
                            <mms:eigenSpace>
                                <xsl:copy-of select="./mms:eigenName"/>
                                <xsl:if test="./mms:order">
                                    <xsl:copy-of select="./mms:order"/>
                                </xsl:if>
                                <xsl:copy-of select="./mms:eigenValue"/>
                                <xsl:if test="./mms:eigenVector">
                                    <mms:eigenVectors>
                                        <xsl:for-each select="./mms:eigenVector">
                                            <mms:eigenVector>
                                                <xsl:copy-of select="./mms:vectorName"/>
                                                <mms:diagonalizationCoefficients>
                                                    <xsl:for-each select="./mms:diagCoefficient">
                                                        <mms:diagonalizationCoefficient>
                                                            <xsl:if test="./mms:indexNumber">
                                                                <xsl:copy-of select="./mms:indexNumber"/>
                                                            </xsl:if>
                                                            <xsl:if test="./mms:field">
                                                                <xsl:copy-of select="./mms:field"/>
                                                            </xsl:if>
                                                            <mms:mathML>
                                                                <xsl:copy-of select=".//mt:math"/>
                                                            </mms:mathML>
                                                        </mms:diagonalizationCoefficient>
                                                    </xsl:for-each>
                                                </mms:diagonalizationCoefficients>
                                            </mms:eigenVector>
                                        </xsl:for-each>
                                    </mms:eigenVectors>
                                </xsl:if>
                            </mms:eigenSpace>
                        </xsl:for-each>
                    </mms:eigenSpaces>
                    <xsl:if test="//mms:generalCharDecomp//mms:undiagonalization">
                        <mms:undiagonalizations>
                            <xsl:for-each select="//mms:generalCharDecomp//mms:undiagonalization">
                                <mms:undiagonalization>
                                    <xsl:copy-of select="./mms:field"/>
                                    <mms:undiagonalizationCoefficients>
                                        <xsl:for-each select="./mms:undiagCoefficient">
                                            <mms:undiagonalizationCoefficient>
                                                <xsl:if test="./mms:indexNumber">
                                                    <xsl:copy-of select="./mms:indexNumber"/>
                                                </xsl:if>
                                                <xsl:copy-of select="./mms:eigenVector"/>
                                                <xsl:copy-of select="./mms:mathML"/>
                                            </mms:undiagonalizationCoefficient>
                                        </xsl:for-each>
                                    </mms:undiagonalizationCoefficients>
                                </mms:undiagonalization>
                            </xsl:for-each>
                        </mms:undiagonalizations>
                    </xsl:if>
                </mms:generalCharDecomp>
            </xsl:if>
            <xsl:if test=".//mms:coordCharDecomp">
                <mms:coordCharDecomps>
                    <xsl:for-each select=".//mms:coordCharDecomp">
                        <mms:coordCharDecomp>
                            <mms:eigenSpaces>
                                <xsl:for-each select=".//mms:eigenSpace">
                                    <mms:eigenSpace>
                                        <xsl:copy-of select="./mms:eigenName"/>
                                        <xsl:if test="./mms:order">
                                            <xsl:copy-of select="./mms:order"/>
                                        </xsl:if>
                                        <xsl:copy-of select="./mms:eigenValue"/>
                                        <xsl:if test="./mms:eigenVector">
                                            <mms:eigenVectors>
                                                <xsl:for-each select="./mms:eigenVector">
                                                    <mms:eigenVector>
                                                        <xsl:copy-of select="./mms:vectorName"/>
                                                        <mms:diagonalizationCoefficients>
                                                            <xsl:for-each select="./mms:diagCoefficient">
                                                                <mms:diagonalizationCoefficient>
                                                                    <xsl:if test="./mms:indexNumber">
                                                                        <xsl:copy-of select="./mms:indexNumber"/>
                                                                    </xsl:if>
                                                                    <xsl:if test="./mms:field">
                                                                        <xsl:copy-of select="./mms:field"/>
                                                                    </xsl:if>
                                                                    <mms:mathML>
                                                                        <xsl:copy-of select=".//mt:math"/>
                                                                    </mms:mathML>
                                                                </mms:diagonalizationCoefficient>
                                                            </xsl:for-each>
                                                        </mms:diagonalizationCoefficients>
                                                    </mms:eigenVector>
                                                </xsl:for-each>
                                            </mms:eigenVectors>
                                        </xsl:if>
                                    </mms:eigenSpace>
                                </xsl:for-each>
                            </mms:eigenSpaces>
                            <xsl:if test=".//mms:undiagonalization">
                                <mms:undiagonalizations>
                                    <xsl:for-each select=".//mms:undiagonalization">
                                        <mms:undiagonalization>
                                            <xsl:copy-of select="./mms:field"/>
                                            <mms:undiagonalizationCoefficients>
                                                <xsl:for-each select="./mms:undiagCoefficient">
                                                    <mms:undiagonalizationCoefficient>
                                                        <xsl:if test="./mms:indexNumber">
                                                            <xsl:copy-of select="./mms:indexNumber"/>
                                                        </xsl:if>
                                                        <xsl:copy-of select="./mms:eigenVector"/>
                                                        <xsl:copy-of select="./mms:mathML"/>
                                                    </mms:undiagonalizationCoefficient>
                                                </xsl:for-each>
                                            </mms:undiagonalizationCoefficients>
                                        </mms:undiagonalization>
                                    </xsl:for-each>
                                </mms:undiagonalizations>
                            </xsl:if>
                            <xsl:copy-of select="./mms:eigenCoordinate"/>
                        </mms:coordCharDecomp>
                    </xsl:for-each>
                </mms:coordCharDecomps>
            </xsl:if>
            <xsl:if test=".//mms:maxCharacteristicSpeed">
                <xsl:copy-of select=".//mms:maxCharacteristicSpeed"/>
            </xsl:if>
            <xsl:if test=".//mms:auxiliarDefinition">
                <mms:auxiliaryDefinitions>
                    <xsl:for-each select=".//mms:auxiliarDefinition">
                        <mms:auxiliaryDefinition>
                            <mms:auxiliaryVar><xsl:value-of select="./mms:auxiliarVar"/></mms:auxiliaryVar>
                            <mms:mathML>
                                <xsl:copy-of select=".//mt:math"/>
                            </mms:mathML>
                        </mms:auxiliaryDefinition>
                    </xsl:for-each>
                </mms:auxiliaryDefinitions>
            </xsl:if>
        </mms:charDecomposition>
    </xsl:template>
    
    <xsl:template match="/mms:discretizedProblem">
        <mms:discretizedProblem xmlns:mms="urn:mathms"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="urn:mathms file:/home/bminano/Escritorio/iac3/simflowny/trunk/mathMS/common/XSD/discretizedProblem.xsd" xmlns:mt="http://www.w3.org/1998/Math/MathML">
            <mms:head>
                <mms:name><xsl:value-of select="//mms:head/mms:name"/></mms:name>
                <mms:id><xsl:value-of select="//mms:head/mms:id"/></mms:id>
                <mms:author><xsl:value-of select="//mms:head/mms:author"/></mms:author>
                <mms:version><xsl:value-of select="//mms:head/mms:version"/></mms:version>
                <mms:date><xsl:value-of select="//mms:head/mms:date"/></mms:date>
                <mms:description><xsl:value-of select="//mms:head/mms:description"/></mms:description>
                <mms:mesh><xsl:value-of select="//mms:head/mms:mesh"/></mms:mesh>
            </mms:head>
            <mms:coordinateSystem>
                <mms:spatialCoordinates>
                    <xsl:for-each select="//mms:baseSpatialCoordinateSystem/mms:spatialCoordinate">
                        <mms:spatialCoordinate><xsl:value-of select="."/></mms:spatialCoordinate>
                    </xsl:for-each>
                </mms:spatialCoordinates>
                <mms:timeCoordinate><xsl:value-of select="//mms:coordinateSystems/mms:timeCoordinate"/></mms:timeCoordinate>
            </mms:coordinateSystem>
            <mms:fields>
                <xsl:for-each select="/*/mms:field">
                    <mms:field><xsl:value-of select="."/></mms:field>
                </xsl:for-each>
            </mms:fields>
            <mms:auxiliaryFields>
                <xsl:for-each select="/*/mms:auxiliarField">
                    <mms:auxiliaryField><xsl:value-of select="."/></mms:auxiliaryField>
                </xsl:for-each>
            </mms:auxiliaryFields>
            <xsl:if test="/*/mms:tensor">
                <mms:tensors>
                    <xsl:for-each select="/*/mms:tensor">
                        <mms:tensor>
                            <mms:name><xsl:value-of select="./mms:name"/></mms:name>
                            <mms:order><xsl:value-of select="./mms:order"/></mms:order>
                            <mms:fields>
                                <xsl:for-each select="./mms:field">
                                    <xsl:copy-of select="."/>
                                </xsl:for-each>
                            </mms:fields>
                        </mms:tensor>
                    </xsl:for-each>
                </mms:tensors>
            </xsl:if>
            <mms:parameters>
                <xsl:for-each select="/*/mms:parameter">
                    <xsl:copy-of select="."/>
                </xsl:for-each>
            </mms:parameters>
            <mms:implicitParameters>
                <xsl:for-each select="/*/mms:implicitParameter">
                    <xsl:copy-of select="."/>
                </xsl:for-each>
            </mms:implicitParameters>
            <xsl:if test="/*/mms:vectorName">
                <mms:vectorNames>
                    <xsl:apply-templates select="/*/mms:vectorName"/>
                </mms:vectorNames>
            </xsl:if>
            <xsl:if test="/*/mms:modelCharDecomposition">
                <mms:modelCharDecompositions>
                    <xsl:apply-templates select="/*/mms:modelCharDecomposition"/>
                </mms:modelCharDecompositions>
            </xsl:if>
            <xsl:apply-templates select="/*/mms:problemDomain"/>
            <mms:segmentGroups>
                <xsl:for-each select="/*/mms:segmentGroups/mms:segmentGroup">
                    <xsl:copy>
                        <xsl:copy-of select="./mms:segmentGroupName"/>
                        <xsl:apply-templates select="./mms:interior"/>
                        <xsl:apply-templates select="./mms:surface"/>
                        <xsl:copy-of select="./mms:segments"/>
                        <xsl:apply-templates select="./mms:segmentInteractions"/>
                        <mms:initialConditions>
                            <xsl:for-each select="./mms:initialConditions/mms:initialCondition">
                                <xsl:copy-of select="."></xsl:copy-of>
                            </xsl:for-each>
                        </mms:initialConditions>
                        <xsl:if test="./mms:initialConditions/mms:postInitialCondition">
                            <mms:postInitialConditions>
                                <xsl:for-each select="./mms:initialConditions/mms:postInitialCondition">
                                    <mms:postInitialCondition>
                                        <mms:segmentType><xsl:value-of select=" local-name(.//mms:instructionSet/*/*)"/></mms:segmentType>
                                        <mms:instructionSet>
                                            <sml:simml>
                                                <xsl:apply-templates select=".//mms:instructionSet/*/*"></xsl:apply-templates>
                                            </sml:simml>
                                        </mms:instructionSet>
                                    </mms:postInitialCondition>
                                </xsl:for-each>
                            </mms:postInitialConditions>
                        </xsl:if>
                        <xsl:apply-templates select="./mms:movement"/>
                        <xsl:apply-templates select="./mms:interiorExecutionFlow"/>
                        <xsl:apply-templates select="./mms:surfaceExecutionFlow"/>
                        <xsl:copy-of select="./mms:discretizationInfo"/>
                    </xsl:copy>
                </xsl:for-each>
            </mms:segmentGroups>
            <xsl:copy-of select="/*/mms:segmentsPrecedence"/>
            <xsl:apply-templates select="/*/mms:boundaries"/>
            <xsl:copy-of select="/*/mms:boundariesPrecedence"/>
            <xsl:apply-templates select="/*/mms:finalizationConditions"/>
            <xsl:apply-templates select="/*/mms:functions"/>
            <xsl:apply-templates select="/*/mms:extrapolationMethod"/>
            <xsl:copy-of select="/*/mms:segmentGroupRelations"/>
        </mms:discretizedProblem>
    </xsl:template>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>
