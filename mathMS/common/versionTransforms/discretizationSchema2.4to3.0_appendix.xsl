<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:mt="http://www.w3.org/1998/Math/MathML" xmlns:mms="urn:mathms"  xmlns:sml="urn:simml">
    
    <xsl:template match="mt:apply[mt:ci[@type='flux']]">
        <sml:flux>
        <mt:ci>
            <xsl:value-of select="./*[position() = 1]/text()"/><xsl:apply-templates select="./*[position() = 1]/*"/>
        </mt:ci>
            <xsl:for-each select="./*[position() > 1]">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </sml:flux>
    </xsl:template>
    
    <xsl:template match="mt:ci[@type='shared']">
        <sml:sharedVariable>
            <mt:ci>
                <xsl:value-of select="./text()"/><xsl:apply-templates select="./*"/>
            </mt:ci>
        </sml:sharedVariable>
    </xsl:template>
    
    <xsl:template match="mt:apply[@sml:type='function']">
        <sml:functionCall>
                <xsl:apply-templates select="./*"/>
        </sml:functionCall>
    </xsl:template>
        
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
   
</xsl:stylesheet>
