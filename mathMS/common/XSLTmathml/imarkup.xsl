<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="urn:mathms" xmlns:fn="http://www.w3.org/2005/02/xpath-functions" version="1.0">

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->
	<xsl:template match="if">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>if (</xsl:text>
		<!-- Conditional block -->
		<xsl:for-each select="*[not(local-name()='then') and not(local-name()='else')]">
			<xsl:if test="position() &gt; 1">
				<xsl:text> .and. </xsl:text>
			</xsl:if>
			<xsl:if test="count(*[not(name(node())='then')]) &gt; 1">
				<xsl:text>(</xsl:text>
			</xsl:if>
			<xsl:apply-templates select=".">
				<xsl:with-param name="condition" select="'true'"/>
			</xsl:apply-templates>
			<xsl:if test="count(*[not(name(node())='then')]) &gt; 1">
				<xsl:text>)</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<!-- then -->
		<xsl:text>) then</xsl:text>
		<xsl:for-each select="then/*">
			<xsl:text>
</xsl:text>
			<xsl:apply-templates select=".">
			    <xsl:with-param name="indent" select="$indent + 1"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
		</xsl:for-each>
		<!-- else -->
		<xsl:if test="./else">
			<xsl:text>
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>else</xsl:text>
			<xsl:for-each select="else/*">
				<xsl:text>
</xsl:text>
				<xsl:apply-templates select=".">
				    <xsl:with-param name="indent" select="$indent + 1"/>
					<xsl:with-param name="condition" select="'false'"/>
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:if>
		<xsl:text>
</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>end if</xsl:text>
	</xsl:template>
	
	<xsl:template match="while">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>do while (</xsl:text>
		<!-- Conditional block -->
		<xsl:for-each select="*[not(local-name()='loop')]">
			<xsl:if test="position() &gt; 1">
				<xsl:text> .and. </xsl:text>
			</xsl:if>
			<xsl:if test="count(*[not(name(node())='then')]) &gt; 1">
				<xsl:text>(</xsl:text>
			</xsl:if>
			<xsl:apply-templates select=".">
				<xsl:with-param name="condition" select="'true'"/>
			</xsl:apply-templates>
			<xsl:if test="count(*[not(name(node())='then')]) &gt; 1">
				<xsl:text>)</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<!-- loop -->
		<xsl:text>)</xsl:text>
		<xsl:for-each select="loop/*">
			<xsl:text>
</xsl:text>
			<xsl:apply-templates select=".">
			    <xsl:with-param name="indent" select="$indent + 1"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
		</xsl:for-each>
		<xsl:text>
</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>end do</xsl:text>
	</xsl:template>

    <xsl:template match="for">
        <xsl:param name="indent" select="$indent"/>
        <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>
        <xsl:text>do </xsl:text>
        <!-- condition -->
        <xsl:apply-templates select="variable/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text> = </xsl:text>
        <xsl:apply-templates select="start/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text>, </xsl:text>
        <xsl:apply-templates select="end/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <!-- loop -->
        <xsl:for-each select="loop/*">
            <xsl:text>
</xsl:text>
            <xsl:apply-templates select=".">
                <xsl:with-param name="indent" select="$indent + 1"/>
                <xsl:with-param name="condition" select="'false'"/>
            </xsl:apply-templates>
        </xsl:for-each>
        <xsl:text>
</xsl:text>
        <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>
        <xsl:text>end do</xsl:text>
    </xsl:template>

	<xsl:template match="exit">
	    <xsl:param name="indent" select="$indent"/>
	    <xsl:call-template name="recIndent">
	        <xsl:with-param name="indent" select="$indent"/>
	    </xsl:call-template>
		<xsl:text>call CCTK_Exit(istat, cctkGH, 0)</xsl:text>
	</xsl:template>
	
    <xsl:template match="return">
        <xsl:param name="indent" select="$indent"/>
        <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>
        <xsl:text>#function# = </xsl:text>
        <xsl:apply-templates select="./*">
            <xsl:with-param name="indent" select="0"/>
        </xsl:apply-templates>
    </xsl:template>
	
	<xsl:template match="moveParticles">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>moveParticles(CCTK_PASS_FTOF)</xsl:text>
	</xsl:template>
	
	<xsl:template name="recIndent">
		<xsl:param name="indent"/>
		<xsl:call-template name="recIndent2">
			<xsl:with-param name="indent" select="$indent"/>
			<xsl:with-param name="initial" select="$indent"/>
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="recIndent2">
		<xsl:param name="indent"/>
		<xsl:param name="initial"/>
		<xsl:if test="$indent &gt; 0 and ($initial - $indent) &lt; 5">
			<xsl:text>&#x9;</xsl:text>
			<xsl:call-template name="recIndent2">
				<xsl:with-param name="indent" select="$indent - 1"/>
				<xsl:with-param name="initial" select="$initial"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	

</xsl:stylesheet>
