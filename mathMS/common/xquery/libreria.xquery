module namespace ds = "http://ds.iac3.eu/";
declare namespace mms = "urn:mathms";
declare namespace mt = "http://www.w3.org/1998/Math/MathML";


declare function ds:functionParameter ( $names as xs:anyAtomicType*, $types as xs:anyAtomicType*)  as element()* {
       for $name at $pos in $names
       return if ($name ne '')
	then 
	<mms:functionParameter>
         	<mms:name>{$name}</mms:name>
                <mms:type>{$types[$pos]}</mms:type>
              </mms:functionParameter>
	else	
	<mms:functionParameter>
                <mms:type>{$types[$pos]}</mms:type>
              </mms:functionParameter>
 };

declare function ds:functionParameters ( $names as xs:anyAtomicType*, $types as xs:anyAtomicType*)  as element() {

       let $funcParam := ds:functionParameter($names, $types) 
       let $functionParameters := element {"functionParameters"} {$funcParam}
       return $functionParameters
 };
 
 declare function ds:returnRule ( $return as element())  as element() {
       <mms:transformationRule>
          <mms:instructionSet>
            <return>
               {$return}
            </return>
          </mms:instructionSet>
       </mms:transformationRule>
 };
 
 
 
 declare function ds:import ( $rule as xs:string, $name as xs:string, $elems as element()*, $params as xs:string*)  as element() {
    <import>
       <rule>{$rule}</rule>
       <name>{$name}{$elems}</name>
       {
         for $param in $params
         return
         <param>{$param}</param>
       }
    </import>
 };
 
  declare function ds:timestep ($instructions as element()*, $stencil as xs:integer, $append as xs:integer)  as element() {
    <timeStep stencil=”{$stencil}” append=”{$append}”>
	   {$instructions}
    </timeStep>
 };
 
  declare function ds:fieldGroup ($instructions as element()*, $name as xs:string)  as element() {
    <fieldGroup name=”{$name}”>
	   {$instructions}
    </fieldGroup>
 };
 
  declare function ds:interiorDomainContext ($instructions as element()*)  as element() {
    <interiorDomainContext>
	   {$instructions}
    </interiorDomainContext>
 };

   declare function ds:boundary ($variable as element(), $previousVariable as element(), $stencil as xs:integer, $type as xs:string)  as element() {
    <boundary>
	{if ($type ne '') then attribute type {$type} else ()}
	   <variable>{for $x in $variable return if (fn:local-name($x)="math") then $x/child::* else $x}</variable>
	   <previousVariable>{for $x in $previousVariable return if (fn:local-name($x)="math") then $x/child::* else $x}</previousVariable>
	   <stencil>{$stencil}</stencil>
    </boundary>
 };
 
    declare function ds:auxiliaryEquations ($timeSlice as element())  as element() {
    <auxiliaryEquations>
    {for $x in $timeSlice return if (fn:local-name($x)="math") then $x/child::* else $x}                  
    </auxiliaryEquations>
 };
 
    declare function ds:checkFinalization ()  as element() {
         <checkFinalization/>
 };

   declare function ds:constraints ($timeSlice as element())  as element() {
    <constraints>
    {for $x in $timeSlice return if (fn:local-name($x)="math") then $x/child::* else $x}
    </constraints>
 };
 
    declare function ds:segmentInteraction ($variable as element(), $timeSlice as element())  as element() {
    <segmentInteraction>
       <variable>{for $x in $variable return if (fn:local-name($x)="math") then $x/child::* else $x}</variable>
       <timeSlice>{for $x in $timeSlice return if (fn:local-name($x)="math") then $x/child::* else $x}</timeSlice>
    </segmentInteraction>
 };

    declare function ds:segmentInteraction ($influenceRadius as element(), $preCalculation as element()*, $distanceVariable as element(), $positionAverageCalculation as element(), $variable as element(), $timeSlice as element())  as element() {
    <segmentInteraction>
       <influenceRadius>
           {$influenceRadius}
       </influenceRadius>
       <preCalculation>
  		    {$preCalculation}
       </preCalculation>
       <distanceVariable>
  		  {for $x in $distanceVariable return if (fn:local-name($x)="math") then $x/child::* else $x}
       </distanceVariable>
       <positionAverageCalculation>
  		  {for $x in $positionAverageCalculation return if (fn:local-name($x)="math") then $x/child::* else $x}
       </positionAverageCalculation>
       <variable>
          {for $x in $variable return if (fn:local-name($x)="math") then $x/child::* else $x}
       </variable>
       <timeSlice>
  		  {for $x in $timeSlice return if (fn:local-name($x)="math") then $x/child::* else $x}
       </timeSlice>
    </segmentInteraction>
 };
 
    declare function ds:choose ($paramName as xs:string, $whenList as xs:string*, $insPerThen as xs:integer*, $instructions as element()*)  as element() {
     <choose>
	    <param>{$paramName}</param>
	    {
	      for $when at $pos in $whenList
	      return 
	      (<when>{$when}</when>,
	      <then>
	      { let $maxant := if ($pos eq 1) then 0 else $insPerThen[$pos -1]
	        let $max := $maxant + $insPerThen[$pos]
	        let $min := $maxant + 1
	        for $then in $min to $max
	        return $instructions[$then]
	      }
	      </then>)
	    }
    </choose>
 };
 
    declare function ds:schemaParameter ($name as xs:string, $paramName as xs:string, $valueList as xs:string*, $values as element()*)  as element() {
         <mms:schemaParameter>
            <mms:name>{$name}</mms:name>
            <mms:paramName>{$paramName}</mms:paramName>
            {
              for $value at $pos in $valueList
              return
              <mms:value>
                <mms:valueName>{$value}</mms:valueName>
                {$values[$pos]}
              </mms:value>
            }
        </mms:schemaParameter>
 };
 
   declare function ds:schema ($insSet as element())  as element() {
        <mms:schema>
        <mms:executionFlow>
            {$insSet}
        </mms:executionFlow>
        </mms:schema>
  };
  
  declare function ds:schema ($insSet as element(), $insSetPost as element()?)  as element() {
        <mms:schema>
        <mms:executionFlow>
            {$insSet}
        </mms:executionFlow>
        {if (string($insSetPost) ne '')
        then
        <mms:postInitialization>
            {$insSetPost}
        </mms:postInitialization>
        else ()}                
        </mms:schema>
  };
  
     declare function ds:schema ($insSet as element(), $fieldGroups as xs:string*, $insSetPost as element()?)  as element() {
        <mms:schema>
        <mms:fieldGroup>
        {
           for $value at $pos in $fieldGroups
           return
              <mms:groupName>{$value}</mms:groupName>
        }
        </mms:fieldGroup>
        <mms:executionFlow>
            {$insSet}
        </mms:executionFlow>
        {if (string($insSetPost) ne '')
        then
        <mms:postInitialization>
            {$insSetPost}
        </mms:postInitialization>
        else ()} 
        </mms:schema>
  };
  
 
  
   declare function ds:if ($fieldVal as xs:string?, $coordinateVal as xs:string?, $condition as element(), $then as element()*, $else as element()*)  as element() {
        <if>
         {if ($fieldVal ne '') then attribute field {$fieldVal} else ()}
         {if ($coordinateVal ne '') then attribute coordinate {$coordinateVal} else ()}
	     {$condition}
	     <then>
		    {$then}
	     </then>
	     {if (not($else/node())) then
          ()
	     else
	      <else>
		    {$else}
	     </else>}
         </if>
    };
    
    
   declare function ds:while ($fieldVal as xs:string?, $coordinateVal as xs:string?, $condition as element(), $loop as element()*)  as element() {
        <while>
         {if ($fieldVal ne '') then attribute field {$fieldVal} else ()}
         {if ($coordinateVal ne '') then attribute coordinate {$coordinateVal} else ()}
	     {$condition}
	     <loop>
		    {$loop}
	     </loop>	     
         </while>
    };        
    
     declare function ds:return ( $return as element())  as element() {
            <return>
               {$return}
            </return>
 };
 
      declare function ds:param ( $value as xs:string)  as element() {
            <param>
               {$value}
            </param>
 };
 
    declare function ds:diagonalize ($variable as element(),$timeSlice as element())  as element() {
	 <diagonalize>
	   <variable>{for $x in $variable return if (fn:local-name($x)="math") then $x/child::* else $x}</variable>
	   <timeSlice>{for $x in $timeSlice return if (fn:local-name($x)="math") then $x/child::* else $x}</timeSlice>
	 </diagonalize>
    };
    declare function ds:undiagonalize ($variable as element(),$timeSlice as element())  as element() {
	 <undiagonalize>
       <variable>{for $x in $variable return if (fn:local-name($x)="math") then $x/child::* else $x}</variable>
       <timeSlice>{for $x in $timeSlice return if (fn:local-name($x)="math") then $x/child::* else $x}</timeSlice>
    </undiagonalize>
    };
    
    declare function ds:coordDiscretizationRule ($rule as xs:string, $params as xs:string*)  as element() {
         <mms:coordDiscretizationRule>
            <mms:rule>{$rule}</mms:rule>
            {$params}
        </mms:coordDiscretizationRule>
    };

    declare function ds:genericFieldContext ($instructions as element()*)  as element() {
        <genericFieldContext>
	     {$instructions}
         </genericFieldContext>
    };

    declare function ds:specialFieldContext ($instructions as element()*)  as element() {
        <specialFieldContext>
	     {$instructions}
         </specialFieldContext>
    };
    
    declare function ds:sumOverInfluenceRadius ($instruction as element(), $instructions as element()*)  as element() {
        <sumOverInfluenceRadius>
	       <influenceRadius>
	           {$instruction}
	       </influenceRadius>
	       {$instructions}
        </sumOverInfluenceRadius>
    };
    
    declare function ds:moveParticles ($newPos as element(), $prevPos as element())  as element() {
        <moveParticles>
	       <newPosition>{for $x in $newPos return if (fn:local-name($x)="math") then $x/child::* else $x}</newPosition>
	       <previousPosition>{for $x in $prevPos return if (fn:local-name($x)="math") then $x/child::* else $x}</previousPosition>
        </moveParticles>
    };
    
    
    declare function ds:parabolicTermsContext ($instructions as element()*)  as element() {
        <parabolicTermsContext>
	     {$instructions}
         </parabolicTermsContext>
    };
    
    declare function ds:omnidirectionalContext ($instructions as element()*)  as element() {
        <omnidirectionalContext>
	     {$instructions}
         </omnidirectionalContext>
    };

    declare function ds:doubleDerivative ($sameCoords as element()*, $diffCoords as element()*)  as element() {
        <doubleDerivative>
            <sameCoordinates>
                {$sameCoords}
            </sameCoordinates>
            <differentCoordinates>
                {$diffCoords}
            </differentCoordinates>
        </doubleDerivative>
    };
