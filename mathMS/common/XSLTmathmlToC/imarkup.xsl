<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sml="urn:simml" xmlns:fn="http://www.w3.org/2005/02/xpath-functions" 
	xmlns:m="http://www.w3.org/1998/Math/MathML" version="2.0">

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->
    
    
    <xsl:template match="sml:iterateOverCells">
        <xsl:apply-templates select="./*"></xsl:apply-templates>
    </xsl:template>
    
	<xsl:template match="sml:minDomainValue">
		<xsl:text>d_ghost_width</xsl:text>
	</xsl:template>
   
   <xsl:template match="sml:timeSubstepNumber">
      <xsl:text>time_substep_number</xsl:text>
   </xsl:template>
    
   <xsl:template match="sml:throwError">
      <xsl:param name="indent" select="$indent"/>
      <xsl:call-template name="recIndent">
         <xsl:with-param name="indent" select="$indent"/>
      </xsl:call-template>
      <xsl:text>TBOX_ERROR("</xsl:text><xsl:value-of select="./text()"/><xsl:text>");
</xsl:text>
   </xsl:template>

   <xsl:template match="sml:comment">
      <xsl:param name="indent" select="$indent"/>
      <xsl:call-template name="recIndent">
         <xsl:with-param name="indent" select="$indent"/>
      </xsl:call-template>
      <xsl:text>//</xsl:text><xsl:value-of select="."/><xsl:text>
</xsl:text>
   </xsl:template>

   <xsl:template match="sml:timeSubstepNumber">
      <xsl:text>time_substep_number</xsl:text>
   </xsl:template>
    
	<xsl:template match="sml:maxDomainValue">
		<xsl:value-of select="./*[1]"/><xsl:text>last - d_ghost_width</xsl:text>
	</xsl:template>
    
   <xsl:template match="sml:*[ matches(local-name(), 'findCoordFromFile[1-3]D_[0-9]+')]|sml:*[ matches(local-name(), 'readFromFileQuintic')]|sml:*[ matches(local-name(), 'readFromFileLinear[1-3]D')]|sml:*[ matches(local-name(), 'readFromFileLinearWithDerivatives[1-3]D')]">
      <xsl:value-of select="local-name()"></xsl:value-of><xsl:text>(</xsl:text><xsl:for-each select="./*"><xsl:apply-templates select="."><xsl:with-param name="condition" select="'false'"/></xsl:apply-templates><xsl:choose><xsl:when test="position() &lt; last()"><xsl:text>, </xsl:text></xsl:when></xsl:choose></xsl:for-each><xsl:text>)</xsl:text>
    </xsl:template>

   <xsl:template match="sml:particlePosition">
      <xsl:text>position</xsl:text><xsl:value-of select="."/>
   </xsl:template>
   
   <xsl:template match="sml:dimensions">
      <xsl:value-of select="$dimensions"/>
   </xsl:template>
   
   <xsl:template match="sml:sign">
      <xsl:text>SIGN(</xsl:text>	
      <xsl:apply-templates select="./*">
         <xsl:with-param name="condition" select="'false'"/>
      </xsl:apply-templates>
      <xsl:text>)</xsl:text>
   </xsl:template>
    
	<xsl:template match="sml:randomNumber">
		<xsl:if test="./@typeAtt = 'int'">
			<xsl:text>int(gsl_rng_uniform(r_var) * (</xsl:text>
			<xsl:value-of select="./@rangeMax"/>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> - </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
			<xsl:text> + 1)</xsl:text>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> + </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
			<xsl:text>)</xsl:text>
		</xsl:if>
		<xsl:if test="./@typeAtt = 'real'">
			<xsl:text>gsl_rng_uniform(r_var) * (</xsl:text>
			<xsl:value-of select="./@rangeMaxAtt"/>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> - </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
			<xsl:text>)</xsl:text>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> + </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="./@typeAtt = 'uniform'">
			<xsl:text>gsl_rng_uniform(r_var)</xsl:text>
		</xsl:if>
	</xsl:template>

	<xsl:template match="sml:if">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>if (</xsl:text>
		<!-- Conditional block -->
		<xsl:for-each select="*[not(local-name()='then') and not(local-name()='else')]">
			<xsl:if test="position() &gt; 1">
				<xsl:text> &amp;&amp; </xsl:text>
			</xsl:if>
			<xsl:if test="count(*) &gt; 1">
				<xsl:text>(</xsl:text>
			</xsl:if>
			<xsl:apply-templates select=".">
				<xsl:with-param name="condition" select="'true'"/>
			</xsl:apply-templates>
			<xsl:if test="count(*) &gt; 1">
				<xsl:text>)</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<!-- then -->
		<xsl:text>) {
</xsl:text>
		<xsl:for-each select="sml:then/*">
			<xsl:apply-templates select=".">
			    <xsl:with-param name="indent" select="$indent + 1"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
		</xsl:for-each>
		<!-- else -->
		<xsl:if test="./sml:else">
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>} else {
</xsl:text>
			<xsl:for-each select="sml:else/*">
				<xsl:apply-templates select=".">
				    <xsl:with-param name="indent" select="$indent + 1"/>
					<xsl:with-param name="condition" select="'false'"/>
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:if>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
	</xsl:template>
	
	<xsl:template match="sml:while">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>while (</xsl:text>
		<!-- Conditional block -->
		<xsl:for-each select="*[not(local-name()='loop')]">
			<xsl:if test="position() &gt; 1">
				<xsl:text> &amp;&amp; </xsl:text>
			</xsl:if>
			<xsl:if test="count(*) &gt; 1">
				<xsl:text>(</xsl:text>
			</xsl:if>
			<xsl:apply-templates select=".">
				<xsl:with-param name="condition" select="'true'"/>
			</xsl:apply-templates>
			<xsl:if test="count(*) &gt; 1">
				<xsl:text>)</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<!-- loop -->
		<xsl:text>) {
</xsl:text>
		<xsl:for-each select="sml:loop/*">
			<xsl:apply-templates select=".">
			    <xsl:with-param name="indent" select="$indent + 1"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
		</xsl:for-each>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
	</xsl:template>

	<xsl:template match="sml:for">
        <xsl:param name="indent" select="$indent"/>
        <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>
        <xsl:text>for (</xsl:text>
        <!-- condition -->
        <xsl:apply-templates select="variable/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text> = </xsl:text>
        <xsl:apply-templates select="start/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text>; </xsl:text>
        <xsl:apply-templates select="variable/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text> &lt; </xsl:text>
        <xsl:apply-templates select="end/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text>; </xsl:text>
        <xsl:apply-templates select="variable/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text>++</xsl:text>
        <!-- loop -->
        <xsl:text>) {
</xsl:text>
		<xsl:for-each select="sml:loop/*">
            <xsl:apply-templates select=".">
                <xsl:with-param name="indent" select="$indent + 1"/>
                <xsl:with-param name="condition" select="'false'"/>
            </xsl:apply-templates>
        </xsl:for-each>
        <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>
        <xsl:text>}
</xsl:text>
	</xsl:template>
   
   <xsl:template match="sml:influenceRadius">
      <xsl:text>level_influenceRadius</xsl:text>
   </xsl:template>
   
	
	<xsl:template match="sml:sharedVariable">
		<xsl:apply-templates select="./*"/>
	</xsl:template>
	
	<xsl:template match="sml:return">
        <xsl:param name="indent" select="$indent"/>
        <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>
        <xsl:text>return </xsl:text>
        <xsl:apply-templates select="./*">
            <xsl:with-param name="indent" select="0"/>
        </xsl:apply-templates>
	   <!-- Empty return -->
	   <xsl:if test="count(./*) = 0">
	      <xsl:text>;
</xsl:text> 
	   </xsl:if>
    </xsl:template>
	
   <xsl:template match="sml:iterateOverParticles">
      <xsl:param name="indent" select="$indent"/>
      <xsl:param name="dimensions" select="$dimensions"/>
      <xsl:param name="condition" select="$condition"/>
      <xsl:variable name="species" select="./@speciesNameAtt"/>
      <xsl:if test="$dimensions = 2">
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
      <xsl:text>if (i + 1 &lt; ilast &amp;&amp; j + 1 &lt; jlast) {  
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent + 1"/>
         </xsl:call-template>
         <xsl:text>hier::Index idx(i + boxfirstP(0), j + boxfirstP(1));
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent + 1"/>
         </xsl:call-template>
         <xsl:text>Particles&lt;Particle_</xsl:text><xsl:value-of select="$species"/><xsl:text>&gt;* part_</xsl:text><xsl:value-of select="$species"/><xsl:text> = particleVariables_</xsl:text><xsl:value-of select="$species"/><xsl:text>->getItem(idx);
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent + 1"/>
         </xsl:call-template>
         <xsl:text>for (int pit = part_</xsl:text><xsl:value-of select="$species"/><xsl:text>->getNumberOfParticles() - 1; pit >= 0; pit--) {
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent + 2"/>
         </xsl:call-template>   
         <xsl:text>Particle_</xsl:text><xsl:value-of select="$species"/><xsl:text>* particle = part_</xsl:text><xsl:value-of select="$species"/><xsl:text>->getParticle(pit);
</xsl:text>
      <xsl:for-each select="./*">
         <xsl:apply-templates select=".">
            <xsl:with-param name="indent" select="$indent + 2"/>
            <xsl:with-param name="condition" select="'false'"/>
         </xsl:apply-templates>
      </xsl:for-each>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent + 1"/>
         </xsl:call-template>
         <xsl:text>}  
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>}  
</xsl:text>
      </xsl:if>
      <xsl:if test="$dimensions = 3">
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>if (i + 1 &lt; ilast &amp;&amp; j + 1 &lt; jlast &amp;&amp; k + 1 &lt; klast) {  
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent + 1"/>
         </xsl:call-template>
         <xsl:text>hier::Index idx(i + boxfirstP(0), j + boxfirstP(1), k + boxfirstP(2));
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent + 1"/>
         </xsl:call-template>
         <xsl:text>Particles&lt;Particle_</xsl:text><xsl:value-of select="$species"/><xsl:text>&gt;* part_</xsl:text><xsl:value-of select="$species"/><xsl:text> = particleVariables_</xsl:text><xsl:value-of select="$species"/><xsl:text>->getItem(idx);
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent + 1"/>
         </xsl:call-template>
         <xsl:text>for (int pit = part_</xsl:text><xsl:value-of select="$species"/><xsl:text>->getNumberOfParticles() - 1; pit >= 0; pit--) {
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent + 2"/>
         </xsl:call-template>   
         <xsl:text>Particle_</xsl:text><xsl:value-of select="$species"/><xsl:text>* particle = part_</xsl:text><xsl:value-of select="$species"/><xsl:text>->getParticle(pit);
</xsl:text>
         <xsl:for-each select="./*">
            <xsl:apply-templates select=".">
               <xsl:with-param name="indent" select="$indent + 2"/>
               <xsl:with-param name="condition" select="'false'"/>
            </xsl:apply-templates>
         </xsl:for-each>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent + 1"/>
         </xsl:call-template>
         <xsl:text>}  
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>}  
</xsl:text>
      </xsl:if>
   </xsl:template>
   
	<xsl:template match="sml:iterateOverInteractions">
		<xsl:param name="indent" select="$indent"/>
		<xsl:param name="dimensions" select="$dimensions"/>
		<xsl:param name="condition" select="$condition"/>
		<xsl:param name="region" select="$region"/>
	   <xsl:variable name="species" select="./@speciesNameAtt"/>
	   <xsl:variable name="position">
	      <xsl:apply-templates select="./sml:positionReference/*[1]">
	      <xsl:with-param name="indent" select="0"/>
	      <xsl:with-param name="condition" select="'true'"/>
	   </xsl:apply-templates>
	   </xsl:variable> 
	   <xsl:variable name="distance">
	      <xsl:call-template name="string-remove-all">
	         <xsl:with-param name="text" select="$position" />
	         <xsl:with-param name="replace" select='"position"'/>
	      </xsl:call-template>
	   </xsl:variable> 
	   
	   <xsl:variable name="posx">
	      <xsl:apply-templates select="./sml:positionReference/*[2]">
	         <xsl:with-param name="indent" select="0"/>
	         <xsl:with-param name="condition" select="'true'"/>
	      </xsl:apply-templates>
	   </xsl:variable> 
	   <xsl:variable name="posy">
	      <xsl:apply-templates select="./sml:positionReference/*[3]">
	         <xsl:with-param name="indent" select="0"/>
	         <xsl:with-param name="condition" select="'true'"/>
	      </xsl:apply-templates>
	   </xsl:variable> 
	   
		<xsl:if test="$dimensions = 2">
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
		   <xsl:text>miniIndex = MAX(0, MAX((int) floor((</xsl:text><xsl:value-of select="$posx"/><xsl:text> - </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i - ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[0])));
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
		   <xsl:text>maxiIndex = MIN(boxlast(0) - boxfirst(0) + 2 * d_ghost_width, MIN((int) floor((</xsl:text><xsl:value-of select="$posx"/><xsl:text> + </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i + ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[0])));
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
		   <xsl:text>minjIndex = MAX(0, MAX((int) floor((</xsl:text><xsl:value-of select="$posy"/><xsl:text> - </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j - ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[1])));
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
		   <xsl:text>maxjIndex = MIN(boxlast(1) - boxfirst(1) + 2 * d_ghost_width, MIN((int) floor((</xsl:text><xsl:value-of select="$posy"/><xsl:text> + </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j + ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[1])));
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>for(int jb = minjIndex; jb &lt;= maxjIndex; jb++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+1"/>
			</xsl:call-template>
			<xsl:text>for(int ib = miniIndex; ib &lt;= maxiIndex; ib++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
		   <xsl:text>hier::Index idxb(ib + boxfirstP(0), jb + boxfirstP(1));
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
		   <xsl:text>Particles&lt;Particle_</xsl:text><xsl:value-of select="$species"/><xsl:text>&gt;* partb_</xsl:text><xsl:value-of select="$species"/><xsl:text> = particleVariables_</xsl:text><xsl:value-of select="$species"/><xsl:text>->getItem(idxb);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
		   <xsl:text>for (int pitb = 0; pitb &lt; partb_</xsl:text><xsl:value-of select="$species"/><xsl:text>-&gt;getNumberOfParticles(); pitb++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
		   <xsl:text>Particle_</xsl:text><xsl:value-of select="$species"/><xsl:text>* particleb = partb_</xsl:text><xsl:value-of select="$species"/><xsl:text>->getParticle(pitb);
</xsl:text>
		   <xsl:call-template name="recIndent">
		      <xsl:with-param name="indent" select="$indent+3"/>
		   </xsl:call-template>
		   <xsl:text>double particle_distance = particle->distance</xsl:text><xsl:value-of select="$distance"/><xsl:text>(particleb);
</xsl:text>		   
		   <xsl:call-template name="recIndent">
		      <xsl:with-param name="indent" select="$indent+3"/>
		   </xsl:call-template>
		   <xsl:text>if (particle_distance &lt; </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius) {
</xsl:text>		
		   <xsl:for-each select="./*[position() &gt; 1]">
					<xsl:apply-templates select=".">
						<xsl:with-param name="indent" select="$indent + 4"/>
						<xsl:with-param name="condition" select="'false'"/>
					</xsl:apply-templates>
		   </xsl:for-each>
		   <xsl:call-template name="recIndent">
		      <xsl:with-param name="indent" select="$indent+3"/>
		   </xsl:call-template>
		   <xsl:text>}
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>}				
</xsl:text>		
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+1"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
		</xsl:if>
		<xsl:if test="$dimensions = 3">
		   <xsl:variable name="posz">
		      <xsl:apply-templates select="./sml:positionReference/*[4]">
		         <xsl:with-param name="indent" select="0"/>
		         <xsl:with-param name="condition" select="'true'"/>
		      </xsl:apply-templates>
		   </xsl:variable> 
		   <xsl:call-template name="recIndent">
		      <xsl:with-param name="indent" select="$indent"/>
		   </xsl:call-template>
		   <xsl:text>miniIndex = MAX(0, MAX((int) floor((</xsl:text><xsl:value-of select="$posx"/><xsl:text> - </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i - ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[0])));
</xsl:text>
		   <xsl:call-template name="recIndent">
		      <xsl:with-param name="indent" select="$indent"/>
		   </xsl:call-template>
		   <xsl:text>maxiIndex = MIN(boxlast(0) - boxfirst(0) + 2 * d_ghost_width, MIN((int) floor((</xsl:text><xsl:value-of select="$posx"/><xsl:text> + </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i + ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[0])));
</xsl:text>
		   <xsl:call-template name="recIndent">
		      <xsl:with-param name="indent" select="$indent"/>
		   </xsl:call-template>
		   <xsl:text>minjIndex = MAX(0, MAX((int) floor((</xsl:text><xsl:value-of select="$posy"/><xsl:text> - </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j - ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[1])));
</xsl:text>
		   <xsl:call-template name="recIndent">
		      <xsl:with-param name="indent" select="$indent"/>
		   </xsl:call-template>
		   <xsl:text>maxjIndex = MIN(boxlast(1) - boxfirst(1) + 2 * d_ghost_width, MIN((int) floor((</xsl:text><xsl:value-of select="$posy"/><xsl:text> + </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j + ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[1])));
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
		   <xsl:text>minkIndex = MAX(0, MAX((int) floor((</xsl:text><xsl:value-of select="$posz"/><xsl:text> - </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[2])/dx[2]) - boxfirst(2) + d_ghost_width, (int) k - ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[2])));
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
		   <xsl:text>maxkIndex = MIN(boxlast(2) - boxfirst(2) + 2 * d_ghost_width, MIN((int) floor((</xsl:text><xsl:value-of select="$posz"/><xsl:text> + </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[2])/dx[2]) - boxfirst(2) + d_ghost_width, (int) k + ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[2])));
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>for(int kb = minkIndex; kb &lt;= maxkIndex; kb++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+1"/>
			</xsl:call-template>
			<xsl:text>for(int jb = minjIndex; jb &lt;= maxjIndex; jb++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>for(int ib = miniIndex; ib &lt;= maxiIndex; ib++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
		   <xsl:text>hier::Index idxb(ib + boxfirstP(0), jb + boxfirstP(1), kb + boxfirstP(2));
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
		   <xsl:text>Particles&lt;Particle_</xsl:text><xsl:value-of select="$species"/><xsl:text>&gt;* partb_</xsl:text><xsl:value-of select="$species"/><xsl:text> = particleVariables_</xsl:text><xsl:value-of select="$species"/><xsl:text>->getItem(idxb);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
		   <xsl:text>for (int pitb = 0; pitb &lt; partb_</xsl:text><xsl:value-of select="$species"/><xsl:text>-&gt;getNumberOfParticles(); pitb++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+4"/>
			</xsl:call-template>
		   <xsl:text>Particle_</xsl:text><xsl:value-of select="$species"/><xsl:text>* particleb = partb_</xsl:text><xsl:value-of select="$species"/><xsl:text>->getParticle(pitb);
</xsl:text>
		   <xsl:call-template name="recIndent">
		      <xsl:with-param name="indent" select="$indent+4"/>
		   </xsl:call-template>
		   <xsl:text>double particle_distance = particle->distance</xsl:text><xsl:value-of select="$distance"/><xsl:text>(particleb);
</xsl:text>		   
		   <xsl:call-template name="recIndent">
		      <xsl:with-param name="indent" select="$indent+4"/>
		   </xsl:call-template>
		   <xsl:text>if (particle_distance &lt; </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius) {
</xsl:text>		
		   <xsl:for-each select="./*[position() &gt; 1]">
				<xsl:apply-templates select=".">
					<xsl:with-param name="indent" select="$indent+5"/>
					<xsl:with-param name="condition" select="'false'"/>
				</xsl:apply-templates>
		   </xsl:for-each>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+4"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>		
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>}				
</xsl:text>		
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+1"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
		</xsl:if>
	</xsl:template>

   <xsl:template match="sml:iterateOverParticlesFromCell">
      <xsl:param name="indent" select="$indent"/>
      <xsl:param name="dimensions" select="$dimensions"/>
      <xsl:param name="condition" select="$condition"/>
      <xsl:param name="region" select="$region"/>
      <xsl:variable name="species" select="./@speciesNameAtt"/>
      <xsl:variable name="text">
         <xsl:apply-templates select="./sml:positionReference/*[1]">
            <xsl:with-param name="indent" select="0"/>
            <xsl:with-param name="condition" select="'true'"/>
         </xsl:apply-templates>
      </xsl:variable> 
      <xsl:variable name="distance">
         <xsl:call-template name="string-remove-all">
            <xsl:with-param name="text" select="$text" />
            <xsl:with-param name="replace" select='"position"'/>
         </xsl:call-template>
      </xsl:variable> 
      
      
      <xsl:if test="$dimensions = 2">
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>   
         <xsl:text>position[0] = d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0];
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>position[1] = d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1];
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>miniIndex = MAX(0, MAX((int) floor((position[0] - </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i - ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[0])));
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>maxiIndex = MIN(boxlast(0) - boxfirst(0) + 2 * d_ghost_width, MIN((int) floor((position[0] + </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i + ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[0])));
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>minjIndex = MAX(0, MAX((int) floor((position[1] - </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j - ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[1])));
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>maxjIndex = MIN(boxlast(1) - boxfirst(1) + 2 * d_ghost_width, MIN((int) floor((position[1] + </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j + ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[1])));
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>for(int jb = minjIndex; jb &lt;= maxjIndex; jb++) {
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+1"/>
         </xsl:call-template>
         <xsl:text>for(int ib = miniIndex; ib &lt;= maxiIndex; ib++) {
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+2"/>
         </xsl:call-template>
         <xsl:text>hier::Index idxb(ib + boxfirstP(0), jb + boxfirstP(1));
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+2"/>
         </xsl:call-template>
         <xsl:text>Particles&lt;Particle_</xsl:text><xsl:value-of select="$species"/><xsl:text>&gt;* partb_</xsl:text><xsl:value-of select="$species"/><xsl:text> = particleVariables_</xsl:text><xsl:value-of select="$species"/><xsl:text>->getItem(idxb);
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+2"/>
         </xsl:call-template>
         <xsl:text>for (int pitb = 0; pitb &lt; partb_</xsl:text><xsl:value-of select="$species"/><xsl:text>-&gt;getNumberOfParticles(); pitb++) {
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+3"/>
         </xsl:call-template>
         <xsl:text>Particle_</xsl:text><xsl:value-of select="$species"/><xsl:text>* particleb = partb_</xsl:text><xsl:value-of select="$species"/><xsl:text>->getParticle(pitb);
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+3"/>
         </xsl:call-template>
         <xsl:text>double particle_distance = particleb->distance</xsl:text><xsl:value-of select="$distance"/><xsl:text>(position);
</xsl:text>	
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+3"/>
         </xsl:call-template>
         <xsl:text>if (particle_distance &lt; </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius) {
</xsl:text>		
         <xsl:for-each select="./*[position() &gt; 1]">
            <xsl:apply-templates select=".">
               <xsl:with-param name="indent" select="$indent + 4"/>
               <xsl:with-param name="condition" select="'false'"/>
            </xsl:apply-templates>
         </xsl:for-each>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+3"/>
         </xsl:call-template>
         <xsl:text>}
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+2"/>
         </xsl:call-template>
         <xsl:text>}				
</xsl:text>		
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+1"/>
         </xsl:call-template>
         <xsl:text>}
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>}
</xsl:text>
      </xsl:if>
      <xsl:if test="$dimensions = 3">
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>   
         <xsl:text>position[0] = d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0];
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>position[1] = d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1];
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>position[2] = d_grid_geometry->getXLower()[2] + ((k + boxfirst(2)) - d_ghost_width) * dx[2];
</xsl:text>         
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>miniIndex = MAX(0, MAX((int) floor((position[0] - </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i - ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[0])));
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>maxiIndex = MIN(boxlast(0) - boxfirst(0) + 2 * d_ghost_width, MIN((int) floor((position[0] + </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i + ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[0])));
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>minjIndex = MAX(0, MAX((int) floor((position[1] - </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j - ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[1])));
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>maxjIndex = MIN(boxlast(1) - boxfirst(1) + 2 * d_ghost_width, MIN((int) floor((position[1] + </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j + ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[1])));
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>minkIndex = MAX(0, MAX((int) floor((position[2] - </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[2])/dx[2]) - boxfirst(2) + d_ghost_width, (int) k - ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[2])));
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>maxkIndex = MIN(boxlast(2) - boxfirst(2) + 2 * d_ghost_width, MIN((int) floor((position[2] + </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius - d_grid_geometry->getXLower()[2])/dx[2]) - boxfirst(2) + d_ghost_width, (int) k + ceil((</xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius)/dx[2])));
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>for(int kb = minkIndex; kb &lt;= maxkIndex; kb++) {
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+1"/>
         </xsl:call-template>
         <xsl:text>for(int jb = minjIndex; jb &lt;= maxjIndex; jb++) {
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+2"/>
         </xsl:call-template>
         <xsl:text>for(int ib = miniIndex; ib &lt;= maxiIndex; ib++) {
</xsl:text>	
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+3"/>
         </xsl:call-template>
         <xsl:text>hier::Index idxb(ib + boxfirstP(0), jb + boxfirstP(1), kb + boxfirstP(2));
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+3"/>
         </xsl:call-template>
         <xsl:text>Particles&lt;Particle_</xsl:text><xsl:value-of select="$species"/><xsl:text>&gt;* partb_</xsl:text><xsl:value-of select="$species"/><xsl:text> = particleVariables_</xsl:text><xsl:value-of select="$species"/><xsl:text>->getItem(idxb);
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+3"/>
         </xsl:call-template>
         <xsl:text>for (int pitb = 0; pitb &lt; partb_</xsl:text><xsl:value-of select="$species"/><xsl:text>-&gt;getNumberOfParticles(); pitb++) {
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+4"/>
         </xsl:call-template>
         <xsl:text>Particle_</xsl:text><xsl:value-of select="$species"/><xsl:text>* particleb = partb_</xsl:text><xsl:value-of select="$species"/><xsl:text>->getParticle(pitb);
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+4"/>
         </xsl:call-template>
         <xsl:text>double particle_distance = particleb->distance</xsl:text><xsl:value-of select="$distance"/><xsl:text>(position);
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+4"/>
         </xsl:call-template>
         <xsl:text>if (particle_distance &lt; </xsl:text><xsl:value-of select="./@compactSupportRatioAtt"/><xsl:text> * level_influenceRadius) {
</xsl:text>		
         <xsl:for-each select="./*[position() &gt; 1]">
            <xsl:apply-templates select=".">
               <xsl:with-param name="indent" select="$indent+5"/>
               <xsl:with-param name="condition" select="'false'"/>
            </xsl:apply-templates>
         </xsl:for-each>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+4"/>
         </xsl:call-template>
         <xsl:text>}
</xsl:text>	
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+3"/>
         </xsl:call-template>
         <xsl:text>}
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+2"/>
         </xsl:call-template>
         <xsl:text>}				
</xsl:text>		
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent+1"/>
         </xsl:call-template>
         <xsl:text>}
</xsl:text>
         <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
         </xsl:call-template>
         <xsl:text>}
</xsl:text>
      </xsl:if>
   </xsl:template>


	<xsl:template match="sml:particleRegionInteraction">
		<xsl:param name="indent" select="$indent"/>
		<xsl:param name="regionInteractionCondition" select="$regionInteractionCondition"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>if(</xsl:text><xsl:value-of select="$regionInteractionCondition"/><xsl:text>) {
</xsl:text>
		<xsl:for-each select="*">
			<xsl:apply-templates select=".">	<xsl:with-param name="indent" select="$indent+1"/></xsl:apply-templates>
		</xsl:for-each>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
	</xsl:template>

	<xsl:template match="sml:timeIncrement">
		<xsl:text>simPlat_dt</xsl:text>
	</xsl:template>

	<xsl:template match="sml:particleDistance">
		<xsl:text>particle_distance</xsl:text>
	</xsl:template>
		
   <xsl:template match="m:apply[*[1][self::sml:particleAccess]]">
		<xsl:param name="indent" select="$indent"/>
		<xsl:text>particle-></xsl:text>
		<xsl:apply-templates select="*[2]"/>
	</xsl:template>

   <xsl:template match="m:apply[*[1][self::sml:particleAccessb]]">
		<xsl:param name="indent" select="$indent"/>
		<xsl:text>particleb-></xsl:text>
		<xsl:apply-templates select="*[2]"/>
	</xsl:template>
	
    <xsl:template match="sml:minRegionValue">
        <xsl:text>d_grid_geometry->getXLower()[</xsl:text><xsl:value-of select="."/><xsl:text>]</xsl:text>
    </xsl:template>
    <xsl:template match="sml:maxRegionValue">
        <xsl:text>d_grid_geometry->getXUpper()[</xsl:text><xsl:value-of select="."/><xsl:text>]</xsl:text>
    </xsl:template>
	
	<xsl:template name="recIndent">
		<xsl:param name="indent"/>
		<xsl:if test="$indent &gt; 0">
			<xsl:text>&#x9;</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent - 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
   <xsl:template name="string-remove-all">
      <xsl:param name="text" />
      <xsl:param name="replace" />
      <xsl:choose>
         <xsl:when test="$text = '' or $replace = ''or not($replace)" >
            <!-- Prevent this routine from hanging -->
            <xsl:value-of select="$text" />
         </xsl:when>
         <xsl:when test="contains($text, $replace)">
            <xsl:value-of select="substring-before($text,$replace)" />
            <xsl:call-template name="string-remove-all">
               <xsl:with-param name="text" select="substring-after($text,$replace)" />
               <xsl:with-param name="replace" select="$replace" />
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$text" />
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

</xsl:stylesheet>
