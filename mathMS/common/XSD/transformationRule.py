# ./transformationRule.py
# -*- coding: utf-8 -*-
# PyXB bindings for NM:d41256391209d696bef7ed435bcacf95977a0241
# Generated 2020-03-17 16:14:57.207496 by PyXB version 1.2.6 using Python 2.7.17.final.0
# Namespace urn:mathms

from __future__ import unicode_literals
import pyxb
import pyxb.binding
import pyxb.binding.saxer
import io
import pyxb.utils.utility
import pyxb.utils.domutils
import sys
import pyxb.utils.six as _six
# Unique identifier for bindings created at the same time
_GenerationUID = pyxb.utils.utility.UniqueIdentifier('urn:uuid:0fee52ee-6862-11ea-a8f8-6036ddb297e2')

# Version of PyXB used to generate the bindings
_PyXBVersion = '1.2.6'
# Generated bindings are not compatible across PyXB versions
if pyxb.__version__ != _PyXBVersion:
    raise pyxb.PyXBVersionError(_PyXBVersion)

# A holder for module-level binding classes so we can access them from
# inside class definitions where property names may conflict.
_module_typeBindings = pyxb.utils.utility.Object()

# Import bindings for namespaces imported into schema
import pyxb.binding.datatypes

# NOTE: All namespace declarations are reserved within the binding
Namespace = pyxb.namespace.NamespaceForURI('urn:mathms', create_if_missing=True)
Namespace.configureCategories(['typeBinding', 'elementBinding'])

def CreateFromDocument (xml_text, default_namespace=None, location_base=None):
    """Parse the given XML and use the document element to create a
    Python instance.

    @param xml_text An XML document.  This should be data (Python 2
    str or Python 3 bytes), or a text (Python 2 unicode or Python 3
    str) in the L{pyxb._InputEncoding} encoding.

    @keyword default_namespace The L{pyxb.Namespace} instance to use as the
    default namespace where there is no default namespace in scope.
    If unspecified or C{None}, the namespace of the module containing
    this function will be used.

    @keyword location_base: An object to be recorded as the base of all
    L{pyxb.utils.utility.Location} instances associated with events and
    objects handled by the parser.  You might pass the URI from which
    the document was obtained.
    """

    if pyxb.XMLStyle_saxer != pyxb._XMLStyle:
        dom = pyxb.utils.domutils.StringToDOM(xml_text)
        return CreateFromDOM(dom.documentElement, default_namespace=default_namespace)
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    saxer = pyxb.binding.saxer.make_parser(fallback_namespace=default_namespace, location_base=location_base)
    handler = saxer.getContentHandler()
    xmld = xml_text
    if isinstance(xmld, _six.text_type):
        xmld = xmld.encode(pyxb._InputEncoding)
    saxer.parse(io.BytesIO(xmld))
    instance = handler.rootObject()
    return instance

def CreateFromDOM (node, default_namespace=None):
    """Create a Python instance from the given DOM node.
    The node tag must correspond to an element declaration in this module.

    @deprecated: Forcing use of DOM interface is unnecessary; use L{CreateFromDocument}."""
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    return pyxb.binding.basis.element.AnyCreateFromDOM(node, default_namespace)


# Atomic simple type: {urn:mathms}fparameterType
class fparameterType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'fparameterType')
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 41, 4)
    _Documentation = None
fparameterType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=fparameterType, enum_prefix=None)
fparameterType.field = fparameterType._CF_enumeration.addEnumeration(unicode_value='field', tag='field')
fparameterType.real = fparameterType._CF_enumeration.addEnumeration(unicode_value='real', tag='real')
fparameterType.int = fparameterType._CF_enumeration.addEnumeration(unicode_value='int', tag='int')
fparameterType.boolean = fparameterType._CF_enumeration.addEnumeration(unicode_value='boolean', tag='boolean')
fparameterType.coordinates = fparameterType._CF_enumeration.addEnumeration(unicode_value='coordinates', tag='coordinates')
fparameterType.function = fparameterType._CF_enumeration.addEnumeration(unicode_value='function', tag='function')
fparameterType._InitializeFacetMap(fparameterType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'fparameterType', fparameterType)
_module_typeBindings.fparameterType = fparameterType

# Atomic simple type: {urn:mathms}functionParameterType
class functionParameterType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'functionParameterType')
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 51, 3)
    _Documentation = None
functionParameterType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=functionParameterType, enum_prefix=None)
functionParameterType.field = functionParameterType._CF_enumeration.addEnumeration(unicode_value='field', tag='field')
functionParameterType.real = functionParameterType._CF_enumeration.addEnumeration(unicode_value='real', tag='real')
functionParameterType.int = functionParameterType._CF_enumeration.addEnumeration(unicode_value='int', tag='int')
functionParameterType.boolean = functionParameterType._CF_enumeration.addEnumeration(unicode_value='boolean', tag='boolean')
functionParameterType.coordinates = functionParameterType._CF_enumeration.addEnumeration(unicode_value='coordinates', tag='coordinates')
functionParameterType._InitializeFacetMap(functionParameterType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'functionParameterType', functionParameterType)
_module_typeBindings.functionParameterType = functionParameterType

# Complex type {urn:mathms}constants with content type ELEMENT_ONLY
class constants (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {urn:mathms}constants with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'constants')
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 4, 3)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}constant uses Python identifier constant
    __constant = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'constant'), 'constant', '__urnmathms_constants_urnmathmsconstant', True, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 6, 9), )

    
    constant = property(__constant.value, __constant.set, None, '')

    _ElementMap.update({
        __constant.name() : __constant
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.constants = constants
Namespace.addCategoryObject('typeBinding', 'constants', constants)


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON (pyxb.binding.basis.complexTypeDefinition):
    """"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 10, 12)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}name uses Python identifier name
    __name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'name'), 'name', '__urnmathms_CTD_ANON_urnmathmsname', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 12, 18), )

    
    name = property(__name.value, __name.set, None, '')

    
    # Element {urn:mathms}value uses Python identifier value_
    __value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'value'), 'value_', '__urnmathms_CTD_ANON_urnmathmsvalue', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 17, 18), )

    
    value_ = property(__value.value, __value.set, None, '')

    _ElementMap.update({
        __name.name() : __name,
        __value.name() : __value
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CTD_ANON = CTD_ANON


# Complex type {urn:mathms}functionParameters with content type ELEMENT_ONLY
class functionParameters (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {urn:mathms}functionParameters with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'functionParameters')
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 4, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}functionParameter uses Python identifier functionParameter
    __functionParameter = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'functionParameter'), 'functionParameter', '__urnmathms_functionParameters_urnmathmsfunctionParameter', True, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 6, 6), )

    
    functionParameter = property(__functionParameter.value, __functionParameter.set, None, '')

    _ElementMap.update({
        __functionParameter.name() : __functionParameter
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.functionParameters = functionParameters
Namespace.addCategoryObject('typeBinding', 'functionParameters', functionParameters)


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_ (pyxb.binding.basis.complexTypeDefinition):
    """"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 10, 8)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}name uses Python identifier name
    __name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'name'), 'name', '__urnmathms_CTD_ANON__urnmathmsname', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 12, 12), )

    
    name = property(__name.value, __name.set, None, '')

    
    # Element {urn:mathms}type uses Python identifier type
    __type = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'type'), 'type', '__urnmathms_CTD_ANON__urnmathmstype', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 17, 13), )

    
    type = property(__type.value, __type.set, None, '')

    
    # Element {urn:mathms}functionParameters uses Python identifier functionParameters
    __functionParameters = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'functionParameters'), 'functionParameters', '__urnmathms_CTD_ANON__urnmathmsfunctionParameters', True, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 22, 13), )

    
    functionParameters = property(__functionParameters.value, __functionParameters.set, None, '')

    _ElementMap.update({
        __name.name() : __name,
        __type.name() : __type,
        __functionParameters.name() : __functionParameters
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CTD_ANON_ = CTD_ANON_


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_2 (pyxb.binding.basis.complexTypeDefinition):
    """"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 26, 16)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}functionParameterType uses Python identifier functionParameterType
    __functionParameterType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'functionParameterType'), 'functionParameterType', '__urnmathms_CTD_ANON_2_urnmathmsfunctionParameterType', True, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 28, 22), )

    
    functionParameterType = property(__functionParameterType.value, __functionParameterType.set, None, '')

    _ElementMap.update({
        __functionParameterType.name() : __functionParameterType
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CTD_ANON_2 = CTD_ANON_2


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_3 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 5, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}schemaParameter uses Python identifier schemaParameter
    __schemaParameter = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'schemaParameter'), 'schemaParameter', '__urnmathms_CTD_ANON_3_urnmathmsschemaParameter', True, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 7, 8), )

    
    schemaParameter = property(__schemaParameter.value, __schemaParameter.set, None, None)

    _ElementMap.update({
        __schemaParameter.name() : __schemaParameter
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CTD_ANON_3 = CTD_ANON_3


# Complex type {urn:mathms}schemaParameter with content type ELEMENT_ONLY
class schemaParameter (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {urn:mathms}schemaParameter with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'schemaParameter')
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 11, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}name uses Python identifier name
    __name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'name'), 'name', '__urnmathms_schemaParameter_urnmathmsname', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 13, 6), )

    
    name = property(__name.value, __name.set, None, None)

    
    # Element {urn:mathms}paramName uses Python identifier paramName
    __paramName = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'paramName'), 'paramName', '__urnmathms_schemaParameter_urnmathmsparamName', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 14, 6), )

    
    paramName = property(__paramName.value, __paramName.set, None, None)

    
    # Element {urn:mathms}values uses Python identifier values
    __values = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'values'), 'values', '__urnmathms_schemaParameter_urnmathmsvalues', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 15, 6), )

    
    values = property(__values.value, __values.set, None, None)

    _ElementMap.update({
        __name.name() : __name,
        __paramName.name() : __paramName,
        __values.name() : __values
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.schemaParameter = schemaParameter
Namespace.addCategoryObject('typeBinding', 'schemaParameter', schemaParameter)


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_4 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 16, 8)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}value uses Python identifier value_
    __value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'value'), 'value_', '__urnmathms_CTD_ANON_4_urnmathmsvalue', True, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 18, 12), )

    
    value_ = property(__value.value, __value.set, None, None)

    _ElementMap.update({
        __value.name() : __value
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CTD_ANON_4 = CTD_ANON_4


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_5 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 19, 14)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}valueName uses Python identifier valueName
    __valueName = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'valueName'), 'valueName', '__urnmathms_CTD_ANON_5_urnmathmsvalueName', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 21, 18), )

    
    valueName = property(__valueName.value, __valueName.set, None, None)

    
    # Element {urn:mathms}schemaParameters uses Python identifier schemaParameters
    __schemaParameters = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'schemaParameters'), 'schemaParameters', '__urnmathms_CTD_ANON_5_urnmathmsschemaParameters', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 22, 18), )

    
    schemaParameters = property(__schemaParameters.value, __schemaParameters.set, None, None)

    _ElementMap.update({
        __valueName.name() : __valueName,
        __schemaParameters.name() : __schemaParameters
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CTD_ANON_5 = CTD_ANON_5


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_6 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 23, 20)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}schemaParameter uses Python identifier schemaParameter
    __schemaParameter = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'schemaParameter'), 'schemaParameter', '__urnmathms_CTD_ANON_6_urnmathmsschemaParameter', True, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 25, 24), )

    
    schemaParameter = property(__schemaParameter.value, __schemaParameter.set, None, None)

    _ElementMap.update({
        __schemaParameter.name() : __schemaParameter
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CTD_ANON_6 = CTD_ANON_6


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_7 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 9, 6)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}head uses Python identifier head
    __head = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'head'), 'head', '__urnmathms_CTD_ANON_7_urnmathmshead', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 11, 12), )

    
    head = property(__head.value, __head.set, None, None)

    
    # Element {urn:mathms}functionParameters uses Python identifier functionParameters
    __functionParameters = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'functionParameters'), 'functionParameters', '__urnmathms_CTD_ANON_7_urnmathmsfunctionParameters', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 30, 15), )

    
    functionParameters = property(__functionParameters.value, __functionParameters.set, None, None)

    
    # Element {urn:mathms}externalVariables uses Python identifier externalVariables
    __externalVariables = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'externalVariables'), 'externalVariables', '__urnmathms_CTD_ANON_7_urnmathmsexternalVariables', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 32, 15), )

    
    externalVariables = property(__externalVariables.value, __externalVariables.set, None, None)

    
    # Element {urn:mathms}constants uses Python identifier constants
    __constants = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'constants'), 'constants', '__urnmathms_CTD_ANON_7_urnmathmsconstants', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 67, 12), )

    
    constants = property(__constants.value, __constants.set, None, None)

    _ElementMap.update({
        __head.name() : __head,
        __functionParameters.name() : __functionParameters,
        __externalVariables.name() : __externalVariables,
        __constants.name() : __constants
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CTD_ANON_7 = CTD_ANON_7


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_8 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 12, 15)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}name uses Python identifier name
    __name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'name'), 'name', '__urnmathms_CTD_ANON_8_urnmathmsname', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 14, 21), )

    
    name = property(__name.value, __name.set, None, None)

    
    # Element {urn:mathms}id uses Python identifier id
    __id = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'id'), 'id', '__urnmathms_CTD_ANON_8_urnmathmsid', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 16, 21), )

    
    id = property(__id.value, __id.set, None, None)

    
    # Element {urn:mathms}author uses Python identifier author
    __author = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'author'), 'author', '__urnmathms_CTD_ANON_8_urnmathmsauthor', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 18, 21), )

    
    author = property(__author.value, __author.set, None, None)

    
    # Element {urn:mathms}version uses Python identifier version
    __version = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'version'), 'version', '__urnmathms_CTD_ANON_8_urnmathmsversion', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 20, 21), )

    
    version = property(__version.value, __version.set, None, None)

    
    # Element {urn:mathms}date uses Python identifier date
    __date = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'date'), 'date', '__urnmathms_CTD_ANON_8_urnmathmsdate', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 22, 21), )

    
    date = property(__date.value, __date.set, None, None)

    
    # Element {urn:mathms}description uses Python identifier description
    __description = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'description'), 'description', '__urnmathms_CTD_ANON_8_urnmathmsdescription', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 24, 21), )

    
    description = property(__description.value, __description.set, None, None)

    _ElementMap.update({
        __name.name() : __name,
        __id.name() : __id,
        __author.name() : __author,
        __version.name() : __version,
        __date.name() : __date,
        __description.name() : __description
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CTD_ANON_8 = CTD_ANON_8


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_9 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 33, 18)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}inputVariables uses Python identifier inputVariables
    __inputVariables = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'inputVariables'), 'inputVariables', '__urnmathms_CTD_ANON_9_urnmathmsinputVariables', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 35, 24), )

    
    inputVariables = property(__inputVariables.value, __inputVariables.set, None, None)

    
    # Element {urn:mathms}outputVariables uses Python identifier outputVariables
    __outputVariables = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'outputVariables'), 'outputVariables', '__urnmathms_CTD_ANON_9_urnmathmsoutputVariables', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 49, 24), )

    
    outputVariables = property(__outputVariables.value, __outputVariables.set, None, None)

    _ElementMap.update({
        __inputVariables.name() : __inputVariables,
        __outputVariables.name() : __outputVariables
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CTD_ANON_9 = CTD_ANON_9


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_10 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 36, 27)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}inputVariable uses Python identifier inputVariable
    __inputVariable = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'inputVariable'), 'inputVariable', '__urnmathms_CTD_ANON_10_urnmathmsinputVariable', True, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 38, 33), )

    
    inputVariable = property(__inputVariable.value, __inputVariable.set, None, None)

    _ElementMap.update({
        __inputVariable.name() : __inputVariable
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CTD_ANON_10 = CTD_ANON_10


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_11 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 39, 36)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}math uses Python identifier math
    __math = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'math'), 'math', '__urnmathms_CTD_ANON_11_urnmathmsmath', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 41, 42), )

    
    math = property(__math.value, __math.set, None, None)

    _ElementMap.update({
        __math.name() : __math
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CTD_ANON_11 = CTD_ANON_11


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_12 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 50, 27)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}outputVariable uses Python identifier outputVariable
    __outputVariable = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'outputVariable'), 'outputVariable', '__urnmathms_CTD_ANON_12_urnmathmsoutputVariable', True, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 52, 33), )

    
    outputVariable = property(__outputVariable.value, __outputVariable.set, None, None)

    _ElementMap.update({
        __outputVariable.name() : __outputVariable
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CTD_ANON_12 = CTD_ANON_12


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_13 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 53, 36)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}math uses Python identifier math
    __math = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'math'), 'math', '__urnmathms_CTD_ANON_13_urnmathmsmath', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 55, 42), )

    
    math = property(__math.value, __math.set, None, None)

    _ElementMap.update({
        __math.name() : __math
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CTD_ANON_13 = CTD_ANON_13


schemaParameters = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'schemaParameters'), CTD_ANON_3, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 4, 2))
Namespace.addCategoryObject('elementBinding', schemaParameters.name().localName(), schemaParameters)

transformationRule = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'transformationRule'), CTD_ANON_7, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 8, 3))
Namespace.addCategoryObject('elementBinding', transformationRule.name().localName(), transformationRule)



constants._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'constant'), CTD_ANON, scope=constants, documentation='', location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 6, 9)))

def _BuildAutomaton ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton
    del _BuildAutomaton
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(constants._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'constant')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 6, 9))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
constants._Automaton = _BuildAutomaton()




CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'name'), pyxb.binding.datatypes.string, scope=CTD_ANON, documentation='', location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 12, 18)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'value'), pyxb.binding.datatypes.double, scope=CTD_ANON, documentation='', location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 17, 18)))

def _BuildAutomaton_ ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_
    del _BuildAutomaton_
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 12, 18))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'value')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 17, 18))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON._Automaton = _BuildAutomaton_()




functionParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'functionParameter'), CTD_ANON_, scope=functionParameters, documentation='', location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 6, 6)))

def _BuildAutomaton_2 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_2
    del _BuildAutomaton_2
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(functionParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'functionParameter')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 6, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
functionParameters._Automaton = _BuildAutomaton_2()




CTD_ANON_._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'name'), pyxb.binding.datatypes.string, scope=CTD_ANON_, documentation='', location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 12, 12)))

CTD_ANON_._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'type'), fparameterType, scope=CTD_ANON_, documentation='', location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 17, 13)))

CTD_ANON_._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'functionParameters'), CTD_ANON_2, scope=CTD_ANON_, documentation='', location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 22, 13)))

def _BuildAutomaton_3 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_3
    del _BuildAutomaton_3
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 12, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 22, 13))
    counters.add(cc_1)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 12, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'type')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 17, 13))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'functionParameters')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 22, 13))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_._Automaton = _BuildAutomaton_3()




CTD_ANON_2._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'functionParameterType'), functionParameterType, scope=CTD_ANON_2, documentation='', location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 28, 22)))

def _BuildAutomaton_4 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_4
    del _BuildAutomaton_4
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_2._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'functionParameterType')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/functionParams.xsd', 28, 22))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_2._Automaton = _BuildAutomaton_4()




CTD_ANON_3._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'schemaParameter'), schemaParameter, scope=CTD_ANON_3, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 7, 8)))

def _BuildAutomaton_5 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_5
    del _BuildAutomaton_5
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_3._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'schemaParameter')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 7, 8))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_3._Automaton = _BuildAutomaton_5()




schemaParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'name'), pyxb.binding.datatypes.string, scope=schemaParameter, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 13, 6)))

schemaParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'paramName'), pyxb.binding.datatypes.string, scope=schemaParameter, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 14, 6)))

schemaParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'values'), CTD_ANON_4, scope=schemaParameter, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 15, 6)))

def _BuildAutomaton_6 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_6
    del _BuildAutomaton_6
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 15, 6))
    counters.add(cc_0)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(schemaParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 13, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(schemaParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'paramName')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 14, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(schemaParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'values')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 15, 6))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
schemaParameter._Automaton = _BuildAutomaton_6()




CTD_ANON_4._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'value'), CTD_ANON_5, scope=CTD_ANON_4, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 18, 12)))

def _BuildAutomaton_7 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_7
    del _BuildAutomaton_7
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_4._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'value')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 18, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_4._Automaton = _BuildAutomaton_7()




CTD_ANON_5._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'valueName'), pyxb.binding.datatypes.string, scope=CTD_ANON_5, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 21, 18)))

CTD_ANON_5._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'schemaParameters'), CTD_ANON_6, scope=CTD_ANON_5, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 22, 18)))

def _BuildAutomaton_8 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_8
    del _BuildAutomaton_8
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 22, 18))
    counters.add(cc_0)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_5._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'valueName')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 21, 18))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_5._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'schemaParameters')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 22, 18))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_5._Automaton = _BuildAutomaton_8()




CTD_ANON_6._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'schemaParameter'), schemaParameter, scope=CTD_ANON_6, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 25, 24)))

def _BuildAutomaton_9 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_9
    del _BuildAutomaton_9
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_6._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'schemaParameter')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/schemaParams.xsd', 25, 24))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_6._Automaton = _BuildAutomaton_9()




CTD_ANON_7._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'head'), CTD_ANON_8, scope=CTD_ANON_7, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 11, 12)))

CTD_ANON_7._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'functionParameters'), functionParameters, scope=CTD_ANON_7, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 30, 15)))

CTD_ANON_7._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'externalVariables'), CTD_ANON_9, scope=CTD_ANON_7, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 32, 15)))

CTD_ANON_7._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'constants'), constants, scope=CTD_ANON_7, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 67, 12)))

def _BuildAutomaton_10 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_10
    del _BuildAutomaton_10
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 67, 12))
    counters.add(cc_0)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_7._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'head')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 11, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_7._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'functionParameters')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 30, 15))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_7._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'externalVariables')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 32, 15))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_7._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'constants')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 67, 12))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_7._Automaton = _BuildAutomaton_10()




CTD_ANON_8._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'name'), pyxb.binding.datatypes.string, scope=CTD_ANON_8, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 14, 21)))

CTD_ANON_8._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'id'), pyxb.binding.datatypes.string, scope=CTD_ANON_8, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 16, 21)))

CTD_ANON_8._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'author'), pyxb.binding.datatypes.string, scope=CTD_ANON_8, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 18, 21)))

CTD_ANON_8._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'version'), pyxb.binding.datatypes.string, scope=CTD_ANON_8, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 20, 21)))

CTD_ANON_8._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'date'), pyxb.binding.datatypes.dateTime, scope=CTD_ANON_8, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 22, 21)))

CTD_ANON_8._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'description'), pyxb.binding.datatypes.string, scope=CTD_ANON_8, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 24, 21)))

def _BuildAutomaton_11 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_11
    del _BuildAutomaton_11
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 24, 21))
    counters.add(cc_0)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_8._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 14, 21))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_8._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'id')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 16, 21))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_8._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'author')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 18, 21))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_8._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'version')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 20, 21))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_8._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'date')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 22, 21))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_8._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'description')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 24, 21))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
         ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_5._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_8._Automaton = _BuildAutomaton_11()




CTD_ANON_9._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'inputVariables'), CTD_ANON_10, scope=CTD_ANON_9, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 35, 24)))

CTD_ANON_9._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'outputVariables'), CTD_ANON_12, scope=CTD_ANON_9, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 49, 24)))

def _BuildAutomaton_12 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_12
    del _BuildAutomaton_12
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_9._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'inputVariables')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 35, 24))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_9._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'outputVariables')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 49, 24))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_9._Automaton = _BuildAutomaton_12()




CTD_ANON_10._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'inputVariable'), CTD_ANON_11, scope=CTD_ANON_10, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 38, 33)))

def _BuildAutomaton_13 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_13
    del _BuildAutomaton_13
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_10._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'inputVariable')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 38, 33))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_10._Automaton = _BuildAutomaton_13()




CTD_ANON_11._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'math'), pyxb.binding.datatypes.string, scope=CTD_ANON_11, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 41, 42)))

def _BuildAutomaton_14 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_14
    del _BuildAutomaton_14
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_11._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'math')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 41, 42))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_11._Automaton = _BuildAutomaton_14()




CTD_ANON_12._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'outputVariable'), CTD_ANON_13, scope=CTD_ANON_12, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 52, 33)))

def _BuildAutomaton_15 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_15
    del _BuildAutomaton_15
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_12._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'outputVariable')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 52, 33))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_12._Automaton = _BuildAutomaton_15()




CTD_ANON_13._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'math'), pyxb.binding.datatypes.string, scope=CTD_ANON_13, location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 55, 42)))

def _BuildAutomaton_16 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_16
    del _BuildAutomaton_16
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_13._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'math')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/transformationRule_fake.xsd', 55, 42))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_13._Automaton = _BuildAutomaton_16()

