# ./constants.py
# -*- coding: utf-8 -*-
# PyXB bindings for NM:d41256391209d696bef7ed435bcacf95977a0241
# Generated 2020-03-17 15:52:27.225422 by PyXB version 1.2.6 using Python 2.7.17.final.0
# Namespace urn:mathms

from __future__ import unicode_literals
import pyxb
import pyxb.binding
import pyxb.binding.saxer
import io
import pyxb.utils.utility
import pyxb.utils.domutils
import sys
import pyxb.utils.six as _six
# Unique identifier for bindings created at the same time
_GenerationUID = pyxb.utils.utility.UniqueIdentifier('urn:uuid:eb35e62c-685e-11ea-a8f8-6036ddb297e2')

# Version of PyXB used to generate the bindings
_PyXBVersion = '1.2.6'
# Generated bindings are not compatible across PyXB versions
if pyxb.__version__ != _PyXBVersion:
    raise pyxb.PyXBVersionError(_PyXBVersion)

# A holder for module-level binding classes so we can access them from
# inside class definitions where property names may conflict.
_module_typeBindings = pyxb.utils.utility.Object()

# Import bindings for namespaces imported into schema
import pyxb.binding.datatypes

# NOTE: All namespace declarations are reserved within the binding
Namespace = pyxb.namespace.NamespaceForURI('urn:mathms', create_if_missing=True)
Namespace.configureCategories(['typeBinding', 'elementBinding'])

def CreateFromDocument (xml_text, default_namespace=None, location_base=None):
    """Parse the given XML and use the document element to create a
    Python instance.

    @param xml_text An XML document.  This should be data (Python 2
    str or Python 3 bytes), or a text (Python 2 unicode or Python 3
    str) in the L{pyxb._InputEncoding} encoding.

    @keyword default_namespace The L{pyxb.Namespace} instance to use as the
    default namespace where there is no default namespace in scope.
    If unspecified or C{None}, the namespace of the module containing
    this function will be used.

    @keyword location_base: An object to be recorded as the base of all
    L{pyxb.utils.utility.Location} instances associated with events and
    objects handled by the parser.  You might pass the URI from which
    the document was obtained.
    """

    if pyxb.XMLStyle_saxer != pyxb._XMLStyle:
        dom = pyxb.utils.domutils.StringToDOM(xml_text)
        return CreateFromDOM(dom.documentElement, default_namespace=default_namespace)
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    saxer = pyxb.binding.saxer.make_parser(fallback_namespace=default_namespace, location_base=location_base)
    handler = saxer.getContentHandler()
    xmld = xml_text
    if isinstance(xmld, _six.text_type):
        xmld = xmld.encode(pyxb._InputEncoding)
    saxer.parse(io.BytesIO(xmld))
    instance = handler.rootObject()
    return instance

def CreateFromDOM (node, default_namespace=None):
    """Create a Python instance from the given DOM node.
    The node tag must correspond to an element declaration in this module.

    @deprecated: Forcing use of DOM interface is unnecessary; use L{CreateFromDocument}."""
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    return pyxb.binding.basis.element.AnyCreateFromDOM(node, default_namespace)


# Complex type {urn:mathms}constants with content type ELEMENT_ONLY
class constants (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {urn:mathms}constants with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'constants')
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 4, 3)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}constant uses Python identifier constant
    __constant = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'constant'), 'constant', '__urnmathms_constants_urnmathmsconstant', True, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 6, 9), )

    
    constant = property(__constant.value, __constant.set, None, '')

    _ElementMap.update({
        __constant.name() : __constant
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.constants = constants
Namespace.addCategoryObject('typeBinding', 'constants', constants)


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON (pyxb.binding.basis.complexTypeDefinition):
    """"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 10, 12)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {urn:mathms}name uses Python identifier name
    __name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'name'), 'name', '__urnmathms_CTD_ANON_urnmathmsname', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 12, 18), )

    
    name = property(__name.value, __name.set, None, '')

    
    # Element {urn:mathms}value uses Python identifier value_
    __value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'value'), 'value_', '__urnmathms_CTD_ANON_urnmathmsvalue', False, pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 17, 18), )

    
    value_ = property(__value.value, __value.set, None, '')

    _ElementMap.update({
        __name.name() : __name,
        __value.name() : __value
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CTD_ANON = CTD_ANON




constants._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'constant'), CTD_ANON, scope=constants, documentation='', location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 6, 9)))

def _BuildAutomaton ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton
    del _BuildAutomaton
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(constants._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'constant')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 6, 9))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
constants._Automaton = _BuildAutomaton()




CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'name'), pyxb.binding.datatypes.string, scope=CTD_ANON, documentation='', location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 12, 18)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'value'), pyxb.binding.datatypes.double, scope=CTD_ANON, documentation='', location=pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 17, 18)))

def _BuildAutomaton_ ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_
    del _BuildAutomaton_
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 12, 18))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'value')), pyxb.utils.utility.Location('/home/iac3user/bitbucket/simflowny/mathMS/common/XSD/constants.xsd', 17, 18))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON._Automaton = _BuildAutomaton_()

