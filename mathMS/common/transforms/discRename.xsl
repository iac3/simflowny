<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mms="urn:mathms" xmlns:sml="urn:simml" xmlns:mt="http://www.w3.org/1998/Math/MathML"
    version="1.0">
        
    <xsl:template match="mms:coordinateSystem">
        <mms:coordinates>
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </mms:coordinates>
    </xsl:template>
    
    <xsl:template match="mt:math">
        <mt:math xmlns:mt="http://www.w3.org/1998/Math/MathML"
            xmlns:sml="urn:simml">
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </mt:math>
    </xsl:template>
    
    <xsl:template match="mms:problemDomain"></xsl:template>
    
    <xsl:template match="mms:segmentGroups">
        <mms:region>
            <mms:name><xsl:value-of select="mms:segmentGroup[descendant::mms:problemDomain]/mms:segmentGroupName"/></mms:name>
            <mms:interiorModels>
                <xsl:for-each select="mms:segmentGroup[descendant::mms:problemDomain]/mms:interior/mms:modelId">
                    <xsl:variable name="modelId" select="./text()"></xsl:variable>
                    <mms:interiorModel><xsl:value-of select="//mms:modelImport[mms:modelId = $modelId]/mms:modelName"/></mms:interiorModel>
                </xsl:for-each>
            </mms:interiorModels>
            <mms:spatialDomain>
                <xsl:for-each select="//mms:problemDomain/mms:coordinateLimit">
                    <mms:coordinateLimits>
                        <xsl:apply-templates select="*"></xsl:apply-templates>
                    </mms:coordinateLimits>
                </xsl:for-each>
            </mms:spatialDomain>
            <xsl:apply-templates select="mms:segmentGroup[descendant::mms:problemDomain]/mms:initialConditions"></xsl:apply-templates>
            <xsl:apply-templates select="mms:segmentGroup[descendant::mms:problemDomain]/mms:fieldGroups"></xsl:apply-templates>
            <xsl:if test="mms:segmentGroup[descendant::mms:problemDomain]/mms:segmentInteractions">
                <mms:interactions>
                    <xsl:for-each select="mms:segmentGroup[descendant::mms:problemDomain]/mms:segmentInteractions/mms:segmentInteraction[mms:interaction/mms:type = 'Fixed']">
                        <mms:fieldInteraction>
                            <xsl:apply-templates select="mms:targetSegments"></xsl:apply-templates>
                            <mms:projections>
                                <xsl:for-each select=".//mms:fixedValue">
                                    <mms:projection>
                                        <mms:variable><xsl:value-of select=".//mms:fixedVariable"/></mms:variable>
                                        <mms:terms>
                                            <xsl:for-each select=".//mms:diagonalizationCoefficient">
                                                <mms:term>
                                                    <xsl:apply-templates select="mms:field"></xsl:apply-templates>
                                                    <xsl:apply-templates select=".//mt:math"></xsl:apply-templates>
                                                </mms:term>
                                            </xsl:for-each>
                                        </mms:terms>
                                    </mms:projection>
                                </xsl:for-each>
                            </mms:projections>
                        </mms:fieldInteraction>
                    </xsl:for-each>
                    <xsl:for-each select="mms:segmentGroup[descendant::mms:problemDomain]/mms:segmentInteractions/mms:segmentInteraction[mms:interaction/mms:type = 'Characteristic']">
                        <mms:eigenVectorInteraction>
                            <xsl:apply-templates select="mms:targetSegments"></xsl:apply-templates>
                            <mms:projections>
                                <xsl:for-each select=".//mms:fixedValue">
                                    <mms:projection>
                                        <mms:variable><xsl:value-of select=".//mms:fixedVariable"/></mms:variable>
                                        <xsl:apply-templates select="mms:diagonalizationCoefficients"></xsl:apply-templates>
                                    </mms:projection>
                                </xsl:for-each>
                            </mms:projections>
                        </mms:eigenVectorInteraction>
                    </xsl:for-each>
                </mms:interactions>
            </xsl:if>
            <xsl:apply-templates select="mms:segmentGroup[descendant::mms:problemDomain]/mms:postInitialConditions"></xsl:apply-templates>
            <xsl:apply-templates select="mms:segmentGroup[descendant::mms:problemDomain]/mms:interiorExecutionFlow"></xsl:apply-templates>
            <xsl:apply-templates select="mms:segmentGroup[descendant::mms:problemDomain]/mms:surfaceExecutionFlow"></xsl:apply-templates>
            <mms:discretizationInfo>
                <xsl:apply-templates select="mms:segmentGroup[descendant::mms:problemDomain]/mms:discretizationInfo/mms:fieldGroup"></xsl:apply-templates>
                <xsl:if test="mms:segmentGroup[descendant::mms:problemDomain]/mms:discretizationInfo/mms:name">
                    <mms:balanceLawPDEDiscretization>
                        <xsl:apply-templates select="mms:segmentGroup[descendant::mms:problemDomain]/mms:discretizationInfo/*"></xsl:apply-templates>
                    </mms:balanceLawPDEDiscretization>
                </xsl:if>
            </mms:discretizationInfo>
        </mms:region>
        <xsl:if test="mms:segmentGroup[not(descendant::mms:problemDomain)]">
            <mms:subregions>
                <xsl:for-each select="mms:segmentGroup[not(descendant::mms:problemDomain)]">
                <mms:subregion>
                    <mms:name><xsl:value-of select="mms:segmentGroupName"/></mms:name>
                    <xsl:if test="mms:interior">
                        <mms:interiorModels>
                            <xsl:for-each select="mms:interior/mms:modelId">
                                <xsl:variable name="modelId" select="./text()"></xsl:variable>
                                <mms:interiorModel><xsl:value-of select="//mms:modelImport[mms:modelId = $modelId]/mms:modelName"/></mms:interiorModel>
                             </xsl:for-each>
                         </mms:interiorModels>
                    </xsl:if>
                    <xsl:if test="mms:surface">
                        <mms:surfaceModels>
                            <xsl:for-each select="mms:surface/mms:modelId">
                                <xsl:variable name="modelId" select="./text()"></xsl:variable>
                                <mms:surfaceModel><xsl:value-of select="//mms:modelImport[mms:modelId = $modelId]/mms:modelName"/></mms:surfaceModel>
                           </xsl:for-each>
                       </mms:surfaceModels>
                    </xsl:if>
                    <mms:location>
                        <xsl:for-each select=".//mms:coordinateLimits">
                            <mms:spatialDomain>
                                <xsl:for-each select=".//mms:coordinateLimit">
                                    <mms:coordinateLimits>
                                        <xsl:apply-templates select="*"></xsl:apply-templates>
                                    </mms:coordinateLimits>
                                </xsl:for-each>
                            </mms:spatialDomain>
                        </xsl:for-each>
                        <xsl:for-each select=".//mms:x3dSegmentId">
                            <mms:x3d>
                                <mms:name>Segment</mms:name>
                                <mms:id><xsl:value-of select="."/></mms:id>
                            </mms:x3d>
                        </xsl:for-each>
                    </mms:location>
                    <xsl:apply-templates select="./mms:initialConditions"></xsl:apply-templates>
                    <xsl:apply-templates select="./mms:fieldGroups"></xsl:apply-templates>
                    <xsl:if test="./mms:segmentInteractions">
                        <mms:interactions>
                            <xsl:for-each select="mms:segmentInteraction[mms:type = 'Fixed']">
                                <mms:fieldInteraction>
                                    <xsl:apply-templates select="mms:targetSegments"></xsl:apply-templates>
                                    <mms:projections>
                                        <xsl:for-each select=".//mms:fixedValue">
                                            <mms:projection>
                                                <mms:variable><xsl:value-of select=".//mms:fixedVariable"/></mms:variable>
                                                <mms:terms>
                                                    <xsl:for-each select=".//mms:diagonalizationCoefficient">
                                                        <mms:term>
                                                            <xsl:apply-templates select="mms:field"></xsl:apply-templates>
                                                            <xsl:apply-templates select=".//mt:math"></xsl:apply-templates>
                                                        </mms:term>
                                                    </xsl:for-each>
                                                </mms:terms>
                                            </mms:projection>
                                        </xsl:for-each>
                                    </mms:projections>
                                </mms:fieldInteraction>
                            </xsl:for-each>
                            <xsl:for-each select="mms:segmentInteraction[mms:type = 'Characteristic']">
                                <mms:eigenVectorInteraction>
                                    <xsl:apply-templates select="mms:targetSegments"></xsl:apply-templates>
                                    <mms:projections>
                                        <xsl:for-each select=".//mms:fixedValue">
                                            <mms:projection>
                                                <mms:variable><xsl:value-of select=".//mms:fixedVariable"/></mms:variable>
                                                <xsl:apply-templates select="mms:diagonalizationCoefficients"></xsl:apply-templates>
                                            </mms:projection>
                                        </xsl:for-each>
                                    </mms:projections>
                                </mms:eigenVectorInteraction>
                            </xsl:for-each>
                        </mms:interactions>
                    </xsl:if>
                    <xsl:apply-templates select="mms:postInitialConditions"></xsl:apply-templates>
                    <xsl:apply-templates select="mms:interiorExecutionFlow"></xsl:apply-templates>
                    <xsl:apply-templates select="mms:surfaceExecutionFlow"></xsl:apply-templates>
                    <mms:discretizationInfo>
                        <xsl:apply-templates select="mms:discretizationInfo/mms:fieldGroup"></xsl:apply-templates>
                        <xsl:if test="mms:discretizationInfo/mms:name">
                            <mms:balanceLawPDEDiscretization>
                                <xsl:apply-templates select="mms:discretizationInfo/*"></xsl:apply-templates>
                            </mms:balanceLawPDEDiscretization>
                        </xsl:if>
                    </mms:discretizationInfo>
                </mms:subregion>
            </xsl:for-each>
            </mms:subregions>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="mms:modelCharDecompositions">
        <mms:modelCharacteristicDecompositions>
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </mms:modelCharacteristicDecompositions>
    </xsl:template>
    
    <xsl:template match="mms:modelCharDecomposition">
        <mms:modelCharacteristicDecomposition>
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </mms:modelCharacteristicDecomposition>
    </xsl:template>
    
    <xsl:template match="mms:charDecomposition">
        <mms:characteristicDecomposition>
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </mms:characteristicDecomposition>
    </xsl:template>
    
    <xsl:template match="mms:generalCharDecomp">
        <mms:generalCharacteristicDecomposition>
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </mms:generalCharacteristicDecomposition>
    </xsl:template>
    
    <xsl:template match="mms:coordCharDecomps">
        <mms:coordinateCharacteristicDecompositions>
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </mms:coordinateCharacteristicDecompositions>
    </xsl:template>
    
    <xsl:template match="mms:coordCharDecomp">
        <mms:coordinateCharacteristicDecomposition>
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </mms:coordinateCharacteristicDecomposition>
    </xsl:template>
    
    <xsl:template match="mms:vectorName">
        <mms:name><xsl:value-of select="."/></mms:name>
    </xsl:template>
    
    <xsl:template match="mms:auxiliaryVar">
        <mms:auxiliaryVariable><xsl:value-of select="."/></mms:auxiliaryVariable>
    </xsl:template>
    
    
    <xsl:template match="mms:segmentsPrecedence">
        <mms:subregionPrecedence>
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </mms:subregionPrecedence>
    </xsl:template>
    
    <xsl:template match="mms:segmentPrecedence">
        <mms:precedence>
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </mms:precedence>
    </xsl:template>
    
    <xsl:template match="mms:prevalentSegment">
        <mms:prevalentRegion><xsl:value-of select="."/></mms:prevalentRegion>
    </xsl:template>
    
    <xsl:template match="mms:surrogateSegment">
        <mms:surrogateRegion><xsl:value-of select="."/></mms:surrogateRegion>
    </xsl:template>
    
    <xsl:template match="mms:boundaries">
        <mms:boundaryConditions>
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </mms:boundaryConditions>
    </xsl:template>
    
    <xsl:template match="mms:boundaryDefinition">
        <mms:boundaryPolicy>
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </mms:boundaryPolicy>
    </xsl:template>
    
    <xsl:template match="mms:boundarySegmentGroups">
        <mms:boundaryRegions>
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </mms:boundaryRegions>
    </xsl:template>
    
    <xsl:template match="mms:segmentGroupName">
        <mms:regionName><xsl:value-of select="."/></mms:regionName>
    </xsl:template>
    
    <xsl:template match="mt:apply[sml:flux[count(*) = 0]]">
        <sml:flux>
            <mt:ci><xsl:value-of select=".//sml:flux"/></mt:ci>
            <xsl:apply-templates select="*[not(local-name() = 'flux')]"></xsl:apply-templates>
        </sml:flux>
    </xsl:template>
    
    <xsl:template match="mms:boundaryCondition">
        <mms:boundaryCondition>
            <mms:type>
                <xsl:choose>
                    <xsl:when test="mms:type = 'Periodical'">
                        <mms:periodical/>
                    </xsl:when>
                    <xsl:when test="mms:type = 'Flat'">
                        <mms:flat/>
                    </xsl:when>
                    <xsl:when test="mms:type = 'Static'">
                        <mms:static>
                            <xsl:apply-templates select=".//mms:initialConditions"/>
                        </mms:static>
                    </xsl:when>
                    <xsl:when test="mms:type = 'None'">
                        <mms:extrapolation/>
                    </xsl:when>
                    <xsl:when test="mms:type = 'Reflection'">
                        <mms:reflection>
                            <xsl:for-each select=".//mms:sign">
                                <xsl:apply-templates select="."></xsl:apply-templates>
                            </xsl:for-each>
                        </mms:reflection>
                    </xsl:when>
                    <xsl:when test="mms:type = 'Fixed'">
                        <mms:algebraic>
                            <xsl:for-each select=".//mms:fixedValue">
                                <mms:expression>
                                    <mms:field><xsl:value-of select=".//mms:fixedField"/></mms:field>
                                        <xsl:apply-templates select=".//mt:math" />
                                </mms:expression>
                            </xsl:for-each>
                        </mms:algebraic>
                    </xsl:when>
                    <xsl:when test="mms:type = 'Maximal dissipation'">
                        <mms:maximalDissipation>
                            <xsl:apply-templates select=".//mms:initialConditions"/>
                        </mms:maximalDissipation>
                    </xsl:when>
                </xsl:choose>
            </mms:type>
            <xsl:apply-templates select="mms:axis"></xsl:apply-templates>
            <xsl:apply-templates select="mms:side"></xsl:apply-templates>
            <xsl:apply-templates select="mms:fields"></xsl:apply-templates>
            <xsl:apply-templates select="mms:condition"></xsl:apply-templates>
        </mms:boundaryCondition>
    </xsl:template>
    
    <xsl:template  match="mms:segmentType">
        <mms:regionType><xsl:value-of select="."/></mms:regionType>
    </xsl:template>
    
    <xsl:template match="mms:mathML">
        <xsl:apply-templates select="*"></xsl:apply-templates>
    </xsl:template>    
    <xsl:template match="mms:instructionSet">
        <xsl:apply-templates select="*"></xsl:apply-templates>
    </xsl:template>
    
    <xsl:template  match="mms:targetSegments">
        <mms:targetRegions>
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </mms:targetRegions>
    </xsl:template>
    
    <xsl:template  match="mms:targetSegment">
        <mms:targetRegion><xsl:value-of select="."/></mms:targetRegion>
    </xsl:template>
    
    <xsl:template  match="mms:boundariesPrecedence">
        <mms:boundaryPrecedence>
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </mms:boundaryPrecedence>
    </xsl:template>
    
    <xsl:template  match="mms:boundaryId">
        <mms:boundary><xsl:value-of select="."/></mms:boundary>
    </xsl:template>
    
    <xsl:template  match="mms:segmentGroupRelations">
        <mms:regionRelations>
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </mms:regionRelations>
    </xsl:template>
    
    <xsl:template  match="mms:segmentGroupRelation">
        <mms:regionRelation>
            <xsl:apply-templates select="*"></xsl:apply-templates>
        </mms:regionRelation>
    </xsl:template>
    
    <xsl:template  match="mms:segmentGroupA">
        <mms:regionA><xsl:value-of select="."/></mms:regionA>
    </xsl:template>
    
    <xsl:template  match="mms:segmentGroupB">
        <mms:regionB><xsl:value-of select="."/></mms:regionB>
    </xsl:template>
    
    <xsl:template  match="mms:condition">
        <mms:applyIf>
            <xsl:apply-templates select=".//mt:math"></xsl:apply-templates>
        </mms:applyIf>
    </xsl:template>
    
    <xsl:template  match="mms:equations">
        <mms:mathExpressions>
            <xsl:apply-templates select=".//mt:math"></xsl:apply-templates>
        </mms:mathExpressions>
    </xsl:template>
    
    <xsl:template match="mms:fieldGroupName">
        <mms:name><xsl:value-of select="."/></mms:name>
    </xsl:template>
    <xsl:template match="mms:fieldGroupField">
        <mms:field><xsl:value-of select="."/></mms:field>
    </xsl:template>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
  
    
</xsl:stylesheet>