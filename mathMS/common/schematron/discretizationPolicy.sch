<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <ns uri="urn:mathms" prefix="mms"/>
    <pattern>
        <rule context="/mms:discretizationPolicy/mms:policies/mms:segmentPolicy/mms:schemas/mms:schema">
            <assert test="./mms:discretizationSchema">
                One used schema is not a discretization schema.
            </assert>
        </rule>
        <rule context="/mms:discretizationPolicy/mms:simProblem">
            <assert test="./mms:balanceLawPDEProblem">
                The document to discretize must be a simulation problem.
            </assert>
        </rule>
        <rule context="/mms:discretizationPolicy/mms:policies/mms:regionPolicy/mms:regionName">
            <assert test="./ancestor-or-self::mms:discretizationPolicy/mms:simProblem/mms:problemId//mms:balanceLawPDEProblem/mms:region/mms:name|./ancestor-or-self::mms:discretizationPolicy/mms:simProblem/mms:problemId//mms:balanceLawPDEProblem/mms:subregions/mms:subregion/mms:name = .">
                Some region does not exist.
            </assert>
        </rule>
        <rule context="/mms:discretizationPolicy/mms:policies/mms:regionPolicy/mms:parametersLists/mms:parametersList/mms:parameter/mms:paramName">
            <assert test="./ancestor-or-self::mms:regionPolicy/mms:schemas/mms:schema/mms:schemaId//mms:discretizationSchema/mms:schemaParameters/mms:schemaParameter/mms:paramName = .">
                Some parameter set does not exist in the schema.
            </assert>
        </rule>
        <rule context="/mms:discretizationPolicy/mms:policies/mms:regionPolicy/mms:parametersLists/mms:parametersList/mms:parameter/mms:paramValue">
            <assert test="./ancestor-or-self::mms:regionPolicy/mms:schemas/mms:schema/mms:schemaId//mms:discretizationSchema/mms:schemaParameters/mms:schemaParameter[mms:paramName = ./ancestor-or-self::mms:parameter/mms:paramName]/mms:values/mms:value = . or true()">
                Parameter value can be one of the existing ones.
            </assert>
        </rule>
        <rule context="/mms:discretizationPolicy/mms:policies/mms:regionPolicy/mms:fieldGroupMaps/mms:fieldGroupMap/mms:regionFieldGroupName">
            <assert test="./ancestor-or-self::mms:discretizationPolicy/mms:simProblem/mms:problemId//mms:balanceLawPDEProblem/mms:region/mms:fieldGroups/mms:fieldGroup/mms:name|./ancestor-or-self::mms:discretizationPolicy/mms:simProblem/mms:problemId//mms:balanceLawPDEProblem/mms:subregions/mms:subregion/mms:fieldGroups/mms:fieldGroup/mms:name = .">
                The field group names from the regions must exist in the problem.
            </assert>
        </rule>
        <rule context="/mms:discretizationPolicy/mms:policies/mms:regionPolicy/mms:fieldGroupMaps/mms:fieldGroupMap/mms:schemaFieldGroupName">
            <assert test="./ancestor-or-self::mms:regionPolicy/mms:schemas/mms:schema/mms:schemaId//mms:discretizationSchema/mms:schema/mms:fieldGroups/mms:groupName = .">
                The schema field group names must exist in the discretization schema.
            </assert>
        </rule>
        <rule context="/mms:discretizationPolicy/mms:policies/mms:regionPolicy/mms:fieldGroups/mms:fieldGroupName">
            <assert test="./ancestor-or-self::mms:discretizationPolicy/mms:simProblem/mms:problemId//mms:balanceLawPDEProblem/mms:region/mms:fieldGroups/mms:fieldGroup/mms:name|./ancestor-or-self::mms:discretizationPolicy/mms:simProblem/mms:problemId//mms:balanceLawPDEProblem/mms:subregions/mms:subregion/mms:fieldGroups/mms:fieldGroup/mms:name = .">
                The field group names from the regions must exist in the problem.
            </assert>
        </rule>
    </pattern>
</schema>