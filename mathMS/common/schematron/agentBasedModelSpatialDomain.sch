<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <ns uri="urn:mathms" prefix="mms"/>
    <pattern>
        <rule context="/mms:agentBasedModelSpatialDomain/mms:rules/mms:gatherRules/mms:gatherRule/mms:agentProperty">
            <assert test="/mms:agentBasedModelSpatialDomain/mms:agentProperties/mms:agentProperty = .">
                Gather agent properties must be model agent properties.
            </assert>
        </rule>
        <rule context="/mms:agentBasedModelSpatialDomain/mms:rules/mms:updateRules/mms:updateRule/mms:agentProperty">
            <assert test="/mms:agentBasedModelSpatialDomain/mms:agentProperties/mms:agentProperty = .">
                Update agent properties must be model agent properties.
            </assert>
        </rule>
        <rule context="/mms:agentBasedModelSpatialDomain/mms:ruleExecutionOrder/mms:rule">
            <assert test="/mms:agentBasedModelSpatialDomain/mms:rules/mms:updateRules/mms:updateRule/mms:name|/mms:agentBasedModelSpatialDomain/mms:rules/mms:gatherRules/mms:gatherRule/mms:name|/mms:agentBasedModelSpatialDomain/mms:topologyChange/mms:name = .">
                The rules in the execution order must be gather or update rules.
            </assert>
        </rule>
        <rule context="/mms:agentBasedModelSpatialDomain/mms:rules">
            <assert test="count(*) > 0">
                At least one gather or update rule must exist.
            </assert>
            <assert test="count(*/*) = count(/mms:agentBasedModelSpatialDomain/mms:ruleExecutionOrder/mms:rule[not(text() ='Topology change')])">
                All gather/update rules must appear once in the rule execution order.
            </assert>
        </rule> 
        <rule context="/mms:agentBasedModelSpatialDomain/mms:ruleExecutionOrder/mms:rule">
            <assert test="./text() = (/mms:agentBasedModelSpatialDomain/mms:rules/mms:gatherRules/mms:gatherRule/mms:name/text()|/mms:agentBasedModelSpatialDomain/mms:rules/mms:updateRules/mms:updateRule/mms:name/text()|/mms:agentBasedModelSpatialDomain/mms:topologyChange/mms:name/text())">
                Every rule in the execution order must be a gather or update rule.
            </assert>
        </rule>
    </pattern>
</schema>