<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <ns uri="urn:mathms" prefix="mms"/>
    <pattern>
        <rule context="/mms:agentBasedProblemSpatialDomain/mms:modelImport">
            <assert test="./mms:agentBasedModelSpatialDomain">
                The imported model must be an agent based model on spatial domain.
            </assert>
        </rule>
        <rule context="/mms:agentBasedProblemSpatialDomain/mms:model/mms:agentPropertyEquivalence/mms:equivalence/mms:problemElement">
            <assert test="/mms:agentBasedProblemSpatialDomain/mms:agentProperties/mms:agentProperty = .">
                Problem equivalence agent properties must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:agentBasedProblemSpatialDomain/mms:model/mms:agentPropertyEquivalence/mms:equivalence/mms:modelElement">
            <assert test="./ancestor-or-self::mms:model/mms:id//mms:agentBasedModelSpatialDomain/mms:agentProperties/mms:agentProperty = .">
                Model equivalence agent properties must exist in the model.
            </assert>
        </rule>
        <rule context="/mms:agentBasedProblemSpatialDomain/mms:model/mms:coordinateEquivalence/mms:equivalence/mms:problemElement">
            <assert test="/mms:agentBasedProblemSpatialDomain/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate|/mms:agentBasedProblemSpatialDomain/mms:coordinates/mms:timeCoordinate = .">
                Problem equivalence coordinates must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:agentBasedProblemSpatialDomain/mms:model/mms:coordinateEquivalence/mms:equivalence/mms:modelElement">
            <assert test="./ancestor-or-self::mms:model/mms:id//mms:agentBasedModelSpatialDomain/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate|./ancestor-or-self::mms:coordinateEquivalence/mms:modelId//mms:agentBasedModelSpatialDomain/mms:coordinates/mms:timeCoordinate = .">
                Model equivalence coordinates must exist in the model.
            </assert>
        </rule>
        <rule context="/mms:agentBasedProblemSpatialDomain/mms:model/mms:parameterEquivalence/mms:equivalence/mms:problemElement">
            <assert test="/mms:agentBasedProblemSpatialDomain/mms:parameters/mms:parameter/mms:name = .">
                Problem equivalence parameters must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:agentBasedProblemSpatialDomain/mms:model/mms:parameterEquivalence/mms:equivalence/mms:modelElement">
            <assert test="./ancestor-or-self::mms:model/mms:id//mms:agentBasedModelSpatialDomain/mms:parameters/mms:parameter/mms:name = .">
            Model equivalence parameters must exist in the problem.
            </assert>
        </rule>
        <rule context="/mms:agentBasedProblemSpatialDomain/mms:mesh/mms:spatialDomain">
            <assert test="count(*) = count(/mms:agentBasedProblemSpatialDomain/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate)">
                Every coordinate must have a mesh limit definition.
            </assert>
        </rule>
        <rule context="/mms:agentBasedProblemSpatialDomain/mms:mesh/mms:spatialDomain/mms:coordinateLimits/mms:coordinate">
            <assert test="/mms:agentBasedProblemSpatialDomain/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate = .">
                Domain coordinates must be defined in the problem.
            </assert>
        </rule>  
        <rule context="/mms:agentBasedProblemSpatialDomain/mms:boundaryConditions/mms:boundaryCondition/mms:agentProperties/mms:agentProperty">
            <assert test="/mms:agentBasedProblemSpatialDomain/mms:agentProperties/mms:agentProperty = .">
                Problem boundary agent properties must be defined in the problem.
            </assert>
        </rule>  
        <rule context="/mms:agentBasedProblemSpatialDomain/mms:mesh/mms:motion/mms:agentSpeed/mms:agentProperty">
            <assert test="/mms:agentBasedProblemSpatialDomain/mms:agentProperties/mms:agentProperty = .">
                Agent speeds must use problem agent properties.
            </assert>
        </rule>  
        <rule context="/mms:agentBasedProblemSpatialDomain/mms:mesh/mms:motion/mms:agentSpeed/mms:coordinate">
            <assert test="/mms:agentBasedProblemSpatialDomain/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate = .">
                Agent speed coordinates must be defined in the problem.
            </assert>
        </rule>  
        <rule context="/mms:agentBasedProblemSpatialDomain/mms:boundaryConditions/mms:boundaryCondition/mms:axis">
            <assert test="/mms:agentBasedProblemSpatialDomain/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate = . or 'All' = .">
                The boundary axis must be an spatial coordinate or 'All'.
            </assert>
        </rule>
        <rule context="/mms:agentBasedProblemSpatialDomain/mms:boundaryPrecedence">
            <assert test="count(*) = 2*count(/mms:agentBasedProblemSpatialDomain/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate)">
                Every spatial coordinate must have a boundary precedence pair (Lower, Upper).
            </assert>
        </rule>
        <rule context="/mms:agentBasedProblemSpatialDomain/mms:mesh/mms:type">
            <assert test="./text() = 'Unstructured' or count(/mms:agentBasedProblemSpatialDomain/mms:mesh/mms:motion) = 0">
                Structured mesh is incompatible with motion.
            </assert>
        </rule> 
        <rule context="/mms:agentBasedProblemSpatialDomain/mms:mesh/mms:motion">
            <assert test="count(*) = 0 or count(*) = count(/mms:agentBasedProblemSpatialDomain/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate)">
                In case motion is set, every spatial coordinate must have a speed agent property for the motion.
            </assert>
        </rule> 
        <rule context="/mms:agentBasedProblemSpatialDomain/mms:boundaryPrecedence/mms:boundary">
            <assert test="/mms:agentBasedProblemSpatialDomain/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate/concat(text(), '-Lower') = . or /mms:agentBasedProblemSpatialDomain/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate/concat(text(), '-Upper') = .">
                The boundary precedence must be [spatialCoordinate]-Lower or [spatialCoordinate]-Upper.
            </assert>
        </rule>
        <rule context="/mms:agentBasedProblemSpatialDomain/mms:boundaryPrecedence">
            <assert test="count(./*) = 2 * count(/mms:agentBasedProblemSpatialDomain/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate)">
                If boundary precedence is set, all boundaries must be added to the precedence definition.
            </assert>
        </rule>
    </pattern>
</schema>
