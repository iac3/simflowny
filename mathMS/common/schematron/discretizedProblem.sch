<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <ns uri="urn:mathms" prefix="mms"/>
    <pattern>
        <rule context="/mms:discretizedProblem/mms:tensors/mms:tensor/mms:fields/mms:field">
            <assert test="/mms:discretizedProblem/mms:fields/mms:field = .">
                Tensor field does not exist.
            </assert>
        </rule>
        <rule context="/mms:discretizedProblem/mms:subregionPrecedence/mms:precedence/mms:prevalentRegion">
            <assert test="/mms:discretizedProblem/mms:region/mms:name|/mms:discretizedProblem/mms:subregions/mms:subregion/mms:name = .">
                Region precedence have to use a valid problem region name.
            </assert>
        </rule>
        <rule context="/mms:discretizedProblem/mms:subregionPrecedence/mms:precedence/mms:surrogateRegion">
            <assert test="/mms:discretizedProblem/mms:region/mms:name|/mms:discretizedProblem/mms:subregions/mms:subregion/mms:name = .">
                Region precedence have to use a valid problem region name.
            </assert>
        </rule>
        <rule context="/mms:discretizedProblem/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition/mms:fields/mms:field">
            <assert test="/mms:discretizedProblem/mms:fields/mms:field|/mms:discretizedProblem/mms:auxiliaryFields/mms:auxiliaryField = .">
                Problem boundary fields must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:discretizedProblem/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition/mms:type/mms:algebraic/mms:expression/mms:field">
            <assert test="/mms:discretizedProblem/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition/mms:fields/mms:field = .">
                All fields used in the boundary must have an algebraic expression.
            </assert>
        </rule> 
        <rule context="/mms:discretizedProblem/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition/mms:type/mms:reflection/mms:signs/mms:sign/mms:field">
            <assert test="/mms:discretizedProblem/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition/mms:fields/mms:field = .">
                All fields used in the boundary must have a sign.
            </assert>
        </rule> 
        <rule context="/mms:simulationProblem/mms:boundaryPrecedence">
            <assert test="count(*) = 2*count(/mms:discretizedProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate)">
                If boundary precedence is set, all boundaries must be added to the precedence definition.
            </assert>
        </rule>
        <rule context="/mms:simulationProblem/mms:boundaryPrecedence/mms:boundary">
            <assert test="/mms:simulationProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate/concat(text(), '-Lower') = . or /mms:simulationProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate/concat(text(), '-Upper') = .">
                The boundary precedence must be [spatialCoordinate]-Lower or [spatialCoordinate]-Upper.
            </assert>
        </rule>
        <rule context="/mms:discretizedProblem/mms:regionRelations/mms:regionRelation/mms:regionA">
            <assert test="/mms:discretizedProblem/mms:region/mms:name|/mms:discretizedProblem/mms:subregions/mms:subregion/mms:name = .">
                Region relations have to use a valid problem region name.
            </assert>
        </rule>        
        <rule context="/mms:discretizedProblem/mms:regionRelations/mms:regionRelation/mms:regionB">
            <assert test="/mms:discretizedProblem/mms:region/mms:name|/mms:discretizedProblem/mms:subregions/mms:subregion/mms:name = .">
                Region relations have to use a valid problem region name.
            </assert>
        </rule>  
    </pattern>
</schema>
