<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <ns uri="urn:mathms" prefix="mms"/>
    <pattern>
        <rule context="/mms:PDEDiscretizationSchema/mms:schema/mms:step/mms:timeDiscretization/mms:transformationRule">
            <assert test="./mms:transformationRule">
                The time discretization step must have a transformation rule.
            </assert>
        </rule>
        <rule context="/mms:PDEDiscretizationSchema/mms:schema/mms:step/mms:dissipation/mms:transformationRule">
            <assert test="./mms:transformationRule">
                The dissipation must have a transformation rule.
            </assert>
        </rule>    
        <rule context="/mms:PDEDiscretizationSchema/mms:schema/mms:step/mms:spatialDiscretization/mms:conservativeTermDiscretization/mms:discretization/mms:transformationRule">
            <assert test="./mms:transformationRule">
                The conservative discretization must have a transformation rule.
            </assert>
        </rule>
    </pattern>
</schema>