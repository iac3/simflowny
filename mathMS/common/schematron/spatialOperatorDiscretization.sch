<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <ns uri="urn:mathms" prefix="mms"/>
    <pattern>
        <rule context="/mms:spatialOperatorDiscretization/mms:discretizations/mms:discretization/mms:derivativeType/mms:coordinateCombination">
            <assert test="matches(./text(), '^i[ijk]*$')">
                The coordinate combination for the derivatives must match the following format: i[ijk]*.
            </assert>
        </rule>
        <rule context="/mms:spatialOperatorDiscretization/mms:discretizations/mms:discretization/mms:mainSchema">
            <assert test="./mms:transformationRule">
                The spatial discretization main schema must be a transformation rule.
            </assert>
        </rule>
        <rule context="/mms:spatialOperatorDiscretization/mms:discretizations/mms:discretization/mms:boundarySchemas/mms:boundarySchema/mms:conditions/mms:condition/mms:axis">
            <assert test="matches(./text(), '^[ijk]$')">
                Each boundary schema must refer to only one coordinate.
            </assert>
            <assert test="contains(./ancestor::mms:discretization/mms:derivativeType/mms:coordinateCombination/text(), ./text()) = true()">
                The asymmetry axis must be a used in the parent coordinate combination discretization.
            </assert>
        </rule>
    </pattern>
</schema>