<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <ns uri="urn:mathms" prefix="mms"/>
    <pattern>
        <rule context="/mms:agentBasedProblemGraph/mms:modelImport">
            <assert test="./mms:agentBasedModelGraph">
                The imported model must be an agent based model on graph.
            </assert>
        </rule>
        <rule context="/mms:agentBasedProblemGraph/mms:model/mms:vertexPropertyEquivalence/mms:equivalence/mms:problemElement">
            <assert test="/mms:agentBasedProblemGraph/mms:vertexProperties/mms:vertexProperty = .">
                Problem equality fields must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:agentBasedProblemGraph/mms:model/mms:vertexPropertyEquivalence/mms:equivalence/mms:modelElement">
            <assert test="./ancestor-or-self::mms:model/mms:id//mms:agentBasedModelGraph/mms:vertexProperties/mms:vertexProperty = .">
                Model equality fields must be defined in the problem.
            </assert>
        </rule>        
        <rule context="/mms:agentBasedProblemGraph/mms:model/mms:edgePropertyEquivalence/mms:equivalence/mms:problemElement">
            <assert test="/mms:agentBasedProblemGraph/mms:edgeProperties/mms:edgeProperty = .">
                Problem equality interaction fields must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:agentBasedProblemGraph/mms:model/mms:edgePropertyEquivalence/mms:equivalence/mms:modelElement">
            <assert test="./ancestor-or-self::mms:model/mms:id//mms:agentBasedModelGraph/mms:edgeProperties/mms:edgeProperty = .">
                Model equality interaction fields must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:agentBasedProblemGraph/mms:model/mms:parameterEquivalence/mms:equivalence/mms:problemElement">
            <assert test="/mms:agentBasedProblemGraph/mms:parameters/mms:parameter/mms:name = .">
                Problem equality parameters must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:agentBasedProblemGraph/mms:model/mms:parameterEquivalence/mms:equivalence/mms:modelElement">
            <assert test="./ancestor-or-self::mms:model/mms:id//mms:agentBasedModelGraph/mms:parameters/mms:parameter/mms:name = .">
                Model equality parameters must be defined in the problem.
            </assert>
        </rule>
    </pattern>
</schema>
