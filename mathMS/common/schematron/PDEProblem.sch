<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <ns uri="urn:mathms" prefix="mms"/>
    <pattern>
        <rule context="/mms:PDEProblem/mms:models/mms:model">
            <assert test="./mms:PDEModel">
                The imported model must be a non conservative model.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:models/mms:model/mms:coordinateEquivalence/mms:modelId">
            <assert test="/mms:PDEProblem/mms:models/mms:model/mms:id = .">
                Coordinate equivalences model Id not imported in problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:models/mms:model/mms:fieldEquivalence/mms:modelId">
            <assert test="/mms:PDEProblem/mms:models/mms:model/mms:id = .">
                Field equivalences model Id not imported in problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:models/mms:model/mms:parameterEquivalence/mms:modelId">
            <assert test="/mms:PDEProblem/mms:models/mms:model/mms:id = .">
                Parameter equivalences model Id not imported in problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:region/mms:interiorModels/mms:interiorModel">
            <assert test="/mms:PDEProblem/mms:models/mms:model/mms:name = .">
                Region model not imported in problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:subregions/mms:subregion/mms:interiorModels/mms:interiorModel">
            <assert test="/mms:PDEProblem/mms:models/mms:model/mms:name = .">
                Subregion model not imported in problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:subregions/mms:subregion/mms:surfaceModels/mms:surfaceModel">
            <assert test="/mms:PDEProblem/mms:models/mms:model/mms:name = .">
                Subregion model not imported in problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:region/mms:fieldGroups/mms:fieldGroup/mms:field">
            <assert test="/mms:PDEProblem/mms:fields/mms:field|/mms:PDEProblem/mms:auxiliaryFields/mms:auxiliaryField = .">
                Fields in field groups must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:subregions/mms:subregion/mms:fieldGroups/mms:fieldGroup/mms:field">
            <assert test="/mms:PDEProblem/mms:fields/mms:field|/mms:PDEProblem/mms:auxiliaryFields/mms:auxiliaryField = .">
                Fields in field groups must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:models/mms:model/mms:coordinateEquivalence/mms:equivalence/mms:problemElement">
            <assert test="/mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate|/mms:PDEProblem/mms:coordinates/mms:timeCoordinate = .">
                Problem equivalent coordinates must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:models/mms:model/mms:coordinateEquivalence/mms:equivalence/mms:modelElement">
           <assert test="./ancestor-or-self::mms:model/mms:id//mms:PDEModel/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate|./ancestor-or-self::mms:model/mms:id//mms:PDEModel/mms:coordinates/mms:timeCoordinate = .">
                Model equivalent fields must exist in the model.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:models/mms:model/mms:fieldEquivalence/mms:equivalence/mms:problemElement">
            <assert test="/mms:PDEProblem/mms:fields/mms:field|/mms:PDEProblem/mms:auxiliaryFields/mms:auxiliaryField = .">
                Problem equivalent fields must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:models/mms:model/mms:variableEquivalence/mms:equivalence/mms:problemElement">
            <assert test="/mms:PDEProblem/mms:auxiliaryVariables/mms:auxiliaryVariable = .">
                Problem equivalent variables must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:models/mms:model/mms:fieldEquivalence/mms:equivalence/mms:modelElement">
            <assert test="./ancestor-or-self::mms:model/mms:id//mms:PDEModel/mms:fields/mms:field|./ancestor-or-self::mms:PDEModel/mms:modelId//mms:PDEModel/mms:auxiliaryFields/mms:auxiliaryField = .">
                Model equivalent fields must exist in the model.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:models/mms:model/mms:variableEquivalence/mms:equivalence/mms:modelElement">
            <assert test="./ancestor-or-self::mms:model/mms:id//mms:PDEModel/mms:auxiliaryVariables/mms:auxiliaryVariable = .">
                Model equivalent variables must exist in the model.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:models/mms:model/mms:parameterEquivalence/mms:equivalence/mms:problemElement">
            <assert test="/mms:PDEProblem/mms:parameters/mms:parameter/mms:name = .">
                Problem equivalent parameters must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:models/mms:model/mms:parameterEquivalence/mms:equivalence/mms:modelElement">
            <assert test="./ancestor-or-self::mms:model/mms:id//mms:PDEModel/mms:parameters/mms:parameter/mms:name = .">
                Model equivalent parameters must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:models/mms:model/mms:tensorEquivalence/mms:equivalence/mms:problemElement">
            <assert test="/mms:PDEProblem/mms:tensors/mms:tensor/mms:name = .">
                Problem equivalence tensors must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:tensors/mms:tensor/mms:fields/mms:field">
            <assert test="/mms:PDEProblem/mms:fields/mms:field|/mms:PDEProblem/mms:auxiliaryFields/mms:auxiliaryField = .">
                Tensor fields must be defined in the problem.
            </assert>
        </rule>        
        <rule context="/mms:PDEProblem/mms:models/mms:model/mms:tensorEquivalence/mms:equivalence/mms:modelElement">
            <assert test="./ancestor-or-self::mms:model/mms:id//mms:PDEModel/mms:tensors/mms:tensor/mms:name = .">
                Model equivalence parameters must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:models/mms:model/mms:eigenVectorEquivalence/mms:equivalence/mms:problemElement">
            <assert test="/mms:PDEProblem/mms:eigenVectors/mms:eigenVector = .">
                Problem equivalence eigenVectors must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:models/mms:model/mms:eigenVectorEquivalence/mms:equivalence/mms:modelElement">
            <assert test="./ancestor-or-self::mms:model/mms:id//mms:PDEModel/mms:characteristicDecomposition/mms:generalCharacteristicDecomposition/mms:eigenSpaces/mms:eigenSpace/mms:eigenVectors/mms:eigenVector/mms:name|./ancestor-or-self::mms:model/mms:id//mms:PDEModel/mms:characteristicDecomposition/mms:coordinateCharacteristicDecompositions/mms:coordinateCharacteristicDecomposition/mms:eigenSpaces/mms:eigenSpace/mms:eigenVectors/mms:eigenVector/mms:name = .">
                Model equivalence vectors must be defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:region/mms:spatialDomain/mms:coordinateLimits/mms:coordinate">
            <assert test="/mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate = .">
                Coordinate limits must use valid spatial coordinates.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:subregions/mms:subregion/mms:location/mms:spatialDomain/mms:coordinateLimits/mms:coordinate">
            <assert test="/mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate = .">
                Coordinate limits must use valid spatial coordinates.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:subregions/mms:subregion/mms:interactions/mms:fieldInteraction/mms:targetRegions/mms:targetRegion">
            <assert test="/mms:PDEProblem/mms:subregions/mms:subregion/mms:name|/mms:PDEProblem/mms:region/mms:name = . or /mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate/concat(text(), '-Lower') = . or /mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate/concat(text(), '-Upper') = .">
                Target regions for interactions must exist.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:subregions/mms:subregion/mms:interactions/mms:eigenVectorInteraction/mms:targetRegions/mms:targetRegion">
            <assert test="/mms:PDEProblem/mms:subregions/mms:subregion/mms:name|/mms:PDEProblem/mms:region/mms:name = . or /mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate/concat(text(), '-Lower') = . or /mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate/concat(text(), '-Upper') = .">
                Target regions for interactions must exist.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:subregions/mms:subregion/mms:interactions/mms:interaction/mms:targetRegions/mms:targetRegion">
            <assert test="/mms:PDEProblem/mms:subregions/mms:subregion/mms:name|/mms:PDEProblem/mms:region/mms:name = . or /mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate/concat(text(), '-Lower') = . or /mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate/concat(text(), '-Upper') = .">
                Target regions for interactions must exist.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:region/mms:interactions/mms:fieldInteraction/mms:targetRegions/mms:targetRegion">
            <assert test="/mms:PDEProblem/mms:subregions/mms:subregion/mms:name|/mms:PDEProblem/mms:region/mms:name = . or /mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate/concat(text(), '-Lower') = . or /mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate/concat(text(), '-Upper') = .">
                Target regions for interactions must exist.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:region/mms:interactions/mms:eigenVectorInteraction/mms:targetRegions/mms:targetRegion">
            <assert test="/mms:PDEProblem/mms:subregions/mms:subregion/mms:name|/mms:PDEProblem/mms:region/mms:name = . or /mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate/concat(text(), '-Lower') = . or /mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate/concat(text(), '-Upper') = .">
                Target regions for interactions must exist.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:region/mms:interactions/mms:interaction/mms:targetRegions/mms:targetRegion">
            <assert test="/mms:PDEProblem/mms:subregions/mms:subregion/mms:name|/mms:PDEProblem/mms:region/mms:name = . or /mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate/concat(text(), '-Lower') = . or /mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate/concat(text(), '-Upper') = .">
                Target regions for interactions must exist.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:region/mms:interactions/mms:fieldInteraction/mms:projections/mms:projection/mms:variable">
            <assert test="/mms:PDEProblem/mms:fields/mms:field|/mms:PDEProblem/mms:auxiliaryFields/mms:auxiliaryField = .">
                Projection variables in interactions must exist.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:region/mms:interactions/mms:eigenVectorInteraction/mms:projections/mms:projection/mms:variable">
            <assert test="/mms:PDEProblem/mms:fields/mms:field|/mms:PDEProblem/mms:auxiliaryFields/mms:auxiliaryField = .">
                Projection variables in interactions must exist.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:region/mms:interactions/mms:interaction/mms:projections/mms:projection/mms:variable">
            <assert test="/mms:PDEProblem/mms:fields/mms:field|/mms:PDEProblem/mms:auxiliaryFields/mms:auxiliaryField = .">
                Projection variables in interactions must exist.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:subregions/mms:subregion/mms:interactions/mms:fieldInteraction/mms:projections/mms:projection/mms:variable">
            <assert test="/mms:PDEProblem/mms:fields/mms:field|/mms:PDEProblem/mms:auxiliaryFields/mms:auxiliaryField = .">
                Projection variables in interactions must exist.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:subregions/mms:subregion/mms:interactions/mms:eigenVectorInteraction/mms:projections/mms:projection/mms:variable">
            <assert test="/mms:PDEProblem/mms:fields/mms:field|/mms:PDEProblem/mms:auxiliaryFields/mms:auxiliaryField = .">
                Projection variables in interactions must exist.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:subregions/mms:subregion/mms:interactions/mms:interaction/mms:projections/mms:projection/mms:variable">
            <assert test="/mms:PDEProblem/mms:fields/mms:field|/mms:PDEProblem/mms:auxiliaryFields/mms:auxiliaryField = .">
                Projection variables in interactions must exist.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:region/mms:interactions/mms:fieldInteraction/mms:projections/mms:projection/mms:terms/mms:term/mms:field">
            <assert test="/mms:PDEProblem/mms:fields/mms:field|/mms:PDEProblem/mms:auxiliaryFields/mms:auxiliaryField = .">
                Projection variables in interactions must exist.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:region/mms:interactions/mms:eigenVectorInteraction/mms:projections/mms:projection/mms:diagonalizationCoefficients/mms:diagonalizationCoefficient/mms:field">
            <assert test="/mms:PDEProblem/mms:eigenVectors/mms:eigenVector = .">
                Projection variables in interactions must exist.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:subregions/mms:subregion/mms:interactions/mms:fieldInteraction/mms:projections/mms:projection/mms:terms/mms:term/mms:field">
            <assert test="/mms:PDEProblem/mms:fields/mms:field|/mms:PDEProblem/mms:auxiliaryFields/mms:auxiliaryField = .">
                Projection variables in interactions must exist.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:subregions/mms:subregion/mms:interactions/mms:eigenVectorInteraction/mms:projections/mms:projection/mms:terms/mms:term/mms:field">
            <assert test="/mms:PDEProblem/mms:eigenVectors/mms:eigenVector = .">
                Projection variables in interactions must exist.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:subregionPrecedence/mms:precedence/mms:prevalentRegion">
            <assert test="/mms:PDEProblem/mms:subregions/mms:subregion/mms:name = .">
                Subregion precedence have to use a valid subregion name.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:subregionPrecedence/mms:precedence/mms:surrogateRegion">
            <assert test="/mms:PDEProblem/mms:subregions/mms:subregion/mms:name = .">
                Subregion precedence have to use a valid subregion name.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:analysisFieldEquations/mms:analysisFieldEquation/mms:analysisField">
            <assert test="/mms:PDEProblem/mms:analysisFields/mms:analysisField = .">
                Analysis field equations must use analysis fields defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:auxiliaryAnalysisVariableEquations/mms:auxiliaryAnalysisVariableEquation/mms:auxiliaryAnalysisVariable">
            <assert test="/mms:PDEProblem/mms:auxiliaryAnalysisVariables/mms:auxiliaryAnalysisVariable = .">
                Auxiliary analysis variable equations must use auxiliary analysis variables defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryRegions/mms:regionName">
            <assert test="/mms:PDEProblem/mms:region/mms:name|/mms:PDEProblem/mms:subregions/mms:subregion/mms:name = .">
                Boundary conditions have to be applied to a valid region name.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition/mms:fields/mms:field">
            <assert test="/mms:PDEProblem/mms:fields/mms:field|/mms:PDEProblem/mms:auxiliaryFields/mms:auxiliaryField = .">
                Boundary fields must be defined in the problem.
            </assert>
        </rule>  
        <rule context="/mms:PDEProblem/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition/mms:axis">
            <assert test="/mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate = . or 'All' = .">
                Boundary axis must be an spatial coordinate or 'All'.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition/mms:type/mms:algebraic/mms:expression/mms:field">
            <assert test="/mms:PDEProblem/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition/mms:fields/mms:field = .">
                Only fields can be fixed within the boundary using them.
            </assert>
        </rule> 
        <rule context="/mms:PDEProblem/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition/mms:type/mms:reflection/mms:sign/mms:field">
            <assert test="/mms:PDEProblem/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition/mms:fields/mms:field = .">
                Only fields can be signed within the boundary using them.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition/mms:type/mms:periodical">
            <assert test="./ancestor::mms:boundaryCondition/mms:side/text() = 'All'">
                Periodical boundaries must be defined in 'All' sides of the axis.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:boundaryPrecedence">
            <assert test="count(./*) = 2 * count(/mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate)">
                If boundary precedence is set, all boundaries must be added to the precedence definition.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:boundaryPrecedence/mms:boundary">
            <assert test="/mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate/concat(text(), '-Lower') = . or /mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate/concat(text(), '-Upper') = .">
                The boundary precedence must be [spatialCoordinate]-Lower or [spatialCoordinate]-Upper.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:analysisFields/mms:analysisField">
            <assert test=". = /mms:PDEProblem/mms:analysisFieldEquations/mms:analysisFieldEquation/mms:analysisField">
                Every analysis field must have an analysis field equation defined in the problem.
            </assert>
        </rule>
        <rule context="/mms:PDEProblem/mms:auxiliaryAnalysisVariables/mms:auxiliaryAnalysisVariable">
            <assert test=". = /mms:PDEProblem/mms:auxiliaryAnalysisVariableEquations/mms:auxiliaryAnalysisVariableEquation/mms:auxiliaryAnalysisVariable">
                Every auxiliary analysis variable must have an auxiliary analysis variable equation defined in the problem.
            </assert>
        </rule>
    </pattern>
</schema>