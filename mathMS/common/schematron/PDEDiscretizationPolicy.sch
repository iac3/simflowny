<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <ns uri="urn:mathms" prefix="mms"/>
    <ns uri="urn:simml" prefix="sml"/>
    <pattern>
        <rule context="/mms:PDEDiscretizationPolicy/mms:regionDiscretizations/mms:regionDiscretization/mms:timeIntegrationSchema">
            <assert test="./mms:PDEDiscretizationSchema">
                The time integration schema is not a valid discretization schema.
            </assert>
        </rule>
        <rule context="/mms:PDEDiscretizationPolicy/mms:problem">
            <assert test="./mms:PDEProblem">
                The document to discretize must be a non conservative problem.
            </assert>
        </rule>
        <rule context="/mms:PDEDiscretizationPolicy/mms:regionDiscretizations/mms:regionDiscretization/mms:region">
           <assert test="./ancestor-or-self::mms:PDEDiscretizationPolicy/mms:problem/mms:id//mms:PDEProblem/mms:region/mms:name|./ancestor-or-self::mms:PDEDiscretizationPolicy/mms:problem/mms:id//mms:PDEProblem/mms:subregions/mms:subregion/mms:name = .">
                Region does not exist in the problem.
            </assert>
        </rule>
        <rule context="/mms:PDEDiscretizationPolicy/mms:regionDiscretizations/mms:regionDiscretization/mms:operatorDiscretizations/mms:operatorDiscretization/mms:operatorName">
           <assert
              test="./ancestor-or-self::mms:PDEDiscretizationPolicy/mms:problem/mms:id//mms:PDEProblem/mms:region/mms:interactions/mms:interaction/mms:projections/mms:projection/mms:operator/mms:name
              = . or
              ./ancestor-or-self::mms:PDEDiscretizationPolicy/mms:problem/mms:id//mms:PDEProblem/mms:subregions/mms:subregion/mms:interactions/mms:interaction/mms:projections/mms:projection/mms:operator/mms:name
              = . or
              ./ancestor-or-self::mms:PDEDiscretizationPolicy/mms:problem/mms:id//mms:PDEProblem/mms:models/mms:model/mms:id//mms:PDEModel/mms:evolutionEquations/mms:evolutionEquation/mms:operator/mms:name
              = . or
              ./ancestor-or-self::mms:PDEDiscretizationPolicy/mms:problem/mms:id//mms:PDEProblem/mms:models/mms:model/mms:id//mms:PDEModel/mms:auxiliaryVariableEquations/mms:auxiliaryVariableEquation/mms:Definition/mms:algebraic/mms:operator/mms:name
              = . or ./ancestor-or-self::mms:PDEDiscretizationPolicy/mms:problem/mms:id//mms:PDEProblem/mms:models/mms:model/mms:id//mms:PDEModel/mms:auxiliaryFieldEquations/mms:auxiliaryFieldEquation/mms:definition/mms:algebraic/mms:operator/mms:name = .">
                Operator does not exist in any model.
            </assert>
        </rule>
        <rule context="/mms:PDEDiscretizationPolicy/mms:regionDiscretizations/mms:regionDiscretization/mms:operatorDiscretizations/mms:operatorDiscretization/mms:operatorPolicy">
            <assert test="./mms:spatialOperatorDiscretization">
                The operator policy imported document is not a valid spatial operator discretization.
            </assert>
        </rule>
       <rule context="/mms:PDEDiscretizationPolicy/mms:meshVariables/mms:meshVariable">
           <assert test="./ancestor-or-self::mms:PDEDiscretizationPolicy/mms:problem/mms:id//mms:PDEProblem/mms:fields/mms:field = . or ./ancestor-or-self::mms:PDEDiscretizationPolicy/mms:problem/mms:id//mms:PDEProblem/mms:auxiliaryFields/mms:auxiliaryField = . or 'All' = .">
             Field does not exist in problem.
          </assert>
       </rule>
       <rule context="/mms:PDEDiscretizationPolicy/mms:particleVariables/mms:species/mms:particleVariable">
           <assert test="./ancestor-or-self::mms:PDEDiscretizationPolicy/mms:problem/mms:id//mms:PDEProblem/mms:fields/mms:field = . or ./ancestor-or-self::mms:PDEDiscretizationPolicy/mms:problem/mms:id//mms:PDEProblem/mms:auxiliaryFields/mms:auxiliaryField = . or 'All' = .">
             Field does not exist in problem.
          </assert>
       </rule>
       <rule context="/mms:PDEDiscretizationPolicy/mms:particleVariables/mms:species/mms:velocities/mms:velocity/mms:component">
          <assert test="./ancestor-or-self::mms:PDEDiscretizationPolicy/mms:problem/mms:id//mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate = .">
             Velocity component must be a problem coordinate.
          </assert>
       </rule>
        <rule context="/mms:PDEDiscretizationPolicy/mms:analysisDiscretization/mms:operatorDiscretization/mms:operatorName">
            <assert test="./ancestor-or-self::mms:PDEDiscretizationPolicy/mms:problem/mms:id//mms:PDEProblem/mms:analysisFieldEquations/mms:analysisFieldEquation/mms:operator/mms:name = . or ./ancestor-or-self::mms:PDEDiscretizationPolicy/mms:problem/mms:id//mms:PDEProblem/mms:auxiliaryAnalysisVariableEquations/mms:auxiliaryAnalysisVariableEquation/mms:operator/mms:name = .">
                Operator does not exist in any problems model.
            </assert>
        </rule>
        <rule context="/mms:PDEDiscretizationPolicy/mms:analysisDiscretization/mms:operatorDiscretization/mms:operatorPolicy">
            <assert test="./mms:spatialOperatorDiscretization">
                The operator policy imported document is not a valid spatial operator discretization.
            </assert>
        </rule>
        <rule context="/mms:PDEDiscretizationPolicy/mms:regionDiscretizations/mms:regionDiscretization/mms:fieldGroupMaps/mms:fieldGroupMap/mms:regionFieldGroupName">
            <assert test="./ancestor-or-self::mms:PDEDiscretizationPolicy/mms:problem/mms:id//mms:PDEProblem/mms:region/mms:fieldGroups/mms:fieldGroup/mms:name|./ancestor-or-self::mms:PDEDiscretizationPolicy/mms:problem/mms:id//mms:PDEProblem/mms:subregions/mms:subregion/mms:fieldGroups/mms:fieldGroup/mms:name = .">
                Region field group does not exist in the problem.
            </assert>
        </rule>
        <rule context="/mms:PDEDiscretizationPolicy/mms:regionDiscretizations/mms:regionDiscretization/mms:fieldGroupMaps/mms:fieldGroupMap/mms:schemaFieldGroupName">
            <assert test="./ancestor-or-self::mms:regionDiscretization/mms:timeIntegrationSchema/mms:id//mms:PDEDiscretizationSchema/mms:schema/mms:step/mms:preStep/sml:simml//sml:fieldGroup/@groupNameAtt = .">
                Schema field group does not exist in the discretization schema.
            </assert>
        </rule> 
    </pattern>
</schema>