<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <ns uri="urn:mathms" prefix="mms"/>
    <pattern>
        <rule context="/mms:agentBasedModelGraph/mms:rules/mms:gatherRules/mms:gatherRule/mms:vertexProperty">
            <assert test="/mms:agentBasedModelGraph/mms:vertexProperties/mms:vertexProperty|/mms:agentBasedModelGraph/mms:edgeProperties/mms:edgeProperty = .">
                Gather properties must be model properties.
            </assert>
        </rule>
        <rule context="/mms:agentBasedModelGraph/mms:rules/mms:updateRules/mms:updateRule/mms:vertexProperty">
            <assert test="/mms:agentBasedModelGraph/mms:vertexProperties/mms:vertexProperty|/mms:agentBasedModelGraph/mms:edgeProperties/mms:edgeProperty = .">
                Update properties must be model properties.
            </assert>
        </rule>
        <rule context="/mms:agentBasedModelGraph/mms:ruleExecutionOrder/mms:rule">
            <assert test="/mms:agentBasedModelGraph/mms:rules/mms:updateRules/mms:updateRule/mms:name|/mms:agentBasedModelGraph/mms:rules/mms:gatherRules/mms:gatherRule/mms:name|/mms:agentBasedModelGraph/mms:topologyChange/mms:name = .">
                The rules in the execution order must be gather or update rules or a topology change.
            </assert>
        </rule>
        <rule context="/mms:agentBasedModelGraph/mms:rules">
            <assert test="count(*) > 0">
                At least one gather or update rule must exist.
            </assert>
            <assert test="count(*/*) = count(/mms:agentBasedModelGraph/mms:ruleExecutionOrder/mms:rule[not(text() ='Topology change')])">
                All gather/update rule must appear once in the execution order.
            </assert>
        </rule> 
        <rule context="/mms:agentBasedModelGraph/mms:ruleExecutionOrder/mms:rule">
            <assert test="./text() = (/mms:agentBasedModelGraph/mms:rules/mms:gatherRules/mms:gatherRule/mms:name/text()|/mms:agentBasedModelGraph/mms:rules/mms:updateRules/mms:updateRule/mms:name/text()|/mms:agentBasedModelGraph/mms:topologyChange/mms:name/text())">
                Every rule in the execution order must be a gather or update definition or a topology change.
            </assert>
        </rule>
    </pattern>
</schema>