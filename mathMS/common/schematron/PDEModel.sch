<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <ns uri="urn:mathms" prefix="mms"/>
    <ns uri="http://www.w3.org/1998/Math/MathML" prefix="mt"/>
    <pattern>
        <rule context="/mms:PDEModel/mms:evolutionEquations/mms:evolutionEquation/mms:operators/mms:term">
            <assert test="count(*) > 0">
                All terms in the evolution equations must have at least one mathematical expression or a derivative term.
            </assert>
        </rule> 
        <rule context="/mms:PDEModel/mms:evolutionEquations/mms:evolutionEquation//mms:partialDerivative">
            <assert test="count(*) > 1">
                All partial derivatives in the evolution equations must have at least one mathematical expression or a derivative term.
            </assert>
        </rule> 
        <rule context="/mms:PDEModel/mms:evolutionEquations/mms:evolutionEquation/mms:field">
            <assert test="/mms:PDEModel/mms:fields/mms:field = .">
                Evolution equations must use fields defined in the model.
            </assert>
        </rule> 
        <rule context="/mms:PDEModel/mms:auxiliaryFieldEquations/mms:auxiliaryFieldEquation/mms:auxiliaryField">
            <assert test="/mms:PDEModel/mms:auxiliaryFields/mms:auxiliaryField = .">
                Auxiliary field equations must use fields defined in the model.
            </assert>
        </rule>
        <rule context="/mms:PDEModel/mms:auxiliaryVariableEquations/mms:auxiliaryVariableEquation/mms:auxiliaryVariable">
            <assert test="/mms:PDEModel/mms:auxiliaryVariables/mms:auxiliaryVariable = .">
                Auxiliary variable equations must use auxiliary variables defined in the model.
            </assert>
        </rule>
       <rule context="/mms:PDEModel/mms:tensors/mms:tensor/mms:fields/mms:field">
          <assert test="/mms:PDEModel/mms:fields/mms:field|/mms:PDEModel/mms:auxiliaryFields/mms:auxiliaryField = .">
             Tensor fields must be defined in the problem.
          </assert>
       </rule>   
        <rule context="/mms:PDEModel/mms:evolutionEquations/mms:evolutionEquation/mms:fluxes/mms:flux/mms:coordinate">
            <assert test="/mms:PDEModel/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate = .">
                The fluxes must use defined spatial coordinates.
            </assert>
        </rule> 
        <rule context="/mms:PDEModel/mms:evolutionEquations/mms:evolutionEquation/mms:operator/mms:term/mms:partialDerivatives/mms:partialDerivative//mms:coordinate">
            <assert test="/mms:PDEModel/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate = .">
                The derivatives must use defined spatial coordinates.
            </assert>
        </rule> 
        <rule context="/mms:PDEModel/mms:auxiliaryVariableEquations/mms:auxiliaryVariableEquation/mms:operator/mms:term/mms:partialDerivatives/mms:partialDerivative//mms:coordinate">
            <assert test="/mms:PDEModel/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate = .">
                The derivatives must use defined spatial coordinates.
            </assert>
        </rule> 
        <rule context="/mms:PDEModel/mms:auxiliaryVariableEquations/mms:auxiliaryVariableEquation/mms:auxiliaryVariable">
            <assert test="not(/mms:PDEModel/descendant::mms:partialDerivative/descendant::mt:ci/text() = ./text())">
                The auxiliary equations using derivatives can be used in source terms, but not inside derivatives 
            </assert>
        </rule> 
        <rule context="/mms:PDEModel/mms:characteristicDecompositions/mms:characteristicDecomposition/mms:coordinateCharacteristicDecompositions/mms:coordinateCharacteristicDecomposition/mms:eigenCoordinate">
            <assert test="/mms:PDEModel/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate = .">
                Coordinates in characteristic decomposition must exist in the model.
            </assert>
        </rule>
        <rule context="/mms:PDEModel/mms:characteristicDecompositions/mms:characteristicDecomposition/mms:coordinateCharacteristicDecompositions/mms:coordinateCharacteristicDecomposition/mms:eigenSpaces/mms:eigenSpace/mms:eigenVectors/mms:eigenVector/mms:diagonalizationCoefficients/mms:diagonalizationCoefficient/mms:field">
            <assert test="/mms:PDEModel/mms:fields/mms:field = .">
                Fields in characteristic decomposition must exist in the model.
            </assert>
        </rule>
        <rule context="/mms:PDEModel/mms:characteristicDecompositions/mms:characteristicDecomposition/mms:generalCharacteristicDecomposition/mms:eigenSpaces/mms:eigenSpace/mms:eigenVectors/mms:eigenVector/mms:diagonalizationCoefficients/mms:diagonalizationCoefficient/mms:field">
            <assert test="/mms:PDEModel/mms:fields/mms:field = .">
                Fields in characteristic decomposition must exist in the model.
            </assert>
        </rule>
        <rule context="/mms:PDEModel/mms:characteristicDecompositions/mms:characteristicDecomposition/mms:coordinateCharacteristicDecompositions/mms:coordinateCharacteristicDecomposition/mms:undiagonalizations/mms:undiagonalization/mms:field">
            <assert test="/mms:PDEModel/mms:fields/mms:field = .">
                Fields in characteristic decomposition must exist in the model.
            </assert>
        </rule>
        <rule context="/mms:PDEModel/mms:characteristicDecompositions/mms:characteristicDecomposition/mms:generalCharacteristicDecomposition/mms:undiagonalizations/mms:undiagonalization/mms:field">
            <assert test="/mms:PDEModel/mms:fields/mms:field = .">
                Fields in characteristic decomposition must exist in the model.
            </assert>
        </rule>
        <rule context="/mms:PDEModel/mms:characteristicDecompositions/mms:characteristicDecomposition/mms:generalCharacteristicDecomposition/mms:undiagonalizations/mms:undiagonalization/mms:undiagonalizationCoefficients/mms:undiagonalizationCoefficient/mms:eigenVector">
            <assert test="/mms:PDEModel/mms:characteristicDecomposition/mms:generalCharacteristicDecomposition/mms:eigenSpaces/mms:eigenSpace/mms:eigenVectors/mms:eigenVector/mms:name = .">
                EigenVectors in characteristic decomposition must be defined.
            </assert>
        </rule>
        <rule context="/mms:PDEModel/mms:characteristicDecompositions/mms:characteristicDecomposition/mms:coordinateCharacteristicDecompositions/mms:coordinateCharacteristicDecomposition/mms:undiagonalizations/mms:undiagonalization/mms:undiagonalizationCoefficients/mms:undiagonalizationCoefficient/mms:eigenVector">
            <assert test="/mms:PDEModel/mms:characteristicDecomposition/mms:coordinateCharacteristicDecompositions/mms:coordinateCharacteristicDecomposition/mms:eigenSpaces/mms:eigenSpace/mms:name = .">
                EigenVectors in characteristic decomposition must be defined.
            </assert>
        </rule> 
        <rule context="/mms:PDEModel/mms:characteristicDecompositions/mms:characteristicDecomposition/mms:applyingFields/mms:field">
            <assert test="/mms:PDEModel/mms:fields/mms:field = .">
                EigenVectors in characteristic decomposition must be defined.
            </assert>
        </rule>
        <rule context="/mms:PDEModel/mms:fields/mms:field">
            <assert test=" . = /mms:PDEModel/mms:evolutionEquations/mms:evolutionEquation/mms:field">
                Every field must have an evolution equation.
            </assert>
        </rule> 
        <rule context="/mms:PDEModel/mms:auxiliaryFields/mms:auxiliaryField">
            <assert test=". = /mms:PDEModel/mms:auxiliaryFieldEquations/mms:auxiliaryFieldEquation/mms:auxiliaryField">
                Every auxiliary field must have an auxiliary field equation.
            </assert>
        </rule>
        <rule context="/mms:PDEModel/mms:auxiliaryVariables/mms:auxiliaryVariable">
            <assert test=". = /mms:PDEModel/mms:auxiliaryVariableEquations/mms:auxiliaryVariableEquation/mms:auxiliaryVariable">
                Every auxiliary variable must have an auxiliary variables equation.
            </assert>
        </rule>
    </pattern>
</schema>