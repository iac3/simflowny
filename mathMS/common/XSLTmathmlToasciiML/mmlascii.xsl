<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:m="http://www.w3.org/1998/Math/MathML" xmlns:s="urn:simml"
                version='1.0'>
                
<xsl:output method="xml" indent="no" encoding="UTF-8"/>

<!-- ====================================================================== -->
<!-- $id: mmltex.xsl, 2002/22/11 Exp $
     This file is part of the XSLT MathML Library distribution.
     See ./README or http://www.raleigh.ru/MathML/mmltex for
     copyright and other information                                        -->
<!-- ====================================================================== -->

<xsl:strip-space elements="m:*"/>

     <xsl:template match="m:*[not(descendant::m:piecewise)]/text()">
          <xsl:call-template name="replaceEntities">
               <xsl:with-param name="content" select="normalize-space()"/>
          </xsl:call-template>
     </xsl:template>

    <xsl:template match="m:math">
         <result>
    	<xsl:apply-templates/>
         </result>
    </xsl:template>
     
     <xsl:template name="replaceEntities">
          <xsl:param name="content"/>
          <xsl:if test="string-length($content)>0">
               <xsl:choose>
                    <!-- ====================================================================== -->
                    <!-- 	Unicode 3.2
                         C1 Controls and Latin-1 Supplement
                         Range: 0080-00FF
                         http://www.unicode.org/charts/PDF/U0080.pdf	                    -->
                    <!-- ====================================================================== -->	
                    <xsl:when test="starts-with($content,'&#x000AC;')"><xsl:value-of select="'negative'" /><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="substring-after($content, '&#x000AC;')"/></xsl:call-template></xsl:when>	<!--/neg /lnot =not sign -->
                    <xsl:when test="starts-with($content,'&#x000D7;')"><xsl:value-of select="'times'" /><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="substring-after($content, '&#x000D7;')"/></xsl:call-template></xsl:when>	<!--/times B: =multiply sign -->
                    
                    <!-- ====================================================================== -->
                    <!-- 	Unicode 3.2
                         Letterlike Symbols
                         Range: 2100-214F
                         http://www.unicode.org/charts/PDF/U2100.pdf	                    -->
                    <!-- ====================================================================== -->
                    
                    <!-- ====================================================================== -->
                    <!-- 	No valid fortran characters used in physics                  -->
                    <!-- ====================================================================== -->
                    <xsl:when test="starts-with($content,'*')"><xsl:value-of select="'ast'" /><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="substring-after($content, '*')"/></xsl:call-template></xsl:when>	
                    <xsl:when test="starts-with($content,'+')"><xsl:value-of select="'pos'" /><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="substring-after($content, '+')"/></xsl:call-template></xsl:when>	
                    <xsl:when test="starts-with($content,'-')"><xsl:value-of select="'neg'" /><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="substring-after($content, '-')"/></xsl:call-template></xsl:when>	
                    <xsl:when test="starts-with($content,'&quot;') and (ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)"><xsl:value-of select="'quot'" /><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="substring-after($content, '&quot;')"/></xsl:call-template></xsl:when>
                    <xsl:when test='starts-with($content,"&apos;") and (ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)'><xsl:value-of select="'prime'" /><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select='substring-after($content, "&apos;")'/></xsl:call-template></xsl:when>
                    <xsl:when test="starts-with($content,'#')"><xsl:value-of select="'sharp'" /><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="substring-after($content, '#')"/></xsl:call-template></xsl:when>		
                    <xsl:when test="starts-with($content,'%')"><xsl:value-of select="'percent'" /><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="substring-after($content, '%')"/></xsl:call-template></xsl:when>
                    <xsl:otherwise>
                         <xsl:value-of select="substring($content,1,1)"/>
                         <xsl:call-template name="replaceEntities">
                              <xsl:with-param name="content" select="substring($content, 2)"/>
                         </xsl:call-template>
                    </xsl:otherwise>
               </xsl:choose></xsl:if>
     </xsl:template>
     
     
    <!-- 4.4.1.1 cn -->
    <xsl:template match="m:cn">
        <xsl:apply-templates/>
    </xsl:template>

    <!-- 4.4.1.1 ci 4.4.1.2 csymbol -->
    <xsl:template match="m:ci | m:csymbol">
        <xsl:apply-templates/>
    </xsl:template>

    <!-- 4.4.2.1 apply 4.4.2.2 reln -->
    <xsl:template match="m:apply | m:reln">
        <xsl:apply-templates select="*[1]"/>
        <xsl:text>(</xsl:text>
        <xsl:for-each select="*[position()>1]">
            <xsl:apply-templates select="."/>
            <xsl:if test="not(position()=last())">
                <xsl:text>, </xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <!-- 4.4.2.3 fn -->
    <xsl:template match="m:fn[m:apply[1]]">
        <!-- for m:fn using default rule -->
        <xsl:text>(</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>)</xsl:text>
    </xsl:template>


    <!-- 4.4.2.5 inverse -->
    <xsl:template match="m:apply[*[1][self::m:inverse] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>^(-1)</xsl:text>
    </xsl:template>

    <!-- 4.4.2.6 sep 4.4.2.7 condition -->
    <xsl:template match="m:sep | m:condition">
        <xsl:apply-templates/>
    </xsl:template>

    <!-- 4.4.3.3 divide -->
    <xsl:template match="m:apply[*[1][self::m:divide] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 3">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="4"/>
        </xsl:apply-templates>
        <xsl:text>/</xsl:text>
        <xsl:apply-templates select="*[3]">
            <xsl:with-param name="p" select="4"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 3">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.3.4 max -->
    <xsl:template match="m:apply[*[1][self::m:max] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:text>max(</xsl:text>
        <xsl:for-each select="*[position() &gt; 1]">
            <xsl:apply-templates select="."/>
            <xsl:if test="position() !=last()">
                <xsl:text>,</xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <!-- 4.4.3.4 min -->
    <xsl:template match="m:apply[*[1][self::m:min] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:text>min(</xsl:text>
        <xsl:for-each select="*[position() &gt; 1]">
            <xsl:apply-templates select="."/>
            <xsl:if test="position() !=last()">
                <xsl:text>,</xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <!-- 4.4.3.5  minus-->
    <xsl:template match="m:apply[*[1][self::m:minus] and count(*)=2 and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 0">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:text>-</xsl:text>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="2"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 0">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="m:apply[*[1][self::m:minus] and count(*)&gt;2 and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 1">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="2"/>
        </xsl:apply-templates>
        <xsl:text>-</xsl:text>
        <xsl:apply-templates select="*[3]">
            <xsl:with-param name="p" select="2"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 1">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.3.6  plus-->
    <xsl:template match="m:apply[*[1][self::m:plus] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 0">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="1"/>
        </xsl:apply-templates>
        <xsl:for-each select="*[position()&gt;2]">
            <xsl:text>+</xsl:text>
            <xsl:apply-templates select=".">
                <xsl:with-param name="p" select="1"/>
            </xsl:apply-templates>
        </xsl:for-each>
        <xsl:if test="$p &gt; 0">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="m:apply[ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup]">
        <xsl:apply-templates select="*[2]"/>
        <xsl:value-of select="local-name(*[1])"/>
        <xsl:apply-templates select="*[3]"/>
    </xsl:template>

    <!-- 4.4.3.7 power -->
    <xsl:template match="m:apply[*[1][self::m:power] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 2">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="5"/>
        </xsl:apply-templates>
        <xsl:text>^</xsl:text>
        <xsl:apply-templates select="*[3]">
            <xsl:with-param name="p" select="5"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 2">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.3.8 remainder -->
    <xsl:template match="m:apply[*[1][self::m:rem]]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 2">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="5"/>
        </xsl:apply-templates>
        <xsl:text>%</xsl:text>
        <xsl:apply-templates select="*[3]">
            <xsl:with-param name="p" select="5"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 2">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.3.9  times-->
    <xsl:template match="m:apply[*[1][self::m:times] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]" name="times">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 3">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="3"/>
        </xsl:apply-templates>
        <xsl:for-each select="*[position()&gt;2]">
            <xsl:text>*</xsl:text>
            <xsl:apply-templates select=".">
                <xsl:with-param name="p" select="3"/>
            </xsl:apply-templates>
        </xsl:for-each>
        <xsl:if test="$p &gt; 3">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.3.10 root (only square root) -->
    <xsl:template match="m:apply[*[1][self::m:root] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:text>sqrt</xsl:text>
        <xsl:text>(</xsl:text>
        <xsl:apply-templates select="*[position()&gt;1 and not(self::m:degree)]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <!-- 4.4.3.12 and -->
    <xsl:template match="m:apply[*[1][self::m:and]]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 5">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="6"/>
        </xsl:apply-templates>
       <xsl:for-each select="*[position()&gt;2]">
          <xsl:text> and </xsl:text>
          <xsl:apply-templates select=".">
             <xsl:with-param name="p" select="6"/>
          </xsl:apply-templates>
       </xsl:for-each>
        <xsl:if test="$p &gt; 5">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.3.13 or -->
    <xsl:template match="m:apply[*[1][self::m:or]]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 5">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="6"/>
        </xsl:apply-templates>
       <xsl:for-each select="*[position()&gt;2]">
          <xsl:text> or </xsl:text>
          <xsl:apply-templates select=".">
             <xsl:with-param name="p" select="6"/>
          </xsl:apply-templates>
       </xsl:for-each>
        <xsl:if test="$p &gt; 5">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.3.15 not -->
    <xsl:template match="m:apply[*[1][self::m:not]]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 5">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:text> not </xsl:text>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="6"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 5">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.3.19 abs -->
    <xsl:template match="m:apply[*[1][self::m:abs] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:text>|</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>|</xsl:text>
    </xsl:template>

    <!-- 4.4.3.25 floor -->
    <xsl:template match="m:apply[*[1][self::m:floor] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:text>|__</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>__|</xsl:text>
    </xsl:template>

    <!-- 4.4.3.25 ceiling -->
    <xsl:template match="m:apply[*[1][self::m:ceiling] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:text>|~</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>~|</xsl:text>
    </xsl:template>

    <!-- 4.4.4.1 eq -->
    <xsl:template match="m:apply[*[1][self::m:eq]] | m:reln[*[1][self::m:eq]]">
        <xsl:param name="p" select="0"/>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>=</xsl:text>
        <xsl:apply-templates select="*[3]"/>
    </xsl:template>

    <!-- 4.4.4.2 neq -->
    <xsl:template match="m:apply[*[1][self::m:neq]] | m:reln[*[1][self::m:neq]]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 0">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="1"/>
        </xsl:apply-templates>
        <xsl:text>!=</xsl:text>
        <xsl:apply-templates select="*[3]">
            <xsl:with-param name="p" select="1"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 0">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.4.3 gt -->
    <xsl:template match="m:apply[*[1][self::m:gt]] | m:reln[*[1][self::m:gt]]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 0">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="1"/>
        </xsl:apply-templates>
        <xsl:text>&gt;</xsl:text>
        <xsl:apply-templates select="*[3]">
            <xsl:with-param name="p" select="1"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 0">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.4.4 lt -->
    <xsl:template match="m:apply[*[1][self::m:lt]] | m:reln[*[1][self::m:lt]]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 0">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="1"/>
        </xsl:apply-templates>
        <xsl:text>&lt;</xsl:text>
        <xsl:apply-templates select="*[3]">
            <xsl:with-param name="p" select="1"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 0">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.4.5 geq -->
    <xsl:template match="m:apply[*[1][self::m:geq]] | m:reln[*[1][self::m:geq]]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 0">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="1"/>
        </xsl:apply-templates>
        <xsl:text>&gt;=</xsl:text>
        <xsl:apply-templates select="*[3]">
            <xsl:with-param name="p" select="1"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 0">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.4.6 leq -->
    <xsl:template match="m:apply[*[1][self::m:leq]] | m:reln[*[1][self::m:leq]]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 0">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="1"/>
        </xsl:apply-templates>
        <xsl:text>&lt;=</xsl:text>
        <xsl:apply-templates select="*[3]">
            <xsl:with-param name="p" select="1"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 0">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.8.1 common tringonometric functions 4.4.8.3 natural logarithm -->
    <xsl:template
        match="m:apply[*[1][self::m:sin]]">
        <xsl:text>sin(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    <xsl:template
        match="m:apply[*[1][self::m:sinh]]">
        <xsl:text>sinh(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    <xsl:template
        match="m:apply[*[1][self::m:tanh]]">
        <xsl:text>tanh(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    <xsl:template
        match="m:apply[*[1][self::m:arctan]]">
        <xsl:text>arctan(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>, </xsl:text>
        <xsl:apply-templates select="*[3]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    <xsl:template
        match="m:apply[*[1][self::m:cos]]">
        <xsl:text>cos(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    <xsl:template
        match="m:apply[*[1][self::m:cosh]]">
        <xsl:text>cosh(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    <xsl:template
        match="m:apply[*[1][self::m:arcsin]]">
        <xsl:text>arcsin(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    <xsl:template
        match="m:apply[*[1][self::m:tan]]">
        <xsl:text>tan(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    <xsl:template
        match="m:apply[*[1][self::m:arccos]]">
        <xsl:text>arccos(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <xsl:template match="m:apply[*[1][self::m:ln]]">
        <xsl:text>ln(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <!-- 4.4.8.2 exp -->
    <xsl:template match="m:apply[*[1][self::m:exp]]">
        <xsl:text>exp(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <!-- 4.4.8.4 log -->
    <xsl:template match="m:apply[*[1][self::m:log]]">
        <xsl:text>log(</xsl:text>
        <xsl:apply-templates select="*[last()]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <!-- 4.4.12.7 exponentiale -->
    <xsl:template match="m:exponentiale">
        <xsl:text>exp(1)</xsl:text>
    </xsl:template>

    <!-- 4.4.12.10 true -->
    <xsl:template match="m:true">
        <xsl:text>true</xsl:text>
    </xsl:template>

    <!-- 4.4.12.11 false -->
    <xsl:template match="m:false">
        <xsl:text>false</xsl:text>
    </xsl:template>

    <!-- 4.4.12.13 pi -->
    <xsl:template match="m:pi">
        <xsl:text>pi</xsl:text>
    </xsl:template>

    <!-- 4.4.12.14 eulergamma -->
    <xsl:template match="m:eulergamma">
        <xsl:text>0.5772156649015328</xsl:text>
    </xsl:template>

    <!-- ****************************** -->
    
    <xsl:template match="s:field">
        <xsl:text>$f</xsl:text>
    </xsl:template>

   <xsl:template match="s:dimensions">
      <xsl:text>$dim</xsl:text>
   </xsl:template>

   <xsl:template match="s:particlePosition">
      <xsl:text>$pp</xsl:text>
   </xsl:template>

   <xsl:template match="s:cellPosition">
      <xsl:text>$cp</xsl:text>
   </xsl:template>
   
   <xsl:template match="s:particleDeltaSpacing">
      <xsl:text>$pds</xsl:text>
   </xsl:template>
   
   <xsl:template match="s:cellDeltaSpacing">
      <xsl:text>$cds</xsl:text>
   </xsl:template>
   
   <xsl:template match="s:particleVelocity">
      <xsl:text>$pvel</xsl:text>
   </xsl:template>
   
    <xsl:template match="m:ci/s:param">
        <xsl:text>$p_</xsl:text><xsl:value-of select="./text()"/>
    </xsl:template>
    
    <xsl:template match="s:eigenVector">
        <xsl:text>$v</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:neighbourAgent">
        <xsl:text>$na</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:parabolicTermsVariable">
        <xsl:text>$ptv</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:spatialCoordinate">
        <xsl:text>$i</xsl:text>
    </xsl:template>
        
    <xsl:template match="s:secondSpatialCoordinate">
        <xsl:text>$i2</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:contSpatialCoordinate">
        <xsl:text>$x</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:secondContSpatialCoordinate">
        <xsl:text>$x2</xsl:text>
    </xsl:template>
        
    <xsl:template match="s:timeCoordinate">
        <xsl:text>$t</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:contTimeCoordinate">
        <xsl:text>$ctc</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:iterationNumber">
        <xsl:text>$in</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:currentTime">
        <xsl:text>$ct</xsl:text>
    </xsl:template>

    <xsl:template match="s:newTime">
        <xsl:text>$nt</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:refinementRatio">
        <xsl:text>$rr</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:substepNumber">
        <xsl:text>$sn</xsl:text>
    </xsl:template>

   <xsl:template match="s:timeSubstepNumber">
      <xsl:text>$tsn</xsl:text>
   </xsl:template>

    <xsl:template match="s:timeIncrement">
        <xsl:text>$ti</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:influenceRadius">
        <xsl:text>$ir</xsl:text>
    </xsl:template>
            
   <xsl:template match="s:particleSeparationProduct">
      <xsl:text>$psp</xsl:text>
   </xsl:template>
    
   <xsl:template match="s:kernel">
      <xsl:text>$k</xsl:text>
   </xsl:template>
    
   <xsl:template match="s:kernelGradient">
      <xsl:text>$kg</xsl:text>
   </xsl:template>
   
    <xsl:template match="s:randomNumber">
        <xsl:text>$rnd_</xsl:text><xsl:value-of select="./@typeAtt"/>
        <xsl:if test="./@rangeMinAtt"><xsl:text>_</xsl:text><xsl:value-of select="./@rangeMinAtt"/></xsl:if>
        <xsl:if test="./@rangeMaxAtt"><xsl:text>_</xsl:text><xsl:value-of select="./@rangeMaxAtt"/></xsl:if>
    </xsl:template>
        
    <xsl:template match="s:globalNumberOfAgents">
        <xsl:text>$gnoa</xsl:text>
    </xsl:template>
    <xsl:template match="s:globalNumberOfVertices">
        <xsl:text>$gnov</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:globalNumberOfEdges">
        <xsl:text>$gnoe</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:localNumberOfEdges">
        <xsl:text>$lnoe_</xsl:text><xsl:value-of select="./@directionAtt"/><xsl:text>(</xsl:text>
        <xsl:apply-templates select="*[1]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:regionInteriorId">
        <xsl:text>$riid_</xsl:text><xsl:value-of select="text()"/>
    </xsl:template>
    
    <xsl:template match="s:regionSurfaceId">
        <xsl:text>$rsid_</xsl:text><xsl:value-of select="text()"/>
    </xsl:template>
    
    <xsl:template match="s:stencil">
        <xsl:text>$stencil</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:interactionRegionId">
        <xsl:text>$irid</xsl:text>
    </xsl:template>
    
   <xsl:template match="s:particleDistance">
      <xsl:text>$pd</xsl:text>
   </xsl:template>
    
    <xsl:template match="m:*[@s:type]">
        <xsl:element name="{local-name()}">
            <xsl:apply-templates select="@*[name()!='type']"/>
        <xsl:text>_</xsl:text><xsl:value-of select="./@type"/>
        </xsl:element>
    </xsl:template>
        
    <!-- Field structure transformation -->
    <xsl:template match="m:ci[m:msup/*[position() = 1 and local-name() = 'ci']/*[local-name()='field'] and m:msup//s:timeCoordinate]">
        <xsl:choose>
            <xsl:when test="m:msup/*[position() = 2 and local-name() = 'apply']/m:minus">
                <xsl:variable name="decrements" select="./m:msup/*[position() = 2 and local-name() = 'apply']/m:cn/text()"/>
                <xsl:text>$f</xsl:text>
                <xsl:call-template name="recPrevious">
                    <xsl:with-param name="iter" select="$decrements + 1"></xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="m:msup/*[position() = 2 and local-name() = 'apply']/m:plus">
                <xsl:text>$f_n</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>$f_p</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
   <!-- Particle position structure transformation -->
   <xsl:template match="m:ci[m:msup/*[position() = 1 and local-name() = 'ci']/*[local-name()='particlePosition'] and m:msup//s:timeCoordinate]">
      <xsl:choose>
         <xsl:when test="m:msup/*[position() = 2 and local-name() = 'apply']/m:minus">
            <xsl:variable name="decrements" select="./m:msup/*[position() = 2 and local-name() = 'apply']/m:cn/text()"/>
            <xsl:text>$pp</xsl:text>
            <xsl:call-template name="recPrevious">
               <xsl:with-param name="iter" select="$decrements + 1"></xsl:with-param>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="m:msup/*[position() = 2 and local-name() = 'apply']/m:plus">
            <xsl:text>$pp_n</xsl:text>
         </xsl:when>
         <xsl:otherwise>
            <xsl:text>$pp_p</xsl:text>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
    
    
   <xsl:template match="s:sign">
      <xsl:text>$sign(</xsl:text>
      <xsl:apply-templates select="*[1]"/>
      <xsl:text>)</xsl:text>
   </xsl:template>
        

    <xsl:template match="s:neighbourParticle">
        <xsl:text>$np(</xsl:text>
        <xsl:apply-templates select="*[1]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <xsl:template match="s:functionCall">
        <xsl:text>$fc(</xsl:text>
        <xsl:for-each select="*">
            <xsl:apply-templates select="."/>
            <xsl:if test="position() &lt; last()"> <xsl:text>,</xsl:text></xsl:if>
        </xsl:for-each>
        <xsl:text>)</xsl:text>
    </xsl:template>


    <xsl:template match="s:flux">
        <xsl:text>$flx(</xsl:text>
        <xsl:for-each select="*">
            <xsl:apply-templates select="."/>
            <xsl:if test="position() &lt; last()"> <xsl:text>,</xsl:text></xsl:if>
        </xsl:for-each>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <xsl:template match="s:readFromFileQuintic">
        <xsl:text>$rffq(</xsl:text>
        <xsl:for-each select="*">
            <xsl:apply-templates select="."/>
            <xsl:if test="position() &lt; last()"> <xsl:text>,</xsl:text></xsl:if>
        </xsl:for-each>
        <xsl:text>)</xsl:text>
    </xsl:template>
   
   <xsl:template match="s:readFromFileLinear1D">
      <xsl:text>$rffl1d(</xsl:text>
      <xsl:for-each select="*">
         <xsl:apply-templates select="."/>
         <xsl:if test="position() &lt; last()"> <xsl:text>,</xsl:text></xsl:if>
      </xsl:for-each>
      <xsl:text>)</xsl:text>
   </xsl:template>
   
   <xsl:template match="s:readFromFileLinear2D">
      <xsl:text>$rffl2d(</xsl:text>
      <xsl:for-each select="*">
         <xsl:apply-templates select="."/>
         <xsl:if test="position() &lt; last()"> <xsl:text>,</xsl:text></xsl:if>
      </xsl:for-each>
      <xsl:text>)</xsl:text>
   </xsl:template>
   
   <xsl:template match="s:readFromFileLinear3D">
      <xsl:text>$rffl3d(</xsl:text>
      <xsl:for-each select="*">
         <xsl:apply-templates select="."/>
         <xsl:if test="position() &lt; last()"> <xsl:text>,</xsl:text></xsl:if>
      </xsl:for-each>
      <xsl:text>)</xsl:text>
   </xsl:template>

    <xsl:template match="s:readFromFileLinearWithDerivatives1D">
        <xsl:text>$rfflwd1d(</xsl:text>
        <xsl:for-each select="*">
            <xsl:apply-templates select="."/>
            <xsl:if test="position() &lt; last()"> <xsl:text>,</xsl:text></xsl:if>
        </xsl:for-each>
        <xsl:text>)</xsl:text>
    </xsl:template>
   
   <xsl:template match="s:readFromFileLinearWithDerivatives2D">
      <xsl:text>$rfflwd2d(</xsl:text>
      <xsl:for-each select="*">
         <xsl:apply-templates select="."/>
         <xsl:if test="position() &lt; last()"> <xsl:text>,</xsl:text></xsl:if>
      </xsl:for-each>
      <xsl:text>)</xsl:text>
   </xsl:template>
   
   <xsl:template match="s:readFromFileLinearWithDerivatives3D">
      <xsl:text>$rfflwd3d(</xsl:text>
      <xsl:for-each select="*">
         <xsl:apply-templates select="."/>
         <xsl:if test="position() &lt; last()"> <xsl:text>,</xsl:text></xsl:if>
      </xsl:for-each>
      <xsl:text>)</xsl:text>
   </xsl:template>

   <xsl:template match="s:findCoordFromFile1D">
      <xsl:text>$fcff1d(</xsl:text>
      <xsl:for-each select="*">
         <xsl:apply-templates select="."/>
         <xsl:if test="position() &lt; last()"> <xsl:text>,</xsl:text></xsl:if>
      </xsl:for-each>
      <xsl:text>)</xsl:text>
   </xsl:template>

   <xsl:template match="s:findCoordFromFile2D">
      <xsl:text>$fcff2d(</xsl:text>
      <xsl:for-each select="*">
         <xsl:apply-templates select="."/>
         <xsl:if test="position() &lt; last()"> <xsl:text>,</xsl:text></xsl:if>
      </xsl:for-each>
      <xsl:text>)</xsl:text>
   </xsl:template>
   
   <xsl:template match="s:findCoordFromFile3D">
      <xsl:text>$fcff3d(</xsl:text>
      <xsl:for-each select="*">
         <xsl:apply-templates select="."/>
         <xsl:if test="position() &lt; last()"> <xsl:text>,</xsl:text></xsl:if>
      </xsl:for-each>
      <xsl:text>)</xsl:text>
   </xsl:template>

    <xsl:template match="s:sources">
        <xsl:text>$src(</xsl:text>
        <xsl:apply-templates select="*[1]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:parabolicFirstDerivative">
        <xsl:text>$pfd(</xsl:text>
        <xsl:apply-templates select="*[1]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <xsl:template match="s:parabolicSecondDerivative">
        <xsl:text>$psd(</xsl:text>
        <xsl:apply-templates select="*[1]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:parabolicTermsSumVariables">
        <xsl:text>$pts(</xsl:text>
        <xsl:apply-templates select="*[1]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:maxCharSpeed">
        <xsl:text>$mcs(</xsl:text>
        <xsl:apply-templates select="*[1]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    
   <xsl:template match="s:positiveCharSpeed">
      <xsl:text>$pcs(</xsl:text>
      <xsl:apply-templates select="*[1]"/>
      <xsl:text>)</xsl:text>
   </xsl:template>
   
   <xsl:template match="s:negativeCharSpeed">
      <xsl:text>$ncs(</xsl:text>
      <xsl:apply-templates select="*[1]"/>
      <xsl:text>)</xsl:text>
   </xsl:template>
    
    <xsl:template match="s:maxCharSpeedCoordinate">
        <xsl:text>$mcsc(</xsl:text>
        <xsl:apply-templates select="*[1]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    
   <xsl:template match="s:positiveCharSpeedCoordinate">
      <xsl:text>$pcsc(</xsl:text>
      <xsl:apply-templates select="*[1]"/>
      <xsl:text>)</xsl:text>
   </xsl:template>
   
   <xsl:template match="s:negativeCharSpeedCoordinate">
      <xsl:text>$ncsc(</xsl:text>
      <xsl:apply-templates select="*[1]"/>
      <xsl:text>)</xsl:text>
   </xsl:template>
   
    <xsl:template match="s:currentCell">
        <xsl:text>$cc</xsl:text>
    </xsl:template>

    <xsl:template match="s:currentAgent">
        <xsl:text>$ca</xsl:text>
    </xsl:template>
    <xsl:template match="s:currentVertex">
        <xsl:text>$cv</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:currentEdge">
        <xsl:text>$ce</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:incrementCoordinate">
        <xsl:text>$ic_</xsl:text><xsl:value-of select="text()"/>
    </xsl:template>
    
    <xsl:template match="s:decrementCoordinate">
        <xsl:text>$dc_</xsl:text><xsl:value-of select="text()"/>
    </xsl:template>
    
    <xsl:template match="s:incrementCoordinate2">
        <xsl:text>$ic2_</xsl:text><xsl:value-of select="text()"/>
    </xsl:template>
    
    <xsl:template match="s:decrementCoordinate2">
        <xsl:text>$dc2_</xsl:text><xsl:value-of select="text()"/>
    </xsl:template>
    
    <xsl:template match="s:incrementCoordinate1IncrementCoordinate2">
        <xsl:text>$icic_</xsl:text><xsl:value-of select="substring-before(text(),',')"/><xsl:text>_</xsl:text><xsl:value-of select="substring-after(text(),',')"/>
    </xsl:template>
    
    <xsl:template match="s:incrementCoordinate1DecrementCoordinate2">
        <xsl:text>$icdc_</xsl:text><xsl:value-of select="substring-before(text(),',')"/><xsl:text>_</xsl:text><xsl:value-of select="substring-after(text(),',')"/>
    </xsl:template>
    
    <xsl:template match="s:decrementCoordinate1IncrementCoordinate2">
        <xsl:text>$dcic_</xsl:text><xsl:value-of select="substring-before(text(),',')"/><xsl:text>_</xsl:text><xsl:value-of select="substring-after(text(),',')"/>
    </xsl:template>
    
    <xsl:template match="s:decrementCoordinate1DecrementCoordinate2">
        <xsl:text>$dcdc_</xsl:text><xsl:value-of select="substring-before(text(),',')"/><xsl:text>_</xsl:text><xsl:value-of select="substring-after(text(),',')"/>
    </xsl:template>
    
    <xsl:template match="s:edgeSource">
        <xsl:text>$es(</xsl:text>
        <xsl:apply-templates select="*[1]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:edgeTarget">
        <xsl:text>$et(</xsl:text>
        <xsl:apply-templates select="*[1]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
     
   <xsl:template match="s:particleVolume">
      <xsl:text>$pv(</xsl:text>
      <xsl:apply-templates select="*[1]"/>
      <xsl:text>)</xsl:text>
   </xsl:template>
     
    <xsl:template match="s:sharedVariable">
        <xsl:text>$sv(</xsl:text>
        <xsl:apply-templates select="*[1]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
   
    <xsl:template match="s:minRegionValue">
        <xsl:text>$minrv</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:maxRegionValue">
        <xsl:text>$maxrv</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:minDomainValue">
        <xsl:text>$mindv_</xsl:text><xsl:value-of select="text()"/>
    </xsl:template>
    
    <xsl:template match="s:maxDomainValue">
        <xsl:text>$maxdv_</xsl:text><xsl:value-of select="text()"/>
    </xsl:template>
    
    <xsl:template name="recPrevious">
        <xsl:param name="iter"/>
        <xsl:if test="$iter &gt; 0">
            <xsl:text>_p</xsl:text>
            <xsl:call-template name="recPrevious">
                <xsl:with-param name="iter" select="$iter - 1"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
</xsl:stylesheet>