<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		      xmlns:mt="http://www.w3.org/1998/Math/MathML"
		      xmlns:mms="urn:mathms"
		      xmlns:sml="urn:simml"
                version='1.0'>                
     <xsl:output method="text" indent="no" encoding="UTF-8" media-type="application/x-tex"/>

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->

     <xsl:include href="mml_ABMGraph/instructionToPseudocode.xsl"/>
     <xsl:include href="parameters.xsl"/>
     <xsl:include href="equivalences.xsl"/>

<xsl:strip-space elements="mt:*"/>
     
 <xsl:template match="mms:agentBasedProblemGraph">
      <xsl:if test="count(mms:head) &gt; 0">   
           \begin{center}
           \textbf{<xsl:choose><xsl:when test="count(mms:head/mms:name) &gt; 0"><xsl:value-of select="mms:head/mms:name"/></xsl:when><xsl:otherwise>Agent Based Problem</xsl:otherwise></xsl:choose>}      
      <!--head-->
           <xsl:if test="mms:head/mms:author/text() != ''">
                by <xsl:value-of select="mms:head/mms:author"/>\\    
           </xsl:if>
           <xsl:if test="mms:head/mms:version/text() != ''">
                Version: <xsl:value-of select="mms:head/mms:version"/>\\
           </xsl:if> 
           <xsl:if test="count(mms:head/mms:date) &gt; 0">
                <xsl:value-of select="mms:head/mms:date"/>\\
           </xsl:if>
           \end{center}
           <xsl:if test="mms:head/mms:description/text() != ''">
                \noindent <xsl:value-of select="mms:head/mms:description"/>\\
           </xsl:if> 
           \noindent \rule{\textwidth}{1pt}\\
      </xsl:if>
      <!--vertexProperties-->
      <xsl:if test="count(mms:vertexProperties/mms:vertexProperty) &gt; 0">      
           \indent {\Large Vertex Properties}
       <xsl:text>                               
       </xsl:text>           
           \indent \indent <xsl:for-each select="mms:vertexProperties/mms:vertexProperty">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
      </xsl:if>
      <!--interaction vertexProperties-->
      <xsl:if test="count(mms:edgeProperties/mms:edgeProperty) &gt; 0">      
           \indent {\Large Edge Properties}
           <xsl:text>                               
           </xsl:text>           
           \indent \indent <xsl:for-each select="mms:edgeProperties/mms:edgeProperty">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
      </xsl:if>
     
      <!--parameters-->      
      <xsl:call-template name="parameters"></xsl:call-template>
      
     <!--imported models-->
          <xsl:if test="count(mms:model) &gt; 0">
               \indent {\Large Imported Model}\\
              \indent \indent {\large <xsl:value-of select="mms:model/mms:name"/>}\\
              
              <!--vertexProperty equalities-->
              <xsl:apply-templates select="mms:model/mms:vertexPropertyEquivalence"></xsl:apply-templates>
              
              <!--edgeProperty equalities-->
              <xsl:apply-templates select="mms:model/mms:edgePropertyEquivalence"></xsl:apply-templates>
              
              <!--parameter equalities-->
              <xsl:apply-templates select="mms:model/mms:parameterEquivalence"></xsl:apply-templates>
              
          </xsl:if>
                  
      <!--problem domain-->
      <xsl:if test="count(mms:graphDefinition) &gt; 0">
           \indent{\Large Graph Definition}
           
           <xsl:choose>
                <xsl:when test="mms:graphDefinition/mms:externalSpecification">
                     \indent \indent External specification 
                </xsl:when>
                <xsl:otherwise>
                     \begin{mytable}{c|c}{15pt}
                     \textbf{\small Edge directionality} &#038; \textbf{\small Degree distribution} \\ \hlinegray
                     $<xsl:value-of select="mms:graphDefinition/mms:topologicalSpecification/mms:edgeDirectionality"/>$  &#038; $<xsl:value-of select="mms:graphDefinition/mms:topologicalSpecification/mms:degreeDistribution"/>$ \\               
                     \end{mytable}
                </xsl:otherwise>
           </xsl:choose>
      </xsl:if>
      
      <!--evolutionStep-->
      <xsl:if test="count(mms:evolutionStep) &gt; 0">
           \indent{\Large Evolution Step}\\
           \indent \indent <xsl:value-of select="mms:evolutionStep"/>\\ 
      </xsl:if>
      
      <!--initialconditions-->      
          <xsl:if test="count(mms:initialConditions) &gt; 0">
               \indent{\Large Initial Conditions}\\
               <xsl:if test="count(mms:initialConditions/mms:initialCondition) &gt; 0">
                    <xsl:for-each select="mms:initialConditions/mms:initialCondition">
                         \begin{mygrouping}
                         <xsl:if test="mms:applyIf">
                              \indent {\large The condition is applied when:}
                              {\color{NavyBlue}
                              \begin{dmath}
                              <xsl:apply-templates select="mms:applyIf/mt:math"><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                              \end{dmath}}
                         </xsl:if>
                         \indent {\large The condition is:}
                         {\color{NavyBlue}
                         <xsl:for-each select="mms:mathExpressions/mt:math">
                              \begin{dmath}
                              <xsl:apply-templates select="."><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                              \end{dmath}
                         </xsl:for-each>}
                         \end{mygrouping}
                    </xsl:for-each>
               </xsl:if>
          </xsl:if>
            
      <!--finalizationconditions-->      
     <xsl:if test="count(mms:finalizationConditions) &gt; 0">
          \indent{\Large Finalization Conditions}        \\     
          <xsl:if test="count(mms:finalizationConditions/mms:finalizationCondition) &gt; 0">
               <xsl:for-each select="mms:finalizationConditions/mms:finalizationCondition">
                    \begin{mygrouping}
                    <xsl:if test="mms:applyIf">
                         \indent {\large The condition is applied when:}
                         {\color{NavyBlue}
                         \begin{dmath}
                         <xsl:apply-templates select="mms:applyIf/mt:math"><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                         \end{dmath}}
                    </xsl:if>
                    \indent {\large The condition is:}
                    {\color{NavyBlue}
                    <xsl:for-each select="mms:mathExpressions/mt:math">
                         \begin{dmath}
                         <xsl:apply-templates select="."><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                         \end{dmath}
                    </xsl:for-each>}
                    \end{mygrouping}
               </xsl:for-each>     
          </xsl:if>
     </xsl:if>
</xsl:template>
</xsl:stylesheet>
