<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:m="http://www.w3.org/1998/Math/MathML" version="1.0">
                
                <!-- ====================================================================== -->
                <!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
                                http://www.iac3.eu/simflowny/copyright_and_license.html  -->
                <!-- ====================================================================== -->
                
                <xsl:param name="condition"/>
                <xsl:param name="encapsulate" select="'yes'"/>
                <xsl:param name="indent" select="1"/>
                
                <xsl:output method="text" indent="no" encoding="UTF-8"/>
                
                
                <xsl:include href="tokens.xsl"/>
                <xsl:include href="glayout.xsl"/>
                <xsl:include href="scripts.xsl"/>
                <xsl:include href="tables.xsl"/>
                <xsl:include href="entities.xsl"/>
                <xsl:include href="cmarkup.xsl"/>                                
                <xsl:include href="imarkupPseudocode.xsl"/>

                <xsl:strip-space elements="m:*"/>

                <xsl:template match="m:math">
                                <xsl:param name="indent" select="$indent"/>
                                <xsl:param name="condition" select="$condition"/>
                                <xsl:param name="encapsulate" select="$encapsulate"/>
                                <xsl:if test="$condition = 'false' or $condition = ''">
                                                <xsl:text>\vspace*{-8pt}</xsl:text>        
                                </xsl:if> 
                                <xsl:if test="$encapsulate != 'no'"><xsl:text>\textcolor{NavyBlue}{
                                </xsl:text></xsl:if>
                                <xsl:choose>
                                                <xsl:when test="$condition = 'false' or $condition = ''">
                                                                <xsl:text>\begin{dmath*}[style={\mathindent=5pt}]
                                                                </xsl:text>  
                                                                <xsl:call-template name="recIndent">
                                                                                <xsl:with-param name="indent" select="$indent"/>
                                                                                <xsl:with-param name="paragraph" select="0"/>
                                                                </xsl:call-template>          
                                                </xsl:when>
                                                <xsl:when test="$condition = 'true'">
                                                                <xsl:text>&#x00024;</xsl:text>
                                                </xsl:when>  
                                </xsl:choose>
                                <xsl:apply-templates>
                                                <xsl:with-param name="condition" select="$condition"/>
                                                <xsl:with-param name="indent" select="$indent"></xsl:with-param>
                                </xsl:apply-templates>
                                <xsl:choose>
                                                <xsl:when test="$condition = 'false' or $condition = ''">
                                                                <xsl:text>\end{dmath*} </xsl:text>       
                                                </xsl:when>
                                                <xsl:when test="$condition = 'true'">
                                                                <xsl:text>&#x00024;</xsl:text>
                                                </xsl:when>
                                </xsl:choose>
                                <xsl:if test="$encapsulate != 'no'"><xsl:text>}</xsl:text></xsl:if><xsl:if test="$condition = 'false' or $condition = ''"><xsl:text>\vspace*{-8pt}</xsl:text></xsl:if>
                </xsl:template>
                
</xsl:stylesheet>
