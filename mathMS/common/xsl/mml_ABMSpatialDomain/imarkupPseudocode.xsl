<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:s="urn:simml" xmlns="urn:mathms" xmlns:fn="http://www.w3.org/2005/02/xpath-functions" version="1.0">

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->


	<xsl:template match="s:iterateOverAgents">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:for-each select="*">
			<!--xsl:text>
				</xsl:text-->
			<xsl:apply-templates select=".">
				<xsl:with-param name="indent" select="$indent"/>
				<xsl:with-param name="condition" select="false"/>
			</xsl:apply-templates>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="s:iterateOverInteractions">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:variable name="cond">
			<xsl:choose>
				<xsl:when test="count(parent::boundary) = 1">true</xsl:when>
				<xsl:otherwise>false</xsl:otherwise>
			</xsl:choose>	
		</xsl:variable>
		<xsl:text>\textbf{\color{BlueViolet} IterateOverInteractions}</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet} MaxInteractionRange}</xsl:text>
		<xsl:apply-templates select="s:maxInteractionRange"><xsl:with-param name="indent" select="$indent + 2"/><xsl:with-param name="condition" select="'false'"/>
		</xsl:apply-templates>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		\textbf{\color{BlueViolet} End MaxInteractionRange}
		<xsl:for-each select="*">
			<xsl:if test="position()  &gt; 1">
				<xsl:apply-templates select=".">
					<xsl:with-param name="indent" select="$indent + 1"/>
					<xsl:with-param name="condition" select="$cond"/>
				</xsl:apply-templates>
			</xsl:if>
		</xsl:for-each>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		\textbf{\color{BlueViolet} End IterateOverInteractions}
	</xsl:template>
	
	<xsl:template match="s:randomNumber">
		<xsl:if test="@typeAtt = 'int' or @typeAtt = 'real'">
			<xsl:text>\textbf{RandomNumber(}</xsl:text>
			<xsl:if test="./@rangeMinAtt">
				<xsl:value-of select="./@rangeMinAtt"/><xsl:text>, </xsl:text>
			</xsl:if>
			<xsl:value-of select="./@rangeMaxAtt"/>
			<xsl:text>\textbf{)}</xsl:text>
		</xsl:if>
		<xsl:if test="@typeAtt = 'uniform'">
			<xsl:text>\textbf{RandomNumber(}0, 1\textbf{)}</xsl:text>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="s:iterationNumber">
		<xsl:text>\textbf{IterationNumber()}</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:currentTime">
		<xsl:text>\textbf{CurrentTime()}</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:currentAgent">
		<xsl:text>\textbf{ca}</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:influenceRadius">
		<xsl:text>\textbf{influenceRadius}</xsl:text>	
	</xsl:template>
	
	<xsl:template match="s:interiorDomainContext">
		<xsl:param name="indent" select="$indent"/>
		<xsl:for-each select="./*">
			<xsl:apply-templates select=".">
				<xsl:with-param name="indent" select="$indent"/>
				<xsl:with-param name="condition" select="false"/>
			</xsl:apply-templates>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template match="s:deleteAgent">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet} DeleteAgent()}</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:createAgent">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet} CreateAgent()}</xsl:text>
	</xsl:template>
		
	<xsl:template match="s:if">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet} if} </xsl:text>
		<xsl:for-each select="*[not(local-name()='then') and not(local-name()='else')]">
			<xsl:if test="position() &gt; 1">
				<xsl:text> \textbf{\color{BlueViolet} and} </xsl:text>
			</xsl:if>
			<xsl:if test="count(*[not(local-name(node())='then')]) &gt; 1">
				<xsl:text>(</xsl:text>
			</xsl:if>
			<xsl:apply-templates select=".">
				<xsl:with-param name="condition" select="'true'"/>
			</xsl:apply-templates>
			<xsl:if test="count(*[not(local-name(node())='then')]) &gt; 1">
				<xsl:text>)</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text> \textbf{\color{BlueViolet} then}</xsl:text>
		<xsl:for-each select="s:then/*">
			<xsl:text>
            </xsl:text>
			<xsl:apply-templates select=".">
			    <xsl:with-param name="indent" select="$indent + 1"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
		</xsl:for-each>
		<xsl:if test="./s:else">
			<xsl:text>
            </xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>\textbf{\color{BlueViolet} else}</xsl:text>
			<xsl:for-each select="s:else/*">
				<xsl:text>
                </xsl:text>
				<xsl:apply-templates select=".">
				    <xsl:with-param name="indent" select="$indent + 1"/>
					<xsl:with-param name="condition" select="'false'"/>
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:if>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet} end if}</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:while">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>		
		<xsl:text>\textbf{\color{BlueViolet} while} </xsl:text>
		<!-- Conditional block -->
		<xsl:for-each select="*[not(local-name()='loop')]">
			<xsl:if test="position() &gt; 1">
				<xsl:text> \textbf{\color{BlueViolet} and} </xsl:text>
			</xsl:if>
			<xsl:if test="count(*[not(local-name(node())='loop')]) &gt; 1">
				<xsl:text>(</xsl:text>
			</xsl:if>
			<xsl:apply-templates select=".">
				<xsl:with-param name="condition" select="'true'"/>
			</xsl:apply-templates>
			<xsl:if test="count(*[not(local-name(node())='loop')]) &gt; 1">
				<xsl:text>)</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<!-- loop -->
		<xsl:text> \textbf{\color{BlueViolet} do}</xsl:text>
		<xsl:for-each select="s:loop/*">
			<xsl:text>
            </xsl:text>
			<xsl:apply-templates select=".">
			    <xsl:with-param name="indent" select="$indent + 1"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
		</xsl:for-each>
		<xsl:text>
        </xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet} end do}</xsl:text>
	</xsl:template>
			
	<xsl:template match="s:checkFinalization">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet} if FinalizationCondition() then}</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet} exit()}</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet} end if}</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:neighbourAgent">
		<xsl:text>\textbf{na}</xsl:text>		
	</xsl:template>
	
	<xsl:template match="s:globalNumberOfAgents">
		<xsl:text>\textbf{\globalNumberOfAgents()}</xsl:text>
	</xsl:template>
	
		<xsl:template match="s:agentDistance">
			<xsl:text>\textbf{distance()}</xsl:text>	
	</xsl:template>
	
	<xsl:template name="recIndent">
		<xsl:param name="indent"/>
		<xsl:param name="paragraph" select="1"></xsl:param>
		<xsl:if test="$paragraph > 0">
			<xsl:text>\par</xsl:text>
		</xsl:if>
		<xsl:text> \hspace*{</xsl:text><xsl:value-of select="15*$indent"/><xsl:text>pt} </xsl:text>
	</xsl:template>
	
</xsl:stylesheet>
