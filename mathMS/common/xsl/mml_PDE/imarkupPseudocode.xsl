<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:s="urn:simml" xmlns="urn:mathms" xmlns:fn="http://www.w3.org/2005/02/xpath-functions" version="1.0">

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->

	<xsl:template match="s:iterateOverCells|s:iterateOverParticles|s:iterateOverParticlesFromCell">
		<xsl:param name="indent" select="$indent"/>
		<xsl:variable name="cond">
			<xsl:choose>
				<xsl:when test="count(parent::s:boundary) = 1">true</xsl:when>
				<xsl:otherwise>false</xsl:otherwise>
			</xsl:choose>	
		</xsl:variable>
		
		<xsl:for-each select="*">
			<xsl:apply-templates select=".">
				<xsl:with-param name="indent" select="$indent"/>
				<xsl:with-param name="condition" select="$cond"/>
			</xsl:apply-templates>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template match="s:if">
		<xsl:param name="indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet}if} </xsl:text>
		<xsl:choose>
			<xsl:when test="@fieldAtt = 'forAll'">
				<xsl:text>\textbf{$\forall$field} </xsl:text>		
			</xsl:when>
			<xsl:when test="@fieldAtt = 'exists'">
				<xsl:text>\textbf{$\exists$field} </xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="@coordinateAtt = 'forAll'">
				<xsl:text>\textbf{$\forall$coord} </xsl:text>		
			</xsl:when>
			<xsl:when test="@coordinateAtt = 'exists'">
				<xsl:text>\textbf{$\exists$coord} </xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:text>(</xsl:text>
		<xsl:for-each select="*[not(local-name()='then') and not(local-name()='else')]">
			<xsl:if test="position() &gt; 1">
				<xsl:text> \textbf{\color{BlueViolet}and} </xsl:text>
			</xsl:if>
			<xsl:if test="count(*[not(local-name(node())='then')]) &gt; 1">
				<xsl:text>(</xsl:text>
			</xsl:if>
			<xsl:apply-templates select=".">
				<xsl:with-param name="condition" select="'true'"/>
			</xsl:apply-templates>
			<xsl:if test="count(*[not(local-name(node())='then')]) &gt; 1">
				<xsl:text>)</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>) \textbf{\color{BlueViolet}then}</xsl:text>
		<xsl:for-each select="s:then/*">
			<xsl:text>
            </xsl:text>
			<xsl:apply-templates select=".">
			    <xsl:with-param name="indent" select="$indent + 1"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
		</xsl:for-each>
		<xsl:if test="./s:else">
			<xsl:text>
            </xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>\textbf{\color{BlueViolet}else}</xsl:text>
			<xsl:for-each select="s:else/*">
				<xsl:text>
                </xsl:text>
				<xsl:apply-templates select=".">
				    <xsl:with-param name="indent" select="$indent + 1"/>
					<xsl:with-param name="condition" select="'false'"/>
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:if>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet}end if}</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:while">
		<xsl:param name="indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>		
		<xsl:text>\textbf{\color{BlueViolet}while} </xsl:text>
		<xsl:choose>
			<xsl:when test="@fieldAtt = 'forAll'">
				<xsl:text>\textbf{$\forall$field} </xsl:text>		
			</xsl:when>
			<xsl:when test="@fieldAtt = 'exists'">
				<xsl:text>\textbf{$\exists$field} </xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="@coordinateAtt = 'forAll'">
				<xsl:text>\textbf{$\forall$coord} </xsl:text>		
			</xsl:when>
			<xsl:when test="@coordinateAtt = 'exists'">
				<xsl:text>\textbf{$\exists$coord} </xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:text>(</xsl:text>
		<!-- Conditional block -->
		<xsl:for-each select="*[not(local-name()='loop')]">
			<xsl:if test="position() &gt; 1">
				<xsl:text> \textbf{\color{BlueViolet}and} </xsl:text>
			</xsl:if>
			<xsl:if test="count(*[not(local-name(node())='loop')]) &gt; 1">
				<xsl:text>(</xsl:text>
			</xsl:if>
			<xsl:apply-templates select=".">
				<xsl:with-param name="condition" select="'true'"/>
			</xsl:apply-templates>
			<xsl:if test="count(*[not(local-name(node())='loop')]) &gt; 1">
				<xsl:text>)</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<!-- loop -->
		<xsl:text>) \textbf{\color{BlueViolet}do}</xsl:text>
		<xsl:for-each select="s:loop/*">
			<xsl:text>
            </xsl:text>
			<xsl:apply-templates select=".">
			    <xsl:with-param name="indent" select="$indent + 1"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
		</xsl:for-each>
		<xsl:text>
        </xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet}end do}</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:choose">
		<xsl:param name="indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet}case} \hspace{1 mm} </xsl:text><xsl:value-of select="s:param"/><xsl:text> \hspace{1 mm} \textbf{\color{BlueViolet}of}</xsl:text>	
		<xsl:for-each select="s:then">
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent + 1"/>
			</xsl:call-template>
			<xsl:variable name="pos"><xsl:value-of select="position()"/></xsl:variable>
			<xsl:text>\textbf{"}</xsl:text><xsl:value-of select="../s:when[position() = $pos]"/><xsl:text>\textbf{":}</xsl:text>
			<xsl:apply-templates select="./*">
				<xsl:with-param name="indent" select="$indent + 2"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>					
		</xsl:for-each>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet}end case}</xsl:text>		
	</xsl:template>	
	
	<xsl:template match="s:import">
		<xsl:param name="indent"/>
		<xsl:variable name="indentAux">
			<xsl:choose>
				<xsl:when test="$indent = ''">1</xsl:when>
				<xsl:otherwise><xsl:value-of select="$indent"/></xsl:otherwise>
			</xsl:choose>						
		</xsl:variable>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indentAux"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet}import}
		</xsl:text>
		<xsl:call-template name="recIndent"><xsl:with-param name="indent" select="$indentAux+1"/></xsl:call-template>\textbf{rule:} <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="s:ruleId"/></xsl:call-template>\\
		<xsl:call-template name="recIndent"><xsl:with-param name="indent" select="$indentAux+1"/></xsl:call-template>\textbf{name:} <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="s:name"/></xsl:call-template>\\
		<xsl:for-each select="s:importParams/s:importParam">
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indentAux+1"/>
			</xsl:call-template>	
			\textbf{\color{BlueViolet}param:}<xsl:value-of select="."/>
		</xsl:for-each>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indentAux"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet}end import}
		</xsl:text>					
	</xsl:template>

	<xsl:template match="s:exit">
	    <xsl:param name="indent"/>
	    <xsl:call-template name="recIndent">
	        <xsl:with-param name="indent" select="$indent"/>
	    </xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet}exit()}</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:periodicalBoundary">
		<xsl:param name="indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet}periodicalBoundary()}</xsl:text>
	</xsl:template>
	
	
	<xsl:template match="s:return">
		<xsl:param name="indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet}return:} </xsl:text><xsl:apply-templates select="child::node()">
				<xsl:with-param name="indent" select="$indent + 1"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>		
	</xsl:template>
	
	<xsl:template match="s:spatialCoordinate">
		<xsl:text>\textbf{i}</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:field">
		<xsl:text>\textbf{u}</xsl:text>	
	</xsl:template>
	
	<xsl:template match="s:timeIncrement">
	   <xsl:text>\Delta\textbf{t}</xsl:text>	
	</xsl:template>
	
	<xsl:template match="s:timeCoordinate">
		<xsl:text>\textbf{n}</xsl:text>	
	</xsl:template>
	
	<xsl:template match="s:currentCell">
		<xsl:text>\textbf{i}</xsl:text>	
	</xsl:template>

	<xsl:template match="s:contSpatialCoordinate">
		<xsl:text>\textbf{x}</xsl:text>	
	</xsl:template>
	
	<xsl:template match="s:contTimeCoordinate">
		<xsl:text>\textbf{t}</xsl:text>	
	</xsl:template>

	<xsl:template match="s:param">
		<xsl:value-of select="."/>	
	</xsl:template>	

	<xsl:template match="s:coordinate">
		<xsl:text>\textbf{i}</xsl:text>	
	</xsl:template>

	<xsl:template match="s:contCoordinate">
		<xsl:text>\textbf{x}</xsl:text>	
	</xsl:template>
	
	<xsl:template match="s:minRegionValue">
		<xsl:text>\textbf{\color{BlueViolet}minRegionValue()}</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:maxRegionValue">
		<xsl:text>\textbf{\color{BlueViolet}maxRegionValue()}</xsl:text>
	</xsl:template>

	<xsl:template match="s:incrementCoordinate">
		<xsl:text>\textbf{i} + </xsl:text><xsl:value-of select="."/><xsl:text></xsl:text>	
	</xsl:template>
	
	<xsl:template match="s:decrementCoordinate">
		<xsl:text>\textbf{i} - </xsl:text><xsl:value-of select="."/><xsl:text></xsl:text>	
	</xsl:template>	
	
	<xsl:template match="s:maxCharSpeedCoordinate">
		<xsl:text>\textbf{max(charSpeedCoordinate)}</xsl:text>	
	</xsl:template>
	
	<xsl:template match="s:maxCharSpeed">
		<xsl:text>\textbf{max(charSpeed)}</xsl:text>	
	</xsl:template>
	
   <xsl:template match="s:sign">
      <xsl:text>\textbf{SIGN(}</xsl:text>
         <xsl:apply-templates select="./*[1]">
            <xsl:with-param name="indent" select="0"/>
            <xsl:with-param name="condition" select="'false'"/>
         </xsl:apply-templates><xsl:text>\textbf{)}</xsl:text>	
   </xsl:template>
	
   <xsl:template match="s:readFromFileLinear1D">
      <xsl:text>\textbf{readFromFileLinear1D(}</xsl:text><xsl:for-each select="*[ position() > 1]">
         <xsl:apply-templates select=".">
            <xsl:with-param name="indent" select="0"/>
            <xsl:with-param name="condition" select="'false'"/>
         </xsl:apply-templates><xsl:if test="position()!=last()">,</xsl:if>
      </xsl:for-each><xsl:text>\textbf{)}</xsl:text>	
   </xsl:template>
	
   <xsl:template match="s:readFromFileLinear2D">
      <xsl:text>\textbf{readFromFileLinear2D(}</xsl:text><xsl:for-each select="*[ position() > 1]">
         <xsl:apply-templates select=".">
            <xsl:with-param name="indent" select="0"/>
            <xsl:with-param name="condition" select="'false'"/>
         </xsl:apply-templates><xsl:if test="position()!=last()">,</xsl:if>
      </xsl:for-each><xsl:text>\textbf{)}</xsl:text>	
   </xsl:template>
   
	<xsl:template match="s:readFromFileLinear3D">
      <xsl:text>\textbf{readFromFileLinear3D(}</xsl:text><xsl:for-each select="*[ position() > 1]">
         <xsl:apply-templates select=".">
            <xsl:with-param name="indent" select="0"/>
            <xsl:with-param name="condition" select="'false'"/>
         </xsl:apply-templates><xsl:if test="position()!=last()">,</xsl:if>
      </xsl:for-each><xsl:text>\textbf{)}</xsl:text>	
   </xsl:template>
	
	<xsl:template match="s:readFromFileLinearWithDerivatives1D">
		<xsl:text>\textbf{readFromFileLinearWithDerivatives1D(}</xsl:text><xsl:for-each select="*[ position() > 1]">
			<xsl:apply-templates select=".">
				<xsl:with-param name="indent" select="0"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates><xsl:if test="position()!=last()">,</xsl:if>
		</xsl:for-each><xsl:text>\textbf{)}</xsl:text>	
	</xsl:template>
	
	<xsl:template match="s:readFromFileLinearWithDerivatives2D">
		<xsl:text>\textbf{readFromFileLinearWithDerivatives2D(}</xsl:text><xsl:for-each select="*[ position() > 1]">
			<xsl:apply-templates select=".">
				<xsl:with-param name="indent" select="0"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates><xsl:if test="position()!=last()">,</xsl:if>
		</xsl:for-each><xsl:text>\textbf{)}</xsl:text>	
	</xsl:template>
	
	<xsl:template match="s:readFromFileLinearWithDerivatives3D">
		<xsl:text>\textbf{readFromFileLinearWithDerivatives3D(}</xsl:text><xsl:for-each select="*[ position() > 1]">
			<xsl:apply-templates select=".">
				<xsl:with-param name="indent" select="0"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates><xsl:if test="position()!=last()">,</xsl:if>
		</xsl:for-each><xsl:text>\textbf{)}</xsl:text>	
	</xsl:template>

	<xsl:template match="findCoordFromFile1D">
		<xsl:text>\textbf{findCoordFromFile1D(}</xsl:text><xsl:for-each select="*[ position() > 1]">
			<xsl:apply-templates select=".">
				<xsl:with-param name="indent" select="0"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates><xsl:if test="position()!=last()">,</xsl:if>
		</xsl:for-each><xsl:text>\textbf{)}</xsl:text>	
	</xsl:template>
	
	
	<xsl:template match="findCoordFromFile2D">
		<xsl:text>\textbf{findCoordFromFile2D(}</xsl:text><xsl:for-each select="*[ position() > 1]">
			<xsl:apply-templates select=".">
				<xsl:with-param name="indent" select="0"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates><xsl:if test="position()!=last()">,</xsl:if>
		</xsl:for-each><xsl:text>\textbf{)}</xsl:text>	
	</xsl:template>
	
	
	<xsl:template match="findCoordFromFile3D">
		<xsl:text>\textbf{findCoordFromFile3D(}</xsl:text><xsl:for-each select="*[ position() > 1]">
			<xsl:apply-templates select=".">
				<xsl:with-param name="indent" select="0"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates><xsl:if test="position()!=last()">,</xsl:if>
		</xsl:for-each><xsl:text>\textbf{)}</xsl:text>	
	</xsl:template>

	<xsl:template match="s:readFromFileQuintic">
		<xsl:text>\textbf{readFromFileQuintic(}</xsl:text><xsl:for-each select="*[ position() > 1]">
			<xsl:apply-templates select=".">
				<xsl:with-param name="indent" select="0"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates><xsl:if test="position()!=last()">,</xsl:if>
		</xsl:for-each><xsl:text>\textbf{)}</xsl:text>	
	</xsl:template>
	
	<!--diagonalize tag-->
	<xsl:template match="s:diagonalize">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet}diagonalize(}</xsl:text><xsl:apply-templates select="./s:variable/*"><xsl:with-param name="indent" select="0"/><xsl:with-param name="condition" select="'true'"/></xsl:apply-templates>, <xsl:apply-templates select="./s:timeSlice/*"><xsl:with-param name="indent" select="0"/><xsl:with-param name="condition" select="'true'"/></xsl:apply-templates><xsl:text>\textbf{\color{BlueViolet})}</xsl:text>	
	</xsl:template>
	<!--undiagonalize tag -->
	<xsl:template match="s:undiagonalize">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet}undiagonalize(}</xsl:text><xsl:apply-templates select="./s:variable/*"><xsl:with-param name="indent" select="0"/><xsl:with-param name="condition" select="'true'"/></xsl:apply-templates>, <xsl:apply-templates select="./s:timeSlice/*"><xsl:with-param name="indent" select="0"/><xsl:with-param name="condition" select="'true'"/></xsl:apply-templates><xsl:text>\textbf{\color{BlueViolet})}</xsl:text>	
	</xsl:template>
	
	
	<xsl:template match="s:source">
		<xsl:text>\textbf{source(}</xsl:text><xsl:apply-templates select="./*[1]"><xsl:with-param name="condition" select="'false'"/></xsl:apply-templates><xsl:text>\textbf{)}</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:boundary">
		<xsl:param name="indent" select="$indent"/>
		<xsl:choose>
			<xsl:when test="count(*) = 0"></xsl:when>
			<xsl:when test="count(s:variable) &gt; 0">
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent"/>
				</xsl:call-template>
				<xsl:choose>
					<xsl:when test="count(s:previousVariable) &gt; 0">
						<xsl:text>\textbf{\color{BlueViolet}boundary(}</xsl:text><xsl:apply-templates select="s:variable/*"><xsl:with-param name="indent" select="$indent"/><xsl:with-param name="condition" select="'true'"/></xsl:apply-templates><xsl:text>,</xsl:text><xsl:apply-templates select="s:previousVariable/*"><xsl:with-param name="indent" select="$indent"/><xsl:with-param name="condition" select="'true'"/></xsl:apply-templates><xsl:text>\textbf{\color{BlueViolet})}</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>\textbf{\color{BlueViolet}boundary(}</xsl:text><xsl:apply-templates select="s:variable/*"><xsl:with-param name="indent" select="$indent"/><xsl:with-param name="condition" select="'true'"/></xsl:apply-templates><xsl:text>)\\</xsl:text>
					</xsl:otherwise>
				</xsl:choose>										
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent"/>
				</xsl:call-template>
				<xsl:text>\textbf{\color{BlueViolet}begin boundary}</xsl:text>
				<xsl:for-each select="*">
					<xsl:text>
                    </xsl:text>
					<xsl:apply-templates select=".">
						<xsl:with-param name="indent" select="$indent + 1"/>
						<xsl:with-param name="condition" select="'false'"/>
					</xsl:apply-templates>
				</xsl:for-each>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent"/>
				</xsl:call-template>
				<xsl:text>\textbf{\color{BlueViolet}end boundary}</xsl:text>
			</xsl:otherwise>
		</xsl:choose>					
	</xsl:template>
		
   <xsl:template match="s:randomNumber">
      <xsl:text>\textbf{RND_</xsl:text><xsl:value-of select="./@typeAtt"/> <xsl:if test="./@rangeMinAtt">
         <xsl:text>_</xsl:text><xsl:value-of select="./@rangeMinAtt"/>
      </xsl:if><xsl:if test="./@rangeMaxAtt">
         <xsl:text>_</xsl:text><xsl:value-of select="./@rangeMaxAtt"/>
      </xsl:if><xsl:text>()}</xsl:text>
   </xsl:template>
		
	<xsl:template match="s:functionCall">
		<xsl:value-of select="*[1]"/><xsl:text>(</xsl:text>
		<xsl:for-each select="*[ position() > 1]">
			<xsl:apply-templates select=".">
				<xsl:with-param name="indent" select="$indent"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates><xsl:if test="position()!=last()">,</xsl:if>
		</xsl:for-each>
		<xsl:text>)</xsl:text>
	</xsl:template>
		
	<xsl:template match="s:flux">
		<xsl:value-of select="*[1]"/><xsl:text>(</xsl:text>
		<xsl:for-each select="*[ position() > 1]">
			<xsl:apply-templates select=".">
				<xsl:with-param name="indent" select="$indent"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates><xsl:if test="position()!=last()">,</xsl:if>
		</xsl:for-each>
		<xsl:text>)</xsl:text>
	</xsl:template>
		
	<xsl:template match="s:sharedVariable">
		<xsl:for-each select="*">
			<xsl:apply-templates select=".">
				<xsl:with-param name="indent" select="$indent"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
		</xsl:for-each>
	</xsl:template>
   
   <xsl:template match="s:particlePosition">
      <xsl:text>position</xsl:text><xsl:apply-templates select="./*">
         <xsl:with-param name="indent" select="0"/>
         <xsl:with-param name="condition" select="'false'"/>
      </xsl:apply-templates>
   </xsl:template>
	
	<xsl:template match="s:auxiliaryEquations">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet}auxiliaryEquations(}</xsl:text><xsl:apply-templates select="child::node()"><xsl:with-param name="indent" select="$indent"/><xsl:with-param name="condition" select="'true'"/></xsl:apply-templates><xsl:text>\textbf{\color{BlueViolet})}</xsl:text>
	</xsl:template>
				
	<xsl:template match="s:checkFinalization">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet} if FinalizationCondition() then}</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet} exit()}</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet} end if}</xsl:text>
	</xsl:template>
	
   <xsl:template match="s:dimensions">
      <xsl:text>\textbf{dimensions()}</xsl:text>
   </xsl:template>
	
	<xsl:template match="s:currentTime">
		<xsl:text>\textbf{currentTime()}</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:iterationNumber">
		<xsl:text>\textbf{itNumber()}</xsl:text>
	</xsl:template>
	<!--___________ field group tag_______________--> 
	<xsl:template match="s:fieldGroup">
		<xsl:param name="indent" select="$indent"/>
		
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		\textbf{\color{BlueViolet}FieldGroup <xsl:value-of select="@name"/>}
		<xsl:for-each select="*">
			<xsl:apply-templates select=".">
				<xsl:with-param name="indent" select="$indent"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
		</xsl:for-each>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		\textbf{\color{BlueViolet}End FieldGroup <xsl:value-of select="@name"/>}
	</xsl:template>
		
	<xsl:template match="s:secondContSpatialCoordinate">
		<xsl:text>y</xsl:text>	
	</xsl:template>
	
	<xsl:template match="s:secondSpatialCoordinate">
		<xsl:text>j</xsl:text>	
	</xsl:template>
	
	
	<xsl:template match="s:incrementCoordinate2">
		<xsl:text>i,j + </xsl:text><xsl:value-of select="."/><xsl:text></xsl:text>	
	</xsl:template>
	
	<xsl:template match="s:decrementCoordinate2">
		<xsl:text>i,j - </xsl:text><xsl:value-of select="."/><xsl:text></xsl:text>	
	</xsl:template>
	
	<xsl:template match="s:incrementCoordinate1">
		<xsl:text>i + </xsl:text><xsl:value-of select="."/>,j<xsl:text></xsl:text>	
	</xsl:template>
	
	<xsl:template match="s:decrementCoordinate1">
		<xsl:text>i - </xsl:text><xsl:value-of select="."/>,j<xsl:text></xsl:text>	
	</xsl:template>
	
	<xsl:template match="s:incrementCoordinate1IncrementCoordinate2">
		<xsl:text>i + </xsl:text><xsl:value-of select="substring-before(.,',')"/><xsl:text>,</xsl:text>
		<xsl:text>j + </xsl:text><xsl:value-of select="substring-after(.,',')"/><xsl:text></xsl:text>
	</xsl:template>
	
	<xsl:template match="s:incrementCoordinate1DecrementCoordinate2">
		<xsl:text>i + </xsl:text><xsl:value-of select="substring-before(.,',')"/><xsl:text>,</xsl:text>
		<xsl:text>j - </xsl:text><xsl:value-of select="substring-after(.,',')"/><xsl:text></xsl:text>
	</xsl:template>
	
	<xsl:template match="s:decrementCoordinate1IncrementCoordinate2">
		<xsl:text>i - </xsl:text><xsl:value-of select="substring-before(.,',')"/><xsl:text>,</xsl:text>
		<xsl:text>j + </xsl:text><xsl:value-of select="substring-after(.,',')"/><xsl:text></xsl:text>
	</xsl:template>
	
	<xsl:template match="s:decrementCoordinate1DecrementCoordinate2">
		<xsl:text>i - </xsl:text><xsl:value-of select="substring-before(.,',')"/><xsl:text>,</xsl:text>
		<xsl:text>j - </xsl:text><xsl:value-of select="substring-after(.,',')"/><xsl:text></xsl:text>
	</xsl:template>
	
	
	<!--____________________________________________ PARTICLES TAGS ___________________________________________________-->
	
	
	<!--IterateOverInteraction-->
	<xsl:template match="s:IterateOverInteraction">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		\textbf{\color{BlueViolet}IterateOverInteraction}
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		\textbf{\color{BlueViolet}MaxInteractionRange}
		<xsl:apply-templates select="s:maxInteractionRange"><xsl:with-param name="indent" select="$indent + 2"/><xsl:with-param name="condition" select="'false'"/>
		</xsl:apply-templates>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		\textbf{\color{BlueViolet}End maxInteractionRange}
		<xsl:for-each select="*">
			<xsl:if test="position()  &gt; 1">
				<xsl:apply-templates select=".">
					<xsl:with-param name="indent" select="$indent + 1"/>
					<xsl:with-param name="condition" select="'false'"/>
				</xsl:apply-templates>
				<xsl:text>
            	</xsl:text>
			</xsl:if>			
		</xsl:for-each>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		\textbf{\color{BlueViolet}End IterateOverInteraction}
	</xsl:template>
	
	<!--______neighbourParticle________-->
	<xsl:template match="s:neighbourParticle">
		<xsl:text>\textbf{neighbour}(</xsl:text><xsl:apply-templates select="./*[1]"><xsl:with-param name="indent" select="0"/><xsl:with-param name="condition" select="'false'"/></xsl:apply-templates><xsl:text>)\\</xsl:text>		
	</xsl:template>
	
		
	<xsl:template match="s:particleDistance">
	   <xsl:text>\textbf{distance()}</xsl:text>	
	</xsl:template>
	
   <xsl:template match="s:influenceRadius">
      <xsl:text>\textbf{influenceRadius}</xsl:text>	
	</xsl:template>
	
	<xsl:template match="s:particleVolume">
	   <xsl:text>\textbf{volume()}</xsl:text>	
	</xsl:template>
   
   <xsl:template match="s:kernel">
      <xsl:text>\textbf{kernel()}</xsl:text>
   </xsl:template>
   
   <xsl:template match="s:particleVelocity">
      <xsl:text>\textbf{velocity()}</xsl:text>	
   </xsl:template>
      
   <xsl:template match="s:kernelGradient">
      <xsl:text>\nabla \textbf{kernel()}</xsl:text>
   </xsl:template>
	
	<xsl:template match="s:moveParticles">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>\textbf{\color{BlueViolet}moveParticles(}</xsl:text><xsl:apply-templates select="s:newPosition"><xsl:with-param name="indent" select="$indent"/><xsl:with-param name="condition" select="'true'"/></xsl:apply-templates><xsl:text>,</xsl:text><xsl:apply-templates select="s:previousPosition"><xsl:with-param name="indent" select="$indent"/><xsl:with-param name="condition" select="'true'"/></xsl:apply-templates><xsl:text>\textbf{\color{BlueViolet})}</xsl:text>		
	</xsl:template>
	
	<xsl:template name="recIndent">
		<xsl:param name="indent"/>
		<xsl:param name="paragraph" select="1"></xsl:param>
			<xsl:if test="$paragraph > 0">
				<xsl:text>
					\par \noindent</xsl:text>
		</xsl:if>
		<xsl:text> \hspace*{</xsl:text><xsl:value-of select="15*$indent"/><xsl:text>pt} </xsl:text>
	</xsl:template>
	
</xsl:stylesheet>
