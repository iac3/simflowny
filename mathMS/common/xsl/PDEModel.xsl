<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:mt="http://www.w3.org/1998/Math/MathML"
   xmlns:mms="urn:mathms"
   xmlns:sml="urn:simml"
   version='2.0'>                
   <xsl:output method="text" indent="no" encoding="UTF-8" media-type="application/x-tex"/>
   
   <!-- ====================================================================== -->
   <!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
      http://www.iac3.eu/simflowny/copyright_and_license.html  -->
   <!-- ====================================================================== -->
   
   <xsl:include href="eigenSpace.xsl"/>
   <xsl:include href="parameters.xsl"/>
   <xsl:include href="mml_PDE/instructionToPseudocode.xsl"/>
   
   <xsl:strip-space elements="mt:*"/>
   
   <xsl:template match="mms:PDEModel">
      <xsl:if test="count(mms:head) &gt; 0">
         \begin{center}
         \textbf{<xsl:choose><xsl:when test="count(mms:head/mms:name) &gt; 0"><xsl:value-of select="mms:head/mms:name"/></xsl:when><xsl:otherwise>PDE Model</xsl:otherwise></xsl:choose>}      
         <!--head-->
         <xsl:if test="mms:head/mms:author/text() != ''">
            by <xsl:value-of select="mms:head/mms:author"/>\\    
         </xsl:if>
         <xsl:if test="mms:head/mms:version/text() != ''">
            Version: <xsl:value-of select="mms:head/mms:version"/>\\
         </xsl:if> 
         <xsl:if test="count(mms:head/mms:date) &gt; 0">
            <xsl:value-of select=' format-dateTime(mms:head/mms:date, "[MNn] [D], [Y]", "en", (), ())'/>\\
         </xsl:if>
         \end{center}
         <xsl:if test="mms:head/mms:description/text() != ''">
            \noindent <xsl:value-of select="mms:head/mms:description"/>\\
         </xsl:if> 
         \noindent \rule{\textwidth}{1pt}\\
      </xsl:if>
      <!--fields-->
      <xsl:if test="count(mms:fields/mms:field) &gt; 0">      
         \indent {\Large Fields}
         
         \indent \indent <xsl:for-each select="mms:fields/mms:field">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
      </xsl:if>
      
      <!--spatial coordinates-->
      <xsl:if test="count(mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate) &gt; 0">
         \indent {\Large Spatial Coordinates}      
         
         \indent \indent <xsl:for-each select="mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
      </xsl:if>
      <!--time coordinate-->
      <xsl:if test="count(mms:coordinates/mms:timeCoordinate) &gt; 0">
         \indent {\Large Time Coordinate}       
         
         \indent \indent $<xsl:value-of select="mms:coordinates/mms:timeCoordinate"/>$\\        
      </xsl:if>
      
      <!--auxiliar fields-->
      <xsl:if test="count(mms:auxiliaryFields/mms:auxiliaryField) &gt; 0">      
         \indent {\Large Auxiliary Fields}
         <xsl:text>                               
         </xsl:text>           
         \indent \indent <xsl:for-each select="mms:auxiliaryFields/mms:auxiliaryField">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
      </xsl:if>
      
      <!--auxiliar variables-->
      <xsl:if test="count(mms:auxiliaryVariables/mms:auxiliaryVariable) &gt; 0">      
         \indent {\Large Auxiliary Variables}
         <xsl:text>                               
         </xsl:text>           
         \indent \indent <xsl:for-each select="mms:auxiliaryVariables/mms:auxiliaryVariable">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
      </xsl:if>
      
      <!--parameters-->      
      <xsl:call-template name="parameters"></xsl:call-template>
      <!--evolution equations-->
      <xsl:if test="count(mms:evolutionEquations/mms:evolutionEquation) &gt; 0">
         \indent {\Large Evolution Equations}      
         <xsl:text>                               
         </xsl:text>
         <xsl:for-each select="mms:evolutionEquations/mms:evolutionEquation">
            <xsl:variable name="nameEq"><xsl:choose><xsl:when test="./mms:name != ''"><xsl:value-of select="./mms:name"/></xsl:when><xsl:otherwise>Evolution equation</xsl:otherwise></xsl:choose></xsl:variable>
            <xsl:variable name="field"><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:field"/></xsl:call-template></xsl:variable>
            \begin{mygrouping}
            \indent \indent {\large <xsl:value-of select="$nameEq"/>}          
            \begin{dmath}
            <xsl:text>{\partial_</xsl:text><xsl:value-of select="/mms:PDEModel/mms:coordinates/mms:timeCoordinate"/> <xsl:value-of select="./mms:field"/><xsl:text>} </xsl:text>
            <xsl:for-each select="./mms:fluxes/mms:flux">+{{\partial F<xsl:if test="$field != ''">^{(<xsl:value-of select="$field"/>)}</xsl:if>_{<xsl:value-of select="mms:coordinate"/>}} \over {\partial <xsl:value-of select="mms:coordinate"/>}}</xsl:for-each>
            <xsl:text> = S</xsl:text><xsl:if test="$field != ''">^{(<xsl:value-of select="$field"/>)}</xsl:if> <xsl:if test="mms:operator"><xsl:text>+</xsl:text></xsl:if><xsl:if test="not(mms:operator)"><xsl:text>\nonumber</xsl:text></xsl:if>
            <xsl:for-each select="mms:operator">
               <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/></xsl:call-template><xsl:text>(</xsl:text><xsl:for-each select="/mms:PDEModel/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate"><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:text>, </xsl:text></xsl:for-each><xsl:value-of select="/mms:PDEModel/mms:coordinates/mms:timeCoordinate"/><xsl:text>)</xsl:text>
               <xsl:if test="position()!=last()">+</xsl:if>
               <xsl:if test="position()=last()">\nonumber</xsl:if>
            </xsl:for-each>
            \end{dmath}
            <xsl:if test="./mms:fluxes">
               where the fluxes and sources are:
               <!--flux--><xsl:for-each select="mms:fluxes/mms:flux">\begin{dmath}
                  F<xsl:if test="$field != ''">^{(<xsl:value-of select="$field"/>)}</xsl:if>_{<xsl:value-of select="mms:coordinate"/>} = <xsl:apply-templates select="mt:math"><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates> \end{dmath}</xsl:for-each>     
               <!--Source-->\begin{dmath}S<xsl:if test="$field != ''">^{(<xsl:value-of select="$field"/>)}</xsl:if> = <xsl:if test="mms:source/mt:math"><xsl:apply-templates select="mms:source/mt:math"><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates></xsl:if><xsl:if test="not(mms:source/mt:math)">0</xsl:if> \end{dmath} 
            </xsl:if>
            
            <xsl:for-each select="./mms:operator">
               \begin{dmath}
               <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/></xsl:call-template><xsl:text>(</xsl:text><xsl:for-each select="/mms:PDEModel/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate"><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:text>, </xsl:text></xsl:for-each><xsl:value-of select="/mms:PDEModel/mms:coordinates/mms:timeCoordinate"/><xsl:text>) = </xsl:text>
               <xsl:for-each select="mms:term">
                  <xsl:if test="position() > 0 and not(./mt:math/mt:apply[mt:minus and count(*) = 2])">
                     <xsl:text>+</xsl:text>
                  </xsl:if>
                  <xsl:call-template name="processTerm">
                     <xsl:with-param name="term" select="."/>
                  </xsl:call-template>
               </xsl:for-each>
               \end{dmath}
            </xsl:for-each>
            
            \end{mygrouping}
         </xsl:for-each>
      </xsl:if>
      
      <!--auxiliary field equations-->
      <xsl:if test="count(mms:auxiliaryFieldEquations/mms:auxiliaryFieldEquation) &gt; 0">
         \indent {\Large Auxiliary Field Equations}
         <xsl:text>                               
         </xsl:text>
         <xsl:for-each select="mms:auxiliaryFieldEquations/mms:auxiliaryFieldEquation">
            <xsl:if test="mms:definition/sml:simml">
               \begin{mygrouping}
               \indent \indent {\large <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/><xsl:with-param name="prefix" select="'$'"/><xsl:with-param name="sufix" select="'$'"/></xsl:call-template>}\\
               \indent \indent <xsl:for-each select="mms:auxiliaryField">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
               \textbf{Algorithm}
               <xsl:for-each select="mms:definition/sml:simml/*"><xsl:apply-templates select="."><xsl:with-param name="indent">1</xsl:with-param><xsl:with-param name="condition">false</xsl:with-param></xsl:apply-templates></xsl:for-each>
               \end{mygrouping}
            </xsl:if>
            <xsl:if test="mms:definition/mms:algebraic/mt:math">
               \begin{mygrouping}
               <xsl:variable name="field"><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:auxiliaryField"/></xsl:call-template></xsl:variable>
               \indent \indent {\large <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/><xsl:with-param name="prefix" select="'$'"/><xsl:with-param name="sufix" select="'$'"/></xsl:call-template>}
               \begin{dmath}
               <xsl:value-of select="$field"/> = <xsl:apply-templates select="mms:definition/mms:algebraic/mt:math"><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>
               \end{dmath}
               \end{mygrouping}
            </xsl:if>
            <xsl:if test="mms:definition/mms:algebraic/mms:operator">
               \begin{mygrouping}
               <xsl:variable name="field"><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:auxiliaryField"/></xsl:call-template></xsl:variable>
               \indent \indent {\large <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:equationName"/><xsl:with-param name="prefix" select="'$'"/><xsl:with-param name="sufix" select="'$'"/></xsl:call-template>}
               \begin{dmath}
               <xsl:value-of select="$field"/><xsl:text> = </xsl:text>
               <xsl:for-each select="mms:definition/mms:algebraic/mms:operator">
                  <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/></xsl:call-template><xsl:text>(</xsl:text><xsl:for-each select="/mms:PDEModel/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate"><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:text>, </xsl:text></xsl:for-each><xsl:value-of select="/mms:PDEModel/mms:coordinates/mms:timeCoordinate"/><xsl:text>)</xsl:text>
                  <xsl:if test="position()!=last()">+</xsl:if>
               </xsl:for-each>
               \end{dmath}
               <xsl:for-each select="mms:definition/mms:algebraic/mms:operator">
                  \begin{dmath}
                  <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/></xsl:call-template><xsl:text>(</xsl:text><xsl:for-each select="/mms:PDEModel/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate"><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:text>, </xsl:text></xsl:for-each><xsl:value-of select="/mms:PDEModel/mms:coordinates/mms:timeCoordinate"/><xsl:text>) = </xsl:text>
                  <xsl:for-each select="mms:term">
                     <xsl:if test="position() > 0 and not(./mt:math/mt:apply[mt:minus and count(*) = 2])">
                        <xsl:text>+</xsl:text>
                     </xsl:if>
                     <xsl:call-template name="processTerm">
                        <xsl:with-param name="term" select="."/>
                     </xsl:call-template>
                  </xsl:for-each>
                  \end{dmath}
               </xsl:for-each>
               \end{mygrouping}
            </xsl:if>
         </xsl:for-each>
      </xsl:if>
      
      <!--auxiliary variables equations-->
      <xsl:if test="count(mms:auxiliaryVariableEquations/mms:auxiliaryVariableEquation) &gt; 0">
         \indent {\Large Auxiliary Variable Equations}
         <xsl:text>                               
         </xsl:text>
         <xsl:for-each select="mms:auxiliaryVariableEquations/mms:auxiliaryVariableEquation">
            <xsl:variable name="nameEq"><xsl:choose><xsl:when test="./mms:name != ''"><xsl:value-of select="./mms:name"/></xsl:when><xsl:otherwise>Auxiliary variable equation</xsl:otherwise></xsl:choose></xsl:variable>
            \begin{mygrouping}
            <xsl:variable name="field"><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:auxiliaryVariable"/></xsl:call-template></xsl:variable>
            \indent \indent {\large <xsl:value-of select="$nameEq"/>}
            \begin{dmath}
            <xsl:value-of select="$field"/><xsl:text> = </xsl:text>
            <xsl:for-each select="mms:operator">
               <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/></xsl:call-template><xsl:text>(</xsl:text><xsl:for-each select="/mms:PDEModel/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate"><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:text>, </xsl:text></xsl:for-each><xsl:value-of select="/mms:PDEModel/mms:coordinates/mms:timeCoordinate"/><xsl:text>)</xsl:text>
               <xsl:if test="position()!=last()">+</xsl:if>
            </xsl:for-each>
            \end{dmath}
            <xsl:for-each select="mms:operator">
               \begin{dmath}
               <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/></xsl:call-template><xsl:text>(</xsl:text><xsl:for-each select="/mms:PDEModel/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate"><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:text>, </xsl:text></xsl:for-each><xsl:value-of select="/mms:PDEModel/mms:coordinates/mms:timeCoordinate"/><xsl:text>) = </xsl:text>
               <xsl:for-each select="mms:term">
                  <xsl:if test="position() > 0 and not(./mt:math/mt:apply[mt:minus and count(*) = 2])">
                     <xsl:text>+</xsl:text>
                  </xsl:if>
                  <xsl:call-template name="processTerm">
                     <xsl:with-param name="term" select="."/>
                  </xsl:call-template>
               </xsl:for-each>
               \end{dmath}
            </xsl:for-each>
            \end{mygrouping}
         </xsl:for-each>
      </xsl:if>
      
      <!--field recoveries-->
      <xsl:if test="count(mms:fieldRecoveries/mms:fieldRecovery) &gt; 0">
         \indent {\Large Field Recoveries}
         <xsl:text>                               
         </xsl:text>
         <xsl:for-each select="mms:fieldRecoveries/mms:fieldRecovery">
            \begin{mygrouping}
            \indent \indent {\large <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/><xsl:with-param name="prefix" select="'$'"/><xsl:with-param name="sufix" select="'$'"/></xsl:call-template>}\\
            \indent \indent <xsl:for-each select="mms:field">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
            \textbf{Algorithm}
            <xsl:for-each select="mms:definition/sml:simml/*"><xsl:apply-templates select="."><xsl:with-param name="indent">1</xsl:with-param><xsl:with-param name="condition">false</xsl:with-param></xsl:apply-templates></xsl:for-each>
            \end{mygrouping}
         </xsl:for-each>
      </xsl:if>
      
      <!--CharDecompositions-->
      <xsl:if test="count(mms:characteristicDecomposition) &gt; 0">
         \indent {\Large Characteristic decomposition}
         <xsl:text>                               
         </xsl:text>
         <xsl:if test="count(mms:characteristicDecomposition/mms:generalCharacteristicDecomposition) &gt; 0">
            \indent \indent {\large General characteristic decomposition}\\
            <xsl:for-each select="//mms:generalCharacteristicDecomposition/mms:eigenSpaces/mms:eigenSpace">
               <xsl:apply-templates select="."><xsl:with-param name="tcoordinateIn"><xsl:value-of select="/mms:PDEModel/mms:coordinates/mms:timeCoordinate"/></xsl:with-param></xsl:apply-templates>
            </xsl:for-each>
            <xsl:if test="count(//mms:generalCharacteristicDecomposition/mms:undiagonalizations/mms:undiagonalization) &gt; 0">
               \indent \indent \indent \textbf{Undiagonalization} 
            </xsl:if>
            \begin{mygrouping}
            <xsl:for-each select="//mms:generalCharacteristicDecomposition/mms:undiagonalizations/mms:undiagonalization">
               <xsl:call-template name="undiagonalization">
                  <xsl:with-param name="tcoordinate"><xsl:value-of select="/mms:PDEModel/mms:coordinates/mms:timeCoordinate"/></xsl:with-param>
               </xsl:call-template>                           
            </xsl:for-each>
            \end{mygrouping}
         </xsl:if>
         <xsl:for-each select="mms:characteristicDecomposition/mms:coordinateCharacteristicDecompositions/mms:coordinateCharacteristicDecomposition">
            \indent \indent {\large Coordinate $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:eigenCoordinate"/></xsl:call-template>$ characteristic decomposition}\\
            <xsl:for-each select=".//mms:eigenSpace">
               <xsl:call-template name="eigenSpace">
                  <xsl:with-param name="tcoordinate"><xsl:value-of select="/mms:PDEModel/mms:coordinates/mms:timeCoordinate"/></xsl:with-param>
               </xsl:call-template>                      
            </xsl:for-each>
            <xsl:if test="count(mms:undiagonalizations/mms:undiagonalization) &gt; 0">
               \indent \indent \indent \textbf{Undiagonalization}
               \begin{mygrouping}
               <xsl:for-each select="mms:undiagonalizations/mms:undiagonalization">
                  <xsl:call-template name="undiagonalization">
                     <xsl:with-param name="tcoordinate"><xsl:value-of select="/mms:PDEModel/mms:coordinates/mms:timeCoordinate"/></xsl:with-param>
                  </xsl:call-template>                           
               </xsl:for-each>
               \end{mygrouping}
            </xsl:if>
         </xsl:for-each>
         <xsl:if test="count(mms:characteristicDecomposition/mms:maxCharacteristicSpeed) &gt; 0">
            \indent \indent {\large Max characteristic speed}
            \begin{mygrouping}
            \begin{dmath}
            V_{max} = <xsl:apply-templates select="mms:characteristicDecomposition/mms:maxCharacteristicSpeed/mt:math"><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>
            \end{dmath}   
            \end{mygrouping}
         </xsl:if>
         
      </xsl:if>      
   </xsl:template>
   
   <!-- process terms-->
   <xsl:template name="processTerm">
      <xsl:param name="term"/>
      <!-- Process math expression -->
      <xsl:if test="$term/mt:math">
         <xsl:apply-templates select="$term/mt:math"><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates><xsl:if test="$term/mms:partialDerivatives/mms:partialDerivative"><xsl:text>\mbox{ }</xsl:text></xsl:if>
      </xsl:if>
      <!-- Process derivatives -->
      <xsl:for-each select="$term/mms:partialDerivatives/mms:partialDerivative">
         <xsl:text>{\partial_</xsl:text><xsl:value-of select="./mms:coordinate"/><xsl:text> </xsl:text><xsl:call-template name="processTerm"><xsl:with-param name="term" select="."></xsl:with-param></xsl:call-template><xsl:text>}</xsl:text> 
      </xsl:for-each>
   </xsl:template>
   
</xsl:stylesheet>
