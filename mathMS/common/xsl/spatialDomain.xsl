<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		      xmlns:mt="http://www.w3.org/1998/Math/MathML"
		      xmlns:mms="urn:mathms"
		      xmlns:my="internalarray"
		      xmlns="urn:mathms"
                version='1.0'>                
<xsl:output method="text" indent="no" encoding="UTF-8"/>

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->

<xsl:strip-space elements="mt:*"/>
     
<xsl:template name="spatialDomain">
     <xsl:param name="sd"></xsl:param>
     <xsl:if test="count($sd) &gt; 0">
          \indent{\Large Spatial Domain}
          
          \begin{mytable}{c|c|c}{15pt}
          \textbf{\small Coordinate} &#038; \textbf{\small Min} &#038; \textbf{\small Max} \\ \hlinegray
          <xsl:for-each select="$sd//mms:coordinateLimits">
               $<xsl:value-of select="mms:coordinate"/>$  &#038; <xsl:apply-templates select="mms:coordinateMin/mt:math"><xsl:with-param name="indent">0</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="condition">true</xsl:with-param></xsl:apply-templates> &#038; <xsl:apply-templates select="mms:coordinateMax/mt:math"><xsl:with-param name="indent">0</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="condition">true</xsl:with-param></xsl:apply-templates> \\          
          </xsl:for-each>
          \end{mytable}
     </xsl:if>
</xsl:template>
   
     
</xsl:stylesheet>
