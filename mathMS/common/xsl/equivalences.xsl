<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		      xmlns:mt="http://www.w3.org/1998/Math/MathML"
		      xmlns:mms="urn:mathms"
		      xmlns:my="internalarray"
		      xmlns="urn:mathms"
                version='1.0'>                
<xsl:output method="text" indent="no" encoding="UTF-8"/>

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->

<xsl:strip-space elements="mt:*"/>
    
    <xsl:template  match="mms:fieldEquivalence">
        \indent \indent {\large Field Equivalence}
            \begin{mytable}{c|c}{30pt}
            \textbf{\small Problem} &#038; \textbf{\small Model} \\ \hlinegray
        <xsl:for-each select="mms:equivalence">
                $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:problemElement"/></xsl:call-template>$ &#038; $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:modelElement"/></xsl:call-template>$ \\  
            </xsl:for-each>
            \end{mytable}  
    </xsl:template>
   
    <xsl:template  match="mms:variableEquivalence">
        \indent \indent {\large Variable Equivalence}
        \begin{mytable}{c|c}{30pt}
        \textbf{\small Problem} &#038; \textbf{\small Model} \\ \hlinegray
        <xsl:for-each select="mms:equivalence">
            $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:problemElement"/></xsl:call-template>$ &#038; $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:modelElement"/></xsl:call-template>$ \\  
        </xsl:for-each>
        \end{mytable}  
    </xsl:template>
   
     <xsl:template match="mms:vertexPropertyEquivalence">
         \indent \indent {\large Vertex Property Equivalence}
            \begin{mytable}{c|c}{30pt}
            \textbf{\small Problem} &#038; \textbf{\small Model} \\ \hlinegray
           <xsl:for-each select="mms:equivalence">
                 $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:problemElement"/></xsl:call-template>$ &#038; $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:modelElement"/></xsl:call-template>$ \\  
            </xsl:for-each>
            \end{mytable}  
     </xsl:template>
 
     <xsl:template match="mms:agentPropertyEquivalence">
         \indent \indent {\large Agent Property Equivalence}
            \begin{mytable}{c|c}{30pt}
            \textbf{\small Problem} &#038; \textbf{\small Model} \\ \hlinegray
      <xsl:for-each select="mms:equivalence">
                 $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:problemElement"/></xsl:call-template>$ &#038; $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:modelElement"/></xsl:call-template>$ \\  
            </xsl:for-each>
            \end{mytable} 
     </xsl:template>
 
     <xsl:template match="mms:edgePropertyEquivalence">
         \indent \indent {\large Edge Property Equivalence}
            \begin{mytable}{c|c}{30pt}
            \textbf{\small Problem} &#038; \textbf{\small Model} \\ \hlinegray
          <xsl:for-each select="mms:equivalence">
                 $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:problemElement"/></xsl:call-template>$ &#038; $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:modelElement"/></xsl:call-template>$ \\  
            </xsl:for-each>
            \hlinegray
            \end{mytable}
     </xsl:template>
      
     <xsl:template match="mms:parameterEquivalence">
         \indent \indent {\large Parameter Equivalence}
            \begin{mytable}{c|c}{30pt}
            \textbf{\small Problem} &#038; \textbf{\small Model} \\ \hlinegray
      <xsl:for-each select="mms:equivalence">
                 $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:problemElement"/></xsl:call-template>$ &#038; $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:modelElement"/></xsl:call-template>$ \\    
            </xsl:for-each>
            \end{mytable}
     </xsl:template>
          
     <xsl:template match="mms:coordinateEquivalence">
         \indent\indent {\large Coordinate Equivalence}                
            \begin{mytable}{c|c}{30pt}
            \textbf{\small Problem} &#038; \textbf{\small Model} \\ \hlinegray
          <xsl:for-each select="mms:equivalence">
                 $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:problemElement"/></xsl:call-template>$ &#038; $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:modelElement"/></xsl:call-template>$ \\     
            </xsl:for-each>
            \end{mytable}
     </xsl:template>
    
    <xsl:template match="mms:tensorEquivalence">
        \indent\indent {\large Tensor Equivalence}
        \begin{mytable}{c|c}{30pt}
        \textbf{\small Problem} &#038; \textbf{\small Model} \\ \hlinegray
        <xsl:for-each select="mms:equivalence">
            $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:problemElement"/></xsl:call-template>$ &#038; $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:modelElement"/></xsl:call-template>$ \\    
        </xsl:for-each>
        \end{mytable}
    </xsl:template>
    
    <xsl:template match="mms:eigenVectorEquivalence">
        \indent\indent {\large Eigen Vector Equivalence}
        \begin{mytable}{c|c}{30pt}
        \textbf{\small Problem} &#038; \textbf{\small Model} \\ \hlinegray
        <xsl:for-each select="mms:equivalence">
            $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:problemElement"/></xsl:call-template>$ &#038; $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:modelElement"/></xsl:call-template>$ \\    
        </xsl:for-each>
        \end{mytable}
    </xsl:template>
    
</xsl:stylesheet>
