<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		      xmlns:mt="http://www.w3.org/1998/Math/MathML"
		      xmlns:mms="urn:mathms"
		      xmlns:s="urn:simml"
                version='2.0'>                
     <xsl:output method="text" indent="no" encoding="UTF-8" media-type="application/x-tex"/>

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->

<xsl:include href="mml_PDE/instructionToPseudocode.xsl"/>    
<xsl:include href="parameters.xsl"/>
     
<xsl:strip-space elements="mt:*"/>
     
 <xsl:template match="mms:PDEDiscretizationSchema">
      <xsl:if test="count(mms:head) &gt; 0">   
           \begin{center}
           \textbf{<xsl:choose><xsl:when test="count(mms:head/mms:name) &gt; 0"><xsl:value-of select="mms:head/mms:name"/></xsl:when><xsl:otherwise>Discretization Schema</xsl:otherwise></xsl:choose>}      
           <!--head-->
           <xsl:if test="mms:head/mms:author/text() != ''">
                by <xsl:value-of select="mms:head/mms:author"/>\\    
           </xsl:if>
           <xsl:if test="mms:head/mms:version/text() != ''">
                Version: <xsl:value-of select="mms:head/mms:version"/>\\
           </xsl:if> 
           <xsl:if test="count(mms:head/mms:date) &gt; 0">
                <xsl:value-of select=' format-dateTime(mms:head/mms:date, "[MNn] [D], [Y]", "en", (), ())'/>\\
           </xsl:if>
           \end{center}
           <xsl:if test="mms:head/mms:description/text() != ''">
                \noindent <xsl:value-of select="mms:head/mms:description"/>\\
           </xsl:if> 
           \noindent \rule{\textwidth}{1pt}\\
      </xsl:if>
      
      <!--parameters-->      
      <xsl:call-template name="parameters"></xsl:call-template>
     
     
      <!--schema-->
      <xsl:if test="count(mms:schema/mms:step) &gt; 0">
          <xsl:for-each select="mms:schema/mms:step">
              \indent{\Large Step <xsl:value-of select="position()"/>}        \\  
              \begin{mygrouping}
               <xsl:if test="mms:meshPreStep">
                    \indent{\large Mesh Prestep}        \\
                    \indent \indent Transformation rule: <xsl:value-of select="mms:meshPreStep/mms:transformationRule/mms:name"></xsl:value-of>\\
                  \indent \indent Input variables: <xsl:for-each select="mms:meshPreStep/mms:inputVariables/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
                  \indent \indent Output variables: <xsl:for-each select="mms:meshPreStep/mms:outputVariables/mms:outputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
               </xsl:if>
             <xsl:if test="mms:particlePreStep">
                \indent{\large Particle Prestep}        \\
                \indent \indent Transformation rule: <xsl:value-of select="mms:particlePreStep/mms:transformationRule/mms:name"></xsl:value-of>\\
                \indent \indent Input variables: <xsl:for-each select="mms:particlePreStep/mms:inputVariables/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
                \indent \indent Output variables: <xsl:for-each select="mms:particlePreStep/mms:outputVariables/mms:outputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
             </xsl:if>
              \indent{\large Spatial discretization}        \\
               <xsl:if test="mms:spatialDiscretization/mms:conservativeTermDiscretization">
                    \indent{\large Conservative Spatial Discretization}        \\
                    <xsl:if test="mms:spatialDiscretization/mms:conservativeTermDiscretization/mms:meshFluxVariables">
                         \indent\indent Mesh Fluxes:        \\
                       <xsl:for-each select="mms:spatialDiscretization/mms:conservativeTermDiscretization/mms:meshFluxVariables/mms:fluxVariable">
                              \indent \indent Input variable: $<xsl:apply-templates select="mms:inputVariable/mt:math"><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$\\
                              \indent \indent Output variable: $<xsl:apply-templates select="mms:outputVariable/mt:math"><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$\\
                         </xsl:for-each>
                    </xsl:if>
                  <xsl:if test="mms:spatialDiscretization/mms:conservativeTermDiscretization/mms:meshDiscretization">
                          \indent\indent Mesh Discretization:    
                               \indent \indent Transformation rule: <xsl:value-of select="mms:spatialDiscretization/mms:conservativeTermDiscretization/mms:meshDiscretization/mms:transformationRule/mms:name"></xsl:value-of>\\
                               \indent \indent Input variables: <xsl:for-each select="mms:spatialDiscretization/mms:conservativeTermDiscretization/mms:meshDiscretization/mms:inputVariables/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
                  </xsl:if>
                  <xsl:if test="mms:spatialDiscretization/mms:conservativeTermDiscretization/mms:particleFluxVariables">
                     \indent\indent Particle Fluxes:        \\
                     <xsl:for-each select="mms:spatialDiscretization/mms:conservativeTermDiscretization/mms:particleFluxVariables/mms:fluxVariable">
                        \indent \indent Input variable: $<xsl:apply-templates select="mms:inputVariable/mt:math"><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$\\
                        \indent \indent Output variable: $<xsl:apply-templates select="mms:outputVariable/mt:math"><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$\\
                     </xsl:for-each>
                  </xsl:if>
                  <xsl:if test="mms:spatialDiscretization/mms:conservativeTermDiscretization/mms:particleDiscretization">
                     \indent\indent Particle Discretization Derivative:    
                     \indent \indent Transformation rule: <xsl:value-of select="mms:spatialDiscretization/mms:conservativeTermDiscretization/mms:particleDiscretization/mms:derivativeFunction/mms:transformationRule/mms:name"></xsl:value-of>\\
                     \indent \indent Input variables: <xsl:for-each select="mms:spatialDiscretization/mms:conservativeTermDiscretization/mms:particleDiscretization/mms:derivativeFunction/mms:inputVariables/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
                     \indent\indent Particle Discretization Normalization:    
                     \indent \indent Transformation rule: <xsl:value-of select="mms:spatialDiscretization/mms:conservativeTermDiscretization/mms:particleDiscretization/mms:normalizationFunction/mms:transformationRule/mms:name"></xsl:value-of>\\
                     \indent \indent Input variables: <xsl:for-each select="mms:spatialDiscretization/mms:conservativeTermDiscretization/mms:particleDiscretization/mms:normalizationFunction/mms:inputVariables/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\                     
                  </xsl:if>
               </xsl:if>
               <xsl:if test="mms:spatialDiscretization/mms:nonConservativeTermDiscretization">
                    \indent{\large Non-Conservative Spatial Discretization}        \\
                    \indent \indent Generic input variable: <xsl:for-each select="mms:spatialDiscretization/mms:nonConservativeTermDiscretization/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
                  <xsl:if test="mms:spatialDiscretization/mms:nonConservativeTermDiscretization/mms:meshDerivativeInputVariables">
                     \indent \indent Mesh input variables: <xsl:for-each select="mms:spatialDiscretization/mms:nonConservativeTermDiscretization/mms:meshDerivativeInputVariables/mms:meshDerivativeInputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
                  </xsl:if>
                  <xsl:if test="mms:spatialDiscretization/mms:nonConservativeTermDiscretization/mms:particleFunctionInputVariables">
                     \indent \indent Particle input variables: <xsl:for-each select="mms:spatialDiscretization/mms:nonConservativeTermDiscretization/mms:particleFunctionInputVariables/mms:particleFunctionInputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
                  </xsl:if>
                  <xsl:if test="mms:spatialDiscretization/mms:nonConservativeTermDiscretization/mms:particleNormalizationInputVariables">
                     \indent \indent Particle normalization input variables: <xsl:for-each select="mms:spatialDiscretization/mms:nonConservativeTermDiscretization/mms:particleNormalizationInputVariables/mms:particleNormalizationInputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
                  </xsl:if>
               </xsl:if>
               \indent \indent Spatial discretization output: $<xsl:apply-templates select="mms:spatialDiscretization/mms:outputVariable/mt:math"><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$\\
              
             <xsl:if test="mms:dissipation/mms:meshDissipation">
                    \indent{\large Mesh Dissipation}        \\
                    \indent \indent Transformation rule: <xsl:value-of select="mms:dissipation/mms:meshDissipation/mms:transformationRule/mms:name"></xsl:value-of>\\
                \indent \indent Input variables: <xsl:for-each select="mms:dissipation/mms:meshDissipation/mms:inputVariables/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
             </xsl:if>
             <xsl:if test="mms:dissipation/mms:particleDissipation">
                \indent{\large Particle Dissipation}        \\
                \indent \indent Transformation rule: <xsl:value-of select="mms:dissipation/mms:particleDissipation/mms:transformationRule/mms:name"></xsl:value-of>\\
                \indent \indent Input variables: <xsl:for-each select="mms:dissipation/mms:particleDissipation/mms:inputVariables/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
                \indent \indent Normalization transformation rule: <xsl:value-of select="mms:dissipation/mms:particleDissipation/mms:normalizationFunction/mms:transformationRule/mms:name"></xsl:value-of>\\
                \indent \indent Normalization input variables: <xsl:for-each select="mms:dissipation/mms:particleDissipation/mms:normalizationFunction/mms:inputVariables/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
             </xsl:if>
             <xsl:if test="mms:particleKernelDiscretization">
                \indent{\large Particle Kernel Functions}        \\
                \indent \indent Kernel transformation rule: <xsl:value-of select="mms:particleKernelDiscretization/mms:kernel/mms:transformationRule/mms:name"></xsl:value-of>\\
                \indent \indent Kernel input variables: <xsl:for-each select="mms:particleKernelDiscretization/mms:kernel/mms:inputVariables/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
                \indent \indent Kernel gradient transformation rule: <xsl:value-of select="mms:particleKernelDiscretization/mms:kernelGradient/mms:transformationRule/mms:name"></xsl:value-of>\\
                \indent \indent Kernel Gradient input variables: <xsl:for-each select="mms:particleKernelDiscretization/mms:kernelGradient/mms:inputVariables/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
             </xsl:if>
             <xsl:if test="mms:particleInterpolationMethods">
                \indent{\large Particle Interpolation Functions}        \\
                \indent \indent Interpolation transformation rule: <xsl:value-of select="mms:particleInterpolationMethods/mms:particleInterpolationFunction/mms:transformationRule/mms:name"></xsl:value-of>\\
                \indent \indent Interpolation input variables: <xsl:for-each select="mms:particleInterpolationMethods/mms:particleInterpolationFunction/mms:inputVariables/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
                \indent \indent Interpolation normalization transformation rule: <xsl:value-of select="mms:particleInterpolationMethods/mms:particleNormalizationFunction/mms:transformationRule/mms:name"></xsl:value-of>\\
                \indent \indent Interpolation normalization input variables: <xsl:for-each select="mms:particleInterpolationMethods/mms:particleNormalizationFunction/mms:inputVariables/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
             </xsl:if>
             <xsl:if test="mms:particleAdvectiveTerm">
                \indent{\large Particle Advective Term Discretization}        \\
                \indent \indent Discretization transformation rule: <xsl:value-of select="mms:particleAdvectiveTerm/mms:particleDerivativeFunction/mms:transformationRule/mms:name"></xsl:value-of>\\
                \indent \indent Discretization input variables: <xsl:for-each select="mms:particleAdvectiveTerm/mms:particleDerivativeFunction/mms:inputVariables/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
                \indent \indent Normalization transformation rule: <xsl:value-of select="mms:particleAdvectiveTerm/mms:particleNormalizationFunction/mms:transformationRule/mms:name"></xsl:value-of>\\
                \indent \indent Normalization input variables: <xsl:for-each select="mms:particleAdvectiveTerm/mms:particleNormalizationFunction/mms:inputVariables/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
             </xsl:if>
              \indent{\large Time discretization}        \\
              \indent \indent Transformation rule: <xsl:value-of select="mms:timeDiscretization/mms:transformationRule/mms:name"></xsl:value-of>\\
              \indent \indent Input variables: <xsl:for-each select="mms:timeDiscretization/mms:inputVariables/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
             <xsl:if test="mms:particleAdvectiveTerm">
                \indent \indent Particle position input variables: <xsl:for-each select="mms:timeDiscretization/mms:positionInputVariables/mms:positionInputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
                
             </xsl:if>
              \indent \indent Output variable: $<xsl:apply-templates select="mms:timeDiscretization/mms:outputVariable/mt:math"><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$\\
               
               <xsl:if test="mms:meshPostStep">
                    \indent{\large Mesh PostStep}        \\
                    \indent \indent Transformation rule: <xsl:value-of select="mms:postStep/mms:transformationRule/mms:name"></xsl:value-of>\\
                    \indent \indent Input variables: <xsl:for-each select="mms:postStep/mms:inputVariables/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
                    \indent \indent Output variables: <xsl:for-each select="mms:postStep/mms:outputVariables/mms:outputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
               </xsl:if>
             
             <xsl:if test="mms:particlePostStep">
                \indent{\large Particle PostStep}        \\
                \indent \indent Transformation rule: <xsl:value-of select="mms:postStep/mms:transformationRule/mms:name"></xsl:value-of>\\
                \indent \indent Input variables: <xsl:for-each select="mms:postStep/mms:inputVariables/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
                \indent \indent Output variables: <xsl:for-each select="mms:postStep/mms:outputVariables/mms:outputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
             </xsl:if>
               
               \indent \indent Time at finalization: $<xsl:apply-templates select="mms:timeAtFinalization/mt:math"><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$\\
               
              \end{mygrouping}
          </xsl:for-each>
      </xsl:if>
      
    <xsl:if test="mms:compactSupportRatio">
       \indent{\large Compact Support Ratio}: <xsl:value-of select="mms:compactSupportRatio"></xsl:value-of>\\
    </xsl:if>
      
      <xsl:if test="mms:timeInterpolation">
         \indent{\large Time Interpolation}        \\  
           \indent \indent Transformation rule: <xsl:value-of select="mms:timeInterpolation/mms:transformationRule/mms:name"></xsl:value-of>\\
           \indent \indent Input variables: <xsl:for-each select="mms:timeInterpolation/mms:inputVariables/mms:inputVariable/mt:math">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>\\
      </xsl:if>
      
      
 </xsl:template>
     
     
</xsl:stylesheet>
