<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		      xmlns:mt="http://www.w3.org/1998/Math/MathML"
		      xmlns:mms="urn:mathms"
		      xmlns:my="internalarray"
		      xmlns="urn:mathms"
                version='1.0'>                
<xsl:output method="text" indent="no" encoding="UTF-8"/>

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->

<xsl:strip-space elements="mt:*"/>
     
<xsl:template name="eigenSpace" match="mms:eigenSpace">
     <xsl:param name="tcoordinate">t</xsl:param>
     \begin{mygrouping}
     \indent \indent \indent \textbf{Eigen space: <xsl:value-of select="mms:eigenName"/>}\\
   \indent \indent \indent \indent{Type: <xsl:value-of select="mms:type"/>}\\
   \indent \indent \indent  \indent Characteristic speed: \begin{dmath}<xsl:apply-templates select="mms:eigenValue/mt:math"><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>\end{dmath}
     <xsl:if test="count(mms:eigenVectors/mms:eigenVector) &gt; 0">
          \indent \indent \indent  \indent Eigen vectors
          <xsl:for-each select="mms:eigenVectors/mms:eigenVector">
               \begin{dmath}
               \partial_{<xsl:value-of select="$tcoordinate"/>,i} (<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/></xsl:call-template>)=
               <xsl:for-each select="mms:diagonalizationCoefficients/mms:diagonalizationCoefficient"><xsl:variable name="coef"><xsl:value-of select="mms:field"/></xsl:variable><xsl:variable name="fieldOrder"><xsl:if test="contains($coef, '_n')">0</xsl:if>
                    <xsl:if test="contains($coef, '_t')"><xsl:value-of select="./ancestor::mms:tensorialPhysicalModel/mms:tensorialField[mms:name = substring-before($coef,'_')]/mms:order"/></xsl:if>
                    <xsl:if test="not(contains($coef, '_n')) and not(contains($coef, '_t'))"><xsl:value-of select="./ancestor::mms:tensorialPhysicalModel/mms:tensorialField[mms:name = $coef]/mms:order"/></xsl:if>
               </xsl:variable>\left(<xsl:apply-templates select="mt:math"><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>\right) \partial_{<xsl:value-of select="$tcoordinate"/>,i} (<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="$coef"/></xsl:call-template>)<xsl:if test="position()!=last()">+</xsl:if></xsl:for-each>
               \end{dmath}          
          </xsl:for-each>
     </xsl:if>
     \end{mygrouping}
</xsl:template>
     
     
     <xsl:template name="undiagonalization" match="mms:undiagonalization">
          <xsl:param name="tcoordinate">t</xsl:param>
          <xsl:variable name="coef"><xsl:value-of select="mms:field"/></xsl:variable><xsl:variable name="fieldOrder"><xsl:if test="contains($coef, '_n')">0</xsl:if>
               <xsl:if test="contains($coef, '_t')"><xsl:value-of select="./ancestor::mms:tensorialPhysicalModel/mms:tensorialField[mms:name = substring-before($coef,'_')]/mms:order"/></xsl:if>
               <xsl:if test="not(contains($coef, '_n')) and not(contains($coef, '_t'))"><xsl:value-of select="./ancestor::mms:tensorialPhysicalModel/mms:tensorialField[mms:name = $coef]/mms:order"/></xsl:if></xsl:variable>
          \begin{dmath} \partial_{<xsl:value-of select="$tcoordinate"/>,i}(<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:field"/></xsl:call-template>)=
          <xsl:for-each select="mms:undiagonalizationCoefficients/mms:undiagonalizationCoefficient"><xsl:variable name="vect"><xsl:value-of select="mms:eigenVector"/></xsl:variable><xsl:variable name="vectorOrder">
               <xsl:value-of select="./parent::*/parent::*/mms:eigenSpaces/mms:eigenSpace[mms:eigenVector/mms:vectorName = $vect]/mms:order"/>
          </xsl:variable>\left(<xsl:apply-templates select="mt:math"><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>\right) \partial_{<xsl:value-of select="$tcoordinate"/>,i} (<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="$vect"/></xsl:call-template>)<xsl:if test="position()!=last()">+</xsl:if></xsl:for-each>\end{dmath}
     </xsl:template>
     
</xsl:stylesheet>
