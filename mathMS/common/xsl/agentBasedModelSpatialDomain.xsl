<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		      xmlns:mt="http://www.w3.org/1998/Math/MathML"
		      xmlns:mms="urn:mathms"
		      xmlns:sml="urn:simml"
                version='1.0'>                
     <xsl:output method="text" indent="no" encoding="UTF-8" media-type="application/x-tex"/>

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->

  <xsl:strip-space elements="mt:*"/>
     
     <xsl:include href="mml_ABMSpatialDomain/instructionToPseudocode.xsl"/>
     <xsl:include href="parameters.xsl"/>
     
 <xsl:template match="mms:agentBasedModelSpatialDomain">
      <xsl:if test="count(mms:head) &gt; 0">
           \begin{center}
           \textbf{<xsl:choose><xsl:when test="count(mms:head/mms:name) &gt; 0"><xsl:value-of select="mms:head/mms:name"/></xsl:when><xsl:otherwise>Agent Based Model</xsl:otherwise></xsl:choose>}      
      <!--head-->
           <xsl:if test="mms:head/mms:author/text() != ''">
                by <xsl:value-of select="mms:head/mms:author"/>\\    
           </xsl:if>
           <xsl:if test="mms:head/mms:version/text() != ''">
                Version: <xsl:value-of select="mms:head/mms:version"/>\\
           </xsl:if> 
           <xsl:if test="count(mms:head/mms:date) &gt; 0">
                <xsl:value-of select="mms:head/mms:date"/>\\
           </xsl:if>
           \end{center}
           <xsl:if test="mms:head/mms:description/text() != ''">
                \noindent <xsl:value-of select="mms:head/mms:description"/>\\
           </xsl:if> 
           \noindent \rule{\textwidth}{1pt}\\
      </xsl:if>
      <!--fields-->
      <xsl:if test="count(mms:agentProperties/mms:agentProperty) &gt; 0">      
           \indent {\Large Agent properties}
       <xsl:text>                               
       </xsl:text>           
           \indent \indent <xsl:for-each select="mms:agentProperties/mms:agentProperty">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
      </xsl:if>
     
      <!--spatial coordinates-->
      <xsl:if test="count(mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate) &gt; 0">
           \indent {\Large Spatial Coordinates}
           <xsl:text>                               
           </xsl:text>                     
           \indent \indent <xsl:for-each select="mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
      </xsl:if>
      <!--time coordinate-->
      <xsl:if test="count(mms:coordinates/mms:timeCoordinate) &gt; 0">
           \indent {\Large Time Coordinate}
           <xsl:text>                               
           </xsl:text>
           \indent \indent $<xsl:value-of select="mms:coordinates/mms:timeCoordinate"/>$ \\      
      </xsl:if>
      <!--parameters-->      
      <xsl:call-template name="parameters"></xsl:call-template>
      
      <!--Definitions-->
      <xsl:if test="count(mms:rules/mms:gatherRules/mms:gatherRule) &gt; 0">
           \indent {\Large Gather rules}
           <xsl:for-each select="mms:rules/mms:gatherRules/mms:gatherRule">
                \begin{mygrouping}
                \textbf{Name: <xsl:value-of select="mms:name"/>}\\
                \textbf{Agent property: <xsl:value-of select="mms:agentProperty"/>}\\
                \textbf{Algorithm}
                <xsl:for-each select="sml:algorithm/*"><xsl:apply-templates select="."><xsl:with-param name="indent">1</xsl:with-param><xsl:with-param name="condition">false</xsl:with-param></xsl:apply-templates></xsl:for-each>
                \end{mygrouping}
           </xsl:for-each>
      </xsl:if>
      <xsl:if test="count(mms:rules/mms:updateRules/mms:updateRule) &gt; 0">
           \indent {\Large Update rules}
           <xsl:for-each select="mms:rules/mms:updateRules/mms:updateRule">
                \begin{mygrouping}
                \textbf{Name: <xsl:value-of select="mms:name"/>}\\
                \textbf{Agent property: <xsl:value-of select="mms:agentProperty"/>}\\
                \textbf{Algorithm}
                <xsl:for-each select="sml:algorithm/*"><xsl:apply-templates select="."><xsl:with-param name="indent">1</xsl:with-param><xsl:with-param name="condition">false</xsl:with-param></xsl:apply-templates></xsl:for-each>
                \end{mygrouping}
           </xsl:for-each>
      </xsl:if>
      
      <!-- Topology change -->
      <xsl:if test="count(mms:topologyChange) &gt; 0">
           \indent {\Large Topology Change}
           \begin{mygrouping}
           \textbf{Name: <xsl:value-of select="mms:topologyChange/mms:name"/>}\\
           \textbf{Algorithm}
           <xsl:for-each select="mms:topologyChange/sml:algorithm/*"><xsl:apply-templates select="."><xsl:with-param name="indent">1</xsl:with-param><xsl:with-param name="condition">false</xsl:with-param></xsl:apply-templates></xsl:for-each>
           \end{mygrouping}
        </xsl:if>
      
      <!-- Execution order -->
      <xsl:if test="count(mms:ruleExecutionOrder) &gt; 0">
           \indent {\Large Rule execution Order}\\
           <xsl:text> \indent \begin{enumerate}[noitemsep, topsep=0pt]</xsl:text>
           <xsl:for-each select="mms:ruleExecutionOrder/mms:rule">\item{<xsl:value-of select="."/>}<xsl:text>
           </xsl:text></xsl:for-each><xsl:text>\end{enumerate}</xsl:text>
      </xsl:if>
      
 </xsl:template>
</xsl:stylesheet>
