<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		      xmlns:mt="http://www.w3.org/1998/Math/MathML"
		      xmlns:mms="urn:mathms"
		      xmlns:sml="urn:simml"
                version='2.0'>
     <xsl:output method="text" indent="no" encoding="UTF-8" media-type="application/x-tex"/>

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->

     <xsl:include href="spatialDomain.xsl"/>
     <xsl:include href="regionInteraction.xsl"/>
     <xsl:include href="parameters.xsl"/>
     <xsl:include href="mml_PDE/instructionToPseudocode.xsl"/>
     
<xsl:strip-space elements="mt:*"/>
     
 <xsl:template match="mms:discretizedProblem">
      <xsl:if test="count(mms:head) &gt; 0">   
           \begin{center}
           \textbf{<xsl:choose><xsl:when test="count(mms:head/mms:name) &gt; 0"><xsl:value-of select="mms:head/mms:name"/></xsl:when><xsl:otherwise>Agent Based Problem</xsl:otherwise></xsl:choose>}      
           <!--head-->
           <xsl:if test="mms:head/mms:author/text() != ''">
                by <xsl:value-of select="mms:head/mms:author"/>\\    
           </xsl:if>
           <xsl:if test="mms:head/mms:version/text() != ''">
                Version: <xsl:value-of select="mms:head/mms:version"/>\\
           </xsl:if> 
           <xsl:if test="count(mms:head/mms:date) &gt; 0">
                <xsl:value-of select=' format-dateTime(mms:head/mms:date, "[MNn] [D], [Y]", "en", (), ())'/>\\
           </xsl:if>
           \end{center}
           <xsl:if test="mms:head/mms:description/text() != ''">
                \noindent <xsl:value-of select="mms:head/mms:description"/>\\
           </xsl:if> 
           \noindent \rule{\textwidth}{1pt}\\
      </xsl:if>
      <!--fields-->
      <xsl:if test="count(mms:fields/mms:field) &gt; 0">      
           \indent {\Large Fields}
           
           \indent \indent <xsl:for-each select="mms:fields/mms:field">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
      </xsl:if>
      <!--tensors-->
      <xsl:if test="count(mms:tensors/mms:tensor) &gt; 0">      
           \indent {\Large Tensors}
           
           \begin{mytable}{c|c|c}{15pt}
           \textbf{\small Name} &#038; \textbf{\small Order} &#038; \textbf{\small Fields} \\ \hlinegray
           <xsl:for-each select="mms:tensors/mms:tensor">
                $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/></xsl:call-template>$  &#038; <xsl:value-of select="mms:order"/> &#038; <xsl:for-each select="mms:fields/mms:field">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each> \\          
           </xsl:for-each>
           \end{mytable}
      </xsl:if>
      <!--coordinate systems-->
      <xsl:if test="count(mms:coordinateSystem/mms:spatialCoordinates/mms:spatialCoordinate) &gt; 0">
           \indent {\Large Spatial Coordinates}      
           
           \indent \indent <xsl:for-each select="mms:coordinateSystem/mms:spatialCoordinates/mms:spatialCoordinate">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
      </xsl:if>
      <!--time coordinate-->
      <xsl:if test="count(mms:coordinateSystem/mms:timeCoordinate) &gt; 0">
           \indent {\Large Time Coordinate}       
           
           \indent \indent $<xsl:value-of select="mms:coordinateSystem/mms:timeCoordinate"/>$\\        
      </xsl:if>
      <!--parameters-->      
      <xsl:call-template name="parameters"></xsl:call-template>
      
      <!--implicit parameters-->      
      <xsl:if test="count(mms:implicitParameters/mms:implicitParameter) &gt; 0">
           \indent {\Large Implicit Parameters} 
           \begin{mytable}{c|c|c}{15pt}
           \textbf{\small Type} &#038; \textbf{\small Value} &#038; \textbf{\small Coordinate} \\ \hlinegray
           <xsl:for-each select="mms:implicitParameters/mms:implicitParameter">
                $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:type"/></xsl:call-template>$ &#038; $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:value"/></xsl:call-template>$ &#038; $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:coordinate"/></xsl:call-template>$\\   
           </xsl:for-each>                                       
           \end{mytable}            
      </xsl:if>
      
      <!--auxiliar fields-->
      <xsl:if test="count(mms:auxiliaryFields/mms:auxiliaryField) &gt; 0">      
           \indent {\Large Auxiliary Fields}
           <xsl:text>                               
           </xsl:text>           
           \indent \indent <xsl:for-each select="mms:auxiliaryFields/mms:auxiliaryField">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
      </xsl:if>
      
     <!--analysis fields-->
     <xsl:if test="count(mms:analysisFields/mms:analysisField) &gt; 0">      
         \indent {\Large Auxiliary Fields}
         <xsl:text>                               
         </xsl:text>           
         \indent \indent <xsl:for-each select="mms:analysisFields/mms:analysisField">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
     </xsl:if>
      
     <!--auxiliar variables-->
     <xsl:if test="count(mms:auxiliaryVariables/mms:auxiliaryVariable) &gt; 0">      
         \indent {\Large Auxiliary Fields}
         <xsl:text>                               
         </xsl:text>           
         \indent \indent <xsl:for-each select="mms:auxiliaryVariables/mms:auxiliaryVariable">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
     </xsl:if>
      
      <!--VectorName -->
      <xsl:if test="count(mms:vectorNames/mms:vectorName) &gt; 0">
           \indent {\Large Eigen Vectors}      
           <xsl:text>                               
           </xsl:text>
           \indent \indent <xsl:for-each select="mms:vectorNames/mms:vectorName">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose> \\
           </xsl:for-each>
      </xsl:if>
            
      <!--Regions--> 
      <xsl:if test="count(mms:region|mms:subregions/mms:subregion) &gt; 0">
           \indent {\Large Regions}
           <xsl:for-each select="mms:region|mms:subregions/mms:subregion">
                \begin{mygrouping}
                \indent \indent {\large <xsl:value-of select="mms:name"/>}
                
                <!--interior models-->
                <xsl:if test="count(.//mms:interiorModel) &gt; 0">
                     \indent \indent {\large Interior models}\\
                     \indent \indent \indent <xsl:value-of select=".//mms:interiorModel" separator=", "/>\\
                </xsl:if>
                <!--surface models-->
                <xsl:if test="count(.//mms:surfaceModel) &gt; 0">
                     \indent \indent {\large Surface models}\\
                     \indent \indent \indent <xsl:value-of select=".//mms:surfaceModel" separator=", "/>\\
                </xsl:if>
                
                <xsl:call-template name="spatialDomain"><xsl:with-param name="sd" select="./mms:spatialDomain"/></xsl:call-template>
                
                <xsl:if test="count(./mms:location) &gt; 0">
                     \indent \indent {\large Location}\\
                     <xsl:for-each select="./mms:spatialDomain">
                          <xsl:call-template name="spatialDomain"><xsl:with-param name="sd" select="."/></xsl:call-template>
                     </xsl:for-each>
                     <xsl:for-each select="./mms:x3d">
                          \indent \indent \indent <xsl:value-of select="mms:name" separator=", "/>\\
                     </xsl:for-each>
                </xsl:if>
                
                <!--initialconditions-->      
                <xsl:if test="count(./mms:initialConditions) &gt; 0">
                     \indent{\Large Initial Conditions}\\
                     <xsl:if test="count(./mms:initialConditions/mms:initialCondition) &gt; 0">
                          <xsl:for-each select="./mms:initialConditions/mms:initialCondition">
                               \begin{mygrouping}
                               <xsl:if test="mms:applyIf">
                                    \indent {\large The condition is applied when:}
                                    {\color{NavyBlue}
                                    \begin{dmath}
                                    <xsl:apply-templates select="mms:applyIf/mt:math"><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                                    \end{dmath}}
                               </xsl:if>
                               \indent {\large The condition is:}
                               {\color{NavyBlue}
                               <xsl:for-each select="mms:mathExpressions/mt:math">
                                    \begin{dmath}
                                    <xsl:apply-templates select="."><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                                    \end{dmath}
                               </xsl:for-each>}
                               \end{mygrouping}
                          </xsl:for-each>
                     </xsl:if>
                </xsl:if>
                <!--field groups-->      
                <xsl:if test="count(mms:fieldGroups) &gt; 0">
                     \indent \indent \indent \textbf{Field groups} 
                     
                     \begin{mytable}{c|c}{15pt}
                     \textbf{\small Name} &#038; \textbf{\small Fields} \\ \hlinegray
                     <xsl:for-each select="mms:fieldGroups/mms:fieldGroup">
                          <xsl:value-of select="mms:fieldGroupName"/>  &#038; $<xsl:for-each select="mms:fieldGroupField">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>$ \\           
                     </xsl:for-each>
                     \end{mytable}          
                </xsl:if>
                <!--segment interaction-->      
                <xsl:if test="count(mms:regionInteractions) &gt; 0">
                     \indent \indent \indent \textbf{Region interactions} 
                     <xsl:for-each select="mms:regionInteractions/mms:regionInteraction">
                          \begin{mygrouping}
                          \indent {\large Interaction <xsl:value-of select="position()"/>}
                          <xsl:apply-templates select="."/>
                          \end{mygrouping}
                     </xsl:for-each>           
                </xsl:if>
                <!-- discretization info -->
                <xsl:if test="count(mms:discretizationInfo) &gt; 0">
                     \indent \indent \indent \textbf{Discretization Information}\\
                     <xsl:if test="count(mms:discretizationInfo/mms:name) &gt; 0">
                          \indent \indent \indent \indent Discretization schema:<xsl:value-of select="mms:discretizationInfo/mms:name"/>\\
                          \indent \indent \indent \indent Stencil:<xsl:value-of select="mms:discretizationInfo/mms:stencil"/>
                     </xsl:if>
                     <xsl:if test="count(mms:discretizationInfo/mms:fieldGroup) &gt; 0">
                          <xsl:for-each select="mms:discretizationInfo/mms:fieldGroup">
                               \indent \indent \indent \indent Field group: <xsl:value-of select="mms:fieldGroupName"/>\\
                               \indent \indent \indent \indent \indent Discretization schema:<xsl:value-of select="mms:name"/>}\\
                               \indent \indent \indent \indent \indent Stencil:<xsl:value-of select="mms:stencil"/>\\
                          </xsl:for-each>
                     </xsl:if>                     
                </xsl:if>
                \end{mygrouping}
           </xsl:for-each>
      </xsl:if>
      
      <!--Segments Precedence-->
      <xsl:if test="count(mms:subregionPrecedence) &gt; 0">
           \indent {\Large Execution Order}\\
           <xsl:text> \indent \begin{enumerate}[noitemsep, topsep=0pt,after=\vspace{\baselineskip}]</xsl:text>
           <xsl:for-each select="mms:subregionPrecedence/mms:precedence">\item{<xsl:value-of select="mms:prevalentSegment"/> has precedence over <xsl:value-of select="mms:surrogateSegment"/>}<xsl:text>
           </xsl:text></xsl:for-each><xsl:text>\end{enumerate}
           </xsl:text>
      </xsl:if>
      
      
      <!--Segment relations-->
      <xsl:if test="count(mms:regionRelations) &gt; 0">
           \indent {\Large Region Relations}\\
           <xsl:for-each select="mms:regionRelations/mms:regionRelation">
                <xsl:variable name="coment">
                     <xsl:choose>
                          <xsl:when test="mms:relation = 'COMPATIBLE'">the same fields and the same discretization schema</xsl:when>
                          <xsl:when test="mms:relation = 'SOFT'">different fields and the same discretization schema</xsl:when>
                          <xsl:when test="mms:relation = 'HARD'">different fields and different discretization schema</xsl:when>
                     </xsl:choose>                     
                </xsl:variable>
                \indent \indent Segments <xsl:value-of select="mms:regionA"/> and <xsl:value-of select="mms:regionpB"/> have <xsl:value-of select="$coment"/>.\\
           </xsl:for-each>
      </xsl:if>

      <!--Boundaries-->      
      <xsl:if test="count(mms:boundaryConditions/mms:boundaryPolicy) &gt; 0">
           \indent {\Large Boundary conditions}\\
           <xsl:for-each select="mms:boundaryConditions/mms:boundaryPolicy">
                \indent \indent Segments: <xsl:value-of select="mms:boundaryRegions/mms:regionName" separator=", "/>
                <xsl:for-each select="mms:boundaryCondition">
                     \begin{mygrouping}
                     Type: <xsl:value-of select=" local-name(mms:type/*)"/>\\
                     Axis: <xsl:value-of select="mms:axis"/>\\
                     Side: <xsl:value-of select="mms:side"/>\\
                     <xsl:if test="count(mms:fields) &gt; 0">
                          Fields: <xsl:for-each select="mms:fields/mms:field">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>
                     </xsl:if>
                     <xsl:if test="count(mms:applyIf) &gt; 0">
                          {\large This boundary is applied when:}
                          {\color{NavyBlue}
                          \begin{dmath}
                          <xsl:apply-templates select="mms:applyIf/mt:math"><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                          \end{dmath}}
                     </xsl:if>
                     
                     <xsl:if test="count(mms:type/mms:algebraic/mms:expression) &gt; 0">
                          Field values\\
                          <xsl:for-each select="mms:type/mms:algebraic/mms:expression">
                               {\color{NavyBlue}
                               \begin{dmath}
                               <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:field"/></xsl:call-template> = <xsl:apply-templates select="mt:math"><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                               \end{dmath}}
                          </xsl:for-each>
                     </xsl:if>
                     <xsl:if test="count(mms:type/mms:reflection/mms:sign) &gt; 0">
                          Reflection parity
                          
                          \begin{mytable}{c|c}{15pt}
                          \textbf{\small Field} &#038; \textbf{\small Parity} \\ \hlinegray
                          <xsl:for-each select="mms:type/mms:reflection/mms:sign">
                               $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:field"/></xsl:call-template>$  &#038; <xsl:value-of select="mms:value"/> \\           
                          </xsl:for-each>
                          \end{mytable}
                     </xsl:if>
                     
                     \end{mygrouping}
                </xsl:for-each>
           </xsl:for-each>        
      </xsl:if>
      
      <!--Boundary precedences-->    
      <xsl:if test="count(mms:boundariesPrecedence) &gt; 0">
           \indent {\Large Boundary precedences}\\
           <xsl:text> \indent \begin{enumerate}[noitemsep, topsep=0pt,after=\vspace{\baselineskip}]</xsl:text>
           <xsl:for-each select="mms:boundariesPrecedence/mms:boundaryId">\item{<xsl:value-of select="."/>}<xsl:text>
           </xsl:text></xsl:for-each><xsl:text>\end{enumerate}
           </xsl:text>
      </xsl:if>
      
      <!--finalizationconditions-->      
      <xsl:if test="count(mms:finalizationConditions) &gt; 0">
           \indent{\Large Finalization Conditions}        \\     
           <xsl:if test="count(mms:finalizationConditions/mms:finalizationCondition) &gt; 0">
                <xsl:for-each select="mms:finalizationConditions/mms:finalizationCondition">
                     \begin{mygrouping}
                     <xsl:if test="mms:condition">
                          \indent {\large The equations are applied when:}
                          {\color{NavyBlue}
                          \begin{dmath}
                          <xsl:apply-templates select="mms:condition/mms:instructionSet//mt:math"><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                          \end{dmath}}
                     </xsl:if>
                     \indent {\large The equations are:}
                     {\color{NavyBlue}
                     <xsl:for-each select="mms:equations/mms:equation/mms:instructionSet//mt:math">
                          \begin{dmath}
                          <xsl:apply-templates select="."><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                          \end{dmath}
                     </xsl:for-each>}
                     \end{mygrouping}
                </xsl:for-each>     
           </xsl:if>
      </xsl:if>
      
      <!--Execution flow-->
      <xsl:if test="count(mms:functions) &gt; 0">  
           \noindent \rule{\textwidth}{1pt}\\
           \indent {\Large Program}
           <!--\begin{mygrouping}-->
           <!--functions-->
           <xsl:for-each select="mms:functions/mms:function">
                <xsl:text> \indent \textbf{function }</xsl:text>$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:functionName"/></xsl:call-template>$<xsl:text>(</xsl:text><xsl:for-each select="mms:functionParameters/mms:functionParameter">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/></xsl:call-template>$:<xsl:value-of select="mms:type"/><xsl:if test="position() != last()">, </xsl:if></xsl:for-each><xsl:text>)\\</xsl:text>     
                <xsl:text> \indent \textbf{begin}\\</xsl:text>
                <xsl:for-each select="mms:functionInstructions/sml:simml/*"><xsl:apply-templates select="."><xsl:with-param name="indent">2</xsl:with-param><xsl:with-param name="condition">false</xsl:with-param></xsl:apply-templates></xsl:for-each><xsl:text> \indent \textbf{end}\\</xsl:text>
               <xsl:if test="position() != last()">
                    <xsl:text>
                         
                    </xsl:text>    
               </xsl:if>                
           </xsl:for-each>
           <!--main program-->
           <xsl:text>\textbf{begin}\\</xsl:text>
           <xsl:call-template name="paintFlows">
                <xsl:with-param name="index" select="1"/>
                <xsl:with-param name="max" select="max(//*[ local-name() = 'interiorExecutionFlow' or local-name() = 'surfaceExecutionFlow']//sml:simml/count(*))"/>
           </xsl:call-template>
           <xsl:text>\textbf{end}\\</xsl:text>                                          
      </xsl:if>
 </xsl:template>
     <xsl:template name="paintFlows">
          <xsl:param name="index"/>
          <xsl:param name="max"/>
          <xsl:if test="$index &lt;= $max">
               <xsl:for-each select="//mms:region|//mms:subregion">
                  <xsl:if test="./mms:interiorExecutionFlow">
                       <xsl:text> \indent \textbf{</xsl:text><xsl:value-of select="mms:Name"/>:<xsl:text> Interior Step}\\</xsl:text>
                       <xsl:if test="count(./mms:interiorExecutionFlow/sml:simml/*[number($index)]/*) = 0">\indent \indent Void\\</xsl:if>  
                       <xsl:apply-templates select="./mms:interiorExecutionFlow/sml:simml/*[number($index)]"><xsl:with-param name="indent">2</xsl:with-param><xsl:with-param name="condition">false</xsl:with-param></xsl:apply-templates>
                  </xsl:if>
                  <xsl:if test="./mms:surfaceExecutionFlow">
                       <xsl:text> \indent \textbf{</xsl:text><xsl:value-of select="mms:Name"/>:<xsl:text> Surface Step}\\</xsl:text>
                       <xsl:if test="count(./mms:surfaceExecutionFlow/sml:simml/*[number($index)]/*) = 0">\indent \indent Void\\</xsl:if>                          
                       <xsl:apply-templates select="./mms:surfaceExecutionFlow/sml:simml/*[number($index)]"><xsl:with-param name="indent">2</xsl:with-param><xsl:with-param name="condition">false</xsl:with-param></xsl:apply-templates>
                  </xsl:if>
             </xsl:for-each>
            <xsl:call-template name="paintFlows">
                 <xsl:with-param name="index" select="$index + 1"/>
                 <xsl:with-param name="max" select="$max"/>
            </xsl:call-template>
          </xsl:if>
     </xsl:template>
</xsl:stylesheet>
