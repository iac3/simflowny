<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		      xmlns:mt="http://www.w3.org/1998/Math/MathML"
		      xmlns:mms="urn:mathms"
		      xmlns:sml="urn:simml"
                version='1.0'>                
     <xsl:output method="text" indent="no" encoding="UTF-8" media-type="application/x-tex"/>

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->

  <xsl:strip-space elements="mt:*"/>
     
     <xsl:include href="mml_ABMGraph/instructionToPseudocode.xsl"/>
     <xsl:include href="parameters.xsl"/>
     
 <xsl:template match="mms:agentBasedModelGraph">
      <xsl:if test="count(mms:head) &gt; 0">
           \begin{center}
           \textbf{<xsl:choose><xsl:when test="count(mms:head/mms:name) &gt; 0"><xsl:value-of select="mms:head/mms:name"/></xsl:when><xsl:otherwise>Agent Based Model</xsl:otherwise></xsl:choose>}      
      <!--head-->
           <xsl:if test="mms:head/mms:author/text() != ''">
                by <xsl:value-of select="mms:head/mms:author"/>\\    
           </xsl:if>
           <xsl:if test="mms:head/mms:version/text() != ''">
                Version: <xsl:value-of select="mms:head/mms:version"/>\\
           </xsl:if> 
           <xsl:if test="count(mms:head/mms:date) &gt; 0">
                <xsl:value-of select="mms:head/mms:date"/>\\
           </xsl:if>
           \end{center}
           <xsl:if test="mms:head/mms:description/text() != ''">
                \noindent <xsl:value-of select="mms:head/mms:description"/>\\
           </xsl:if> 
           \noindent \rule{\textwidth}{1pt}\\
      </xsl:if>
      <!--vertexProperties-->
      <xsl:if test="count(mms:vertexProperties/mms:vertexProperty) &gt; 0">      
           \indent {\Large Vertex Properties}
       <xsl:text>                               
       </xsl:text>           
           \indent \indent <xsl:for-each select="mms:vertexProperties/mms:vertexProperty">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each> \\
      </xsl:if>
      <!--edgeProperties-->
      <xsl:if test="count(mms:edgeProperties/mms:edgeProperty) &gt; 0">      
           \indent {\Large Edge Properties}
           <xsl:text>                               
           </xsl:text>           
           \indent \indent <xsl:for-each select="mms:edgeProperties/mms:edgeProperty">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each> \\
      </xsl:if>
      <!--parameters-->      
      <xsl:call-template name="parameters"></xsl:call-template>
      
      <!--Rules-->
      <xsl:if test="count(mms:rules/mms:gatherRules/mms:gatherRule) &gt; 0">
           \indent {\Large Gather rules}
           <xsl:for-each select="mms:rules/mms:gatherRules/mms:gatherRule">
                \begin{mygrouping}
                \textbf{Name: <xsl:value-of select="mms:name"/>}\\
                \textbf{Vertex property: <xsl:value-of select="mms:vertexProperty"/>}\\
                \textbf{Algorithm}
                <xsl:for-each select="sml:algorithm/*"><xsl:apply-templates select="."><xsl:with-param name="indent">1</xsl:with-param><xsl:with-param name="condition">false</xsl:with-param></xsl:apply-templates></xsl:for-each>
                \end{mygrouping}
           </xsl:for-each>
      </xsl:if>
      <xsl:if test="count(mms:rules/mms:updateRules/mms:updateRule) &gt; 0">
           \indent {\Large Update rules}
           <xsl:for-each select="mms:rules/mms:updateRules/mms:updateRule">
                \begin{mygrouping}
                \textbf{Name: <xsl:value-of select="mms:name"/>}\\
                \textbf{Vertex property: <xsl:value-of select="mms:vertexProperty"/>}\\
                \textbf{Algorithm}
                <xsl:for-each select="sml:algorithm/*"><xsl:apply-templates select="."><xsl:with-param name="indent">1</xsl:with-param><xsl:with-param name="condition">false</xsl:with-param></xsl:apply-templates></xsl:for-each>
                \end{mygrouping}
           </xsl:for-each>
      </xsl:if>
      
      <!-- Topology change -->
      <xsl:if test="count(mms:topologyChange) &gt; 0">
           \indent {\Large Topology Change}
           \begin{mygrouping}
           \textbf{Name: <xsl:value-of select="mms:topologyChange/mms:name"/>}\\
           \textbf{Algorithm}
           <xsl:for-each select="mms:topologyChange/sml:algorithm/*"><xsl:apply-templates select="."><xsl:with-param name="indent">1</xsl:with-param><xsl:with-param name="condition">false</xsl:with-param></xsl:apply-templates></xsl:for-each>
           \end{mygrouping}
        </xsl:if>
      
      <!-- Execution order -->
      <xsl:if test="count(mms:ruleExecutionOrder) &gt; 0">
           \indent {\Large Rule Execution Order}\\
           <xsl:text> \indent \begin{enumerate}[noitemsep, topsep=0pt]</xsl:text>
           <xsl:for-each select="mms:ruleExecutionOrder/mms:rule">\item{<xsl:value-of select="."/>}<xsl:text>
           </xsl:text></xsl:for-each><xsl:text>\end{enumerate}</xsl:text>
      </xsl:if>
      
 </xsl:template>
</xsl:stylesheet>
