<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		      xmlns:mt="http://www.w3.org/1998/Math/MathML"
		      xmlns:mms="urn:mathms"
		      xmlns:sml="urn:simml"
                version='1.0'>                
     <xsl:output method="text" indent="no" encoding="UTF-8" media-type="application/x-tex"/>

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->

     <xsl:include href="mml_ABMSpatialDomain/instructionToPseudocode.xsl"/>
     <xsl:include href="parameters.xsl"/>
     <xsl:include href="problemDomain.xsl"/>
     <xsl:include href="equivalences.xsl"/>
     

<xsl:strip-space elements="mt:*"/>
     
     <xsl:template match="mms:agentBasedProblemSpatialDomain">
      <xsl:if test="count(mms:head) &gt; 0">   
           \begin{center}
           \textbf{<xsl:choose><xsl:when test="count(mms:head/mms:name) &gt; 0"><xsl:value-of select="mms:head/mms:name"/></xsl:when><xsl:otherwise>Agent Based Problem</xsl:otherwise></xsl:choose>}      
      <!--head-->
           <xsl:if test="mms:head/mms:author/text() != ''">
                by <xsl:value-of select="mms:head/mms:author"/>\\    
           </xsl:if>
           <xsl:if test="mms:head/mms:version/text() != ''">
                Version: <xsl:value-of select="mms:head/mms:version"/>\\
           </xsl:if> 
           <xsl:if test="count(mms:head/mms:date) &gt; 0">
                <xsl:value-of select="mms:head/mms:date"/>\\
           </xsl:if>
           \end{center}
           <xsl:if test="mms:head/mms:description/text() != ''">
                \noindent <xsl:value-of select="mms:head/mms:description"/>\\
           </xsl:if> 
           \noindent \rule{\textwidth}{1pt}\\
      </xsl:if>
      <!--agentPropertys-->
      <xsl:if test="count(mms:agentProperties/mms:agentProperty) &gt; 0">      
           \indent {\Large Agent properties}
       <xsl:text>                               
       </xsl:text>           
           \indent \indent <xsl:for-each select="mms:agentProperties/mms:agentProperty">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
      </xsl:if>

          <!--spatial coordinates-->
          <xsl:if test="count(mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate) &gt; 0">
               \indent {\Large Spatial Coordinates}      
               <xsl:text>                               
               </xsl:text>                     
               \indent \indent <xsl:for-each select="mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
          </xsl:if>
          <!--time coordinate-->
          <xsl:if test="count(mms:coordinates/mms:timeCoordinate) &gt; 0">
               \indent {\Large Time Coordinate}       
               <xsl:text>                               
               </xsl:text>
               \indent \indent $<xsl:value-of select="mms:coordinates/mms:timeCoordinate"/>$\\        
          </xsl:if>
      <!--parameters-->      
      <xsl:call-template name="parameters"></xsl:call-template>
      
     <!--imported models-->
          <xsl:if test="count(mms:model) &gt; 0">
               \indent {\Large Imported Model}\\
               \indent \indent {\large <xsl:value-of select="mms:model/mms:name"/>}\\
              
              <!--agentProperty equalities-->
              <xsl:apply-templates select="mms:model/mms:agentPropertyEquivalence"></xsl:apply-templates>
              
              <!--coordinate equalities-->
              <xsl:apply-templates select="mms:model/mms:coordinateEquivalence"></xsl:apply-templates>
              
              <!--parameter equalities-->
              <xsl:apply-templates select="mms:model/mms:parameterEquivalence"></xsl:apply-templates>
              
          </xsl:if>
            
      <!--mesh-->
     <xsl:if test="count(mms:mesh) &gt; 0">
          \indent{\Large Mesh}
          
          \indent \indent {\large <xsl:value-of select="mms:mesh/mms:type"/>}\\
          
          \indent \indent {\large Spatial domain}\\  
          \begin{mytable}{c|c|c}{15pt}
          \textbf{\small Coordinate} &#038; \textbf{\small Min} &#038; \textbf{\small Max} \\ \hlinegray
          <xsl:for-each select="mms:mesh//mms:coordinateLimits">
               $<xsl:value-of select="mms:coordinate"/>$  &#038; <xsl:apply-templates select="mms:coordinateMin/mt:math"><xsl:with-param name="indent">0</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="condition">true</xsl:with-param></xsl:apply-templates> &#038; <xsl:apply-templates select="mms:coordinateMax/mt:math"><xsl:with-param name="indent">0</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="condition">true</xsl:with-param></xsl:apply-templates> \\          
          </xsl:for-each>
          \end{mytable}
          
          <xsl:if test="count(mms:mesh/mms:motion) &gt; 0">
               \indent \indent {\large Motion}\\
               
               \begin{mytable}{c|c}{15pt}
               \textbf{\small Coordinate} &#038; \textbf{\small Field} \\ \hlinegray
               <xsl:for-each select="mms:mesh/mms:motion/mms:speedField">
                    $<xsl:value-of select="mms:coordinate"/>$  &#038; $<xsl:value-of select="mms:agentProperty"/>$ \\           
               </xsl:for-each>
               \end{mytable}
          </xsl:if> 
     </xsl:if>
          
          <!--synchronization-->
          <xsl:if test="count(mms:evolutionStep) &gt; 0">
               \indent{\Large Evolution step}\\
               \indent \indent <xsl:value-of select="mms:evolutionStep"/>\\ 
          </xsl:if>
                    
          <!--initialconditions-->      
          <xsl:if test="count(mms:initialConditions) &gt; 0">
               \indent{\Large Initial Conditions}\\
               <xsl:if test="count(mms:initialConditions/mms:initialCondition) &gt; 0">
                    <xsl:for-each select="mms:initialConditions/mms:initialCondition">
                         \begin{mygrouping}
                         <xsl:if test="mms:applyIf">
                              \indent {\large The condition is applied when:}
                              {\color{NavyBlue}
                              \begin{dmath}
                              <xsl:apply-templates select="mms:applyIf/mt:math"><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                              \end{dmath}}
                         </xsl:if>
                         \indent {\large The condition is:}
                         {\color{NavyBlue}
                         <xsl:for-each select="mms:mathExpressions/mt:math">
                              \begin{dmath}
                              <xsl:apply-templates select="."><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                              \end{dmath}
                         </xsl:for-each>}
                         \end{mygrouping}
                    </xsl:for-each>
               </xsl:if>
          </xsl:if>
      
      <!--Boundaries-->      
      <xsl:if test="count(mms:boundaries) &gt; 0">
           \indent {\Large Boundary conditions}\\
           <xsl:for-each select="mms:boundaries/mms:boundaryCondition">
                \begin{mygrouping}
                Type: <xsl:value-of select="local-name(mms:type/mms:*)"/>\\
                Axis: <xsl:value-of select="mms:axis"/>\\
               Side: <xsl:value-of select="mms:side"/><xsl:if test="count(mms:applyIf) &gt; 0 or count(mms:type//mms:algorithm) &gt; 0">\\</xsl:if>
                <xsl:if test="count(mms:condition) &gt; 0">
                     \indent {\large The boundary is applied when:}
                     {\color{NavyBlue}
                     \begin{dmath}
                     <xsl:apply-templates select="mms:applyIf/mt:math"><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                     \end{dmath}}
                </xsl:if>
               <xsl:if test="count(mms:type//mms:algorithm) &gt; 0">
                     \indent {\large The boundary is:}
                     {\color{NavyBlue}
                   <xsl:for-each select="mms:type//mms:algorithm/mt:math">
                          \begin{dmath}
                          <xsl:apply-templates select="."><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                          \end{dmath}
                     </xsl:for-each>}
                </xsl:if>
                \end{mygrouping}
           </xsl:for-each>      
      </xsl:if>
      
      <!--Boundary precedences-->    
      <xsl:if test="count(mms:boundaryPrecedence) &gt; 0">
           \indent {\Large Boundary precedences}\\
           <xsl:text> \indent \begin{enumerate}[noitemsep, topsep=0pt,after=\vspace{\baselineskip}]</xsl:text>
           <xsl:for-each select="mms:boundaryPrecedence/mms:boundary">\item{<xsl:value-of select="."/>}<xsl:text>
           </xsl:text></xsl:for-each><xsl:text>\end{enumerate}</xsl:text>
      </xsl:if>
      
      <!--finalizationconditions-->      
     <xsl:if test="count(mms:finalizationConditions) &gt; 0">
          \indent{\Large Finalization Conditions}        \\     
          <xsl:if test="count(mms:finalizationConditions/mms:finalizationCondition) &gt; 0">
               <xsl:for-each select="mms:finalizationConditions/mms:finalizationCondition">
                    \begin{mygrouping}
                    <xsl:if test="mms:condition">
                         \indent {\large The condition is applied when:}
                         {\color{NavyBlue}
                         \begin{dmath}
                         <xsl:apply-templates select="mms:applyIf/mt:math"><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                         \end{dmath}}
                    </xsl:if>
                    \indent {\large The condition is:}
                    {\color{NavyBlue}
                    <xsl:for-each select="mms:mathExpressions/mt:math">
                         \begin{dmath}
                         <xsl:apply-templates select="."><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                         \end{dmath}
                    </xsl:for-each>}
                    \end{mygrouping}
               </xsl:for-each>     
          </xsl:if>
     </xsl:if>
</xsl:template>
</xsl:stylesheet>
