<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		      xmlns:mt="http://www.w3.org/1998/Math/MathML"
		      xmlns:mms="urn:mathms"
		      xmlns:sml="urn:simml"
                version='2.0'>                
     <xsl:output method="text" indent="no" encoding="UTF-8" media-type="application/x-tex"/>

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->

     <xsl:include href="parameters.xsl"/>
    <xsl:include href="regionInteraction.xsl"/>
     <xsl:include href="spatialDomain.xsl"/>
     <xsl:include href="equivalences.xsl"/>
     <xsl:include href="mml_PDE/instructionToPseudocode.xsl"/>

<xsl:strip-space elements="mt:*"/>
     
 <xsl:template match="mms:PDEProblem">
      <xsl:if test="count(mms:head) &gt; 0">   
           \begin{center}
           \textbf{<xsl:choose><xsl:when test="count(mms:head/mms:name) &gt; 0"><xsl:value-of select="mms:head/mms:name"/></xsl:when><xsl:otherwise>PDE Problem</xsl:otherwise></xsl:choose>}      
           <!--head-->
           <xsl:if test="mms:head/mms:author/text() != ''">
                by <xsl:value-of select="mms:head/mms:author"/>\\    
           </xsl:if>
           <xsl:if test="mms:head/mms:version/text() != ''">
                Version: <xsl:value-of select="mms:head/mms:version"/>\\
           </xsl:if> 
           <xsl:if test="count(mms:head/mms:date) &gt; 0">
               <xsl:value-of select=' format-dateTime(mms:head/mms:date, "[MNn] [D], [Y]", "en", (), ())'/>\\
           </xsl:if>
           \end{center}
           <xsl:if test="mms:head/mms:description/text() != ''">
                \noindent <xsl:value-of select="mms:head/mms:description"/>\\
           </xsl:if> 
           \noindent \rule{\textwidth}{1pt}\\
      </xsl:if>
      <!--fields-->
      <xsl:if test="count(mms:fields/mms:field) &gt; 0">      
           \indent {\Large Fields}
           
           \indent \indent <xsl:for-each select="mms:fields/mms:field">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
      </xsl:if>

      <!--coordinate systems-->
      <xsl:if test="count(mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate) &gt; 0">
           \indent {\Large Spatial Coordinates}      
           
           \indent \indent <xsl:for-each select="mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
      </xsl:if>
      <!--time coordinate-->
     <xsl:if test="count(mms:coordinates/mms:timeCoordinate) &gt; 0">
           \indent {\Large Time Coordinate}       
           
           \indent \indent $<xsl:value-of select="mms:coordinates/mms:timeCoordinate"/>$\\        
      </xsl:if>
      <!--parameters-->      
      <xsl:call-template name="parameters"></xsl:call-template>
      
      <!--auxiliar fields-->
      <xsl:if test="count(mms:auxiliaryFields/mms:auxiliaryField) &gt; 0">      
           \indent {\Large Auxiliary Fields}
           <xsl:text>                               
           </xsl:text>           
           \indent \indent <xsl:for-each select="mms:auxiliaryFields/mms:auxiliaryField">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
      </xsl:if>

     <!--auxiliary variables-->
     <xsl:if test="count(mms:auxiliaryVariables/mms:auxiliaryVariable) &gt; 0">      
         \indent {\Large Auxiliary Variables}
         <xsl:text>                               
         </xsl:text>           
         \indent \indent <xsl:for-each select="mms:auxiliaryVariables/mms:auxiliaryVariable">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
     </xsl:if>

     <!--analysis fields-->
     <xsl:if test="count(mms:analysisFields/mms:analysisField) &gt; 0">      
         \indent {\Large Analysis Fields}
         <xsl:text>                               
         </xsl:text>           
         \indent \indent <xsl:for-each select="mms:analysisFields/mms:analysisField">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
     </xsl:if>
  
     <!--analysis auxiliary variables-->
     <xsl:if test="count(mms:auxiliaryAnalysisVariables/mms:auxiliaryAnalysisVariable) &gt; 0">      
         \indent {\Large Auxiliary Analysis Variables}
         <xsl:text>                               
         </xsl:text>           
         \indent \indent <xsl:for-each select="mms:auxiliaryAnalysisVariables/mms:auxiliaryAnalysisVariable">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>\\
     </xsl:if>
     
      <!--imported models-->
     <xsl:if test="count(mms:models/mms:model) &gt; 0">
           \indent {\Large Imported Models}
           <xsl:text>                               
           </xsl:text> 
         <xsl:for-each select="mms:models/mms:model">
             \indent \indent {\large <xsl:value-of select="mms:name"/>}\\
             
             <!--field equalities-->
             <xsl:apply-templates select="mms:fieldEquivalence"></xsl:apply-templates>
             
             <!--variable equalities-->
             <xsl:apply-templates select="mms:variableEquivalence"></xsl:apply-templates>
             
             <!--coordinate equalities-->
             <xsl:apply-templates select="mms:coordinateEquivalence"></xsl:apply-templates>
             
             <!--parameter equalities-->
             <xsl:apply-templates select="mms:parameterEquivalence"></xsl:apply-templates>
             
         </xsl:for-each>
      </xsl:if>
      
     <!--Region-->
     <xsl:if test="count(mms:region) &gt; 0">
         \indent {\Large Region}
         <xsl:call-template name="region">
             <xsl:with-param name="region" select="mms:region"></xsl:with-param>
         </xsl:call-template>
     </xsl:if>
      
      <!--Subregions--> 
      <xsl:if test="count(mms:subregions/mms:subregion) &gt; 0">
           \indent {\Large Subregions}
          <xsl:for-each select="mms:subregions/mms:subregion">
              <xsl:call-template name="region">
                  <xsl:with-param name="region" select="."></xsl:with-param>
              </xsl:call-template>
          </xsl:for-each>
      </xsl:if>
      
      <!--Subregions Precedence-->
      <xsl:if test="count(mms:subregionPrecedence) &gt; 0">
           \indent {\Large Subregion precedence}\\
           <xsl:text> \indent \begin{enumerate}[noitemsep, topsep=0pt,after=\vspace{\baselineskip}]</xsl:text>
          <xsl:for-each select="mms:subregionPrecedence/mms:precedence">\item{<xsl:value-of select="mms:prevalentRegion"/> has precedence over <xsl:value-of select="mms:surrogateRegion"/>}<xsl:text>
           </xsl:text></xsl:for-each><xsl:text>\end{enumerate}
           </xsl:text>
      </xsl:if>
      
     <!--analysis equations-->
     <xsl:if test="count(mms:analysisFieldEquations/mms:analysisFieldEquation) &gt; 0">
         \indent {\Large Analysis Field Equations}
         <xsl:text>                               
         </xsl:text>
         <xsl:for-each select="mms:analysisFieldEquations/mms:analysisFieldEquation">
             <xsl:variable name="nameEq"><xsl:choose><xsl:when test="./mms:name != ''"><xsl:value-of select="./mms:name"/></xsl:when><xsl:otherwise>Analysis field equation</xsl:otherwise></xsl:choose></xsl:variable>
             \begin{mygrouping}
             <xsl:variable name="field"><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:analysisField"/></xsl:call-template></xsl:variable>
             \indent \indent {\large <xsl:value-of select="$nameEq"/>}
             \begin{dmath}
             <xsl:value-of select="$field"/><xsl:text> = </xsl:text>
             <xsl:for-each select="mms:operator">
                 <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/></xsl:call-template><xsl:text>(</xsl:text><xsl:for-each select="/mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate"><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:text>, </xsl:text></xsl:for-each><xsl:value-of select="/mms:PDEProblem/mms:coordinates/mms:timeCoordinate"/><xsl:text>)</xsl:text>
                 <xsl:if test="position()!=last()">+</xsl:if>
             </xsl:for-each>
             \end{dmath}
             <xsl:for-each select="mms:operator">
                 \begin{dmath}
                 <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/></xsl:call-template><xsl:text>(</xsl:text><xsl:for-each select="/mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate"><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:text>, </xsl:text></xsl:for-each><xsl:value-of select="/mms:PDEProblem/mms:coordinates/mms:timeCoordinate"/><xsl:text>) = </xsl:text>
                 <xsl:for-each select="mms:term">
                    <xsl:if test="position() > 0 and not(./mt:math/mt:apply[mt:minus and count(*) = 2])">
                       <xsl:text>+</xsl:text>
                    </xsl:if>
                     <xsl:call-template name="processTerm">
                         <xsl:with-param name="term" select="."/>
                     </xsl:call-template>
                 </xsl:for-each>
                 \end{dmath}
             </xsl:for-each>
             \end{mygrouping}
         </xsl:for-each>
     </xsl:if>
     
     
     <!--auxiliar equations-->
     <xsl:if test="count(mms:auxiliaryAnalysisVariableEquations/mms:auxiliaryAnalysisVariableEquation) &gt; 0">
         \indent {\Large Auxiliary Analysis Equations}
         <xsl:text>                               
         </xsl:text>
         <xsl:for-each select="mms:auxiliaryAnalysisVariableEquations/mms:auxiliaryAnalysisVariableEquation">
             <xsl:variable name="nameEq"><xsl:choose><xsl:when test="./mms:name != ''"><xsl:value-of select="./mms:name"/></xsl:when><xsl:otherwise>Auxiliary analysis variable equation</xsl:otherwise></xsl:choose></xsl:variable>
             \begin{mygrouping}
             <xsl:variable name="field"><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:auxiliaryAnalysisVariable"/></xsl:call-template></xsl:variable>
             \indent \indent {\large <xsl:value-of select="$nameEq"/>}
             \begin{dmath}
             <xsl:value-of select="$field"/><xsl:text> = </xsl:text>
             <xsl:for-each select="mms:operator">
                 <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/></xsl:call-template><xsl:text>(</xsl:text><xsl:for-each select="/mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate"><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:text>, </xsl:text></xsl:for-each><xsl:value-of select="/mms:PDEProblem/mms:coordinates/mms:timeCoordinate"/><xsl:text>)</xsl:text>
                 <xsl:if test="position()!=last()">+</xsl:if>
             </xsl:for-each>
             \end{dmath}
             <xsl:for-each select="mms:operator">
                 \begin{dmath}
                 <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/></xsl:call-template><xsl:text>(</xsl:text><xsl:for-each select="/mms:PDEProblem/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate"><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:text>, </xsl:text></xsl:for-each><xsl:value-of select="/mms:PDEProblem/mms:coordinates/mms:timeCoordinate"/><xsl:text>) = </xsl:text>
                 <xsl:for-each select="mms:term">
                    <xsl:if test="position() > 0 and not(./mt:math/mt:apply[mt:minus and count(*) = 2])">
                       <xsl:text>+</xsl:text>
                    </xsl:if>
                     <xsl:call-template name="processTerm">
                         <xsl:with-param name="term" select="."/>
                     </xsl:call-template>
                 </xsl:for-each>
                 \end{dmath}
             </xsl:for-each>
             \end{mygrouping}
         </xsl:for-each>
     </xsl:if>
      
      <!--Boundaries-->      
      <xsl:if test="count(mms:boundaryConditions/mms:boundaryPolicy) &gt; 0">
           \indent {\Large Boundary conditions}\\
          <xsl:for-each select="mms:boundaryConditions/mms:boundaryPolicy">
                \indent \indent Segments: <xsl:value-of select="mms:boundaryRegions/mms:regionName" separator=", "/>
                <xsl:for-each select="mms:boundaryCondition">
                     \begin{mygrouping}
                     Type: <xsl:value-of select=" local-name(mms:type/*)"/>\\
                     Axis: <xsl:value-of select="mms:axis"/>\\
                     Side: <xsl:value-of select="mms:side"/>\\
                     <xsl:if test="count(mms:fields) &gt; 0">
                          Fields: <xsl:for-each select="mms:fields/mms:field">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="."/></xsl:call-template><xsl:choose><xsl:when test="position()=last()">$</xsl:when><xsl:otherwise><xsl:text>$, </xsl:text></xsl:otherwise></xsl:choose></xsl:for-each>
                     </xsl:if>
                     <xsl:if test="count(mms:applyIf) &gt; 0">
                          {\large This boundary is applied when:}
                          {\color{NavyBlue}
                          \begin{dmath}
                         <xsl:apply-templates select="mms:applyIf/mt:math"><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                          \end{dmath}}
                     </xsl:if>
                    
                     <xsl:if test="count(mms:type/mms:algebraic/mms:expression) &gt; 0">
                          Field values\\
                         <xsl:for-each select="mms:type/mms:algebraic/mms:expression">
                                {\color{NavyBlue}
                                \begin{dmath}
                                <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:field"/></xsl:call-template> = <xsl:apply-templates select="mt:math"><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                                \end{dmath}}
                          </xsl:for-each>
                     </xsl:if>
                    <xsl:if test="count(mms:type/mms:reflection/mms:sign) &gt; 0">
                          Reflection parity
                          
                          \begin{mytable}{c|c}{15pt}
                          \textbf{\small Field} &#038; \textbf{\small Parity} \\ \hlinegray
                        <xsl:for-each select="mms:type/mms:reflection/mms:sign">
                               $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:field"/></xsl:call-template>$  &#038; <xsl:value-of select="mms:value"/> \\           
                          </xsl:for-each>
                          \end{mytable}
                     </xsl:if>
                    
                     \end{mygrouping}
                </xsl:for-each>
           </xsl:for-each>        
      </xsl:if>
      
      <!--Boundary precedences-->    
      <xsl:if test="count(mms:boundaryPrecedence) &gt; 0">
           \indent {\Large Boundary precedences}\\
           <xsl:text> \indent \begin{enumerate}[noitemsep, topsep=0pt,after=\vspace{\baselineskip}]</xsl:text>
           <xsl:for-each select="mms:boundaryPrecedence/mms:boundary">\item{<xsl:value-of select="."/>}<xsl:text>
           </xsl:text></xsl:for-each><xsl:text>\end{enumerate}
           </xsl:text>
      </xsl:if>
     
      
      <!--finalization conditions-->      
     <xsl:if test="count(mms:finalizationConditions) &gt; 0">
         \indent{\Large Finalization Conditions}        \\     
         <xsl:if test="count(mms:finalizationConditions/mms:finalizationCondition) &gt; 0">
             <xsl:for-each select="mms:finalizationConditions/mms:finalizationCondition">
                 \begin{mygrouping}
                 <xsl:if test="mms:condition">
                     \indent {\large The condition is applied when:}
                     {\color{NavyBlue}
                     \begin{dmath}
                     <xsl:apply-templates select="mms:applyIf/mt:math"><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                     \end{dmath}}
                 </xsl:if>
                 \indent {\large The condition is:}
                 {\color{NavyBlue}
                 <xsl:for-each select="mms:mathExpressions/mt:math">
                     \begin{dmath}
                     <xsl:apply-templates select="."><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                     \end{dmath}
                 </xsl:for-each>}
                 \end{mygrouping}
             </xsl:for-each>     
         </xsl:if>
     </xsl:if>
 </xsl:template>
    
    
    <xsl:template name="region">
        <xsl:param name="region"/>
        \begin{mygrouping}
        \indent \indent {\large <xsl:value-of select="$region/mms:name"/>}
        <!--interior models-->
        <xsl:if test="count($region//mms:interiorModel) &gt; 0">
            \indent \indent {\large Interior models}\\
            \indent \indent \indent <xsl:value-of select="$region//mms:interiorModel" separator=", "/>\\
        </xsl:if>
        <!--surface models-->
        <xsl:if test="count($region//mms:surfaceModel) &gt; 0">
            \indent \indent {\large Surface models}\\
            \indent \indent \indent <xsl:value-of select="$region//mms:surfaceModel" separator=", "/>\\
        </xsl:if>
        
        <xsl:call-template name="spatialDomain"><xsl:with-param name="sd" select="$region/mms:spatialDomain"/></xsl:call-template>
        
        <xsl:if test="count($region/mms:location) &gt; 0">
            \indent \indent {\large Location}\\
            <xsl:for-each select="$region/mms:spatialDomain">
                <xsl:call-template name="spatialDomain"><xsl:with-param name="sd" select="."/></xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="$region/mms:x3d">
                \indent \indent \indent <xsl:value-of select="mms:name" separator=", "/>\\
            </xsl:for-each>
        </xsl:if>
        
        <!--initialconditions-->      
        <xsl:if test="count($region/mms:initialConditions) &gt; 0">
            \indent{\Large Initial Conditions}\\
            <xsl:if test="count($region/mms:initialConditions/mms:initialCondition) &gt; 0">
                <xsl:for-each select="$region/mms:initialConditions/mms:initialCondition">
                    \begin{mygrouping}
                    <xsl:if test="mms:applyIf">
                        \indent {\large The condition is applied when:}
                        {\color{NavyBlue}
                        \begin{dmath}
                        <xsl:apply-templates select="mms:applyIf/mt:math"><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                        \end{dmath}}
                    </xsl:if>
                    \indent {\large The condition is:}
                    {\color{NavyBlue}
                    <xsl:for-each select="mms:mathExpressions/mt:math">
                        \begin{dmath}
                        <xsl:apply-templates select="."><xsl:with-param name="condition">noSymbol</xsl:with-param><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="indent">0</xsl:with-param></xsl:apply-templates>
                        \end{dmath}
                    </xsl:for-each>}
                    \end{mygrouping}
                </xsl:for-each>
            </xsl:if>
        </xsl:if>
        
        <!-- interactions -->      
        <xsl:if test="count($region/mms:interactions) &gt; 0">
            \indent \indent \indent \textbf{Segment interactions} 
            <xsl:for-each select="$region/mms:interactions/mms:interaction">
                <xsl:apply-templates select="."></xsl:apply-templates>
            </xsl:for-each>           
        </xsl:if>
        \end{mygrouping}
    </xsl:template>
    
    <!-- process terms-->
    <xsl:template name="processTerm">
        <xsl:param name="term"/>
        <!-- Process math expression -->
        <xsl:if test="$term/mt:math">
            <xsl:apply-templates select="$term/mt:math"><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates><xsl:if test="$term/mms:partialDerivatives/mms:partialDerivative"><xsl:text>\mbox{ }</xsl:text></xsl:if>
        </xsl:if>
        <!-- Process derivatives -->
        <xsl:for-each select="$term/mms:partialDerivatives/mms:partialDerivative">
            <xsl:text>{\partial_</xsl:text><xsl:value-of select="./mms:coordinate"/><xsl:text> </xsl:text><xsl:call-template name="processTerm"><xsl:with-param name="term" select="."></xsl:with-param></xsl:call-template><xsl:text>}</xsl:text> 
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
