<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		      xmlns:mt="http://www.w3.org/1998/Math/MathML"
		      xmlns:mms="urn:mathms"
		      xmlns:my="internalarray"
		      xmlns="urn:mathms"
                version='1.0'>                
<xsl:output method="text" indent="no" encoding="UTF-8"/>

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->

<xsl:strip-space elements="mt:*"/>
     
<xsl:template name="parameters" match="mms:parameters">
     <xsl:if test="count(mms:parameters/mms:parameter) &gt; 0">
          \indent {\Large Parameters} 
          \begin{mytable}{c|c|c}{15pt}
          \textbf{\small Parameter} &#038; \textbf{\small Type} &#038; \textbf{\small Default value} \\ \hlinegray
          <xsl:for-each select="mms:parameters/mms:parameter">
               $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/></xsl:call-template>$ &#038; <xsl:value-of select="mms:type"/> &#038; <xsl:if test="not(mms:defaultValue)">Not set</xsl:if><xsl:if test="mms:defaultValue">$<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:defaultValue"/></xsl:call-template>$</xsl:if> \\ 
          </xsl:for-each>
          \end{mytable}
     </xsl:if>
</xsl:template>
   
     <xsl:template name="schemaParameters" match="mms:schemaParameters">
          <xsl:if test="count(mms:schemaParameters/mms:schemaParameter) &gt; 0">
               \indent {\Large Schema Parameters} 
               \begin{mytable}{c|c|c}{15pt}
               \textbf{\small Parameter} &#038; \textbf{\small Name} &#038; \textbf{\small Values} \\ \hlinegray
               <xsl:for-each select="mms:schemaParameters/mms:schemaParameter">
                    $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/></xsl:call-template>$ &#038; <xsl:value-of select="mms:paramName"/> &#038; <xsl:if test="not(mms:values)">Not set</xsl:if><xsl:for-each select="mms:values/mms:value">$<xsl:apply-templates select="."><xsl:with-param name="encapsulate" select="no"></xsl:with-param><xsl:with-param name="condition" select="sp"></xsl:with-param></xsl:apply-templates>$<xsl:if test="position()!=last()">, </xsl:if></xsl:for-each> \\ 
               </xsl:for-each>
               \end{mytable}
          </xsl:if>
     </xsl:template>
 
     <xsl:template name="functionParameters" match="mms:functionParameters">
     <xsl:if test="count(mms:functionParameters/mms:functionParameter) &gt; 0">
          \indent {\Large Function Parameters} 
          \begin{mytable}{c|c|c}{15pt}
          \textbf{\small Parameter} &#038; \textbf{\small Type} \\ \hlinegray
          <xsl:for-each select="mms:functionParameters/mms:functionParameter">
               $<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:name"/></xsl:call-template>$ &#038; <xsl:value-of select="mms:type"/> \\ 
          </xsl:for-each>
          \end{mytable}
     </xsl:if>
     </xsl:template>
     
</xsl:stylesheet>
