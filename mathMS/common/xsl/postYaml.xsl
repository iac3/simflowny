<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"  xmlns:mms="urn:mathms"  xmlns:sml="urn:simml" xmlns:mt="http://www.w3.org/1998/Math/MathML">
    
   <xsl:template match="mms:term">
      <mms:term>
         <xsl:call-template name="term">
            <xsl:with-param name="term" select="./mt:math/*"></xsl:with-param>
         </xsl:call-template>
      </mms:term>
   </xsl:template>
   
   <xsl:template name="term">
      <xsl:param name="term"></xsl:param>
         <xsl:choose>
            <!-- Only math without derivative -->
            <xsl:when test="not($term/descendant-or-self::mt:apply/*[1][text() = 'Der'])">
               <mt:math>
               <xsl:copy-of select="$term"></xsl:copy-of>
               </mt:math>
            </xsl:when>
            <!-- Only one derivative -->
            <xsl:when test="$term/*[1][text() = 'Der']">
               <mms:partialDerivatives>
               <xsl:call-template name="derivative">
                  <xsl:with-param name="der" select="$term"></xsl:with-param>
               </xsl:call-template>
               </mms:partialDerivatives>
            </xsl:when>
            <xsl:otherwise>
               <!-- Extract math if exists -->
            
               <xsl:if test="$term/*[position() > 1][not(descendant-or-self::mt:apply/*[1][text() = 'Der'])]">
                  <mt:math>
                     <xsl:choose>
                        <xsl:when test="count($term/*[position() > 1][not(descendant-or-self::mt:apply/*[1][text() = 'Der'])]) > 1">
                           <mt:apply>
                              <mt:times/>
                              <xsl:copy-of select="$term/*[position() > 1][not(descendant-or-self::mt:apply/*[1][text() = 'Der'])]"></xsl:copy-of>
                           </mt:apply>
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:copy-of select="$term/*[position() > 1][not(descendant-or-self::mt:apply/*[1][text() = 'Der'])]"></xsl:copy-of>
                        </xsl:otherwise>
                     </xsl:choose>                   
                  </mt:math>
               </xsl:if>
               <!-- Extract derivatives -->
               <mms:partialDerivatives>
               <xsl:for-each select="$term/mt:apply[*[1][text() = 'Der']]">
                  <xsl:call-template name="derivative">
                     <xsl:with-param name="der" select="."></xsl:with-param>
                  </xsl:call-template>
               </xsl:for-each>
               </mms:partialDerivatives>
            </xsl:otherwise>
         </xsl:choose>
   </xsl:template>
   
   <!-- To avoid lists in initial conditions -->
   <xsl:template match="mms:mathExpressions">
      <mms:mathExpressions>
         <xsl:for-each select="*">
            <xsl:apply-templates select="./*"></xsl:apply-templates>
         </xsl:for-each>
      </mms:mathExpressions>
   </xsl:template>
   
   <xsl:template name="derivative">
      <xsl:param name="der"></xsl:param>
      <mms:partialDerivative>
         <xsl:choose>
            <xsl:when test="$der[vector]">
               <mms:coordinate><xsl:value-of select="$der/vector/*[1]"/></mms:coordinate>
               <xsl:call-template name="term">
                  <xsl:with-param name="term" select="$der/vector/*[2]"></xsl:with-param>
               </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
               <mms:coordinate><xsl:value-of select="$der/*[2]"/></mms:coordinate>
               <xsl:call-template name="term">
                  <xsl:with-param name="term" select="$der/*[3]"></xsl:with-param>
               </xsl:call-template>
            </xsl:otherwise>
         </xsl:choose>
      </mms:partialDerivative>
   </xsl:template>
   
   <xsl:template match="@*|node()">
      <xsl:copy>
         <xsl:apply-templates select="@*|node()"/>
      </xsl:copy>
   </xsl:template>
</xsl:stylesheet>
