<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		      xmlns:mt="http://www.w3.org/1998/Math/MathML"
		      xmlns:mms="urn:mathms"
		      xmlns="urn:mathms"
                version='2.0'>                
<xsl:output method="text" indent="no" encoding="UTF-8"/>

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->

<xsl:strip-space elements="mt:*"/>
     
    
    <xsl:template match="mms:fieldInteraction">        
        \begin{mygrouping}
        \indent {\large Field Interaction <xsl:value-of select="position()"/>}
        
        \indent \indent Target segments: <xsl:value-of select="mms:targetRegions/mms:targetRegion" separator=", "/>\\
                
        {\color{NavyBlue}
        <xsl:for-each select="mms:projections/mms:projection">
            \begin{dmath}
            <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:variable"/></xsl:call-template>=<xsl:for-each select="mms:terms/mms:term"><xsl:if test="mms:field">\left(</xsl:if><xsl:apply-templates select="mt:math"><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="condition">noSymbol</xsl:with-param></xsl:apply-templates><xsl:if test="mms:field">\right)</xsl:if> <xsl:if test="mms:field"><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="*[1]"/></xsl:call-template></xsl:if><xsl:if test="position()!=last()">+</xsl:if></xsl:for-each>
            \end{dmath}
        </xsl:for-each>
        }
        \end{mygrouping}
    </xsl:template>
    
    <xsl:template match="mms:eigenVectorInteraction">        
        \begin{mygrouping}
        \indent {\large Eigen Vector Interaction <xsl:value-of select="position()"/>}
        
        \indent \indent Target segments: <xsl:value-of select="mms:targetRegions/mms:targetRegion" separator=", "/>\\
        
        {\color{NavyBlue}
        <xsl:for-each select="mms:projections/mms:projection">
            \begin{dmath}
            <xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:variable"/></xsl:call-template>=<xsl:for-each select="mms:diagonalizationCoefficients/mms:diagonalizationCoefficient"><xsl:if test="mms:field">\left(</xsl:if><xsl:apply-templates select="mt:math"><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="condition">noSymbol</xsl:with-param></xsl:apply-templates><xsl:if test="mms:field">\right)</xsl:if> <xsl:if test="mms:field"><xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="*[1]"/></xsl:call-template></xsl:if><xsl:if test="position()!=last()">+</xsl:if></xsl:for-each>
            \end{dmath}
        </xsl:for-each>
        }
        \end{mygrouping}
    </xsl:template>
     
    <xsl:template match="mms:interaction">        
        \begin{mygrouping}
        \indent {\large Interaction <xsl:value-of select="position()"/>}
        
        \indent \indent Target segments: <xsl:value-of select="mms:targetRegions/mms:targetRegion" separator=", "/>\\
        
        \indent \indent <xsl:value-of select="mms:interaction/mms:type"/> interaction
        
        <xsl:variable name="type"><xsl:value-of select="mms:interaction/mms:type"/></xsl:variable>
        {\color{NavyBlue}
        <xsl:for-each select="mms:interaction/mms:fixedValues/mms:fixedValue">
            \begin{dmath}<xsl:call-template name="replaceEntities"><xsl:with-param name="content" select="mms:variable"/></xsl:call-template>=<xsl:apply-templates select="mt:math"><xsl:with-param name="encapsulate">no</xsl:with-param><xsl:with-param name="condition">noSymbol</xsl:with-param></xsl:apply-templates>\end{dmath}</xsl:for-each>
        }
        
        \end{mygrouping}
    </xsl:template>
     
</xsl:stylesheet>
