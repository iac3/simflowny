<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"  xmlns:mms="urn:mathms"  xmlns:sml="urn:simml" xmlns:mt="http://www.w3.org/1998/Math/MathML">
    
   <xsl:template match="mms:operator">
      <mms:operator>
         <mms:name><xsl:value-of select="./mms:name"/></mms:name>
         <mms:derivativeTerms><xsl:call-template name="terms"></xsl:call-template></mms:derivativeTerms>
      </mms:operator>
   </xsl:template>
   
   <xsl:template name="terms">
      <mt:math>
         <xsl:choose>
            <xsl:when test="count(./mms:term) > 1">
               <mt:apply>
                  <mt:plus/>
                  <xsl:for-each select="./mms:term">
                     <xsl:call-template name="term">
                        <xsl:with-param name="term" select="."></xsl:with-param>
                     </xsl:call-template>
                  </xsl:for-each>
               </mt:apply>
            </xsl:when>
            <xsl:otherwise>
               <xsl:call-template name="term">
                  <xsl:with-param name="term" select="./mms:term"></xsl:with-param>
               </xsl:call-template>
            </xsl:otherwise>
         </xsl:choose>
      </mt:math>
   </xsl:template>
   
   <xsl:template name="term">
      <xsl:param name="term"></xsl:param>
      <xsl:for-each select="$term">
         <xsl:choose>
            <xsl:when test="(./mms:partialDerivatives/mms:partialDerivative and ./mt:math) or count(./mms:partialDerivatives/mms:partialDerivative) > 1">
               <mt:apply>
                  <mt:times/>
                  <xsl:if test="./mt:math">
                     <xsl:call-template name="processMath">
                        <xsl:with-param name="node" select="./mt:math"></xsl:with-param>
                     </xsl:call-template>
                  </xsl:if> 
                  <xsl:for-each select="./mms:partialDerivatives/mms:partialDerivative">
                     <xsl:call-template name="derivative">
                        <xsl:with-param name="der" select="."></xsl:with-param>
                     </xsl:call-template> 
                  </xsl:for-each>
               </mt:apply>
            </xsl:when>
            <xsl:otherwise>
               <xsl:if test="./mt:math">
                  <xsl:call-template name="processMath">
                     <xsl:with-param name="node" select="./mt:math"></xsl:with-param>
                  </xsl:call-template>
               </xsl:if>
               <xsl:if test="./mms:partialDerivatives/mms:partialDerivative">
               <xsl:call-template name="derivative">
                  <xsl:with-param name="der" select="./mms:partialDerivatives/mms:partialDerivative"></xsl:with-param>
               </xsl:call-template> 
               </xsl:if>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:for-each>
   </xsl:template>
   
   <xsl:template name="derivative">
      <xsl:param name="der"></xsl:param>
      <mt:apply>
         <mt:ci>Der</mt:ci>
         <mt:ci><xsl:value-of select="$der/mms:coordinate"/></mt:ci>
         <xsl:call-template name="term">
            <xsl:with-param name="term" select="$der"></xsl:with-param>
         </xsl:call-template>
      </mt:apply>
   </xsl:template>
   
   <xsl:template name="processMath">
      <xsl:param name="node"></xsl:param>
      <xsl:apply-templates select="$node/*"></xsl:apply-templates>
   </xsl:template>
   
   <xsl:template match="@*|node()">
      <xsl:copy>
         <xsl:apply-templates select="@*|node()"/>
      </xsl:copy>
   </xsl:template>
</xsl:stylesheet>
