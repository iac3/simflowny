<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
     xmlns:m="http://www.w3.org/1998/Math/MathML" version="1.0">

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->

                <xsl:param name="condition"/>
                <xsl:param name="indent">0</xsl:param>
                <xsl:param name="dimensions">2</xsl:param>
                <xsl:param name="coords"/>
                <xsl:param name="timeCoord"/>
                <xsl:output method="text" indent="no" encoding="UTF-8"/>

                <xsl:include href="entities.xsl"/>
                <xsl:include href="cmarkup.xsl"/>
                <xsl:include href="imarkup.xsl"/>

                <xsl:strip-space elements="m:*"/>
    <xsl:template match="m:*[not(descendant::m:piecewise)]/text()">
                                <xsl:call-template name="replaceEntities">
                                                <xsl:with-param name="content" select="normalize-space()"/>
                                </xsl:call-template>
                </xsl:template>

    <xsl:template match="m:math[not(descendant::m:piecewise) and not(@mode) or @mode='inline'][not(descendant::m:piecewise) and not(@display)] | m:math[not(descendant::m:piecewise) and @display='inline']">
                    <xsl:param name="indent" select="$indent"/>
                    <xsl:param name="condition" select="$condition"/>
                    <xsl:param name="inversion" select="0"/>
                                <xsl:if test="$condition = 'false'">
                                                <xsl:call-template name="recIndent">
                                                                <xsl:with-param name="indent" select="$indent"/>
                                                </xsl:call-template>
                                </xsl:if>
                                <xsl:apply-templates>
                                                <xsl:with-param name="condition" select="$condition"/>
                                    <xsl:with-param name="inversion" select="$inversion"/>
                                </xsl:apply-templates>
                    <xsl:if test="$condition = 'false'">
                        <xsl:text>;
</xsl:text>
                    </xsl:if>
                </xsl:template>

    <xsl:template match="m:math[not(descendant::m:piecewise) and @display='block'] | m:math[not(descendant::m:piecewise) and @mode='display'][not(descendant::m:piecewise) and not(@display)]">
                    <xsl:param name="indent" select="$indent"/>
        <xsl:param name="condition" select="$condition"/>
                    <xsl:param name="inversion" select="0"/>
        <xsl:if test="$condition = 'false'">
                                    <xsl:call-template name="recIndent">
                                                    <xsl:with-param name="indent" select="$indent"/>
                                    </xsl:call-template>
                    </xsl:if>
                                <xsl:apply-templates>
                                                <xsl:with-param name="condition" select="$condition"/>
                                    <xsl:with-param name="inversion" select="$inversion"/>
                                </xsl:apply-templates>
                    <xsl:if test="$condition = 'false'">
                        <xsl:text>;
</xsl:text>
                    </xsl:if>
                </xsl:template>
              
    <!-- 4.4.2.16 piecewise special case-->
    <xsl:template match="m:math[descendant::m:piecewise]">
        <xsl:param name="condition" select="'false'"/>
        <xsl:param name="indent" select="$indent"/>
        <xsl:variable name="leftPart">
            <xsl:apply-templates select="m:apply/*[2]"/>
        </xsl:variable>
        <xsl:for-each select="//m:piece">
            <xsl:call-template name="recIndent">
                <xsl:with-param name="indent" select="$indent + position() - 1"/>
            </xsl:call-template>
            <xsl:text>if (</xsl:text>
            <xsl:apply-templates select="*[2]">
                <xsl:with-param name="condition" select="'true'"/>
            </xsl:apply-templates>
            <xsl:text>) {</xsl:text>
            <xsl:text>
</xsl:text>
            <xsl:call-template name="recIndent">
                <xsl:with-param name="indent" select="$indent + 1 + position() - 1"/>
            </xsl:call-template>
            <xsl:value-of select="$leftPart"/>
            <xsl:text> = </xsl:text>
            <xsl:apply-templates select="*[1]">
                <xsl:with-param name="condition" select="$condition"/>
            </xsl:apply-templates>
            <xsl:text>;</xsl:text>
                <xsl:text>
</xsl:text>
            <xsl:if test="not(position()=last()) or ../m:otherwise">
                <xsl:call-template name="recIndent">
                    <xsl:with-param name="indent" select="$indent + position() - 1"/>
                </xsl:call-template>
                <xsl:text>} else {</xsl:text>
                <xsl:text>
</xsl:text>
            </xsl:if>
            <!-- Otherwise -->
            <xsl:if test="position() = last() and ../m:otherwise">
                <xsl:call-template name="recIndent">
                    <xsl:with-param name="indent" select="$indent + 1 + position() - 1"/>
                </xsl:call-template>
                <xsl:value-of select="$leftPart"/>
                <xsl:text> = </xsl:text>
                <xsl:apply-templates select="../m:otherwise">
                    <xsl:with-param name="condition" select="$condition"/>
                </xsl:apply-templates>
                <xsl:text>;</xsl:text>
                <xsl:text>
</xsl:text>
            </xsl:if>
        </xsl:for-each>
        
        <!-- Close ifs -->
        <xsl:for-each select="//m:piece">     
            <xsl:call-template name="recIndent">
                <xsl:with-param name="indent" select="$indent + 1 + last() - position() - 1"/>
            </xsl:call-template>
            <xsl:text>}</xsl:text>
            <xsl:text>
</xsl:text>
        </xsl:for-each>
    </xsl:template>
              
</xsl:stylesheet>
