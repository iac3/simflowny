<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:s="urn:simml" xmlns:fn="http://www.w3.org/2005/02/xpath-functions" 
	xmlns:m="http://www.w3.org/1998/Math/MathML" version="1.0"
	xmlns:mms="urn:mathms">

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->

	<xsl:template match="s:timeIncrement">
		<xsl:text>simPlat_dt</xsl:text>
	</xsl:template>

    <xsl:template match="s:readFromFile1D">
        <xsl:text>readFromFile1D(position1_</xsl:text><xsl:value-of select="./*[1]"/><xsl:text>, data_</xsl:text><xsl:value-of select="./*[1]"/><xsl:text>, </xsl:text><xsl:apply-templates select="./*[2]"/><xsl:text>)</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:readFromFile2D">
        <xsl:text>readFromFile2D(position1_</xsl:text><xsl:value-of select="./*[1]"/><xsl:text>, position2_</xsl:text><xsl:value-of select="./*[1]"/><xsl:text>, data_</xsl:text><xsl:value-of select="./*[1]"/><xsl:text>, </xsl:text><xsl:apply-templates select="./*[2]"/><xsl:text>, </xsl:text><xsl:apply-templates select="./*[3]"/><xsl:text>)</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:readFromFile3D">
        <xsl:text>readFromFile3D(position1_</xsl:text><xsl:value-of select="./*[1]"/><xsl:text>, position2_</xsl:text><xsl:value-of select="./*[1]"/><xsl:text>, position3_</xsl:text><xsl:value-of select="./*[1]"/><xsl:text>, data_</xsl:text><xsl:value-of select="./*[1]"/><xsl:text>, </xsl:text><xsl:apply-templates select="./*[2]"/><xsl:text>, </xsl:text><xsl:apply-templates select="./*[3]"/><xsl:text>, </xsl:text><xsl:apply-templates select="./*[4]"/><xsl:text>)</xsl:text>
    </xsl:template>

	<xsl:template match="m:ci[text() = $timeCoord]">
		<xsl:text>current_time</xsl:text>
	</xsl:template>
   
   <xsl:template match="s:dimensions">
      <xsl:value-of select="$dimensions"/>
   </xsl:template>

   <xsl:template match="s:sign">
      <xsl:text>SIGN(</xsl:text>	
      <xsl:apply-templates select="./*">
         <xsl:with-param name="condition" select="'false'"/>
      </xsl:apply-templates>
      <xsl:text>)</xsl:text>
   </xsl:template>

 	<xsl:template match="s:selectAgent">
		<xsl:param name="indent" select="$indent"/>
 		<xsl:call-template name="selectAgentRec">
			<xsl:with-param name="indent" select="$indent"/>
 			<xsl:with-param name="counter" select="$dimensions"/>
		</xsl:call-template>
 	</xsl:template> 
	
	
	<xsl:template name="selectAgentRec">
		<xsl:param name="counter"/>  
		<xsl:variable name="next" select="$counter - 1" />  
		<xsl:if test="$next >= 0" >
			<xsl:call-template name="selectAgentRec">  
				<xsl:with-param name="counter" select="$next" />
			</xsl:call-template>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>index</xsl:text><xsl:value-of select="$next"/><xsl:text> = index</xsl:text><xsl:value-of select="$coords/*[$next + 1]"/><xsl:text>Of(</xsl:text>
			<xsl:apply-templates select="./*[1]">
				<xsl:with-param name="indent" select="0"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
			<xsl:text>);
</xsl:text>
		</xsl:if>
	</xsl:template>

	<xsl:template match="s:iterateOverInteractions">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="cellInterLimits">  
			<xsl:with-param name="counter" select="$dimensions" />  
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:call-template name="cellInterloop">  
			<xsl:with-param name="counter" select="$dimensions" />  
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>  
	</xsl:template>

	<xsl:template name="cellInterLimits">  
		<xsl:param name="counter"/>  
		<xsl:param name="indent" select="$indent"/>
		<!-- do whatever you want for this iteration of the loop -->  
		<xsl:variable name="next" select="$counter - 1" />  
		<xsl:if test="$next >= 0" >
			<xsl:call-template name="cellInterLimits">  
				<xsl:with-param name="counter" select="$next" />  
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>int min</xsl:text><xsl:value-of select="$next"/><xsl:text>_index = MAX(0, index</xsl:text><xsl:value-of select="$next"/><xsl:text> - 1);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>int max</xsl:text><xsl:value-of select="$next"/><xsl:text>_index = MIN(</xsl:text><xsl:value-of select="$coords/*[$next + 1]"/><xsl:text>last - 1, index</xsl:text><xsl:value-of select="$next"/><xsl:text> + 1);
</xsl:text>
		</xsl:if>
	</xsl:template> 

	<xsl:template name="cellInterloop">  
		<xsl:param name="counter"/>  
		<xsl:param name="indent" select="$indent"/>
		<!-- do whatever you want for this iteration of the loop -->  
		<xsl:variable name="next" select="$counter - 1" />  
		<xsl:if test="$next >= 0" >
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>for(int index</xsl:text><xsl:value-of select="$next"/><xsl:text>_n = min</xsl:text><xsl:value-of select="$next"/><xsl:text>_index; index</xsl:text><xsl:value-of select="$next"/><xsl:text>_n &lt;= max</xsl:text><xsl:value-of select="$next"/><xsl:text>_index; index</xsl:text><xsl:value-of select="$next"/><xsl:text>_n++) {
</xsl:text>
			<xsl:if test="$next = 0" >
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent + 1"/>
				</xsl:call-template>
				<xsl:text>if (</xsl:text>
				<xsl:call-template name="conditionCellInterLoop">
					<xsl:with-param name="dimesions" select="$dimensions"/>
					<xsl:with-param name="indent" select="$indent + 1"/>
				</xsl:call-template>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent + 1"/>
				</xsl:call-template>
				<xsl:text>) {
</xsl:text>
				<xsl:apply-templates select="./*">
					<xsl:with-param name="indent" select="$indent + 2"/>
					<xsl:with-param name="condition" select="'false'"/>
				</xsl:apply-templates>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent + 1"/>
				</xsl:call-template>
				<xsl:text>}
</xsl:text>
			</xsl:if>
			<xsl:call-template name="cellInterloop">  
				<xsl:with-param name="counter" select="$next" />  
				<xsl:with-param name="indent" select="$indent + 1"/>
			</xsl:call-template>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
		</xsl:if>
	</xsl:template> 

	<xsl:template name="conditionCellInterLoop">
		<xsl:param name="dimesions" select="$dimensions"/>
		<xsl:param name="indent" select="$indent"/>
		<xsl:text>(</xsl:text>
		<xsl:call-template name="differentCell">
			<xsl:with-param name="counter" select="$dimensions"/>
		</xsl:call-template>
		<xsl:text>)
</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>//Do not use corners. Remove the following line if you want to use corners for the calculation
</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>&amp;&amp; !(</xsl:text>
		<xsl:call-template name="noCornersCondition">
			<xsl:with-param name="dimesions" select="$dimesions"></xsl:with-param>
		</xsl:call-template>
		<xsl:text>)
</xsl:text>
	</xsl:template>

	<xsl:template name="differentCell">  
		<xsl:param name="counter"/>
		<!-- do whatever you want for this iteration of the loop -->  
		<xsl:variable name="next" select="$counter - 1" />  
		<xsl:if test="$next >= 0" >
			<xsl:call-template name="differentCell">  
				<xsl:with-param name="counter" select="$next" />  
				<xsl:with-param name="indent" select="$indent + 1"/>
			</xsl:call-template>
			<xsl:if test="$next > 0" >
				<xsl:text> || </xsl:text>
			</xsl:if>
			<xsl:text>(index</xsl:text><xsl:value-of select="$next"/><xsl:text>_n != index</xsl:text><xsl:value-of select="$next"/><xsl:text>)</xsl:text>
		</xsl:if>
	</xsl:template> 

	<xsl:template name="noCornersCondition">  
		<xsl:param name="dimesions"/>
		<xsl:if test="$dimesions = 2" >
			<xsl:text>((index0_n == index0 + 1 &amp;&amp; index1_n == index1 + 1) || (index0_n == index0 + 1 &amp;&amp; index1_n == index1 - 1) || (index0_n == index0 - 1 &amp;&amp; index1_n == index1 + 1) || (index0_n == index0 - 1 &amp;&amp; index1_n == index1 - 1))</xsl:text>
		</xsl:if>
		<xsl:if test="$dimesions = 3" >
			<xsl:text>((index0_n == index0 + 1 &amp;&amp; index1_n == index1 + 1 &amp;&amp; index2_n == index2 + 1) || (index0_n == index0 + 1 &amp;&amp; index1_n == index1 + 1 &amp;&amp; index2_n == index2 - 1) || (index0_n == index0 + 1 &amp;&amp; index1_n == index1 + 1 &amp;&amp; index2_n == index2) || (index0_n == index0 + 1 &amp;&amp; index1_n == index1 - 1 &amp;&amp; index2_n == index2 + 1) || (index0_n == index0 + 1 &amp;&amp; index1_n == index1 - 1 &amp;&amp; index2_n == index2 - 1) || (index0_n == index0 + 1 &amp;&amp; index1_n == index1 - 1 &amp;&amp; index2_n == index2) || (index0_n == index0 + 1 &amp;&amp; index1_n == index1 &amp;&amp; index2_n == index2 + 1) || (index0_n == index0 + 1 &amp;&amp; index1_n == index1 &amp;&amp; index2_n == index2 - 1) || (index0_n == index0 - 1 &amp;&amp; index1_n == index1 + 1 &amp;&amp; index2_n == index2 + 1) || (index0_n == index0 - 1 &amp;&amp; index1_n == index1 + 1 &amp;&amp; index2_n == index2 - 1) || (index0_n == index0 - 1 &amp;&amp; index1_n == index1 + 1 &amp;&amp; index2_n == index2) || (index0_n == index0 - 1 &amp;&amp; index1_n == index1 - 1 &amp;&amp; index2_n == index2 + 1) || (index0_n == index0 - 1 &amp;&amp; index1_n == index1 - 1 &amp;&amp; index2_n == index2 - 1) || (index0_n == index0 - 1 &amp;&amp; index1_n == index1 - 1 &amp;&amp; index2_n == index2) || (index0_n == index0 - 1 &amp;&amp; index1_n == index1 &amp;&amp; index2_n == index2 + 1) || (index0_n == index0 - 1 &amp;&amp; index1_n == index1 &amp;&amp; index2_n == index2 - 1) || (index0_n == index0 &amp;&amp; index1_n == index1 + 1 &amp;&amp; index2_n == index2 + 1) || (index0_n == index0 &amp;&amp; index1_n == index1 + 1 &amp;&amp; index2_n == index2 - 1) || (index0_n == index0 &amp;&amp; index1_n == index1 - 1 &amp;&amp; index2_n == index2 + 1) || (index0_n == index0 &amp;&amp; index1_n == index1 - 1 &amp;&amp; index2_n == index2 - 1))</xsl:text>
		</xsl:if>
	</xsl:template> 


	<xsl:template match="s:iterateOverAgents">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="cellloop">  
			<xsl:with-param name="counter" select="$dimensions" />  
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>  
	</xsl:template>
	
	<xsl:template name="cellloop">  
		<xsl:param name="counter"/>  
		<xsl:param name="indent" select="$indent">0</xsl:param>
		<xsl:variable name="next" select="$counter - 1" />  
		<xsl:if test="$next >= 0" >
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>for(int index</xsl:text><xsl:value-of select="$next"/><xsl:text> = 1; index</xsl:text><xsl:value-of select="$next"/><xsl:text> &lt; </xsl:text><xsl:value-of select="$coords/*[$next + 1]"/><xsl:text>last - 1; index</xsl:text><xsl:value-of select="$next"/><xsl:text>++) {
</xsl:text>
			<xsl:if test="$next = 0" >
				<xsl:apply-templates select="./*">
					<xsl:with-param name="indent" select="$indent + 1"/>
					<xsl:with-param name="condition" select="'false'"/>
				</xsl:apply-templates>
			</xsl:if>
			<xsl:call-template name="cellloop">  
				<xsl:with-param name="counter" select="$next" />  
				<xsl:with-param name="indent" select="$indent + 1"/>
			</xsl:call-template>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
		</xsl:if>
	</xsl:template>  
	
	<xsl:template match="s:randomNumber">
		<xsl:if test="./@typeAtt = 'int'">
			<xsl:text>int(gsl_rng_uniform(r_var) * (</xsl:text>
			<xsl:value-of select="./@rangeMaxAtt"/>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> - </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
			<xsl:text> + 1)</xsl:text>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> + </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
			<xsl:text>)</xsl:text>
		</xsl:if>
		<xsl:if test="./@typeAtt = 'real'">
			<xsl:text>gsl_rng_uniform(r_var) * (</xsl:text>
			<xsl:value-of select="./@rangeMaxAtt"/>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> - </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
			<xsl:text>)</xsl:text>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> + </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="./@typeAtt = 'uniform'">
			<xsl:text>gsl_rng_uniform(r_var)</xsl:text>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="s:iterationNumber">
		<xsl:text>simPlat_iteration</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:currentTime">
		<xsl:text>simPlat_time</xsl:text>
	</xsl:template>

	<xsl:template match="s:currentAgent">
		<xsl:call-template name="createIndex">
			<xsl:with-param name="counter" select="$dimensions" />  
		</xsl:call-template>
	</xsl:template>
		
	<xsl:template name="createIndex">
		<xsl:param name="counter"/>  
		<xsl:variable name="next" select="$counter - 1" />  
		<xsl:if test="$next > 0" >
			<xsl:call-template name="createIndex">  
				<xsl:with-param name="counter" select="$next" />
			</xsl:call-template>
			<xsl:text>, index</xsl:text><xsl:value-of select="$next"/>
		</xsl:if>
		<xsl:if test="$next = 0" >
			<xsl:text>index</xsl:text><xsl:value-of select="$next"/>
		</xsl:if>
	</xsl:template>
		
		
	<xsl:template match="s:neighbourAgent">
		<xsl:call-template name="createIndex_n">
			<xsl:with-param name="counter" select="$dimensions" />  
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="createIndex_n">
		<xsl:param name="counter"/>  
		<xsl:variable name="next" select="$counter - 1" />  
		<xsl:if test="$next > 0" >
			<xsl:call-template name="createIndex_n">  
				<xsl:with-param name="counter" select="$next" />
			</xsl:call-template>
			<xsl:text>, index</xsl:text><xsl:value-of select="$next"/><xsl:text>_n</xsl:text>
		</xsl:if>
		<xsl:if test="$next = 0" >
			<xsl:text>index</xsl:text><xsl:value-of select="$next"/><xsl:text>_n</xsl:text>
		</xsl:if>
	</xsl:template>
		
	<xsl:template match="s:globalNumberOfAgents">
		<xsl:call-template name="numberOfAgentsRec">
			<xsl:with-param name="counter" select="$dimensions" />  
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="numberOfAgentsRec">
		<xsl:param name="counter"/>  
		<xsl:variable name="next" select="$counter - 1" />  
		<xsl:if test="$next > 0" >
			<xsl:call-template name="numberOfAgentsRec">  
				<xsl:with-param name="counter" select="$next" />
			</xsl:call-template>
			<xsl:text>*(boxlast(</xsl:text><xsl:value-of select="$next"/><xsl:text>) - boxfirst(</xsl:text><xsl:value-of select="$next"/><xsl:text>))</xsl:text>
		</xsl:if>
		<xsl:if test="$next = 0" >
			<xsl:call-template name="createIndex">  
				<xsl:with-param name="counter" select="$next" />
			</xsl:call-template>
			<xsl:text>(boxlast(</xsl:text><xsl:value-of select="$next"/><xsl:text>) - boxfirst(</xsl:text><xsl:value-of select="$next"/><xsl:text>))</xsl:text>
		</xsl:if>
	</xsl:template>

	<xsl:template match="s:if">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>if (</xsl:text>
		<!-- Conditional block -->
		<xsl:for-each select="*[not(local-name()='then') and not(local-name()='else')]">
			<xsl:if test="position() &gt; 1">
				<xsl:text> &amp;&amp; </xsl:text>
			</xsl:if>
			<xsl:if test="count(*[not(local-name(node())='then')]) &gt; 1">
				<xsl:text>(</xsl:text>
			</xsl:if>
			<xsl:apply-templates select=".">
				<xsl:with-param name="condition" select="'true'"/>
			</xsl:apply-templates>
			<xsl:if test="count(*[not(local-name(node())='then')]) &gt; 1">
				<xsl:text>)</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<!-- then -->
		<xsl:text>) {
</xsl:text>
		<xsl:for-each select="s:then/*">
			<xsl:apply-templates select=".">
			    <xsl:with-param name="indent" select="$indent + 1"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
		</xsl:for-each>
		<!-- else -->
		<xsl:if test="./s:else">
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>} else {
</xsl:text>
			<xsl:for-each select="s:else/*">
				<xsl:apply-templates select=".">
				    <xsl:with-param name="indent" select="$indent + 1"/>
					<xsl:with-param name="condition" select="'false'"/>
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:if>
		<xsl:text>
</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:while">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>while (</xsl:text>
		<!-- Conditional block -->
		<xsl:for-each select="*[not(local-name()='loop')]">
			<xsl:if test="position() &gt; 1">
				<xsl:text> &amp;&amp; </xsl:text>
			</xsl:if>
			<xsl:if test="count(*[not(local-name(node())='then')]) &gt; 1">
				<xsl:text>(</xsl:text>
			</xsl:if>
			<xsl:apply-templates select=".">
				<xsl:with-param name="condition" select="'true'"/>
			</xsl:apply-templates>
			<xsl:if test="count(*[not(local-name(node())='then')]) &gt; 1">
				<xsl:text>)</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<!-- loop -->
		<xsl:text>) {</xsl:text>
		<xsl:for-each select="s:loop/*">
			<xsl:text>
</xsl:text>
			<xsl:apply-templates select=".">
			    <xsl:with-param name="indent" select="$indent + 1"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
		</xsl:for-each>
		<xsl:text>
</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
	</xsl:template>

	<xsl:template match="s:for">
        <xsl:param name="indent" select="$indent"/>
        <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>
        <xsl:text>for (</xsl:text>
        <!-- condition -->
        <xsl:apply-templates select="s:variable/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text> = </xsl:text>
        <xsl:apply-templates select="s:start/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text>; </xsl:text>
        <xsl:apply-templates select="s:variable/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text> &lt; </xsl:text>
        <xsl:apply-templates select="s:end/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text>; </xsl:text>
        <xsl:apply-templates select="s:variable/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text>++</xsl:text>
        <!-- loop -->
        <xsl:text>) {</xsl:text>
		<xsl:for-each select="s:loop/*">
            <xsl:text>
</xsl:text>
            <xsl:apply-templates select=".">
                <xsl:with-param name="indent" select="$indent + 1"/>
                <xsl:with-param name="condition" select="'false'"/>
            </xsl:apply-templates>
        </xsl:for-each>
        <xsl:text>
</xsl:text>
        <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>
        <xsl:text>}
</xsl:text>
    </xsl:template>
	
	<xsl:template match="s:return">
        <xsl:param name="indent" select="$indent"/>
        <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>
        <xsl:text>return </xsl:text>
        <xsl:apply-templates select="./*">
            <xsl:with-param name="indent" select="0"/>
        </xsl:apply-templates>
    </xsl:template>
	
	<xsl:template match="s:checkFinalization">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>if (</xsl:text>
		<xsl:apply-templates select="//mms:finalizationCondition/mms:equation/mms:instructionSet/*">
			<xsl:with-param name="indent" select="0"/><xsl:with-param name="condition" select="'true'"/>
		</xsl:apply-templates>
		<xsl:text>) {</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		<xsl:text>break;</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
	</xsl:template>
	
	<xsl:template name="recIndent">
		<xsl:param name="indent"/>
		<xsl:if test="$indent &gt; 0">
			<xsl:text>&#x9;</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent - 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	

</xsl:stylesheet>
