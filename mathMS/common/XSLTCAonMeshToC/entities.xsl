<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:m="http://www.w3.org/1998/Math/MathML"
                version='2.0'>

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->
    <xsl:template name="replaceEntities">
        <xsl:param name="content"/>
        <xsl:variable name="tmp" select='replace(replace(replace(replace(replace(replace($content, "&apos;", "prime")
            , "\*", "ast")
            , "\+", "pos")
            , "-", "neg")
            , "#", "sharp")
            , "%", "percent")'></xsl:variable>
        <xsl:value-of select=" replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace($tmp, '&#x0025B;', 'epsilon')
            , '&#x000AC;', 'negative')
            , '&#x000B5;', 'mu')
            , '&#x000D7;', 'times')
            , '&#x000E6;', 'ae')
            , '&#x00391;', 'Alpha')
            , '&#x00392;', 'Beta')
            , '&#x00393;', 'Gamma')
            , '&#x00394;', 'delta')
            , '&#x00395;', 'Epsilon')
            , '&#x00396;', 'Zeta')
            , '&#x00397;', 'Eta')
            , '&#x00398;', 'Theta')
            , '&#x00399;', 'Iota')
            , '&#x0039A;', 'Kappa')
            , '&#x0039B;', 'Lambda')
            , '&#x0039C;', 'Mu')
            , '&#x0039D;', 'Nu')
            , '&#x0039E;', 'Xi')
            , '&#x0039F;', 'Omicron')
            , '&#x003A0;', 'Pi')
            , '&#x003A1;', 'Rho')
            , '&#x003A3;', 'Sigma')
            , '&#x003A4;', 'Tau')
            , '&#x003A5;', 'Upsilon')
            , '&#x003A6;', 'Phi')
            , '&#x003A7;', 'Chi')
            , '&#x003A8;', 'Psi')
            , '&#x003A9;', 'Omega')
            , '&#x003B1;', 'alpha')
            , '&#x003B2;', 'beta')
            , '&#x003B3;', 'gamma')
            , '&#x003B4;', 'deltaSmall')
            , '&#x003B5;', 'epsilon')
            , '&#x003B6;', 'zeta')
            , '&#x003B7;', 'eta')
            , '&#x003B8;', 'theta')
            , '&#x003B9;', 'iota')
            , '&#x003BA;', 'kappa')
            , '&#x003BB;', 'lambda')
            , '&#x003BC;', 'mu')
            , '&#x003BD;', 'nu')
            , '&#x003BE;', 'xi')
            , '&#x003BF;', 'o')
            , '&#x003C0;', 'pi')
            , '&#x003C1;', 'rho')
            , '&#x003C2;', 'stigma')
            , '&#x003C3;', 'sigma')
            , '&#x003C4;', 'tau')
            , '&#x003C5;', 'upsilon')
            , '&#x003C6;', 'phi')
            , '&#x003C7;', 'chi')
            , '&#x003C8;', 'psi')
            , '&#x003C9;', 'omega')
            , '&#x003D1;', 'theta')
            , '&#x003D2;', 'upsilon')
            , '&#x003D5;', 'phi')
            , '&#x003D6;', 'pi')
            , '&#x003F0;', 'kappa')
            , '&#x003F1;', 'rho')
            , '&quot;', 'quot')"/>
    </xsl:template>

</xsl:stylesheet>
