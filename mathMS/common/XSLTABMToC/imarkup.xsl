<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:s="urn:simml" xmlns:fn="http://www.w3.org/2005/02/xpath-functions" 
	xmlns:m="http://www.w3.org/1998/Math/MathML" version="1.0"
	xmlns:mms="urn:mathms">

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->

    <xsl:template match="s:readFromFile1D">
        <xsl:text>readFromFile1D(position1_</xsl:text><xsl:value-of select="./*[1]"/><xsl:text>, data_</xsl:text><xsl:value-of select="./*[1]"/><xsl:text>, </xsl:text><xsl:apply-templates select="./*[2]"/><xsl:text>)</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:readFromFile2D">
        <xsl:text>readFromFile2D(position1_</xsl:text><xsl:value-of select="./*[1]"/><xsl:text>, position2_</xsl:text><xsl:value-of select="./*[1]"/><xsl:text>, data_</xsl:text><xsl:value-of select="./*[1]"/><xsl:text>, </xsl:text><xsl:apply-templates select="./*[2]"/><xsl:text>, </xsl:text><xsl:apply-templates select="./*[3]"/><xsl:text>)</xsl:text>
    </xsl:template>
    
    <xsl:template match="s:readFromFile3D">
        <xsl:text>readFromFile3D(position1_</xsl:text><xsl:value-of select="./*[1]"/><xsl:text>, position2_</xsl:text><xsl:value-of select="./*[1]"/><xsl:text>, position3_</xsl:text><xsl:value-of select="./*[1]"/><xsl:text>, data_</xsl:text><xsl:value-of select="./*[1]"/><xsl:text>, </xsl:text><xsl:apply-templates select="./*[2]"/><xsl:text>, </xsl:text><xsl:apply-templates select="./*[3]"/><xsl:text>, </xsl:text><xsl:apply-templates select="./*[4]"/><xsl:text>)</xsl:text>
    </xsl:template>
   
   <xsl:template match="s:dimensions">
      <xsl:value-of select="$dimensions"/>
   </xsl:template>

   <xsl:template match="s:sign">
      <xsl:text>SIGN(</xsl:text>	
      <xsl:apply-templates select="./*">
         <xsl:with-param name="condition" select="'false'"/>
      </xsl:apply-templates>
      <xsl:text>)</xsl:text>
   </xsl:template>

	<xsl:template match="m:ci[m:msup/*[position() = 1 and $coords/*/text() = ./text()]]">
		<xsl:text>particle->position</xsl:text>
		<xsl:apply-templates select="./*">
			<xsl:with-param name="indent" select="0"/>
			<xsl:with-param name="condition" select="'false'"/>
		</xsl:apply-templates>
	</xsl:template>
	
	<xsl:template match="m:ci[text() = $timeCoord]">
		<xsl:text>current_time</xsl:text>
	</xsl:template>

	<xsl:template match="s:iterateOverAgents">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="cellloop">  
			<xsl:with-param name="counter" select="$dimensions" />  
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>  
	</xsl:template>
	
	<xsl:template match="s:randomNumber">
		<xsl:if test="./@typeAtt = 'int'">
			<xsl:text>int(gsl_rng_uniform(r_var) * (</xsl:text>
			<xsl:value-of select="./@rangeMax"/>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> - </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
			<xsl:text> + 1)</xsl:text>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> + </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
			<xsl:text>)</xsl:text>
		</xsl:if>
		<xsl:if test="./@typeAtt = 'real'">
			<xsl:text>gsl_rng_uniform(r_var) * (</xsl:text>
			<xsl:value-of select="./@rangeMaxAtt"/>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> - </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
			<xsl:text>)</xsl:text>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> + </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="./@typeAtt = 'uniform'">
			<xsl:text>gsl_rng_uniform(r_var)</xsl:text>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="cellloop">  
		<xsl:param name="counter"/>  
		<xsl:param name="indent"/>
		<!-- do whatever you want for this iteration of the loop -->  
		<xsl:variable name="next" select="$counter - 1" />  
		<xsl:if test="$next >= 0" >
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>for(int index</xsl:text><xsl:value-of select="$next"/><xsl:text> = boxfirst(</xsl:text><xsl:value-of select="$next"/><xsl:text>); index</xsl:text><xsl:value-of select="$next"/><xsl:text> &lt;= boxlast(</xsl:text><xsl:value-of select="$next"/><xsl:text>); index</xsl:text><xsl:value-of select="$next"/><xsl:text>++) {
</xsl:text>
			<xsl:if test="$next = 0" >
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent + 1"/>
				</xsl:call-template>
				
				<xsl:text>hier::Index idx(</xsl:text>
				<xsl:call-template name="createIndex">
					<xsl:with-param name="counter" select="$dimensions" />  
				</xsl:call-template><xsl:text>);
</xsl:text>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent + 1"/>
				</xsl:call-template>
				<xsl:text>Particles* part = problemVariable->getItem(idx);
</xsl:text>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent + 1"/>
				</xsl:call-template>
				<xsl:text>for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
</xsl:text>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent + 2"/>
				</xsl:call-template>
				<xsl:text>Particle* particle = part->getParticle(pit);
</xsl:text>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent + 2"/>
				</xsl:call-template>
				<xsl:text>if (</xsl:text><xsl:value-of select="$segCondition"/><xsl:text>) {
</xsl:text>
				<xsl:apply-templates select="./*">
					<xsl:with-param name="indent" select="$indent + 3"/>
					<xsl:with-param name="condition" select="'false'"/>
				</xsl:apply-templates>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent + 2"/>
				</xsl:call-template>
				<xsl:text>}
</xsl:text>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent + 1"/>
				</xsl:call-template>
				<xsl:text>}
</xsl:text>
			</xsl:if>
			<xsl:call-template name="cellloop">  
				<xsl:with-param name="counter" select="$next" />  
				<xsl:with-param name="indent" select="$indent + 1"/>
			</xsl:call-template>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
		</xsl:if>

	</xsl:template>  


	<xsl:template name="createIndex">
		<xsl:param name="counter"/>  
		<xsl:variable name="next" select="$counter - 1" />  
		<xsl:if test="$next > 0" >
			<xsl:call-template name="createIndex">  
				<xsl:with-param name="counter" select="$next" />
			</xsl:call-template>
			<xsl:text>, index</xsl:text><xsl:value-of select="$next"/>
		</xsl:if>
		<xsl:if test="$next = 0" >
			<xsl:text>index</xsl:text><xsl:value-of select="$next"/>
		</xsl:if>
	</xsl:template>
	

	<xsl:template match="s:iterateOverInteractions">
		<xsl:param name="indent" select="$indent"/>
		<xsl:param name="dimensions" select="$dimensions"/>
		<!--<xsl:param name="hardCondition" select="$hardCondition"/>-->
		<xsl:if test="$dimensions = 2">
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>int miniIndex = MAX(boxfirst(0), index0 - numberOfCellsRadius_</xsl:text><xsl:value-of select="$coords/*[1]"/><xsl:text>);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>int maxiIndex = MIN(boxlast(0), index0 + numberOfCellsRadius_</xsl:text><xsl:value-of select="$coords/*[1]"/><xsl:text>);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>int maxjIndex = MIN(boxlast(1), index1 + numberOfCellsRadius_</xsl:text><xsl:value-of select="$coords/*[2]"/><xsl:text>);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>for(int index1b = index1; index1b &lt;= maxjIndex; index1b++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+1"/>
			</xsl:call-template>
			<xsl:text>if (index1b == index1)	{index0min = index0;}
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+1"/>
			</xsl:call-template>
			<xsl:text>else								  {index0min = miniIndex;}
</xsl:text>	
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+1"/>
			</xsl:call-template>
			<xsl:text>for(int index0b = index0min; index0b &lt;= maxiIndex; index0b++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>hier::Index idxb(index0b, index1b);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>Particles* partb = problemVariable->getItem(idxb);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>if ((index1b==index1) &amp;&amp; (index0b==index0)) {pitbmin = pit+1;}
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>else								  {pitbmin = 0;}
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>for (int pitb = pitbmin; pitb &lt; partb-&gt;getNumberOfParticles(); pitb++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>Particle* particleb = partb->getParticle(pitb);
</xsl:text>
			<xsl:if test="string-length($hardCondition) > 0">
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent+3"/>
				</xsl:call-template>
				<xsl:text>if (</xsl:text><xsl:value-of select="$hardCondition"/><xsl:text>) {
</xsl:text>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent+4"/>
				</xsl:call-template>
				<xsl:text>//A-B and B-A interactions
</xsl:text>
				<xsl:for-each select="./*[position() &gt; 1]">
					<xsl:apply-templates select=".">
						<xsl:with-param name="indent" select="$indent + 4"/>
						<xsl:with-param name="condition" select="'false'"/>
					</xsl:apply-templates>
					<xsl:text>
</xsl:text>
				</xsl:for-each>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent+3"/>
				</xsl:call-template>
				<xsl:text>}
</xsl:text>
			</xsl:if>
			<xsl:if test="string-length($hardCondition) = 0">
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent+3"/>
				</xsl:call-template>
				<xsl:text>//A-B and B-A interactions
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>double dist_1 = particleb->distance_p(particle);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>if ((dist_1 &lt; </xsl:text>
			<xsl:apply-templates select="./*[position() = 1]/*">
				<xsl:with-param name="indent" select="0"/>
				<xsl:with-param name="condition" select="'true'"/>
				<xsl:with-param name="inversion" select="0"/>
			</xsl:apply-templates>
			<xsl:text>) &amp;&amp; (dist_1 &gt; 0.0)) {
</xsl:text>
			<xsl:for-each select="./*[position() &gt; 1]">
				<xsl:apply-templates select=".">
					<xsl:with-param name="indent" select="$indent+4"/>
					<xsl:with-param name="condition" select="'false'"/>
					<xsl:with-param name="inversion" select="0"/>
				</xsl:apply-templates>
			</xsl:for-each>
			<xsl:for-each select="./*[position() &gt; 1]">
				<xsl:apply-templates select=".">
					<xsl:with-param name="indent" select="$indent+4"/>
					<xsl:with-param name="condition" select="'false'"/>
					<xsl:with-param name="inversion" select="1"/>
				</xsl:apply-templates>
			</xsl:for-each>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
			</xsl:if>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>	
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+1"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
		</xsl:if>
		<xsl:if test="$dimensions = 3">
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>int miniIndex = MAX(boxfirst(0), index0 - numberOfCellsRadius_</xsl:text><xsl:value-of select="$coords/*[1]"/><xsl:text>);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>int maxiIndex = MIN(boxlast(0), index0 + numberOfCellsRadius_</xsl:text><xsl:value-of select="$coords/*[1]"/><xsl:text>);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>int minjIndex = MAX(boxfirst(1), index1 - numberOfCellsRadius_</xsl:text><xsl:value-of select="$coords/*[2]"/><xsl:text>);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>int maxjIndex = MIN(boxlast(1), index1 + numberOfCellsRadius_</xsl:text><xsl:value-of select="$coords/*[2]"/><xsl:text>);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>int maxkIndex = MIN(boxlast(2), index2 + numberOfCellsRadius_</xsl:text><xsl:value-of select="$coords/*[3]"/><xsl:text>);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>for(int index2b = index2; index2b &lt;= maxkIndex; index2b++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+1"/>
			</xsl:call-template>
			<xsl:text>if (index2b == index2)	{index1min = index1;}
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+1"/>
			</xsl:call-template>
			<xsl:text>else					  {index1min = minjIndex;}
</xsl:text>	
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+1"/>
			</xsl:call-template>
			<xsl:text>for(int index1b = index1min; index1b &lt;= maxjIndex; index1b++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>if ((index2b == index2) &amp;&amp; (index1b == index1))	{index0min = index0;}
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>else								  {index0min = miniIndex;}
</xsl:text>	
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>for(int index0b = index0min; index0b &lt;= maxiIndex; index0b++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>hier::Index idxb(index0b, index1b, index2b);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>Particles* partb = problemVariable->getItem(idxb);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>if ( (index2b==index2) &amp;&amp; (index1b==index1) &amp;&amp; (index0b==index0)) {pitbmin = pit+1;}
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>else								  {pitbmin = 0;}
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>for (int pitb = pitbmin; pitb &lt; partb-&gt;getNumberOfParticles(); pitb++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+4"/>
			</xsl:call-template>
			<xsl:text>Particle* particleb = partb->getParticle(pitb);
</xsl:text>
			<xsl:if test="string-length($hardCondition) > 0">
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent+4"/>
				</xsl:call-template>
				<xsl:text>if (</xsl:text><xsl:value-of select="$hardCondition"/><xsl:text>) {
</xsl:text>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent+5"/>
				</xsl:call-template>
				<xsl:text>//A-B and B-A interactions
</xsl:text>
				<xsl:for-each select="./*[position() &gt; 1]">
					<xsl:apply-templates select=".">
						<xsl:with-param name="indent" select="$indent+5"/>
						<xsl:with-param name="condition" select="'false'"/>
					</xsl:apply-templates>
					<xsl:text>
</xsl:text>
				</xsl:for-each>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent+4"/>
				</xsl:call-template>
				<xsl:text>}
</xsl:text>
			</xsl:if>
			<xsl:if test="string-length($hardCondition) = 0">
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent+4"/>
				</xsl:call-template>
				<xsl:text>//A-B and B-A interactions
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>double dist_1 = particleb->distance_p(particle);
</xsl:text>	
			<xsl:for-each select="./*[position() &gt; 1]">
					<xsl:apply-templates select=".">
						<xsl:with-param name="indent" select="$indent+4"/>
						<xsl:with-param name="condition" select="'false'"/>
						<xsl:with-param name="inversion" select="'false'"/>
					</xsl:apply-templates>
				</xsl:for-each>
			<xsl:for-each select="./*[position() &gt; 1]">
				<xsl:apply-templates select=".">
					<xsl:with-param name="indent" select="$indent+4"/>
					<xsl:with-param name="condition" select="'false'"/>
					<xsl:with-param name="inversion" select="'true'"/>
				</xsl:apply-templates>
			</xsl:for-each>
			</xsl:if>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>		
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+1"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="s:iterateOverInteractionsComplete">
		<xsl:param name="indent" select="$indent"/>
		<xsl:param name="dimensions" select="$dimensions"/>
		<!--<xsl:param name="hardCondition" select="$hardCondition"/>-->
		<xsl:if test="$dimensions = 2">
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>for(int index1b = min</xsl:text><xsl:value-of select="$coords/*[2]"/><xsl:text>Index; index1b &lt;= max</xsl:text><xsl:value-of select="$coords/*[2]"/><xsl:text>Index; index1b++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+1"/>
			</xsl:call-template>
			<xsl:text>for(int index0b = min</xsl:text><xsl:value-of select="$coords/*[1]"/><xsl:text>Index; index0b &lt;= max</xsl:text><xsl:value-of select="$coords/*[1]"/><xsl:text>Index; index0b++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>hier::Index idxb(index0b, index1b);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>Particles* partb = problemVariable->getItem(idxb);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>for (int pitb = 0; pitb &lt; partb-&gt;getNumberOfParticles(); pitb++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>Particle* particleb = partb->getParticle(pitb);
</xsl:text>
			<xsl:if test="string-length($hardCondition) > 0">
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent+3"/>
				</xsl:call-template>
				<xsl:text>if (</xsl:text><xsl:value-of select="$hardCondition"/><xsl:text>) {
</xsl:text>
				<xsl:for-each select="./*[position() &gt; 1]">
					<xsl:apply-templates select=".">
						<xsl:with-param name="indent" select="$indent + 4"/>
						<xsl:with-param name="condition" select="'false'"/>
					</xsl:apply-templates>
					<xsl:text>
</xsl:text>
				</xsl:for-each>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent+3"/>
				</xsl:call-template>
				<xsl:text>}
</xsl:text>
			</xsl:if>
			<xsl:if test="string-length($hardCondition) = 0">
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>double dist_1 = particleb->distance_p(particle);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>if ((dist_1 &lt; </xsl:text>
			<xsl:apply-templates select="./*[position() = 1]/*">
				<xsl:with-param name="indent" select="0"/>
				<xsl:with-param name="condition" select="'true'"/>
				<xsl:with-param name="inversion" select="0"/>
			</xsl:apply-templates>
			<xsl:text>) &amp;&amp; (dist_1 &gt; 0.0)) {
</xsl:text>
			<xsl:for-each select="./*[position() &gt; 1]">
				<xsl:apply-templates select=".">
					<xsl:with-param name="indent" select="$indent+4"/>
					<xsl:with-param name="condition" select="'false'"/>
					<xsl:with-param name="inversion" select="0"/>
				</xsl:apply-templates>
			</xsl:for-each>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
			</xsl:if>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>	
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+1"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
		</xsl:if>
		<xsl:if test="$dimensions = 3">
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>for(int index2b = min</xsl:text><xsl:value-of select="$coords/*[3]"/><xsl:text>Index; index2b &lt;= max</xsl:text><xsl:value-of select="$coords/*[3]"/><xsl:text>Index; index2b++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+1"/>
			</xsl:call-template>
			<xsl:text>for(int index1b = min</xsl:text><xsl:value-of select="$coords/*[2]"/><xsl:text>Index; index1b &lt;= max</xsl:text><xsl:value-of select="$coords/*[2]"/><xsl:text>Index; index1b++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>for(int index0b = min</xsl:text><xsl:value-of select="$coords/*[1]"/><xsl:text>Index; index0b &lt;= max</xsl:text><xsl:value-of select="$coords/*[1]"/><xsl:text>Index; index0b++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>hier::Index idxb(index0b, index1b, index2b);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>Particles* partb = problemVariable->getItem(idxb);
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>for (int pitb = 0; pitb &lt; partb-&gt;getNumberOfParticles(); pitb++) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+4"/>
			</xsl:call-template>
			<xsl:text>Particle* particleb = partb->getParticle(pitb);
</xsl:text>
			<xsl:if test="string-length($hardCondition) > 0">
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent+4"/>
				</xsl:call-template>
				<xsl:text>if (</xsl:text><xsl:value-of select="$hardCondition"/><xsl:text>) {
</xsl:text>
				<xsl:for-each select="./*[position() &gt; 1]">
					<xsl:apply-templates select=".">
						<xsl:with-param name="indent" select="$indent+5"/>
						<xsl:with-param name="condition" select="'false'"/>
					</xsl:apply-templates>
					<xsl:text>
</xsl:text>
				</xsl:for-each>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent+4"/>
				</xsl:call-template>
				<xsl:text>}
</xsl:text>
			</xsl:if>
			<xsl:if test="string-length($hardCondition) = 0">
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>double dist_1 = particleb->distance_p(particle);
</xsl:text>	
			<xsl:for-each select="./*[position() &gt; 1]">
					<xsl:apply-templates select=".">
						<xsl:with-param name="indent" select="$indent+4"/>
						<xsl:with-param name="condition" select="'false'"/>
						<xsl:with-param name="inversion" select="'false'"/>
					</xsl:apply-templates>
				</xsl:for-each>
			</xsl:if>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+3"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>		
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+2"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent+1"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="s:randomNumber">
		<xsl:if test="./@typeAtt = 'int'">
			<xsl:text>int(gsl_rng_uniform(r_var) * (</xsl:text>
			<xsl:value-of select="./@rangeMaxAtt"/>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> - </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
			<xsl:text> + 1)</xsl:text>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> + </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
			<xsl:text>)</xsl:text>
		</xsl:if>
		<xsl:if test="./@typeAtt = 'real'">
			<xsl:text>gsl_rng_uniform(r_var) * (</xsl:text>
			<xsl:value-of select="./@rangeMaxAtt"/>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> - </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
			<xsl:text>)</xsl:text>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> + </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="./@typeAtt = 'uniform'">
			<xsl:text>gsl_rng_uniform(r_var)</xsl:text>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="s:iterationNumber">
		<xsl:text>simPlat_iteration</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:currentTime">
		<xsl:text>simPlat_time</xsl:text>
	</xsl:template>

	<xsl:template match="s:influenceRadius">
		<xsl:text>influence_radius</xsl:text>
	</xsl:template>



	<xsl:template match="m:apply[*[2][self::s:currentAgent]]">
		<xsl:param name="inversion" select="0"/>
		<xsl:if test="$inversion = 0">
			<xsl:text>particle-></xsl:text>
		</xsl:if>
		<xsl:if test="$inversion = 1">
			<xsl:text>particleb-></xsl:text>
		</xsl:if>
		<xsl:apply-templates select="*[1]"/>
	</xsl:template>
		
	<xsl:template match="m:apply[*[2][self::s:neighbourAgent]]">
		<xsl:param name="inversion" select="0"/>
		<xsl:if test="$inversion = 0">
			<xsl:text>particleb-></xsl:text>
		</xsl:if>
		<xsl:if test="$inversion = 1">
			<xsl:text>particle-></xsl:text>
		</xsl:if>
		<xsl:apply-templates select="*[1]"/>
	</xsl:template>

	<xsl:template match="s:interiorDomainContext">
		<xsl:param name="indent" select="$indent"/>
		<xsl:param name="dimensions" select="$dimensions"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>if (</xsl:text>
		<xsl:call-template name="interiorDomainLoop">  
			<xsl:with-param name="counter" select="$dimensions" />  
		</xsl:call-template>
		<xsl:text>) {
</xsl:text>
		<xsl:for-each select="./*">
			<xsl:apply-templates select=".">
				<xsl:with-param name="indent" select="$indent + 1"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
		</xsl:for-each>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
	</xsl:template>

	<xsl:template name="interiorDomainLoop">  
		<xsl:param name="counter"/>  
		<!-- do whatever you want for this iteration of the loop -->  
		<xsl:variable name="next" select="$counter - 1" />  
		<xsl:if test="$next > 0" >
			<xsl:call-template name="interiorDomainLoop">  
				<xsl:with-param name="counter" select="$next" />  
			</xsl:call-template>
			<xsl:text> &amp;&amp; index</xsl:text><xsl:value-of select="$next"/><xsl:text> >= boxfirst1(</xsl:text><xsl:value-of select="$next"/><xsl:text>) &amp;&amp; index</xsl:text><xsl:value-of select="$next"/><xsl:text> &lt;= boxlast1(</xsl:text><xsl:value-of select="$next"/><xsl:text>)</xsl:text>
		</xsl:if>
		<xsl:if test="$next = 0" >
			<xsl:text>index</xsl:text><xsl:value-of select="$next"/><xsl:text> >= boxfirst1(</xsl:text><xsl:value-of select="$next"/><xsl:text>) &amp;&amp; index</xsl:text><xsl:value-of select="$next"/><xsl:text> &lt;= boxlast1(</xsl:text><xsl:value-of select="$next"/><xsl:text>)</xsl:text>
		</xsl:if>
	</xsl:template>  

	<xsl:template match="s:deleteAgent">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>part->deleteParticle(*particle);
</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:createAgent">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>particle = part->addParticle(*new Particle(*particle, true));
</xsl:text>
	</xsl:template>



	<xsl:template match="s:moveParticles">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="moveloop">  
			<xsl:with-param name="counter" select="$dimensions" />  
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>  
	</xsl:template>
	
	<xsl:template name="moveloop">  
		<xsl:param name="counter"/>  
		<xsl:param name="indent"/>
		<!-- do whatever you want for this iteration of the loop -->  
		<xsl:variable name="next" select="$counter - 1" />  
		<xsl:if test="$next >= 0" >
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>for(int index</xsl:text><xsl:value-of select="$next"/><xsl:text> = boxfirst(</xsl:text><xsl:value-of select="$next"/><xsl:text>); index</xsl:text><xsl:value-of select="$next"/><xsl:text> &lt;= boxlast(</xsl:text><xsl:value-of select="$next"/><xsl:text>); index</xsl:text><xsl:value-of select="$next"/><xsl:text>++) {
</xsl:text>
			<xsl:if test="$next = 0" >
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent + 1"/>
				</xsl:call-template>
				
				<xsl:text>hier::Index idx(</xsl:text>
				<xsl:call-template name="createIndex">
					<xsl:with-param name="counter" select="$dimensions" />  
				</xsl:call-template><xsl:text>);
</xsl:text>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent + 1"/>
				</xsl:call-template>
				<xsl:text>Particles* part = problemVariable->getItem(idx);
</xsl:text>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent + 1"/>
				</xsl:call-template>
				<xsl:text>for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
</xsl:text>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent + 2"/>
				</xsl:call-template>
				<xsl:text>Particle* particle = part->getParticle(pit);
</xsl:text>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent + 2"/>
				</xsl:call-template>
				<xsl:text>moveParticles(problemVariable, part, particle, </xsl:text>
				<xsl:call-template name="createIndex">
					<xsl:with-param name="counter" select="$dimensions" />  
				</xsl:call-template>
				<xsl:text>, </xsl:text>
				<xsl:for-each select="./*[2]/*">
					<xsl:apply-templates select=".">
						<xsl:with-param name="indent" select="0"/>
						<xsl:with-param name="condition" select="'false'"/>
					</xsl:apply-templates>
					<xsl:text>,</xsl:text>
				</xsl:for-each>
				<xsl:for-each select="./*[1]/*">
					<xsl:apply-templates select=".">
						<xsl:with-param name="indent" select="0"/>
						<xsl:with-param name="condition" select="'false'"/>
					</xsl:apply-templates>
					<xsl:text>,</xsl:text>
				</xsl:for-each>
				<xsl:text>simPlat_dt, simPlat_time, pit);
</xsl:text>
				<xsl:call-template name="recIndent">
					<xsl:with-param name="indent" select="$indent + 1"/>
				</xsl:call-template>
				<xsl:text>}
</xsl:text>
			</xsl:if>
			<xsl:call-template name="moveloop">  
				<xsl:with-param name="counter" select="$next" />  
				<xsl:with-param name="indent" select="$indent + 1"/>
			</xsl:call-template>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>}
</xsl:text>
		</xsl:if>
	</xsl:template>  


	<xsl:template match="s:if">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>if (</xsl:text>
		<!-- Conditional block -->
		<xsl:for-each select="*[not(local-name()='then') and not(local-name()='else')]">
			<xsl:if test="position() &gt; 1">
				<xsl:text> &amp;&amp; </xsl:text>
			</xsl:if>
			<xsl:if test="count(*[not(local-name(node())='then')]) &gt; 1">
				<xsl:text>(</xsl:text>
			</xsl:if>
			<xsl:apply-templates select=".">
				<xsl:with-param name="condition" select="'true'"/>
			</xsl:apply-templates>
			<xsl:if test="count(*[not(local-name(node())='then')]) &gt; 1">
				<xsl:text>)</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<!-- then -->
		<xsl:text>) {</xsl:text>
		<xsl:for-each select="s:then/*">
			<xsl:text>
</xsl:text>
			<xsl:apply-templates select=".">
			    <xsl:with-param name="indent" select="$indent + 1"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
		</xsl:for-each>
		<!-- else -->
		<xsl:if test="./s:else">
			<xsl:text>
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>} else {</xsl:text>
			<xsl:for-each select="s:else/*">
				<xsl:text>
</xsl:text>
				<xsl:apply-templates select=".">
				    <xsl:with-param name="indent" select="$indent + 1"/>
					<xsl:with-param name="condition" select="'false'"/>
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:if>
		<xsl:text>
</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:while">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>while (</xsl:text>
		<!-- Conditional block -->
		<xsl:for-each select="*[not(local-name()='loop')]">
			<xsl:if test="position() &gt; 1">
				<xsl:text> &amp;&amp; </xsl:text>
			</xsl:if>
			<xsl:if test="count(*[not(local-name(node())='then')]) &gt; 1">
				<xsl:text>(</xsl:text>
			</xsl:if>
			<xsl:apply-templates select=".">
				<xsl:with-param name="condition" select="'true'"/>
			</xsl:apply-templates>
			<xsl:if test="count(*[not(local-name(node())='then')]) &gt; 1">
				<xsl:text>)</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<!-- loop -->
		<xsl:text>) {</xsl:text>
		<xsl:for-each select="s:loop/*">
			<xsl:text>
</xsl:text>
			<xsl:apply-templates select=".">
			    <xsl:with-param name="indent" select="$indent + 1"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
		</xsl:for-each>
		<xsl:text>
</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
	</xsl:template>

	<xsl:template match="s:for">
        <xsl:param name="indent" select="$indent"/>
        <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>
        <xsl:text>for (</xsl:text>
        <!-- condition -->
        <xsl:apply-templates select="s:variable/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text> = </xsl:text>
		<xsl:apply-templates select="s:start/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text>; </xsl:text>
		<xsl:apply-templates select="s:variable/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text> &lt; </xsl:text>
		<xsl:apply-templates select="s:end/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text>; </xsl:text>
		<xsl:apply-templates select="s:variable/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text>++</xsl:text>
        <!-- loop -->
        <xsl:text>) {</xsl:text>
		<xsl:for-each select="s:loop/*">
            <xsl:text>
</xsl:text>
            <xsl:apply-templates select=".">
                <xsl:with-param name="indent" select="$indent + 1"/>
                <xsl:with-param name="condition" select="'false'"/>
            </xsl:apply-templates>
        </xsl:for-each>
        <xsl:text>
</xsl:text>
        <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>
        <xsl:text>}
</xsl:text>
    </xsl:template>
	
	<xsl:template match="s:return">
        <xsl:param name="indent" select="$indent"/>
        <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>
        <xsl:text>return </xsl:text>
        <xsl:apply-templates select="./*">
            <xsl:with-param name="indent" select="0"/>
        </xsl:apply-templates>
    </xsl:template>
	
	<xsl:template match="s:checkFinalization">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>if (</xsl:text>
		<xsl:apply-templates select="//mms:finalizationCondition/mms:equation/mms:instructionSet/*">
			<xsl:with-param name="indent" select="0"/><xsl:with-param name="condition" select="'true'"/>
		</xsl:apply-templates>
		<xsl:text>) {</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		<xsl:text>break;</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
	</xsl:template>
	
	<xsl:template name="recIndent">
		<xsl:param name="indent"/>
		<xsl:if test="$indent &gt; 0">
			<xsl:text>&#x9;</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent - 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	

</xsl:stylesheet>
