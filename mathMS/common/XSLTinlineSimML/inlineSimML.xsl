<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:mt="http://www.w3.org/1998/Math/MathML" xmlns:mms="urn:mathms"  xmlns:sml="urn:simml">
        
    <xsl:template match="mt:ci[text() = '&#x03C0;']">
        <mt:pi/>
    </xsl:template>
        
    <xsl:template match="mt:ci[not(descendant::*) and not(text() = '&#x03C0;')]">
        <xsl:variable name="a" select="./text()"></xsl:variable>
        <xsl:if test="count($inline-simml-tags/*[contains($a, @infix)]) > 0">
            <mt:ci>
                <xsl:call-template name="simmlTokenizer">
                    <xsl:with-param name="variable" select="$a"/>
                </xsl:call-template>
            </mt:ci>
        </xsl:if>
        <xsl:if test="count($inline-simml-tags/*[contains($a, @infix)]) = 0">
            <xsl:copy>
                <xsl:apply-templates select="@*|node()"/>
            </xsl:copy>
        </xsl:if>
    </xsl:template>
    
    
    <xsl:template name="simmlTokenizer">
        <xsl:param name="variable"/>
        <xsl:param name="counter" select="1"/>
        <xsl:if test="$counter > count($inline-simml-tags/*)">
            <xsl:value-of select="$variable"></xsl:value-of>
        </xsl:if>
        <xsl:if test="$counter &lt;= count($inline-simml-tags/*)">
            <xsl:variable name="tag" select="$inline-simml-tags/*[$counter]/@tag"/>
            <xsl:variable name="infix" select="$inline-simml-tags/*[$counter]/@infix"/>
            <xsl:variable name="last" select="count(tokenize($variable, concat('\', $infix)))"/>
            <xsl:for-each select="tokenize($variable,  concat('\', $infix))">
                <xsl:variable name="pos" select="position()"/>
                <xsl:call-template name="simmlTokenizer">
                    <xsl:with-param name="variable" select="."/>
                    <xsl:with-param name="counter" select="$counter + 1"/>
                </xsl:call-template>
                <xsl:if test="$pos!=$last">
                    <xsl:element name="{$tag}" namespace="urn:simml"/>
                </xsl:if>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>
    
    <xsl:variable name="inline-simml-tags">
        <simml infix="$x2" tag="sml:secondContSpatialCoordinate"/>
        <simml infix="$f" tag="sml:field"/>
        <simml infix="$x" tag="sml:contSpatialCoordinate"/>
        <simml infix="$ctc" tag="sml:contTimeCoordinate"/>
        <simml infix="$na" tag="sml:neighbourAgent"/>
        <simml infix="$v" tag="sml:eigenVector"/>
        <simml infix="$i2" tag="sml:secondSpatialCoordinate"/>
        <simml infix="$i" tag="sml:spatialCoordinate"/>
        <simml infix="$ti" tag="sml:timeIncrement"/>
        <simml infix="$rr" tag="sml:refinementRatio"/>
        <simml infix="$nt" tag="sml:newTime"/>
        <simml infix="$sn" tag="sml:substepNumber"/>
       <simml infix="$tsn" tag="sml:timeSubstepNumber"/>
        <simml infix="$t" tag="sml:timeCoordinate"/>
       <simml infix="$pp" tag="sml:particlePosition"/>
       <simml infix="$cp" tag="sml:cellPosition"/>
       <simml infix="$pds" tag="sml:particleDeltaSpacing"/>
       <simml infix="$cds" tag="sml:cellDeltaSpacing"/>
       <simml infix="$pvel" tag="sml:particleVelocity"/>
       <simml infix="$psp" tag="sml:particleSeparationProduct"/>
       <simml infix="$kg" tag="sml:kernelGradient"/>
       <simml infix="$k" tag="sml:kernel"/>
       <simml infix="$pd" tag="sml:particleDistance"/>
       <simml infix="$dim" tag="sml:dimensions"/>
    </xsl:variable>
        
    <xsl:template match="vector">
        <xsl:apply-templates/>
    </xsl:template>
    
    <!-- Special structures -->
    
            
    <xsl:template match="mt:ci[matches(.,'\$f_n')]">
        <mt:ci>
            <mt:msup>
                <mt:ci><sml:field/></mt:ci>
                <mt:apply>
                    <mt:plus/>
                    <mt:ci><sml:timeCoordinate/></mt:ci>
                    <mt:cn>1</mt:cn>
                </mt:apply>
            </mt:msup>
        </mt:ci>
    </xsl:template>
    
    <xsl:template match="mt:ci[matches(.,'\$f_p')]">
        <mt:ci>
            <mt:msup>
                <mt:ci><sml:field/></mt:ci>
                <mt:ci><sml:timeCoordinate/></mt:ci>
            </mt:msup>
        </mt:ci>
    </xsl:template>
    
    <xsl:template match="mt:ci[matches(.,'\$f_p(_p)+')]">
        <mt:ci>
            <mt:msup>
                <mt:ci><sml:field/></mt:ci>
                <mt:apply>
                    <mt:minus/>
                    <mt:ci><sml:timeCoordinate/></mt:ci>
                    <mt:cn><xsl:value-of select="count(tokenize(.,'_')) - 2"></xsl:value-of></mt:cn>
                </mt:apply>
            </mt:msup>
        </mt:ci>
    </xsl:template>
   
   <xsl:template match="mt:ci[matches(.,'\$pp_n')]">
      <mt:ci>
         <mt:msup>
            <mt:ci><sml:particlePosition/></mt:ci>
            <mt:apply>
               <mt:plus/>
               <mt:ci><sml:timeCoordinate/></mt:ci>
               <mt:cn>1</mt:cn>
            </mt:apply>
         </mt:msup>
      </mt:ci>
   </xsl:template>
   
   <xsl:template match="mt:ci[matches(.,'\$pp_p')]">
      <mt:ci>
         <mt:msup>
            <mt:ci><sml:particlePosition/></mt:ci>
            <mt:ci><sml:timeCoordinate/></mt:ci>
         </mt:msup>
      </mt:ci>
   </xsl:template>
   
   <xsl:template match="mt:ci[matches(.,'\$pp_p(_p)+')]">
      <mt:ci>
         <mt:msup>
            <mt:ci><sml:particlePosition/></mt:ci>
            <mt:apply>
               <mt:minus/>
               <mt:ci><sml:timeCoordinate/></mt:ci>
               <mt:cn><xsl:value-of select="count(tokenize(.,'_')) - 2"></xsl:value-of></mt:cn>
            </mt:apply>
         </mt:msup>
      </mt:ci>
   </xsl:template>
    
    <xsl:template match="mt:apply[mt:ci = '$flx']">
        <sml:flux>
            <xsl:for-each select="*[not(text() = '$flx')]">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </sml:flux>
    </xsl:template>
    
    <xsl:template match="mt:apply[mt:ci = '$rffq']">
        <sml:readFromFileQuintic>
            <xsl:for-each select="*[not(text() = '$rffq')]">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </sml:readFromFileQuintic>
    </xsl:template>
    
   <xsl:template match="mt:apply[mt:ci = '$rffl1d']">
      <sml:readFromFileLinear1D>
         <xsl:for-each select="*[not(text() = '$rffl1d')]">
            <xsl:apply-templates select="."/>
         </xsl:for-each>
      </sml:readFromFileLinear1D>
   </xsl:template>
   
   <xsl:template match="mt:apply[mt:ci = '$rffl2d']">
      <sml:readFromFileLinear2D>
         <xsl:for-each select="*[not(text() = '$rffl2d')]">
            <xsl:apply-templates select="."/>
         </xsl:for-each>
      </sml:readFromFileLinear2D>
   </xsl:template>
   
   <xsl:template match="mt:apply[mt:ci = '$rffl3d']">
      <sml:readFromFileLinear3D>
         <xsl:for-each select="*[not(text() = '$rffl3d')]">
            <xsl:apply-templates select="."/>
         </xsl:for-each>
      </sml:readFromFileLinear3D>
   </xsl:template>
   
    <xsl:template match="mt:apply[mt:ci = '$rfflwd1d']">
        <sml:readFromFileLinearWithDerivatives1D>
           <xsl:for-each select="*[not(text() = '$rfflwd1d')]">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </sml:readFromFileLinearWithDerivatives1D>
    </xsl:template>

   <xsl:template match="mt:apply[mt:ci = '$rfflwd2d']">
      <sml:readFromFileLinearWithDerivatives2D>
         <xsl:for-each select="*[not(text() = '$rfflwd2d')]">
            <xsl:apply-templates select="."/>
         </xsl:for-each>
      </sml:readFromFileLinearWithDerivatives2D>
   </xsl:template>
   
   <xsl:template match="mt:apply[mt:ci = '$rfflwd3d']">
      <sml:readFromFileLinearWithDerivatives3D>
         <xsl:for-each select="*[not(text() = '$rfflwd3d')]">
            <xsl:apply-templates select="."/>
         </xsl:for-each>
      </sml:readFromFileLinearWithDerivatives3D>
   </xsl:template>

   <xsl:template match="mt:apply[mt:ci = '$fcff1d']">
      <sml:findCoordFromFile1D>
         <xsl:for-each select="*[not(text() = '$fcff1d')]">
            <xsl:apply-templates select="."/>
         </xsl:for-each>
      </sml:findCoordFromFile1D>
   </xsl:template>
    
   <xsl:template match="mt:apply[mt:ci = '$fcff2d']">
      <sml:findCoordFromFile2D>
         <xsl:for-each select="*[not(text() = '$fcff2d')]">
            <xsl:apply-templates select="."/>
         </xsl:for-each>
      </sml:findCoordFromFile2D>
   </xsl:template>
   
   <xsl:template match="mt:apply[mt:ci = '$fcff3d']">
      <sml:findCoordFromFile3D>
         <xsl:for-each select="*[not(text() = '$fcff3d')]">
            <xsl:apply-templates select="."/>
         </xsl:for-each>
      </sml:findCoordFromFile3D>
   </xsl:template>
    
    <xsl:template match="mt:apply[mt:ci = '$fc']">
        <sml:functionCall>
            <xsl:for-each select="*[not(text() = '$fc')]">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </sml:functionCall>
    </xsl:template>
    
   <xsl:template match="mt:apply[mt:ci = '$pv']">
      <sml:particleVolume>
         <xsl:for-each select="*[not(text() = '$pv')]">
            <xsl:apply-templates select="."/>
         </xsl:for-each>
      </sml:particleVolume>
   </xsl:template>
    
    <xsl:template match="mt:apply[mt:ci = '$sv']">
        <sml:sharedVariable>
            <xsl:for-each select="*[not(text() = '$sv')]">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </sml:sharedVariable>
    </xsl:template>
    
    <xsl:template match="mt:apply[mt:ci = '$np']">
        <sml:neighbourParticle>
            <xsl:for-each select="*[not(text() = '$np')]">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </sml:neighbourParticle>
    </xsl:template>
   
   <xsl:template match="mt:apply[mt:ci = '$sign']">
      <sml:sign>
         <xsl:for-each select="*[not(text() = '$sign')]">
            <xsl:apply-templates select="."/>
         </xsl:for-each>
      </sml:sign>
   </xsl:template>
        
    <xsl:template match="mt:ci[matches(.,'\$mindv_[a-zA-Z]+')]">
        <sml:minDomainValue><xsl:value-of select="substring-after(., '_')"/></sml:minDomainValue>
    </xsl:template>
        
    <xsl:template match="mt:ci[matches(.,'\$maxdv_[a-zA-Z]+')]">
       <sml:maxDomainValue><xsl:value-of select="substring-after(., '_')"/></sml:maxDomainValue>
    </xsl:template>
        
    <xsl:template match="not">
        <mt:not/>
    </xsl:template>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
   
</xsl:stylesheet>
