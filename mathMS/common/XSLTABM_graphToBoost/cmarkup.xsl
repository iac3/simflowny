<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:m="http://www.w3.org/1998/Math/MathML" version="1.0">

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->

    <!-- 4.4.1.1 cn -->
    <xsl:template match="m:cn">
        <xsl:value-of select="."/>
    </xsl:template>

    <!-- 4.4.1.1 ci 4.4.1.2 csymbol -->
    <xsl:template match="m:ci | m:csymbol">
        <xsl:apply-templates/>
    </xsl:template>

    <!-- 4.4.2.1 apply 4.4.2.2 reln -->
    <xsl:template match="m:apply | m:reln">
        <xsl:param name="condition" select="'false'"/>
        <xsl:apply-templates select="*[1]">
            <xsl:with-param name="condition" select="$condition"/>
        </xsl:apply-templates>
        <xsl:text>[</xsl:text>
        <xsl:for-each select="*[position()>1]">
            <xsl:apply-templates select=".">
                <xsl:with-param name="condition" select="$condition"/>
            </xsl:apply-templates>
            <xsl:if test="not(position()=last())">
                <xsl:text>, </xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:text>]</xsl:text>
    </xsl:template>

    <!-- 4.4.2.3 fn -->
    <xsl:template match="m:fn[m:apply[1]]">
        <xsl:param name="condition" select="'false'"/>
        <!-- for m:fn using default rule -->
        <xsl:text>(</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>)</xsl:text>
    </xsl:template>


    <!-- 4.4.2.5 inverse -->
    <xsl:template match="m:apply[*[1][self::m:inverse] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:param name="condition" select="'false'"/>
        <xsl:text>pow(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>, -1)</xsl:text>
    </xsl:template>

    <!-- 4.4.2.6 sep 4.4.2.7 condition -->
    <xsl:template match="m:sep | m:condition">
        <xsl:param name="condition" select="'false'"/>
        <xsl:apply-templates/>
    </xsl:template>

    <!-- 4.4.3.3 divide -->
    <xsl:template match="m:apply[*[1][self::m:divide] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 3">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="4"/>
        </xsl:apply-templates>
        <xsl:text> / </xsl:text>
        <xsl:apply-templates select="*[3]">
            <xsl:with-param name="p" select="4"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 3">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.3.4 max -->
    <xsl:template match="m:apply[*[1][self::m:max] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:text>MAX(</xsl:text>
        <xsl:apply-templates select="*[position() = 2]"/>
        <xsl:text>, </xsl:text>
        <xsl:for-each select="*[position() &gt; 2]">
            <xsl:if test="position() !=last()">
                <xsl:text>MAX(</xsl:text>
            </xsl:if>
            <xsl:apply-templates select="."/>
            <xsl:if test="position() !=last()">
                <xsl:text>, </xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:for-each select="*[position() &gt; 2]">
            <xsl:text>)</xsl:text>
        </xsl:for-each>
    </xsl:template>

    <!-- 4.4.3.4 min -->
    <xsl:template match="m:apply[*[1][self::m:min] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:text>MIN(</xsl:text>
        <xsl:apply-templates select="*[position() = 2]"/>
        <xsl:text>, </xsl:text>
        <xsl:for-each select="*[position() &gt; 2]">
            <xsl:if test="position() !=last()">
                <xsl:text>MIN(</xsl:text>
            </xsl:if>
            <xsl:apply-templates select="."/>
            <xsl:if test="position() !=last()">
                <xsl:text>, </xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:for-each select="*[position() &gt; 2]">
            <xsl:text>)</xsl:text>
        </xsl:for-each>
    </xsl:template>

    <!-- 4.4.3.5  minus-->
    <xsl:template match="m:apply[*[1][self::m:minus] and count(*)=2 and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 0">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:text>-</xsl:text>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="2"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 0">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="m:apply[*[1][self::m:minus] and count(*)&gt;2 and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 1">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="2"/>
        </xsl:apply-templates>
        <xsl:text> - </xsl:text>
        <xsl:apply-templates select="*[3]">
            <xsl:with-param name="p" select="2"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 1">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.3.6  plus-->
    <xsl:template match="m:apply[*[1][self::m:plus] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 0">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="1"/>
        </xsl:apply-templates>
        <xsl:for-each select="*[position()&gt;2]">
            <xsl:text> + </xsl:text>
            <xsl:apply-templates select=".">
                <xsl:with-param name="p" select="1"/>
            </xsl:apply-templates>
        </xsl:for-each>
        <xsl:if test="$p &gt; 0">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="m:apply[ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup]">
        <xsl:apply-templates select="*[2]"/>
        <xsl:value-of select="local-name(*[1])"/>
        <xsl:apply-templates select="*[3]"/>
    </xsl:template>

    <!-- 4.4.3.7 power -->
    <xsl:template match="m:apply[*[1][self::m:power] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 2">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:if test="round(number(*[3])) and count(*[3]/*) = 0 and ceiling(number(*[3])) = floor(number(*[3]))">
            <xsl:apply-templates select="*[2]">
                <xsl:with-param name="p" select="5"/>
            </xsl:apply-templates>
            <xsl:call-template name="recPow">
                <xsl:with-param name="iter" select="round(number(*[3])) - 1"/>
                <xsl:with-param name="oper" select="*[2]"/>
            </xsl:call-template>
        </xsl:if>
        <xsl:if test="not(round(number(*[3]))) or count(*[3]/*) &gt; 0 or not(ceiling(number(*[3])) = floor(number(*[3])))">
            <xsl:text>pow(</xsl:text>
            <xsl:apply-templates select="*[2]">
                <xsl:with-param name="p" select="5"/>
            </xsl:apply-templates>
            <xsl:text>, </xsl:text>
            <xsl:apply-templates select="*[3]">
                <xsl:with-param name="p" select="5"/>
            </xsl:apply-templates>
            <xsl:text>)</xsl:text>
        </xsl:if>
        <xsl:if test="$p &gt; 2">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- Recursive power. The power operand has low performance and should be omitted when possible -->
    <xsl:template name="recPow">
        <xsl:param name="iter"/>
        <xsl:param name="oper"/>
        <xsl:if test="$iter &gt; 0">
            <xsl:text>*</xsl:text>
            <xsl:apply-templates select="$oper">
                <xsl:with-param name="p" select="5"/>
            </xsl:apply-templates>
            <xsl:call-template name="recPow">
                <xsl:with-param name="iter" select="$iter - 1"/>
                <xsl:with-param name="oper" select="$oper"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.3.8 remainder -->
    <xsl:template match="m:apply[*[1][self::m:rem]]">
        <xsl:text>(int(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>) % int(</xsl:text>
        <xsl:apply-templates select="*[3]"/>
        <xsl:text>))</xsl:text>
    </xsl:template>
    
    
    <!-- integer -->
    <xsl:template match="m:apply[*[1][self::m:int]]">
        <xsl:text>int(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <!-- 4.4.3.9  times-->
    <xsl:template match="m:apply[*[1][self::m:times] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]" name="times">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 3">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="3"/>
        </xsl:apply-templates>
        <xsl:for-each select="*[position()&gt;2]">
            <xsl:text> * </xsl:text>
            <xsl:apply-templates select=".">
                <xsl:with-param name="p" select="3"/>
            </xsl:apply-templates>
        </xsl:for-each>
        <xsl:if test="$p &gt; 3">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.3.10 root (only square root) -->
    <xsl:template match="m:apply[*[1][self::m:root] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:text>sqrt</xsl:text>
        <xsl:text>(</xsl:text>
        <xsl:apply-templates select="*[position()&gt;1 and not(self::m:degree)]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <!-- 4.4.3.12 and -->
    <xsl:template match="m:apply[*[1][self::m:and]]">
        <xsl:param name="condition" select="'false'"/>
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 5">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="6"/>
            <xsl:with-param name="condition" select="$condition"/>
        </xsl:apply-templates>
       <xsl:for-each select="*[position()&gt;2]">
          <xsl:text> &amp;&amp; </xsl:text>
          <xsl:apply-templates select=".">
             <xsl:with-param name="p" select="6"/>
             <xsl:with-param name="condition" select="$condition"/>
          </xsl:apply-templates>
       </xsl:for-each>
        <xsl:if test="$p &gt; 5">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.3.13 or -->
    <xsl:template match="m:apply[*[1][self::m:or]]">
        <xsl:param name="condition" select="'false'"/>
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 5">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="6"/>
            <xsl:with-param name="condition" select="$condition"/>
        </xsl:apply-templates>
       <xsl:for-each select="*[position()&gt;2]">
        <xsl:text> || </xsl:text>
          <xsl:apply-templates select=".">
             <xsl:with-param name="p" select="6"/>
             <xsl:with-param name="condition" select="$condition"/>
          </xsl:apply-templates>
       </xsl:for-each>
        <xsl:if test="$p &gt; 5">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>
    
    <!-- 4.4.3.14 xor -->
    <xsl:template match="m:apply[*[1][self::m:xor]]">
        <xsl:param name="condition" select="'false'"/>
        <xsl:text>((</xsl:text>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="condition" select="$condition"/>
        </xsl:apply-templates>
        <xsl:text> || </xsl:text>
        <xsl:apply-templates select="*[3]">
            <xsl:with-param name="condition" select="$condition"/>
        </xsl:apply-templates>
        <xsl:text>) &amp;&amp; !(</xsl:text>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="condition" select="$condition"/>
        </xsl:apply-templates>
        <xsl:text> &amp;&amp; </xsl:text>
        <xsl:apply-templates select="*[3]">
            <xsl:with-param name="condition" select="$condition"/>
        </xsl:apply-templates>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <!-- 4.4.3.15 not -->
    <xsl:template match="m:apply[*[1][self::m:not]]">
        <xsl:param name="condition" select="'false'"/>
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 5">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:text> !</xsl:text>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="6"/>
            <xsl:with-param name="condition" select="$condition"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 5">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.3.19 abs -->
    <xsl:template match="m:apply[*[1][self::m:abs] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:text>fabs(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <!-- 4.4.3.25 floor -->
    <xsl:template match="m:apply[*[1][self::m:floor] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:text>floor(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <!-- 4.4.3.25 ceiling -->
    <xsl:template match="m:apply[*[1][self::m:ceiling] and not(ancestor::m:msubsup or ancestor::m:msub or ancestor::m:msup)]">
        <xsl:text>ceil(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <!-- 4.4.4.1 eq -->
    <xsl:template match="m:apply[*[1][self::m:eq]] | m:reln[*[1][self::m:eq]]">
        <xsl:param name="condition" select="'false'"/>
        <xsl:param name="p" select="0"/>
        <xsl:choose>
            <xsl:when test="$condition = 'true'">
                <xsl:if test="$p &gt; 5">
                    <xsl:text>(</xsl:text>
                </xsl:if>
                <xsl:apply-templates select="*[2]">
                    <xsl:with-param name="p" select="6"/>
                </xsl:apply-templates>
                <xsl:text> == </xsl:text>
                <xsl:apply-templates select="*[3]">
                    <xsl:with-param name="p" select="6"/>
                </xsl:apply-templates>
                <xsl:if test="$p &gt; 5">
                    <xsl:text>)</xsl:text>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="*[2]"/>
                <xsl:text> = </xsl:text>
                <xsl:apply-templates select="*[3]"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- 4.4.4.1 eq -->
    <xsl:template match="m:apply[*[1][self::m:tendsto]] | m:reln[*[1][self::m:tendsto]]">
        <xsl:param name="condition" select="'false'"/>
        <xsl:param name="p" select="0"/>
                <xsl:apply-templates select="*[2]"/>
                <xsl:text>-></xsl:text>
                <xsl:apply-templates select="*[3]"/>
    </xsl:template>

    <!-- 4.4.4.2 neq -->
    <xsl:template match="m:apply[*[1][self::m:neq]] | m:reln[*[1][self::m:neq]]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 5">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="6"/>
        </xsl:apply-templates>
        <xsl:text> != </xsl:text>
        <xsl:apply-templates select="*[3]">
            <xsl:with-param name="p" select="6"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 5">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.4.3 gt -->
    <xsl:template match="m:apply[*[1][self::m:gt]] | m:reln[*[1][self::m:gt]]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 5">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="6"/>
        </xsl:apply-templates>
        <xsl:text> &gt; </xsl:text>
        <xsl:apply-templates select="*[3]">
            <xsl:with-param name="p" select="6"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 5">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.4.4 lt -->
    <xsl:template match="m:apply[*[1][self::m:lt]] | m:reln[*[1][self::m:lt]]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 5">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="6"/>
        </xsl:apply-templates>
        <xsl:text> &lt; </xsl:text>
        <xsl:apply-templates select="*[3]">
            <xsl:with-param name="p" select="6"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 5">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.4.5 geq -->
    <xsl:template match="m:apply[*[1][self::m:geq]] | m:reln[*[1][self::m:geq]]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 5">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="6"/>
        </xsl:apply-templates>
        <xsl:text> &gt;= </xsl:text>
        <xsl:apply-templates select="*[3]">
            <xsl:with-param name="p" select="6"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 5">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.4.6 leq -->
    <xsl:template match="m:apply[*[1][self::m:leq]] | m:reln[*[1][self::m:leq]]">
        <xsl:param name="p" select="0"/>
        <xsl:if test="$p &gt; 5">
            <xsl:text>(</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]">
            <xsl:with-param name="p" select="6"/>
        </xsl:apply-templates>
        <xsl:text> &lt;= </xsl:text>
        <xsl:apply-templates select="*[3]">
            <xsl:with-param name="p" select="6"/>
        </xsl:apply-templates>
        <xsl:if test="$p &gt; 5">
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- 4.4.8.1 common tringonometric functions 4.4.8.3 natural logarithm -->
    <xsl:template
        match="m:apply[*[1][self::m:sin]]">
        <xsl:text>sin(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    <xsl:template
        match="m:apply[*[1][self::m:sinh]]">
        <xsl:text>sinh(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    <xsl:template
        match="m:apply[*[1][self::m:tanh]]">
        <xsl:text>tanh(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    <xsl:template
        match="m:apply[*[1][self::m:arctan]]">
        <xsl:if test="count(*) = 2">
            <xsl:text>atan(</xsl:text>
            <xsl:apply-templates select="*[2]"/>
            <xsl:text>)</xsl:text>
        </xsl:if>
        <xsl:if test="count(*) = 3">
            <xsl:text>atan2(</xsl:text>
            <xsl:apply-templates select="*[2]"/>
            <xsl:text>, </xsl:text>
            <xsl:apply-templates select="*[3]"/>
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>
    <xsl:template
        match="m:apply[*[1][self::m:cos]]">
        <xsl:text>cos(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    <xsl:template
        match="m:apply[*[1][self::m:cosh]]">
        <xsl:text>cosh(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    <xsl:template
        match="m:apply[*[1][self::m:arcsin]]">
        <xsl:text>asin(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    <xsl:template
        match="m:apply[*[1][self::m:tan]]">
        <xsl:text>tan(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
    <xsl:template
        match="m:apply[*[1][self::m:arccos]]">
        <xsl:text>acos(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <xsl:template match="m:apply[*[1][self::m:ln]]">
        <xsl:text>log(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <!-- 4.4.8.2 exp -->
    <xsl:template match="m:apply[*[1][self::m:exp]]">
        <xsl:text>exp(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <!-- 4.4.8.4 log -->
    <xsl:template match="m:apply[*[1][self::m:log]]">
        <xsl:text>log10(</xsl:text>
        <xsl:apply-templates select="*[last()]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <!-- 4.4.12.7 exponentiale -->
    <xsl:template match="m:exponentiale">
        <xsl:text>exp(1)</xsl:text>
    </xsl:template>

    <!-- 4.4.12.8 imaginaryi -->
    <xsl:template match="m:imaginaryi">
        <xsl:text>- 1</xsl:text>
    </xsl:template>

    <!-- 4.4.12.10 true -->
    <xsl:template match="m:true">
        <xsl:text> true </xsl:text>
    </xsl:template>

    <!-- 4.4.12.11 false -->
    <xsl:template match="m:false">
        <xsl:text> false </xsl:text>
    </xsl:template>

    <!-- 4.4.12.13 pi -->
    <xsl:template match="m:pi">
        <xsl:text>3.1415926535897932384626433832795028841971693993751</xsl:text>
    </xsl:template>

    <!-- 4.4.12.14 eulergamma -->
    <xsl:template match="m:eulergamma">
        <xsl:text>0.577215664901532860606512090082402431042159335939</xsl:text>
    </xsl:template>

    <!-- ****************************** -->

    <xsl:template match="m:msup">
        <xsl:apply-templates select="./*[1]"/>
        <xsl:choose>
            <xsl:when test="count(./*[2]/*) &gt; 0">
                <xsl:if test="starts-with(./*[2]/*[2]/text(), '(')">
                    <xsl:if test="local-name(./*[2]/*[1]) = 'minus'">
                        <xsl:call-template name="recPrevious">
                            <xsl:with-param name="iter" select="number(./*[2]/*[3]/text()) + 1"/>
                        </xsl:call-template>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="not(starts-with(./*[2]/*[2]/text(), '('))">
                    <xsl:text>SP</xsl:text>
                    <xsl:apply-templates select="./*[2]"/>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="starts-with(./*[2]/text(), '(')">
                    <xsl:text>_p</xsl:text>
                </xsl:if>
                <xsl:if test="not(starts-with(./*[2]/text(), '('))">
                    <xsl:text>SP</xsl:text>
                    <xsl:apply-templates select="./*[2]"/>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="m:msubsup">
        <xsl:apply-templates select="./*[1]"/>
        <xsl:text>SB</xsl:text>
        <xsl:apply-templates select="./*[2]"/>
        <xsl:text>SP</xsl:text>	
        <xsl:apply-templates select="./*[3]"/>
    </xsl:template>
   
    
    <xsl:template match="m:msub">
        <xsl:apply-templates select="./*[1]"/>
        <xsl:text>SB</xsl:text>
        <xsl:apply-templates select="./*[2]"/>
    </xsl:template>

    <xsl:template name="recPrevious">
        <xsl:param name="iter"/>
        <xsl:if test="$iter &gt; 0">
            <xsl:text>_p</xsl:text>
            <xsl:call-template name="recPrevious">
                <xsl:with-param name="iter" select="$iter - 1"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>
