<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:s="urn:simml" xmlns:fn="http://www.w3.org/2005/02/xpath-functions" 
	xmlns:m="http://www.w3.org/1998/Math/MathML" version="1.0"
	xmlns:mms="urn:mathms">

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html  -->
<!-- ====================================================================== -->

	<xsl:template match="s:selectVertex">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>Vertex n_id = vertex(</xsl:text>
		<xsl:apply-templates select="./*[1]">
			<xsl:with-param name="indent" select="0"/>
			<xsl:with-param name="condition" select="'true'"/>
		</xsl:apply-templates>
		<xsl:text>, g_id);
</xsl:text>	
	</xsl:template>
	
	<xsl:template match="s:selectEdge">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>Edge e_id = edge(</xsl:text>
		<xsl:apply-templates select="./s:sourceVertex/*[1]">
			<xsl:with-param name="indent" select="0"/>
			<xsl:with-param name="condition" select="'true'"/>
		</xsl:apply-templates>
		<xsl:text>, </xsl:text>
		<xsl:apply-templates select="./s:targetVertex/*[1]">
			<xsl:with-param name="indent" select="0"/>
			<xsl:with-param name="condition" select="'true'"/>
		</xsl:apply-templates>
		<xsl:text>, g_id);
</xsl:text>	
	</xsl:template>

	<xsl:template match="s:iterateOverVertices">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>BGL_FORALL_VERTICES(n_id, g_id, Graph) {
</xsl:text>	
		<xsl:apply-templates select="./*">
			<xsl:with-param name="indent" select="$indent + 1"/>
				<xsl:with-param name="condition" select="'false'"/>
		</xsl:apply-templates>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:iterateOverVerticesDelete">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>deleted_vertex = true;
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>while(deleted_vertex) {
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		<xsl:text>deleted_vertex = false;
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		<xsl:text>BGL_FORALL_VERTICES(n_id, g_id, Graph) {
</xsl:text>	
		<xsl:apply-templates select="./*">
			<xsl:with-param name="indent" select="$indent + 2"/>
			<xsl:with-param name="condition" select="'false'"/>
		</xsl:apply-templates>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 2"/>
		</xsl:call-template>
		<xsl:text>if (deleted_vertex) {
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 3"/>
		</xsl:call-template>
		<xsl:text>break;
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 2"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:iterateOverEdges">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:if test=".[not(@*) or @directionAtt='']">
		<xsl:text>BGL_FORALL_EDGES(e_id, g_id, Graph) {
</xsl:text>
		</xsl:if>
		<xsl:if test=".[@directionAtt='out']">
			<xsl:text>BGL_FORALL_OUTEDGES(n_id, e_id, g_id, Graph) {
</xsl:text>
		</xsl:if>
		<xsl:if test=".[@directionAtt='in']">
			<xsl:text>BGL_FORALL_INEDGES(n_id, e_id, g_id, Graph) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent + 1"/>
			</xsl:call-template>
			<xsl:text>remote_key_e remote_e_id(rank, e_id);
</xsl:text>
		</xsl:if>
		<xsl:apply-templates select="./*">
			<xsl:with-param name="indent" select="$indent + 1"/>
			<xsl:with-param name="condition" select="'false'"/>
		</xsl:apply-templates>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
	</xsl:template>
		
	<xsl:template match="s:iterateOverEdgesDelVertex">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:if test=".[not(@*) or @directionAtt='']">
			<xsl:text>BGL_FORALL_EDGES(e_id, g_id, Graph) {
</xsl:text>
		</xsl:if>
		<xsl:if test=".[@directionAtt='out']">
			<xsl:text>BGL_FORALL_OUTEDGES(n_id, e_id, g_id, Graph) {
</xsl:text>
		</xsl:if>
		<xsl:if test=".[@directionAtt='in']">
			<xsl:text>BGL_FORALL_INEDGES(n_id, e_id, g_id, Graph) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent + 1"/>
			</xsl:call-template>
			<xsl:text>remote_key_e remote_e_id(rank, e_id);
</xsl:text>
		</xsl:if>
		<xsl:apply-templates select="./*">
			<xsl:with-param name="indent" select="$indent + 1"/>
			<xsl:with-param name="condition" select="'false'"/>
		</xsl:apply-templates>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		<xsl:text>if (deleted_edge) {
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 2"/>
		</xsl:call-template>
		<xsl:text>break;
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		<xsl:text>if (deleted_vertex) {
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 2"/>
		</xsl:call-template>
		<xsl:text>break;
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
	</xsl:template>
		
	<xsl:template match="s:iterateOverEdgesDelete">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>deleted_edge = true;
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>while(deleted_edge) {
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		<xsl:text>deleted_edge = false;
</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		<xsl:if test=".[not(@*) or @directionAtt='']">
			<xsl:text>BGL_FORALL_EDGES(e_id, g_id, Graph) {
</xsl:text>
		</xsl:if>
		<xsl:if test=".[@directionAtt='out']">
			<xsl:text>BGL_FORALL_OUTEDGES(n_id, e_id, g_id, Graph) {
</xsl:text>
		</xsl:if>
		<xsl:if test=".[@directionAtt='in']">
			<xsl:text>BGL_FORALL_INEDGES(n_id, e_id, g_id, Graph) {
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent + 2"/>
			</xsl:call-template>
			<xsl:text>remote_key_e remote_e_id(rank, e_id);
</xsl:text>
		</xsl:if>
		<xsl:apply-templates select="./*">
			<xsl:with-param name="indent" select="$indent + 2"/>
			<xsl:with-param name="condition" select="'false'"/>
		</xsl:apply-templates>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 2"/>
		</xsl:call-template>
		<xsl:text>if (deleted_edge) {
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 3"/>
		</xsl:call-template>
		<xsl:text>break;
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 2"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 2"/>
		</xsl:call-template>
		<xsl:text>if (deleted_vertex) {
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 3"/>
		</xsl:call-template>
		<xsl:text>break;
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 2"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>	
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
	</xsl:template>

	<xsl:template match="s:randomNumber">
		<xsl:if test="./@typeAtt = 'int'">
			<xsl:text>int(gsl_rng_uniform(r_var) * (</xsl:text>
			<xsl:value-of select="./@rangeMaxAtt"/>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> - </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
			<xsl:text> + 1)</xsl:text>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> + </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
			<xsl:text>)</xsl:text>
		</xsl:if>
		<xsl:if test="./@typeAtt = 'real'">
			<xsl:text>gsl_rng_uniform(r_var) * (</xsl:text>
			<xsl:value-of select="./@rangeMaxAtt"/>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> - </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
			<xsl:text>)</xsl:text>
			<xsl:if test="./@rangeMinAtt">
				<xsl:text> + </xsl:text><xsl:value-of select="./@rangeMinAtt"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="./@typeAtt = 'uniform'">
			<xsl:text>gsl_rng_uniform(r_var)</xsl:text>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="s:iterationNumber">
		<xsl:text>t_it</xsl:text>
	</xsl:template>

	<xsl:template match="s:currentVertex">
		<xsl:text>n_id</xsl:text>
	</xsl:template>
		
	<xsl:template match="s:currentEdge">
		<xsl:text>e_id</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:remoteEdge">
		<xsl:text>put(</xsl:text>
		<xsl:apply-templates select="./*[1]">
			<xsl:with-param name="condition" select="'false'"/>
		</xsl:apply-templates>
		<xsl:text>, remote_e_id ,</xsl:text>
		<xsl:apply-templates select="./*[2]">
			<xsl:with-param name="condition" select="'false'"/>
		</xsl:apply-templates>
		<xsl:text>)</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:edgeTarget">
		<xsl:text>target(</xsl:text>
		<xsl:apply-templates select="./*">
			<xsl:with-param name="condition" select="'false'"/>
		</xsl:apply-templates>
		<xsl:text>, g_id)</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:edgeSource">
		<xsl:text>source(</xsl:text>
		<xsl:apply-templates select="./*">
			<xsl:with-param name="condition" select="'false'"/>
		</xsl:apply-templates>
		<xsl:text>, g_id)</xsl:text>
	</xsl:template>

   <xsl:template match="s:sign">
      <xsl:text>SIGN(</xsl:text>	
      <xsl:apply-templates select="./*">
         <xsl:with-param name="condition" select="'false'"/>
      </xsl:apply-templates>
      <xsl:text>)</xsl:text>
   </xsl:template>

	<xsl:template match="s:globalNumberOfVertices">
		<xsl:text>num_vertices(g_id)</xsl:text>
	</xsl:template>

	<xsl:template match="s:globalNumberOfEdges">
		<xsl:text>num_edges(g_id)</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:localNumberOfEdges">
		<xsl:if test="./@directionAtt = 'in'">
			<xsl:text>in_degree(</xsl:text>
			<xsl:apply-templates select="./*">
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
			<xsl:text>, g_id)</xsl:text>
		</xsl:if>
		<xsl:if test="./@directionAtt = 'out'">
			<xsl:text>out_degree(</xsl:text>
			<xsl:apply-templates select="./*">
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
			<xsl:text>, g_id)</xsl:text>
		</xsl:if>
	</xsl:template>
	

	<xsl:template match="s:deleteEdge">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>remove_edge(</xsl:text>
		<xsl:apply-templates select="./*">
			<xsl:with-param name="condition" select="'false'"/>
		</xsl:apply-templates>
		<xsl:text>, g_id);
</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>deleted_edge = true;
</xsl:text>
	</xsl:template>

	<xsl:template match="s:createEdge">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>add_edge(</xsl:text>
		<xsl:apply-templates select="./s:sourceVertex/*[1]">
			<xsl:with-param name="indent" select="0"/>
			<xsl:with-param name="condition" select="'true'"/>
		</xsl:apply-templates>
		<xsl:text>, </xsl:text>
		<xsl:apply-templates select="./s:targetVertex/*[1]">
			<xsl:with-param name="indent" select="0"/>
			<xsl:with-param name="condition" select="'true'"/>
		</xsl:apply-templates>
		<xsl:text>, g_id);
</xsl:text>
	</xsl:template>

	<xsl:template match="s:deleteVertex">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>remove_vertex(</xsl:text>
		<xsl:apply-templates select="./*">
			<xsl:with-param name="condition" select="'false'"/>
		</xsl:apply-templates>
		<xsl:text>, g_id);
</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>deleted_vertex = true;
</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:createVertex">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>Vertex n_id = add_vertex(g_id);
</xsl:text>
	</xsl:template>



	<xsl:template match="s:if">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>if (</xsl:text>
		<!-- Conditional block -->
		<xsl:for-each select="*[not(local-name()='then') and not(local-name()='else')]">
			<xsl:if test="position() &gt; 1">
				<xsl:text> &amp;&amp; </xsl:text>
			</xsl:if>
			<xsl:if test="count(*[not(local-name(node())='then')]) &gt; 1">
				<xsl:text>(</xsl:text>
			</xsl:if>
			<xsl:apply-templates select=".">
				<xsl:with-param name="condition" select="'true'"/>
			</xsl:apply-templates>
			<xsl:if test="count(*[not(local-name(node())='then')]) &gt; 1">
				<xsl:text>)</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<!-- then -->
		<xsl:text>) {</xsl:text>
		<xsl:for-each select="s:then/*">
			<xsl:text>
</xsl:text>
			<xsl:apply-templates select=".">
			    <xsl:with-param name="indent" select="$indent + 1"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
		</xsl:for-each>
		<!-- else -->
		<xsl:if test="./s:else">
			<xsl:text>
</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:call-template>
			<xsl:text>} else {</xsl:text>
			<xsl:for-each select="s:else/*">
				<xsl:text>
</xsl:text>
				<xsl:apply-templates select=".">
				    <xsl:with-param name="indent" select="$indent + 1"/>
					<xsl:with-param name="condition" select="'false'"/>
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:if>
		<xsl:text>
</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
	</xsl:template>
	
	<xsl:template match="s:while">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>while (</xsl:text>
		<!-- Conditional block -->
		<xsl:for-each select="*[not(local-name()='loop')]">
			<xsl:if test="position() &gt; 1">
				<xsl:text> &amp;&amp; </xsl:text>
			</xsl:if>
			<xsl:if test="count(*[not(local-name(node())='then')]) &gt; 1">
				<xsl:text>(</xsl:text>
			</xsl:if>
			<xsl:apply-templates select=".">
				<xsl:with-param name="condition" select="'true'"/>
			</xsl:apply-templates>
			<xsl:if test="count(*[not(local-name(node())='then')]) &gt; 1">
				<xsl:text>)</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<!-- loop -->
		<xsl:text>) {</xsl:text>
		<xsl:for-each select="s:loop/*">
			<xsl:text>
</xsl:text>
			<xsl:apply-templates select=".">
			    <xsl:with-param name="indent" select="$indent + 1"/>
				<xsl:with-param name="condition" select="'false'"/>
			</xsl:apply-templates>
		</xsl:for-each>
		<xsl:text>
</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
	</xsl:template>

	<xsl:template match="s:for">
        <xsl:param name="indent" select="$indent"/>
        <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>
        <xsl:text>for (</xsl:text>
        <!-- condition -->
        <xsl:apply-templates select="s:variable/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text> = </xsl:text>
        <xsl:apply-templates select="s:start/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text>; </xsl:text>
        <xsl:apply-templates select="s:variable/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text> &lt; </xsl:text>
        <xsl:apply-templates select="s:end/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text>; </xsl:text>
        <xsl:apply-templates select="s:variable/.">
            <xsl:with-param name="condition" select="'true'"/>
        </xsl:apply-templates>
        <xsl:text>++</xsl:text>
        <!-- loop -->
        <xsl:text>) {</xsl:text>
        <xsl:for-each select="loop/*">
            <xsl:text>
</xsl:text>
            <xsl:apply-templates select=".">
                <xsl:with-param name="indent" select="$indent + 1"/>
                <xsl:with-param name="condition" select="'false'"/>
            </xsl:apply-templates>
        </xsl:for-each>
        <xsl:text>
</xsl:text>
        <xsl:call-template name="recIndent">
            <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>
        <xsl:text>}
</xsl:text>
    </xsl:template>
	
	<xsl:template match="s:checkFinalization">
		<xsl:param name="indent" select="$indent"/>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>if (</xsl:text>
		<xsl:apply-templates select="//mms:finalizationCondition//m:math">
			<xsl:with-param name="indent" select="0"/><xsl:with-param name="condition" select="'true'"/>
		</xsl:apply-templates>
		<xsl:text>) {</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent + 1"/>
		</xsl:call-template>
		<xsl:text>break;</xsl:text>
		<xsl:call-template name="recIndent">
			<xsl:with-param name="indent" select="$indent"/>
		</xsl:call-template>
		<xsl:text>}
</xsl:text>
	</xsl:template>
	
	<xsl:template name="recIndent">
		<xsl:param name="indent"/>
		<xsl:if test="$indent &gt; 0">
			<xsl:text>&#x9;</xsl:text>
			<xsl:call-template name="recIndent">
				<xsl:with-param name="indent" select="$indent - 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	

</xsl:stylesheet>
