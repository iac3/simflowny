#!/usr/bin/env python
import os
from os import listdir
from os.path import isfile, join, exists
from subprocess import call


mypath = "../../wiki"
if exists(mypath):
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

    if not exists('wiki'):
        os.makedirs('wiki')

    for f in onlyfiles:
        if f.endswith('.rst'):
            call(["rst2pdf", mypath + '/' + f, 'wiki/' + f[:f.find('.rst')]])
