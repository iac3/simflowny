\appendix

\section{Definition of the Collective Motion model and problem}

\subsubsection{Model creation}

This model is defined in a 2D spatial domain, characterised by
spatial coordinates $\{x, y\}$. Therefore, after setting the head information, the user sets \emph{Coordinates} element properly, with the mentioned spatial coordinates and a time coordinate $t$.

In contrast with ABM on graphs, which have vertex properties, ABM on spatial domain has agent properties. Those properties travel with the agents if they move within the spatial domain. The Collective Motion example has the properties \emph{speedx}, \emph{speedy}, \emph{num}, \emph{denom}, \emph{random}, \emph{neighbours}, and \emph{theta}.

To continue with variable definition, two parameters have to be explicitly added (as \emph{Parameters} are optional), the agent maximum speed $v\_0$ and the noise intensity $\eta$.

ABM on spatial domain relies on gather and update rules, as ABM on graph do. For the sake of simplicity, only one gather rule is explained in detail for this example, assuming the user can introduce the others with a similar process.
The user creates a new \emph{Gather Rule} for the accumulator property \emph{num}. The \emph{Algorithm} makes use of the instruction \emph{Iterate Over Interactions}. Its \emph{Max Interaction Range} element is determined by the \emph{Influence Radius} (\emph{\$ir} in \emph{SimML}). Inside the loop, the user adds the \emph{Math} expression \emph{$num(\$ca)=num(\$ca)+sin(\theta(\$na))$}, which means that the \emph{num} property from the agent accumulates is updated with the sinus of the neighbour agent's angular direction. A similar gather rule is added to accumulate the cosinus of $\theta$, and another rule to count the number of neighbours for each agent. The figure~\ref{fig:collective-motion-model-1} displays all gather rules.

On the other hand, the update rules (see fig. \ref{fig:collective-motion-model-2}) consist on three property resets, the generation of a random number, application of noise distorsion to sinus and cosinus accumulators, the update of agent orientation, and agent speed updates.

\begin{figure}
  \begin{center}
  \includegraphics[width=\columnwidth]{images/collective-motion-model-1.png}
  \caption{Flocking model general definition and gather rules.}
  \label{fig:collective-motion-model-1}
  \end{center}
\end{figure}

\begin{figure}
  \begin{center}
  \includegraphics[width=\columnwidth]{images/collective-motion-model-2.png}
  \caption{Flocking model update rules and rule execution order.}
  \label{fig:collective-motion-model-2}
  \end{center}
\end{figure}

Finally, the user defines the \emph{Rule Execution Order} to execute the rules in the correct model order. The complete problem is shown in figures~\ref{fig:collective-motion-model-1} and~\ref{fig:collective-motion-model-2}.

\subsubsection{Problem creation}

The next stage is the problem definition.
The common initial definitions of \emph{Head}, \emph{Coordinates}, and \emph{Agent Properties} elements are set. Then, the user adds the \emph{Parameters} element, in which, apart from adding the model parameters, the user adds a new parameter \emph{time\_steps} to control the finalization condition at simulation runtime.

As a second step, the model created in the previous subsection is linked into this problem by using the \emph{Model} element.

The third step for the user is to define the \emph{Mesh} element, representing the domain of the simulation. Two mesh types are possible: \emph{structured}, consisting in a regular lattice (akin to Cellular Automata, where agents are represented as cells in the mesh), and \emph{unstructured}, with agents considered as freely moving particles. Following, the user must specify the \emph{Spatial Domain}. Additionally, for \emph{unstructured} mesh types, there is the possibility to allow motion for the agents. In that case, the optional element \emph{Motion} can be added in the \emph{Mesh} element, in which the user indicates the agent properties representing the speed for every spatial coordinate. In the Collective Motion example, an \emph{unstructured} mesh is selected with a bidimensional domain $[0, 1]^2$. Obviously, in a Collective Motion simulations the agents may move. Consequently, the \emph{Motion} element is added, defining \emph{speedx} and \emph{speedy} as the speed components for $x$ and $y$ respectively.

As explained in ABM on graph problem creation, the \emph{Evolution Step} elements specify the kind of evolution. In the current example, the user chooses \emph{AllAgents}.

The following elements, \emph{Initial Conditions}, \emph{Boundary Conditions}, \emph{Boundary Precedence}, and \emph{Finalization Conditions} are in common with ABM on graph and Generic PDE. For simplicity, their definition is not reproduced again. See figures~\ref{fig:collective-motion-problem-1} and~\ref{fig:collective-motion-problem-2}, where the complete problem is shown, including those last elements.

\begin{figure}
  \begin{center}
  \includegraphics[width=\columnwidth]{images/collective-motion-problem-1.png}
  \caption{Collective motion problem. Basic data and mesh definition.}
  \label{fig:collective-motion-problem-1}
  \end{center}
\end{figure}

\begin{figure}
  \begin{center}
  \includegraphics[width=\columnwidth]{images/collective-motion-problem-2.png}
  \caption{Collective motion problem. Motion and conditions definition.}
  \label{fig:collective-motion-problem-2}
  \end{center}
\end{figure}

Once finished, the user may click on the button \texttt{Validate} to assess the problem integrity.
