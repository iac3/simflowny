\section{New GUI architecture}
\label{sec:newgui}

In Simflowny, problems, models, and discretization schemes are represented as XML files. 
In the former GUI, the edition of these files was done with many ad hoc
non-reusable GUI components, specifically developed for such documents.

As the families of models supported by Simflowny grew, this approach became unsustainable due to the following issues:
\begin{itemize}

\item it did not take advantage of already existing structure embedded in the XML documents, rather creating a new data structure specifically for the GUI. \CBC{Capital letters should be used after each bullet point, like in the Introduction, This happens later in the text many times.}

\item it suffered of a lack of coherence, since there was no guarantee --when the GUI components were developed manually-- that similar components were designed in a similar way. In fact, it is possible, specially when the work is done by several developers, that the GUI components differ substantially even if they correspond to similar information. \CBC{Is it still posible, or it WAS possible? If it's still possible, do we want to highlight this?}

\item it was becoming rather inefficient, with significant development costs due to such new and heterogeneous designs.

\end{itemize}


These drawbacks led us to completely redesign the GUI to convert it into a specialized XML
editor. Each XML document, accompanied by its schema (XSD) provides all the
necessary information to automatically generate a graphical editor for such document on the fly. 


Simflowny's web GUI is composed by two elements: a document manager, which is also the GUI's main page, and a document editor. 

\subsection{Document manager}

The document manager consists of three areas (see figure \ref{fig:docManager}) : i) a toolbar, located at the top, and containing contextual actions for the selected document; ii) a document tree on the left area; and iii) a document list from the selected tree folder on the right area.

\begin{figure*}
  \begin{center}
  \includegraphics[width=\linewidth]{images/documentManager.png}
  \caption{Document manager screen. Notice the tree area on the left. The area on the right shows the available documents in the selected folder. \CBC{Design is very nice. Congratulations!}}
  \label{fig:docManager}
  \end{center}
\end{figure*}

 The specific buttons appearing in the toolbar depend on the document actively selected in the document manager. For instance, with no document selected, the only actions are \texttt{Create new} and \texttt{Import}, as shown in figure \ref{fig:docManager}.

When selecting a document, other buttons appear in the toolbar.
For instance in the case of a document describing an ABM simulation problem, the buttons are those shown in figure \ref{fig:barcode}. The five actions on the left are common to any kind of document; from left to right: \texttt{Create new, Import, Edit, Download, Delete}. The remaining three ones are contextual actions, in this case specific for the ABM simulation problem; from left to right: \texttt{XML to LaTeX, XML to PDF,} and \texttt{Generate Boost Code}. 

\begin{figure}
  \begin{center}
  \includegraphics[scale=1]{images/action-bar-code-generation.png}
  \caption{Action bar on the document manager when a problem is selected. The five actions on the left \texttt{Create new, Import, Edit, Download, Delete} are common to all documents. The remaining three ones \texttt{XML to LaTeX, XML to PDF, Generate Boost Code} are contextual actions, in this case specific to the ABM simulation problem.} 
  \label{fig:barcode}
  \end{center}
\end{figure}

The \texttt{Create new} button is used to create new documents, whose type is selected on the fly in the button submenu. When clicked, a document editor tab opens with a template document of the selected type. The document edition is explained in detail in the following subsection. The document will be stored in the folder selected in the document tree.
The \texttt{Import} button allows uploading documents to the current database. Every document previously exported from Simflowny can be imported.
The \texttt{Edit} button opens the document editor with the selected document loaded.
All the documents in Simflowny can be exported. When using the \texttt{Export} button the selected documents will be downloaded in a zip, which will contain the selected document and any documents it references. Clicking on the \texttt{Delete} button will delete the selected documents.

The \texttt{XML to LaTeX} and \texttt{XML to PDF} actions generate and download, respectively, \LaTeX and PDF versions of the selected document. The last action generates code for an ABM on a graph (see section \ref{sec:gencode}).

The document tree on the left area is a customizable hierarchical structure. Each element in the tree can be seen as folder where to place the documents. It is possible to reorganize the tree. Right-clicking on any folder opens a contextual menu to \texttt{Add} a new folder, or \texttt{Rename} or \texttt{Delete} an existing folder. It is also possible to move a folder (including subfolders and documents) or documents into another by drag and drop.

The third area in the document manager, at the right of the tree is a document list from the selected tree folder. A double-click on any document will open it in the document editor. The documents can be ordered  and filtered using the header columns.

\subsection{Document editor}

The Document Editor is a specialised XML editor. Underneath,
all documents in Simflowny are XML files, which have a tree
structure. The Document Editor presents such tree structure
in the left panel, which is editable, while a presentation
panel is available on the right, offering a more readable
view of the document being edited. See figures in sections \ref{sec:pde} and \ref{sec:abm}.

The functionalities available for editing documents include:

\begin{itemize}
\item those addressed to manage the XML elements, among them: \CBC{Capital letters should be used after each bullet point, like in the Introduction}
	\begin{itemize}
	\item Decorated versions of the XML tags, to improve
	readability. For instance, the XML tag \emph{addPartialDerivatives} is shown as \emph{Add Partial Derivatives}.
	\item Expanding and collapsing nodes.
	\item Preemptive creation of compulsory elements.
	\item Adding and removing children elements to a parent node. 
	When adding children only
	those allowed by the XSD are shown in contextual menus.
    \item Reordering (moving upwards or downwards) certain elements in a list.
	\item Copy/paste capabilities for SimML elements.
	\end{itemize}
\item those addressed to manage the content of each XML element, among them: 
	\begin{itemize}
	\item Typing the text content. To enable the edition of an element value the empty input area next to its label must be clicked.
	\item Support for unicode characters, for instance Greek letters. A graphical tool is provided to facilitate the introduction of such characters. The user may copy a symbol from the pop-up and use it in the editor.
	\item Dropdown lists when the elements can only be chosen from
	a closed list, either defined in the XSD or corresponding to previously defined content in the same XML document.
	\item A specialised editor for SimML instructions.
	\item A specialised math editor for MathML expressions using the AsciiMath standard (see figure \ref{fig:mathMLEditor} for an example)
	\footnote{http://asciimath.org}.
	\item Document cross-reference (hyperlink) graphical tool, used for instance to
	reference a specific model from a problem.
	\item Highlighting missing content to avoid errors.
	\end{itemize}
\end{itemize}


\begin{figure*}
  \begin{center}
  \includegraphics[width=\linewidth]{images/mathMLEditor.png}
  \caption{MathML editor. An example of an algebraic expression 
  introduced using the MathML editor. This editor hints the user when 
  writing mathematical expressions, which are imputed in the AsciiMath 
  standard in Simflowny. Some SimML instructions can be used in the 
  expressions. The buttons below the editor show all the possible SimML 
  tags available and provide a contextual help for them. \CBC{If a contextual help is provided, can't it be shown in the picture?.}}
  \label{fig:mathMLEditor}
  \end{center}
\end{figure*}

The document is auto-saved when changes are detected and when the browser tab is closed.
At the bottom of the document there is a  button to validate the structure and, as far as possible, the content of the document.
In the same toolbar  area there are two buttons to Expand or Collapse all the nodes in the tree.  

