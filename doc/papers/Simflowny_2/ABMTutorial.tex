\section{New functionality: Agent Based Models}
\label{sec:abm}

This section is comprised of two subsections. First we introduce
Agent Based Models on a graph, and then ABM on a spatial domain.

The difference between ABM on a spatial domain and ABM on a graph
is only the topology of the domain: in the former case agents
have spatial positions and neighbours
of an agent are determined by a distance, while in the latter
agents are the vertices of a graph, and neighbours are determined
by the graph topology. Within the ABM on a spatial domain we
distinguish two subcategories, related to agent motion:
fixed agents (i.e. Cellular Automata) or moving agents.

The ABM models allowed in Simflowny are of the form:

\begin{equation}
\partial_t u = \cal{F}(u)
\end{equation}

where $\cal{F}$ is a discrete arbitrary algorithm using the properties
of an agent, the properties of its neighbours, parameters, and
coordinates or characteristics of the graph local topology.
A specific discretization must be provided at model level for 
$\partial_t u$.

\subsection{Agent Based Models on a graph}

In this section we validate the new capabilities in Simflowny 2
by building an ABM on a graph model (the voter model, a simple model in which
essentially the binary value of an agent is changed according to
the consensus of its neighbours bare a random noise) and running a simulation with it. 

\subsubsection{Model creation}

 In the document manager, the user should
select any folder, or create a new one in order to store the
model there. Next, the user clicks the \texttt{plus} button.
This expands a new menu, where the option \texttt{Agent Based Model on graph} should be selected.
Then the document editor will open the basic skeleton for a model
on a graph, as shown in figure \ref{fig:editorVacio}.

\begin{figure*}
  \begin{center}
  \includegraphics[width=\linewidth]{images/editorVacio.png}
 % From now on use 1200x(600)? for the screenshots (for both frames) 
 % and [width=2\columnwidth]
 % For a single frame use width=600 in the screenshot and [width=\columnwidth] 
 % \includegraphics[width=2\columnwidth]{images/test2.png}
  \caption{Editor showing the basic skeleton for a model}
  \label{fig:editorVacio}
  \end{center}
\end{figure*}



Now, the user fills in the general description of the model, contained in
the head tag. By expanding the head, its children tags are shown, namely
name, id, author, version, and date.

The next step consists in defining the \emph{vertexProperties}. In
Simflowny a \emph{vertexProperty} is a variable which has a specific
value for each vertex in a graph. One \emph{vertexProperty} is the
minimum necessary to define a model.

To build the voter model, the user may define two  \emph{vertexProperties} called \emph{state} and \emph{acc}. 
\emph{state} stands for the voter model state, which can take two values.  
\emph{acc} is just an auxiliary variable to
accumulate neighbour values.

With this the user is ready to proceed with the definition of the update
rules, the algorithms that tell the model how to change its \emph{vertexProperties} from
step \emph{n} to step \emph{n+1}. In Simflowny this is specified under
the element \emph{rules}, whose contextual menu allows to
add two children: \emph{gatherRules} and \emph{updateRules}.

\emph{gatherRules} are potentially non-local update operations on a
\emph{vertexProperty} which, for a certain vertex, may involve
information about its neighbours. On the other hand, \emph{updateRules}
are local update operations, using only information from the same
vertex. While the user can write any algorithm by using only gather rules,
parallel performance and optimization are improved by carefully
distilling local operation rules from operations which may involve
neighbours. This is because no synchronization between CPU processors is needed for
update rules, and, as an added bonus, the model description becomes much
clearer when update rules are used wherever possible.

When creating a \emph{gatherRules} element, one child comes
automatically with it: a \emph{gatherRule} operation. A model may contain as many as needed, one being the minimum.

A \emph{gatherRule} operation is defined by \emph{name}, \emph{vertexProperty}, and
\emph{algorithm}, which contains the actual update rule. In the voter model, this first
\emph{gatherRule}, called here \texttt{Acc gather 1}, corresponds to the \emph{vertexProperty} \emph{acc}.


The \emph{algorithm} is defined using SimML and MathML.  The \emph{algorithm}
contextual menu allows the user to select the needed SimML element.  See figure \ref{fig:simmlContextual}.

\begin{figure*}
  \begin{center}
  \includegraphics[width=\linewidth]{images/simmlContextual.png}
  \caption{Construction of the voter model. In the left panel the contextual menu to add SimML elements can be appreciated.}
  \label{fig:simmlContextual}
  \end{center}
\end{figure*}


To follow with the building of the voter model, the user
adds an \emph{iterateOverEdges} element under the \emph{algorithm} element. This is a loop
over all the edges of a certain vertex. 



\emph{iterateOverEdges} has a child called \emph{directionAtt}  
to indicate the edge direction, \emph{in} in our case.  After this, the user adds a \emph{math} child to
\emph{iterateOverEdges} with the following content:

\begin{quote}
\emph{acc}(\emph{\$cv}) = \emph{acc}(\emph{\$cv}) + \emph{state}(\emph{\$es}(\emph{\$ce}))
\end{quote}

where
  \emph{\$cv} stands for current vertex,
  \emph{\$ce} stands for current edge,
and  \emph{\$es}(\emph{\$ce}) stands for the neighbour vertex at the other end of the edge (edge source). 

%The initialization of \emph{acc}(\emph{\$cv}) will be taken care of in an
%appropriate update rule. See update rules below.

%The SimML reserved words, in this example \$cv, \$es and \$ce, can be
%added through the buttons below or directly typed.

All together, the expression updates the \emph{acc}
\emph{vertexProperty} of each vertex by adding the value of the
\emph{state} \emph{vertexProperty} of the vertices participating in the
edge with the current vertex (i.e., its neighbours). 
This is, \emph{acc} accumulates the values
of \emph{state} for all the (incoming) pairs of a certain vertex.


In a similar fashion, the user adds a mathematical expression in an update rule (\texttt{State update 1})
for the \emph{vertexProperty state}:

\begin{quote}
\emph{tmp} = \emph{acc}(\emph{\$cv})/\emph{\$lnoe}\_\emph{in}(\emph{\$cv}) - \emph{\$rnd}\_\emph{uniform}
\end{quote}

In this case we have needed two additional functions:
  \emph{\$lnoe\_in} for the local number of incoming edges (for the current
  vertex) and
  \emph{\$rnd\_uniform} to generate a random uniform variable (between 0 and 1).
Therefore, this expression computes the average value of the incoming
neighbour vertices and then subtracts a random value between 0 and 1.
Notice \emph{tmp} is a temporary variable. 

At this point, the
value of \emph{state} is not yet updated. To do so, the user should add an \emph{if-then-else}
instruction as a child of \emph{algorithm}, the \emph{if} condition being  $tmp>=0$, the \emph{then} statement $state(\$cv)=1$, and the \emph{else} statement $state(\$cv) = 0$.
Notice that mathematical conditions and statements are introduced as a \emph{math} element.
See figure \ref{fig:updateIf}.


\begin{figure*}
  \begin{center}
  \includegraphics[width=\linewidth]{images/updatecompleteif.png}
  \caption{Detail of the \emph{if-then-else structure} while building an update rule.}
  \label{fig:updateIf}
  \end{center}
\end{figure*}

In a similar fashion, the user initializes the \emph{acc}
\emph{vertexProperty} by adding an \emph{updateRule} called \texttt{Acc update 1} and containing the statement $acc(\$cv) = 0$.


Finally, the user just needs to specify
the order in which the different \emph{gatherRule} and \emph{updateRule}
instructions must be executed by using the
\emph{ruleExecutionOrder} element. A \emph{rule} element, child of \emph{ruleExecutionOrder},  is added referencing either a \emph{gatherRule}
or an \emph{updateRule}. The user chooses the desired element as
the first rule, and then follows on with additional rules
to complete the sequence, in this case: \texttt{Acc update 1}, \texttt{Acc gather 1}, and \texttt{State update 1}


Now the definition of the voter model is complete, can be validated, and it is displayed in the right frame of the editor, as shown in figure \ref{fig:rightframe}.

\begin{figure}
  \begin{center}
  \includegraphics[width=\columnwidth]{images/rightframe.png}
  \caption{Detail of the right frame of the editor showing the completed voter model.}
  \label{fig:rightframe}
  \end{center}
\end{figure}

\subsubsection{Problem creation}

The next stage is the creation of the problem based on the voter model
defined in the previous subsection. 

A problem on a graph domain is
created by selecting the option \texttt{Agent Based Problem on graph} from
the add document button.
As in the case of the model, this creates an empty template for the
construction of a problem.

The user fills in the \emph{head} details and then the
vertex properties: \emph{state} and \emph{acc}, as in the model
(although different names may be used).

As seen in figure \ref{fig:modelid}, the \emph{parameters} and \emph{parameter} elements are used to define the \texttt{time\_steps} integer parameter.


The following step is referencing a model, using the cross reference graphical tool as explained in section \ref{sec:newgui}. In this example, the user selects the voter model as shown in figure \ref{fig:modelid}.


\begin{figure*}
  \begin{center}
  \includegraphics[width=\linewidth]{images/modelid.png}
  \caption{Building the voter problem: head, fields, parameters and model reference.}
  \label{fig:modelid}
  \end{center}
\end{figure*}

Now, using the contextual menus provided by the GUI, the user
adds a \texttt{time\_steps} parameter of integer type
\texttt{INT} and default value \texttt{1000}. See figure \ref{fig:modelid}.

Simflowny supports either loading a graph from a file or defining it through properties. Such properties allow the user to define the graph as either directed or undirected (\emph{edgeDirectionality}) and either random, scale-free or circular (\emph{degreeDistribution}). Our problem is based on a directed random graph. See figure \ref{fig:voterproblem}.

The user can choose to evolve one vertex per time step, or all of them as in this example (\emph{evolutionStep}).  See figure \ref{fig:voterproblem}.

\begin{figure}
  \begin{center}
  \includegraphics[width=\columnwidth]{images/voter-problem.png}
  \caption{Detail of the right frame of the editor showing the completed voter problem.}
  \label{fig:voterproblem}
  \end{center}
\end{figure}


Problems can be optionally endowed with initial conditions, which provide the values for the first step of the simulation. In
this case, a simple mathematical expression suffices:

$state(\$cv) = \$rnd\_int\_1$,

which sets the initial value for state as a random integer
(0 or 1). There is not need to initialize the accumulator, since in the model it is
set to 0 at the begining of each time step.

Finally, the user needs to establish a finalization condition, that will
specify when to stop the simulation, in this case after a fixed number of time steps.
Notice \emph{\$in}, the iteration number counter, is a
reserved word and its value is automatically increased by one after each
evolution step.
See figure \ref{fig:voterproblem}.

Once finished, the user may click on the button \texttt{Validate} to assess the problem integrity.


\subsubsection{Generating code}
\label{sec:gencode}

The button to launch the code generation is located on the action bar on the document manager. 
The user should select the voter problem in the document manager, and click on this button. Then, Simflowny starts the
code generation process. After a few seconds, the code generation
finishes and a new document of type \texttt{Generated Code} appears in the document manager. By selecting such document and clicking on the \texttt{Download}
button the code is obtained in a zip file, see figure \ref{fig:barcode} and section \ref{sec:newgui}.


\subsubsection{Running a simulation}

The user may download the file to any computer where Simflowny 2 has been properly installed and configured, and unzip it into a folder. This folder will be the
location for the compilation and simulation and contains the source code files, a
sample parameter file and a makefile. 

The use case consists in simulating a random voter model for 10 timesteps, 500 vertices
and 1000 edges. To do so one should set the following values in the \texttt{problem.input} file.
\begin{verbatim}
  number_of_vertices = 500;
  vertex_properties=[''state''];
  time_steps = 10;
\end{verbatim}

The code compiles using make.
Next, the simulation may be launch  by running:

\verb|./Voterproblem problem.input|

The results are saved by default in \emph{outputDir} directory which can be visualized with any DOT~\cite{dot} viewer. The figure~\ref{fig:VoterResults} shows a snapshot of the results.

\begin{figure}
  \begin{center}
  \includegraphics[width=\columnwidth]{images/VoterResults.png}
  \caption{Voter model results.}
  \label{fig:VoterResults}
  \end{center}
\end{figure}



\subsection{Agent Based Models on a spatial domain}

In the previous subsection, we have detailed how to create an ABM on a graph,
an associated problem, how to generate its code and how to run it.
Simflowny 2 also includes the new family of ABM on a spatial domain,
which we validate in this section. As many features are common with the
ABM on a graph family, we will directly present an example without
detailing the process of building the model and the problem 
%(this is done in the Appendix for the interested reader).

In this example, a two-dimensional flocking model is implemented onto Simflowny.
We use the vectorial
noise model of Gregoire
and Chat\'e~\cite{Gregoire2004}. The model is a variation of the
Original Vicsek Model~\cite{Vicsek1995} (OVM),
devised to reproduce the collective motion - or flocking - we
find in many biological and non-biological
systems (see, for instance, references~\cite{Toner1995},\cite{Gregoire2008},
and~\cite{Deutsch2012}and~\cite{Vicsek2012} for reviews).
In these systems long-range orientation order emerges after spontaneous
symmetry breaking.

In the OVM, point-like agents move synchronously in
discrete time steps, with a fixed common speed $v_0$. In 2D the
orientation of agent $\alpha$ is an angle $\theta_{\alpha}$. The evolution
rule provides the new angles at each time step, based on the
angles of the agent's neighbours (agents within a certain
influence radius) in the previous time step. Essentially, the
agent tries to align itself with its neighbours. This alignment is
perturbed by a white noise.

The Gregoire and Chat\'e model is based on the OVM, but
modifies the manner in which noise is incorporated into the model.
They define \textit{vectorial noise} as generated by errors when
estimating interactions, in comparison with the \textit{angular
noise} in the OVM, related to errors in trying to follow
the newly computed direction. Altogether, the update rule for the
Gregoire and Chat\'e model is:

\begin{equation}
\theta^{t+1}_{\alpha} = arg \left[\sum_{\beta \sim \alpha}e^{i \theta^t_{\beta}} + \
\eta n^t_{\alpha} e^{i\xi^t_{\alpha}} \right],
\end{equation}
where $\xi$ is a delta-correlated white noise, $\eta$ represents noise-strength, and $n$ is the number of neighbours. The sum is made over all
the neighbours ($\beta$) of agent $\alpha$.

The solution of this system ranges from nearly complete orientation order
for low noise intensity to random orientation for high noise intensity.
These phases are separated by a novel phase transition. The solutions
near the transition point are characterized, for a wide spectrum of
parameters, by ordered moving structures
(bands) separated by disordered interband regions.


\subsubsection{Generating code}

Assuming we already have the model and problem introduced in Simflowny,
rather similarly as in the voter model case, the code generation can be launched using the button from the action bar appearing when the problem is selected.
The user should select the Collective Motion problem in the document manager, and click on this button. Then, Simflowny starts the
code generation process. After a few seconds, a new document representing the generated code appears in the document manager. It can be downloaded by selecting such document and clicking on the \texttt{Download}
button.

\subsubsection{Running a simulation}

After downloading the code, the user may unzip it and run a simulation. The unzipped folder will be the
location for the compilation and simulation and contains the source code files, a
sample parameter file and a makefile. 

The use case consists in simulating a Collective Motion for 10 timesteps in a $[0,100]^2$ domain. To do so one should modify the following values in the \texttt{problem.input} file.
\begin{verbatim}
  time_steps = 10
  ...
  dt = 1
  ...
  x_up = 100.0, 100.0
\end{verbatim}

The code compiles using make.
Next, the simulation may be launch  by running:

\verb|./Collectivemotionproblem problem.input|

The results are saved by default in \emph{outputDir} directory and can be visualized with Visit~\cite{HPV:VisIt}. The figure~\ref{fig:CollectiveMotionResults} shows a snapshot of the results.

\begin{figure}
  \begin{center}
  \includegraphics[width=\columnwidth]{images/CollectiveMotionResults.png}
  \caption{Collective motion results.}
  \label{fig:CollectiveMotionResults}
  \end{center}
\end{figure}

See \citep{Arbona2014} and \citep{Arbona2015} for the use of Simflowny's flocking model in more complex scenarios.