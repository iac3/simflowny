
\section{Models based on PDEs}
\label{sec:pde}
%\section{New functionality: Generic PDEs}

 In this section it is discussed in detail one of the new families incorporated into Simflowny 2, namely a generic PDE with finite difference discretization. In order to stress the differences with respect to the previous version, we first briefly summarize the old model for PDEs in Balance Law form. Next we describe the generic PDE family and present the specific application/example of the wave equation.
 
 Notice that both families are restricted to  systems with first order derivatives in times, a requirement that can always be satisfied by introducing new evolution fields. This condition allow us to use the Method of Lines (MoL) to ensure stability of the system when converting continuum equations into discrete ones.

\subsection{Old functionality: PDEs in Balance Law form}

The old family can explicitly solve any PDE written as
\begin{equation}
   \partial_t {\bf u} + \partial_i {\bf F}^i({\bf u}) = 
   {\bf S} ({\bf u}) + 
   \partial_i \left({\bf Q}^{i}_{j}({\bf u})\mbox{ }\partial_k {\bf P}^{kj}({\bf u}) \right)
\end{equation}
where $\bf{u}$ is an array with all the evolved fields, 
${\bf F}$ are the fluxes, ${\bf S}$ the sources, and ${\bf P}$ and ${\bf Q}$ are the algebraic expressions conforming the parabolic terms.
%, ${\bf P} ({\bf u})$ for the inner
%second order derivative and ${\bf Q} ({\bf u})$ for the outter first order derivative.
 Since the fluxes and the sources depend only algebraically on the fields, the evolution system is restricted to be first order both in time and space, except for the parabolic terms.
 The advantage of this model is that 
both Finite Difference and Finite Volume methods can
be applied on this form of the equations.


\subsection{New functionality: Generic PDEs}

As it was explained previously, before the actual generation of the numerical code we must define the model, the problem
and the discretization schemes. This family can explicitly solve any PDE written as
\begin{equation}
   \partial_t \bf{u} = \cal{L (\bf{u},\partial_i \bf{u})}
\label{genericPDE}   
\end{equation}
where $\bf{u}$ is an array with all the evolved fields
and $\cal{L (\bf{u},\partial_i \bf{u})}$ is any operator depending on the fields and its spatial derivatives (i.e., of any order). Therefore, the only restriction here is that the system can only involve first time derivatives of the fields.

Arbitrary operators can be constructed by using $n$ recursive rules, which can be written formally as 
\begin{eqnarray}
   {\cal L}_i^{(0)} &=& f_i(\bf{u}) 
\label{recursive_step0}      
\\
                 &...&   
\\   
   {\cal L}_m^{(n)} &=& \sum_{r=0}^{n-1} \sum_i g_i({\bf u}) 
   \prod_j
   \left( \sum_k \partial_k 
    \left( \sum_l {\cal L}_l^{(r)} C^{ijkl}_{m} \right)
    \right)
\label{recursive_stepn}          
\end{eqnarray}

where $\{f_i,..,g_i\}$ are arbitrary functions depending on
$\bf{u}$ but not on its derivatives, and $C^{ijkl}_{m}$
is a generic matrix which in practice will have only
one non-trivial component. Notice that this new family can also be expressed in a logical abstract language as
\begin{eqnarray}
   \partial_t \bf{u} &=& \sum(f(\bf{u}) {\rm D}) 
   \\
   {\rm D} &=& \prod\partial_i(\rm g(\bf{u}) {\rm D}) 
\end{eqnarray}
where, similarly to the former definition, $\{f, g\}$ are arbitrary algebraic functions on $\bf{u}$, and ${\rm D}$ is recursive term allowing for 
a complex set of expressions using differential calculation. Both the algebraic and the recursive terms are optional at every level of the recursion.

Once the model is well defined, we need to set up the problem, namely, the domain of the simulation, the initial and the boundary conditions for the evolved fields, the finalization condition, and the analysis quantities to be computed. 

Before the code generation we must define the discretization
rules. As we mentioned before, we will take advantage of
the theoretical background developed for PDEs solved by
using the Method of Lines. We have implemented,
as a basic set available in the database provided with the software, some discretization operators  which ensures stability of the discrete
problem (i.e., a third-order Runge-Kutta for the time integration and fourth order centered space discretization), although any other scheme can be defined. By default, it is  assumed a recursive rule such that the $(n)$-order discrete derivative of a field is obtained by applying the discretization operator to the $(n-1)$ derivative of that field.
This procedure provides a straightforward way to compute space derivatives of any order.
Notice however that all these schemes --for the time and space discretization-- can be 
modified freely by setting a different  discretization policy. 
\AA{It sounds as if there is a default set of discretization schemes, which is not the case} \CP{is it? we do not say
it.}

\subsection{Generic PDE example : the wave equation}

The use of Simflowny for Generic PDEs is illustrated using the widely known wave equation. The wave equation for a scalar field $\phi$ in 2D
can be written as a system of equations with only first order time derivative, namely
\begin{eqnarray}
   \partial_t \phi &=& K  
\label{wave_eq_phi}   
\\   
   \partial_t K &=& \partial_{xx} \phi + \partial_{yy} \phi 
\label{wave_eq_K}      
\end{eqnarray}

The recursive rules are straightforward here, with
\begin{eqnarray}
   {\cal L}_i^{(0)} &=& \{ \phi, K \} 
\label{recursive_wave_step0}      
\\   
   {\cal L}_j^{(1)} &=& \{ \partial_x \phi , \partial_y \phi\}
                     =  \{ \partial_x {\cal L}_0^{(0)}, \partial_y {\cal L}_0^{(0)}\}
\label{recursive_wave_step1}      
\\   
   {\cal L}_k^{(2)} &=& \{ \partial_{xx} \phi , \partial_{yy} \phi \} = \{ \partial_{x} {\cal L}_0^{(1)},
   \partial_{y} {\cal L}_1^{(1)} \}
\label{recursive_wave_step2}         
\end{eqnarray}
%\BM{Suggestion (or something similar):
%\begin{eqnarray}
%   {\cal L}_i^{(0)} &=& \{ \phi, K \} 
%\\   
%   {\cal L}_m^{(1)} &=& \{ \partial_x {\cal L}_0^{(0)}, \emptyset \}
%\\   
%   {\cal L}_m^{(2)} &=& \{ \partial_{x} {\cal L}_0^{(1)}, \emptyset \}
%\end{eqnarray}
%With the direct formula the recursivity does not appear. Maybe it can be shown in two steps, ending with Carlos formulation.
%}

To construct the problem we need to set the domain, the initial data and the boundary conditions.
For this simple example we will set a square domain $[-0.5,0.5]^2$ with an initial parametrized Gaussian profile for the scalar field and periodic boundary conditions.

We use our preferred choice of the discretization policy (i.e., third order Runge-Kutta for the time integration together with fourth order accurate centered difference operators satisfying the Summation By Parts rule), which ensures stability and convergence of the discrete problem.
The second order space derivative are calculated by using 5 point stencil.


\subsection{Model creation}

 In the document manager, the user should
select a folder, or create a new one, to store the
model. Next, the user clicks the \emph{plus} button,
expanding a new menu where the option \emph{Generic PDE Model} should be selected. The document editor will open the basic skeleton to define a generic PDE model, as shown in figure \ref{fig:editorVacioGenericPDE}. We will now describe the
different parts of this skeleton.

\begin{figure*}
  \begin{center}
  \includegraphics[width=\linewidth]{images/editorVacioGenericPDE.png}
  \caption{Editor showing the basic skeleton for a generic PDE model}
  \label{fig:editorVacioGenericPDE}
  \end{center}
\end{figure*}

The head element contains the general description of the model,
which might be filled by the user. By expanding this head, its children are shown (i.e., name, id, author, version, and date).

The first step is to set the spatial dimensions and the names of all the coordinates (i.e., space and time) in \emph{Spatial Coordinates}. By default there is only one \emph{Spatial Coordinate} element. New spatial coordinates can be added 
by the user from the \emph{Spatial Coordinates} element through
its contextual menu. In our particular example, there are two spatial coordinates $\{x,y\}$  and a time coordinate $t$. 

The second step consists in defining the \emph{fields}, that is,  the variables which are going to be evolved in
Simflowny. One \emph{field} is the minimum required to define a model. For the wave equation, the user must define two fields $\{\phi ,K\}$. 

The core of a PDE model relies on the last mandatory section, where the evolution equation (i.e., the operators) associated to every field must be set with a descriptive name.
% For the sake of clarity, an evolution equation must have a name. 
%Once the name has been given and the field for the equation select, it is time to fill the operators. 
The generic PDEs have a structure/formalism, described by eqs.
\ref{recursive_step0}-\ref{recursive_stepn}, in which the last (n)-recursive step consists on a summation of terms --defined as \emph{Term} elements--, each one with many combinations of derivatives. The terms which are going to be discretized in the same way are grouped by elements called \emph{Operator}.
Notice that it is possible to use different discretization schemes for each \emph{Operator}.  As it is shown in figure \ref{fig:genericPDECreation}, the user can introduce both algebraic expressions (\emph{Math}) or derivative ones (\emph{Partial Derivatives}) in the contextual menu of \emph{Term}.


In the example with the wave equation, the simplest choice is to use only one operator for both equations, that it will be referred as \emph{default} from now on. For the equation ~\ref{wave_eq_phi} (i.e., the time derivative of the scalar field $\phi$), the \emph{default} operator has only one algebraic expression, \emph{$K$}, to be added as a \emph{Math} element. The mathematical expressions are introduced through the editor explained in section~\ref{sec:newgui}.
The time evolution of $K$, given by eq.~\ref{wave_eq_K}, contains two terms $\{ \partial_{xx} \phi ,\partial_{yy} \phi \}$. 
These terms, which must be introduced as \emph{Partial Derivatives} elements, are calculated by using the recursive rules given in eq.~\ref{recursive_wave_step0}-\ref{recursive_wave_step2} from the top to the bottom level.
For the first term $\partial_{xx} \phi$, the user defines first the most external derivative ${\cal L}_0^{(2)} \equiv \partial_{x} {\cal L}_0^{(1)}$ and sets the coordinate \emph{$x$} in \emph{Partial Derivative}. At the second iteration the next level ${\cal L}_0^{(1)} \equiv \partial_x {\cal L}_0^{(0)}$ is defined following a similar procedure. Finally, at the bottom level the user adds a
\emph{Math} element to defined the algebraic function ${\cal L}_0^{(0)} \equiv \phi$. This procedure is displayed in Figure \ref{fig:secondDerivative}.  The process to add the second term \emph{$\partial_{yy} \phi $} is similar than the explained above. Figure \ref{fig:genericPDEModelComplete} shows the model, completed at this point.
\AA{I think a full view
of the model right panel would help to follow the discussion.} \BM{The image has been incorporated with a brief reference, though it may be at the next sencence.}


\begin{figure*}
  \begin{center}
  \includegraphics[width=\linewidth]{images/genericPDECreation.png}
  \caption{Construction of the wave equation model. In the left panel the contextual menu to add mathematical term for the scalar field 
  equation term can be appreciated.}
  \label{fig:genericPDECreation}
  \end{center}
\end{figure*}


\begin{figure*}
  \begin{center}
  \includegraphics[width=\columnwidth]{images/secondDerivative.png}
  \caption{Construction of the wave equation model. Second derivative term introduced.}
  \label{fig:secondDerivative}
  \end{center}
\end{figure*}

\begin{figure*}
  \begin{center}
%  \includegraphics[width=\linewidth]{images/genericPDEModelComplete.png}
  \caption{Detail of the right frame of the editor showing the completed wave equation model.}
  \label{fig:genericPDEModelComplete}
  \end{center}
\end{figure*}


Once the model is finished one can validate it as seen in section \ref{sec:newgui}.


\subsection{Problem creation}

The next stage is to create a problem containing the model, the domain and the initial and boundary conditions.  The problem can be created from the document manager by adding a new document \emph{Generic PDE Problem}.
After setting the header information, the coordinates and fields must be added. 


Two parameters $\{a,b\}$ are added for the Gaussian profile of the initial data by using the contextual menu. 



The user may now select the model. This selection is performed through a small document manager pop-up, as seen in Figure \ref{fig:modelSelection}. 

\begin{figure*}
  \begin{center}
  \includegraphics[width=\linewidth]{images/modelSelection.png}
  \caption{Construction of the wave equation problem. Selection of the wave equation model.}
  \label{fig:modelSelection}
  \end{center}
\end{figure*}

%In this example, there is only one model in this problem.
In general, a given problem might include an arbitrary number of models. This is useful, for instance, if these models are to be applied to different regions of the simulation domain, since Simflowny provides a multi-region capability. By default, the user must define a region covering the full simulation domain. Optionally, one can also define subregions, each one employing different models. 

In our example, the wave problem has a single region. The user must select the wave model as an \emph{Interior Model} in this region.
The spatial domain is set to $x^i \epsilon [-0.5,0.5]^2$ by
assigning a \emph{Coordinate Min} of \emph{$-0.5$} and \emph{Coordinate Max} of \emph{$0.5$} for coordinate \emph{$x$}. The same process is repeated for coordinate \emph{$y$}.

The last compulsory element to set in a region is the \emph{Initial Condition}, which might contain mathematical formulas (\emph{Math Expressions}) and logical conditions (\emph{Apply If}).
In this test case, the initial conditions are:
\begin{equation}
    K=0  ~~~~~,~~~~~ \phi=a*\exp(-(x^2+y^2)/b)
\nonumber
\end{equation}
see figure \ref{fig:initialConditions}.

\begin{figure*}
  \begin{center}
  \includegraphics[width=\linewidth]{images/initialConditions.png}
  \caption{Construction of the wave equation problem. Initial conditions created.}
  \label{fig:initialConditions}
  \end{center}
\end{figure*}


The next step consists in adding the domain \emph{Boundary Conditions}. 
In this element the user sets
first the \emph{Boundary Policy}, which is a mapping of the regions, in this case the region \emph{main},
to a specific \emph{Boundary Condition}. 
Periodic boundary conditions are applied to all the edges and fields by setting the \emph{Boundary Condition} \emph{Type} as  \emph{Periodical} and the 
the \emph{side} and \emph{axis} properties to \emph{all} for all fields, meaning that periodic boundary conditions are applied to all coordinates (i.e., $x$ and $y$) and to both sides (i.e., lower and upper). 

The last setting regarding the boundaries is the \emph{Boundary Precedence}, which indicates in what order overlapping boundary conditions are applied. All the boundary conditions must be added to the \emph{Boundary Precedence} list, in this case no matter in what order as it does not alter the result.

The last piece of information needed in a problem is the \emph{Finalization Conditions} element. These conditions have to be logical expressions depending on evolution fields, parameters or variables (i.e., including the coordinates). In this test case, the simulation stops when the evolution time $t$ reaches a certain value provided by the user. Therefore, a new parameter \emph{t\_end} is to be added. Then, the finalization condition \emph{Math} is \emph{$t>=t\_end$}. See figure \ref{fig:genericPDEproblem}, where the complete problem is shown.

\begin{figure}
  \begin{center}
  \includegraphics[width=\columnwidth]{images/genericPDEproblem.png}
  \caption{Detail of the right frame of the editor showing the completed wave problem.}
  \label{fig:genericPDEproblem}
  \end{center}
\end{figure}


\subsection{Problem discretization}

The following stage, after the problem definition, is the discretization. Although the users can create their own, Simflowny provides a complete set of discretization schemas.

Discretization schemas can be applied to multiple problems and vice versa. Additionally, a schema might be parametrized, so it is possible to use the same schema with different parameter choices to generate different discretized problems. As a consequence, it is needed a mapping between discretization
schemas and a given problem. This mapping is known as \emph{Discretization Policy} in Simflowny and it is common to all the PDE families. 
Although this policy can be created from scratch, it is easier to generate it from an existing \emph{Generic PDE Problem}: by selecting the problem in the document manager the toolbar shows a button to generate the \emph{Generic PDE Discretization Policy}. 

The new document automatically incorporates the problem information. Therefore, the only required actions are selecting the space and time discretization schema (i.e., \emph{Operator Discretization} and \emph{Time Integration Schema} respectively) for every region in the problem. In the wave model example, the spatial \emph{Operator Policy} applied in the only region is the \emph{4th Order Operators} schema. The time integration chosen is \emph{RK3 with dissipation}, also available in the database. At this point, the policy is completed,
as seen in Fig.~\ref{fig:genericPDEDiscretizationPolicy}.

\begin{figure*}
  \begin{center}
  \includegraphics[width=\linewidth]{images/genericPDEDiscretizationPolicy.png}
  \caption{Discretization policy completed.}
  \label{fig:genericPDEDiscretizationPolicy}
  \end{center}
\end{figure*}

Once the discretization policy is defined the user must explicitly discretize the problem. This step is performed in the document manager, after selecting the newly created policy, with the toolbar operation \emph{Discretize Problem}. 

\subsubsection{Generating code}

By using the toolbar, the \emph{Discretized Problem} abstract formulation can be translated into an specific code to be run on a certain simulation platform.
Then, Simflowny starts the code generation process, which finishes with the creation of the \texttt{Generated Code}. The code can be
downloaded in a zip file format, see figure \ref{fig:barcode} and section \ref{sec:newgui}. 

\subsubsection{Running a simulation}

The code compressed in
the zip file can be compiled and executed in any machine with an installed version of SAMRAI.
The unzipped folder will be the
location for the compilation and simulation and contains the source code files, a sample parameter file and a Makefile. 

The wave problem example consists on running a simulation up to $t=1$ using a mesh of $100^2$ cells. This can be achieved by modifying the following values in the \texttt{problem.input} file:
\begin{verbatim}
  tend = 1
  ...
  dt = 0.005
\end{verbatim}

The user can compile and launch the simulation by running in a terminal the following commands:


\begin{verbatim}
  make
  ./Waveproblem problem.input
\end{verbatim}


The results are saved by default in the \emph{outputDir} directory and can be visualized with Visit~\cite{HPV:VisIt}. The figure~\ref{fig:WaveEquationResults} shows a snapshot of the results for the scalar field at $t=1$.

\begin{figure}
  \begin{center}
  \includegraphics[width=\columnwidth]{images/WaveEquationResults.png}
  \caption{Scalar field results from wave problem simulation.}
  \label{fig:WaveEquationResults}
  \end{center}
\end{figure}