%\documentclass[10pt,twocolumn]{article}
%\documentclass[11pt,a4paper]{article}
%\documentclass[preprint]{elsarticle}
\documentclass[final,5p,times,twocolumn]{elsarticle}


\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{color}
%\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{pdfsync}
\usepackage[pdftex]{hyperref}
\DeclareMathOperator{\randomMinusPi}{\$rnd\_real\_-3.1415\_3.1415}

%Directives comentaris
\def\AA#1{{\textcolor{magenta}{\bf AA: #1}}}
\def\AR#1{{\textcolor{green}{\bf AR: #1}}}
\def\CP#1{{\textcolor{red}{\bf CP: #1}}}
\def\BM#1{{\textcolor{cyan}{\bf BM: #1}}}
\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}
\def\CBC#1{{\textcolor{orange}{\bf CBC: #1}}}

\bibliographystyle{unsrt}

\begin{document}

\title{Simflowny 2: An upgraded platform for scientific modeling and simulation}

\author[iac3]{A.~Arbona\corref{cor1}} \ead{aarbona@iac3.eu}
\author[iac3]{A.~Artigues}            \ead{antoni.artigues@gmail.com}
\author[iac3]{C.~Bona}                \ead{cbona@uib.es}
\author[iac3]{C.~Bona-Casas}          \ead{carles.bona@uib.es}
\author[iac3]{J.~Mass\'o}             \ead{jmasso@iac3.eu}
\author[iac3]{B.~Mi\~{n}ano}          \ead{bminyano@iac3.eu}
\author[iac3]{C.~Palenzuela}          \ead{carlos.palenzuela@uib.es}
\author[iac3]{A.~Rigo}                \ead{arigo@iac3.eu}
%
\cortext[cor1]{Corresponding author}
\address[iac3]{IAC$\,^3$, University of the Balearic Islands, Mateu Orfila, Cra. de Valldemossa km 7.5, 07122, Palma, Spain}

\maketitle

{\bf PROGRAM SUMMARY}
  
\begin{small}
\noindent
{\em Program Title: }Simflowny                                 \\
{\em Licensing provisions: } Apache License, 2.0                      \\
{\em Programming language: } Java, C++ and JavaScript   \\
{\em Journal Reference of previous version:} Comput. Phys. Comm. 184 (2013) 2321--2331  \\
{\em Does the new version supersede the previous version?:} Yes  \\
{\em Reasons for the new version:} Additional features \\
{\em Summary of revisions:}\\
Expanded support for Partial Differential Equations.\\
Support for Agent Based Models.\\
New Graphical User Interface.\\
{\em Computer: }\\
  Simflowny runs in any computer with Docker [1], the installation details can be checked in Simflowny documentation. It also can be compiled from scratch in any Linux system, provided the requirements are properly installed following documentation indications.\\
  The generated code runs on any Linux platform ranging from personal workstations to clusters and parallel supercomputers.  \\
{\em Nature of problem:}\\
Simflowny generates code for numerical simulation for a wide range of models \\
{\em Solution method:}\\
  Any discretization scheme based on either Finite Volume Methods, Finite Difference Methods, or meshless methods for Partial Differential Equations.\\
  Agent Based Model simulations execute their own algorithm as set in their models. \\
{\em Additional comments:}\\
  The software architecture is easily extensible for future additional model families and simulation frameworks.\\
Full documentation is available in \href{https://bitbucket.org/iac3/simflowny/wiki/Home}{Simflowny wiki home}. \\  
{\em References:}
\begin{thebibliography}{0}
\bibitem{1} https://www.docker.com/ [online] (2017)
\end{thebibliography}
%* Items marked with an asterisk are only required for new versions
%of programs previously published in the CPC Program Library.\\
\end{small}

\section{Introduction}

We present a significantly upgraded  version 2 of Simflowny~\cite{Arbona20132321},
an  open platform for scientific dynamical models, composed by a Domain Specific Language (DSL)  and a web based Integrated Development Environment (IDE), which automatically generates parallel code for simulation frameworks. 

Originally, Simflowny was built to address the fact that
efforts towards a formalization of mathematical equations in 
simulation, at a more abstract level than the mere program source code, are 
still rare. Usually, the process that goes from an idea reflected in a set 
of equations to the final code is pure art, in the sense that it lacks 
formalization and general tools to ease it.

With these problems in mind, Simflowny 
is aimed to create a community contributed platform where some elements of 
the simulation flow for Initial Value Problems are formalized. This formalization allows the reuse and 
exchange of such elements, even outside the platform itself. 


%The DSL in the first version of Simflowny only supported scientific models of Partial Differential Equations (PDEs) written as a system of balance laws.

The DSL is based on an XML Schema Definition (XSD) representation. The XSD schemas
prescribe the structure of the XML documents for models, problems, and discretization schemes. Where algorithms are to be included in either of these XML documents, this is done through a specific markup language developed for Simflowny, called SimML (Simulation Markup Language). SimML constitutes a
full-blown rule specification language (technically speaking,
the language is Turing-complete, meaning you can create any possible
algorithm with it). To insert mathematical expressions in the algorithms and documents, Simflowny prescribes MathML, the standard markup language  for representing mathematical expressions.
 
In version 2 the DSL has been expanded and 
currently supports the following scientific model paradigms, or \emph{families}:
\begin{itemize}
    \item Partial Differential Equations (PDE) written in balance-law form. This family was the only one supported in version 1 of Simflowny.    
    The PDEs are written as an 
    evolution system containing only first order derivatives both in time and space, which allow users to use numerical schemes based on Finite Volume Methods to deal with shocks and discontinuities. Some examples of balance
    law systems include the wave equation, Maxwell equations, Einstein equations,... This family also allows parabolic terms like the ones appearing, for instance, in the Navier-Stokes equations.
    
    \item Generic PDE, a new family of models that allows
    almost arbitrary forms of PDE evolution equations, including spatial derivatives of any order. The only restriction is that
    the system must be still first order in time. One could write in this form all the examples in balance-law form but directly as second order system (in space). Additionally, it also includes many other equations like the heat equation, the Navier-Stokes-Korteweg equation (3rd order in space), the Cahn-Hilliard equation (4th order in space) and the Phase-Field-Crystal equation (6th order in space).
    
    \item Agent-Based Models (ABM), another new family of models which simulate the evolution of a system of multiple interacting agents. These models are often built in order to study the emergence of complex phenomena. Agents might live either on a spatial domain (Spatial ABM) or on a graph (ABM on a Graph). Additionally,
Spatial ABM contain two subfamilies: Cellular Automata, i.e. 
agents statically bound to a cell in a mesh, and kinematic ABM, 
where agents roam freely in a meshless domain. Some well-known examples are the Ising and Collective Motion (flocking) models for the Spatial ABM, and the Voter model and Cash and Goods \cite{Razakanirina2010} for ABM on a Graph. \CBC{is there a reason why capital letters goa  bit crazy in this sentence? graph/Graph  spatial/Spatial}
\end{itemize}


From the computer science point of view, 
Simflowny is built on the well-known three-tier architecture:
\begin{itemize}
\item A presentation tier, implemented as a web browser based 
graphical user interface (GUI).
\item A logic tier, based on an application server.
\item A data tier, combining native XML databases with bulk data storage.
\end{itemize}

In Simflowny 2, the original Graphical User Interface (GUI), written ad hoc for the original PDE family, has been completely redone so as to make it flexible enough to automatically accommodate new families of equations, both of PDE nature or otherwise (such as ABM).

The current procedure to generate code is similar to 
the previous version Simflowny 1, although new
features have been introduced to allow for more flexibility
with the new families. Although the details vary among
the families, the process to convert a mathematical model 
into a numerical code can be split in four stages (see fig. \ref{fig:workflow}):  


\begin{figure*}
  \begin{center}
  \includegraphics[width=\columnwidth]{images/functional-capabilities-simflowny2.png}
  \caption{Workflow diagram, containing the following stages: 1) the mathematical {\bf model} containing either the evolution equations for PDEs, or a description of the interactions among the multiple agents in ABM. 2) the {\bf problem}, which mainly includes the model, the domain of the simulation and the initial and boundary conditions. 3) 
  the {\bf discrete scheme}, which converts the continuous problem into a discrete one. Notice that ABM models are already discrete.
  4) the code generation, converting the discrete problem to code for the final framework. }
  \label{fig:workflow}
  \end{center}
\end{figure*}

\begin{itemize}
\item  The representation of the mathematical model, which  contains either the evolution equations to be evolved in the case of PDEs, or a description of the interactions among the multiple agents in ABM. 
\item The representation of the problem, which includes the mathematical model, the domain of the simulation, the analysis quantities and the initial and boundary conditions to be applied, either to the evolution fields (in PDEs) or to agent properties (in ABM). 
\item The representation of the discrete scheme, which converts the continuous problem into a discrete one by defining the space and time discretization operators
(for PDEs). This stage does not apply to the ABM families since the original models are already discrete.
\item The generation of the code from the discrete problem
to the final framework. These frameworks will essentially play the role of a mesh/memory manager by setting the domain, distributing the usage of memory of the fields and parallelizing the workload among the different processors.  

\end{itemize}

While Simflowny 1 supported the Balance Law PDE family through the
Cactus toolkit \cite{cactus, cactusWeb}, Simflowny 2 provides support for SAMRAI mesh
management toolkit~\cite{samrai} for spatial-domain problems and Boost Graph Library~\cite{boost} for graph problems.

Through SAMRAI, Simflowny 2 supports mesh-based discretization schemes
for PDEs, specifically Finite Difference Methods (FDM) and Finite Volume Methods (FVM). It also
supports particle-based mesh-free Lagrangian methods (e.g.: Smooth Particle Hydrodynamics, SPH), as well as spatial agent
models and Cellular Automata. Notice that SAMRAI only allows models with spatial dimensions
$N>1$. 

Simflowny 2 reflects the parallelization capabilities of both SAMRAI and Boost, and therefore any model developed on Simflowny can generate
parallel code. For efficiency reasons, parallel code is only generated
for the parts of the model which are not local: in PDE this amounts to
parallelize fluxes and spatial derivatives while keeping source terms local; in ABM such split is reflected through the gather / update
paradigm: gathers are operations involving neighbours, and are
performed in parallel, while updates do not involve neighbours and
are consequently not parallelized.

Adaptive Mesh Refinement is available in Simflowny by leveraging
the capabilities of SAMRAI.

Notice that Simflowny's 4 stage structure allows us to achieve our main goal: a complete split of the physics (the model and the problem) from the computational 
methods (the discretization schemes) and from the parallelization/distribution issues, which are hidden
on the framework (mesh manager or graph manager). Therefore, we can use the same representation of the discrete problem (which involves the three first stages) in different simulation frameworks, which might allow for higher scalability and efficiency.


Regarding user roles, in Simflowny 2 we consider final users, who are interested in using the 
platform to build and numerically solve a simulation problem (the user C
pictured in fig. \ref{fig:workflow}), and developer users, who are interested in creating 
new physical models and discretization schemes. The latter should not be 
confused with the developers of Simflowny, who are interested in adding new 
functionality to the platform itself.

Simflowny 2 includes example databases for typical physical models and 
discretization schemes. However, in complex simulation scenarios the 
development or tailoring of both physical models and discretization schemes 
by the corresponding experts (A, B) becomes necessary. These elements are 
the starting point for the simulation workflow driven by the final user 
(C).

Simflowny 2 is open source, and it is available in the form of compilable
source code and also as a Docker \cite{docker} container. See 
Simflowny's wiki for further details
\footnote{https://bitbucket.org/iac3/simflowny/wiki/Home}.

The paper is organized as follows. The new GUI architecture is described in Section II. Section III is devoted to the generic PDEs, while Section IV focuses on ABM on spatial domain and on graphs. We finish with some final remarks.


\include{new-gui}

\include{PDETutorial}

\include{ABMTutorial}


\section{Conclusions and future work}

We have presented version 2 of Simflowny, a platform for scientific
modelling and simulation on parallel frameworks. Alongside the description of a new architecture to allow for a more agile inclusion of new paradigms of scientific models, we have presented two newly
available paradigms: generic PDE (PDE written in free-style form, in contrast with the
Balance Law form), and ABM, either on a spatial domain or on a graph. We have
illustrated the new paradigms with simple models, but there are a number
of more advanced models available, either to use directly or as a starting point 
to more tailored models. These include Navier-Stokes Equations (Hydrodynamics), 
Maxwell Equations (Electromagnetism), Einstein Equations (General Relativity), 
Brusselator, or Ising models. All of them are included in the basic model 
library in Simflowny.

In the future, we will continue expanding the database of available models.
We are also working towards supporting unstructured meshes and allowing for
discretization schemes on such meshes (such as Finite Element Methods). 

\section{Acknowledgements}
The research leading to these results
has received funding from the European
Union Seventh Framework Programme (FP7/2007-2013) under grant agreement no 317534 (the Sophocles project).


\bibliography{iac3}

%\include{Appendix}
\include{Appendix_Simml}

\end{document}
